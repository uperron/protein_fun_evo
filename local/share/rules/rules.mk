ORIG_PDB ?= $(BIOINFO_ROOT)/task/prot_structure/dataset/30012017
ORIG_ALIGN ?= $(PRJ_ROOT)/local/share/data/Spielman2015
ORIG_LIBRARY ?= $(PRJ_ROOT)/local/share/data/Shapovalov2011/ExtendedOpt2-5
FULL ?= False
GENERAL_QUERY ?= False

extern $(ORIG_PDB)/theoretical_models

# from PFAM-PDB mapping or from Uniprot if RefSeq / uniprot term query
extern $(ORIG_PDB)/exp.PDBmap
extern $(ORIG_PDB)/no-mutant_ids
extern $(ORIG_PDB)/resolution_ids
extern $(ORIG_PDB)/xray_only_ids

ifeq ($(PFAM), yes)
extern $(ORIG_PDB)/PFAM_alignment.exp.PDBmap.fasta

# this is the original PFAM tree available online
ORIG_TREE = $(ORIG_PDB)/PFAM_orig.fasta-comparable.tree
# this tree is obtained using mafft from the PFAM alignment
TREE = $(ORIG_PDB)/PFAM_alignment.exp.PDBmap.fasta.tree 
extern $(TREE)

.PRECIOUS: experimental_strct.polypeptide.mapped.angle_matrix.chi1_assigned.GLY-ALA.no_0-NA.merged_poly.count_matrix.in

.INTERMEDIATE: experimental_strct.polypeptide.aligned.fasta experimental_strct.polypeptide.mapped.angle_matrix.gz rotamer_library experimental_strct.polypeptide.mapped.angle_matrix.chi1_assigned.no_0-NA.gz experimental_strct.polypeptide.aligned.infoalign experimental_strct_in_mltalg.dict experimental_strct.polypeptide_only.aligned.infoalign merged_poly.XRAY_only experimental_strct.polypeptide.mapped.angle_matrix.bfactor.gz experimental_strct.polypeptide.mapped.angle_matrix.chi1_assigned.no_0-NA.merged_poly.heatmap.in 

.PHONY: PRE_ANALYSIS
PRE_ANALYSIS: experimental_strct.polypeptide.mapped.angle_matrix.chi1_assigned.GLY-ALA.no_0-NA.merged_poly.adj_count_matrix.uniq-Uniprot.PHYLIP experimental_strct.polypeptide.mapped.angle_matrix.chi1_assigned.GLY-ALA.no_0-NA.merged_poly.adj_count_matrix.uniq-Uniprot.masked.PHYLIP experimental_strct.polypeptide.mapped.angle_matrix.chi1_assigned.GLY-ALA.no_0-NA.merged_poly.adj_count_matrix.uniq-Uniprot.tree.newick

#more restrictive filters for the full version
ifeq ($(FULL), True)
# filter out mutants and structures with low resolution
experimental_strct_in_mltalg: $(ORIG_PDB)/exp.PDBmap $(ORIG_PDB)/no-mutant_ids $(ORIG_PDB)/resolution_ids
	bawk '{print $$1, $$2, $$6, $$7, $$4, $$3}' $< | filter_1col 1 <(cat $^2) \
	| filter_1col 1 <(bawk '$$2<=$(RES_LIM) {print $$1}' $^3) >$@


experimental_strct_in_mltalg.dict: $(ORIG_PDB)/exp.PDBmap $(ORIG_PDB)/PFAM_alignment.exp.PDBmap.fasta experimental_strct_in_mltalg $(ORIG_PDB)/resolution_ids
	bawk '{print $$5, $$1}' $< | translate -a -d -j <(cat $^2 | fasta2tab -s | cut -f1 | tr "/" "\t" | bawk '{print $$1, $$1"/"$$2}') 1 \
	| filter_1col 3 <(cut -f1 $^3) | translate -a <(cat $^4) 3 >$@

.PHONY: CLEAN_ORIG
CLEAN_ORIG: experimental_strct_in_mltalg $(ORIG_PDB)/xray_only_ids 
	ls $(ORIG_PDB)/*.cif | xargs -I% basename % | tr '\s' '\n' | sed 's/\.cif//g' | filter_1col 1 -v <(cut -f1 $< | filter_1col 1 <(cat $^2)) \
	| while read i ; do rm $(ORIG_PDB)/$$i.cif ; \
	done
		

# for the single-clan versions (Pkinase, zinc-finger)
else
# no filter for vertebrates
experimental_strct_in_mltalg: $(ORIG_PDB)/exp.PDBmap
	bawk '{print $$1, $$2, $$6, $$7, $$4, $$3}' $< >$@

experimental_strct_in_mltalg.dict: $(ORIG_PDB)/exp.PDBmap $(ORIG_PDB)/PFAM_alignment.exp.PDBmap.fasta $(ORIG_PDB)/resolution_ids
	bawk '{print $$5, $$1}' $< | translate -a -d -j <(cat $^2 | fasta2tab -s | cut -f1 | tr "/" "\t" | bawk '{print $$1, $$1"/"$$2}') 1 >$@

endif



.META: experimental_strct_in_mltalg
	1	PDB_ID
	2	CHAIN_ID
	3	PdbResNumStart
	4	PdbResNumEnd
	5	PFAM_ACC
	6	PFAM_Name

.META: experimental_strct_in_mltalg.dict
	1	Uniprot_entry
	2	PDBmap_id
	3	PDB
	4	resolution



# for Spielmann and single term query versions
else

ifeq ($(GENERAL_QUERY),True)

.PRECIOUS: experimental_strct.polypeptide.mapped.angle_matrix.chi1_assigned.GLY-ALA.no_0-NA.merged_poly.count_matrix.in

.INTERMEDIATE: experimental_strct.polypeptide.aligned.fasta experimental_strct.polypeptide.mapped.angle_matrix.gz rotamer_library experimental_strct.polypeptide.mapped.angle_matrix.chi1_assigned.no_0-NA.gz experimental_strct.polypeptide.aligned.infoalign experimental_strct_in_mltalg.dict experimental_strct.polypeptide_only.aligned.infoalign merged_poly.XRAY_only experimental_strct.polypeptide.mapped.angle_matrix.bfactor.gz experimental_strct.polypeptide.mapped.angle_matrix.chi1_assigned.no_0-NA.merged_poly.heatmap.in 

.PHONY: PRE_ANALYSIS
PRE_ANALYSIS: experimental_strct.polypeptide.mapped.angle_matrix.chi1_assigned.GLY-ALA.no_0-NA.merged_poly.adj_count_matrix.uniq-Uniprot.PHYLIP experimental_strct.polypeptide.mapped.angle_matrix.chi1_assigned.GLY-ALA.no_0-NA.merged_poly.adj_count_matrix.uniq-Uniprot.masked.PHYLIP experimental_strct.polypeptide.mapped.angle_matrix.chi1_assigned.GLY-ALA.no_0-NA.merged_poly.adj_count_matrix.uniq-Uniprot.tree.newick


# just filter exp.PDB for mutants, x-ray ad apply resolution cutoff
experimental_strct_in_mltalg: $(ORIG_PDB)/exp.PDBmap $(ORIG_PDB)/theoretical_models $(ORIG_PDB)/no-mutant_ids $(ORIG_PDB)/resolution_ids
	cat $< | filter_1col -i -v 1 $^2 | filter_1col 1 $^3 | filter_1col 1 <(bawk '$$2<=$(RES_LIM) {print $$1}' $^4) >$@
# no need for this file since here we don't have any residue mapping, preserved for clarity
experimental_strct_in_mltalg.dict: experimental_strct_in_mltalg
	bawk '{print $$2, $$1}' $< >$@


.META: experimental_strct_in_mltalg
	1	PDB_ID
	2	Uniprot_ID
.META: experimental_strct_in_mltalg.dict
	1	Uniprot_ID
	2	PDB_ID

else
# preserved only for Spielmann version which is based on a RefSeq_Protein alignment
# all RefSeq ids in the original alignment
RefSeq_IDs_in_mltalg: $(ORIG_ALIGN)/sequence_descriptions.txt
	cat $< | unhead  | cut -f 1 | perl -F'_' -lane 'print "$$F[0]_$$F[1]"' | sed 's/\.[0-9]*$$//g' >$@

#translates RefSeq ids  into  existing corresponding PDB ids and filters out theoretical models
experimental_strct_in_mltalg: RefSeq_IDs_in_mltalg $(ORIG_PDB)/exp.PDBmap $(ORIG_PDB)/theoretical_models $(ORIG_PDB)/no-mutant_ids $(ORIG_PDB)/resolution_ids
	cat $< | translate -a -d -j -k <(cat $^2 | bawk '{print $$2,$$1}') 1 \
	| filter_1col -i -v 2 $^3 | filter_1col 2 <(cat $^4) | filter_1col 2 <(bawk '$$2<=$(RES_LIM) {print $$1}' $^5) | bawk '{print $$2, $$1}' | sort | uniq >$@

experimental_strct_in_mltalg.dict: experimental_strct_in_mltalg $(ORIG_ALIGN)/sequence_descriptions.txt
	cat $< | translate -a -k -d -r -f 1 <(cut -f 1 $^2 | unhead | perl -F'_' -lane 'print "$$F[0]_$$F[1]\t$$F[2]_$$F[3]"' | sed 's/\.[0-9]*$$//g') 1 \
	| bawk '{print $$1"_"$$3, $$2}' | sort | uniq | >$@


.META: experimental_strct_in_mltalg
	1	PDB_ID
	2	RefSeq_ID

.META: experimental_strct_in_mltalg.dict
	1	Uniprot_entry
	2	PDBmap_id
	3	PDB
	4	resolution


endif
endif


ifeq ($(PFAM),yes)
# retrieve polypeptide sequences from all .cif files in ORIG_PDB
experimental_strct.polypeptide.fasta: experimental_strct_in_mltalg
	cut -f1 $< | xargs -I% --max-args=1 echo "$(ORIG_PDB)/%.cif" | tr '\n' ' ' | $(PRJ_ROOT)/local/bin/structure2fasta -c experimental_strct.canonical.fasta -p $@ -P $<


# using mafft mafft-7.222-with-extensions, version 7.313 cannot be run from bmake or a python wrapper (Error 1) but still works from plain bash
# multiple aligment of polypeptides vs existing multialignment (unmasked) of full protein sequences in ORIG_ALIGN
# in Pfam versions mafft -add is used since the existing multialignment is composed of domain sequences with length comparable to polypeptides
experimental_strct.polypeptide.aligned.fasta: experimental_strct.polypeptide.fasta $(ORIG_PDB)/PFAM_alignment.exp.PDBmap.fasta
	mafft --quiet --anysymbol --add $< --reorder --thread -1 $^2 >$@
else
# for Spielmann and General query versions
# retrieve polypeptide sequences from all .cif files whose PDB_IDs passed all filtering stages 
# (i.e. no mutants, xray only, no theoretical models, resoliution cutoff)
experimental_strct.polypeptide.fasta: experimental_strct_in_mltalg
	cut -f1 $< | xargs -I% --max-args=1 echo "$(ORIG_PDB)/%.cif" | tr '\n' ' ' | $(PRJ_ROOT)/local/bin/structure2fasta -c experimental_strct.canonical.fasta -p $@

ifeq ($(GENERAL_QUERY),True)
# for general query only
# align canonical sequences
experimental_strct.canonical.aligned.fasta: experimental_strct.polypeptide.fasta
	mafft --quiet --anysymbol --reorder --thread -1 experimental_strct.canonical.fasta >$@

# reconstruct tree from canonical sequence alignment using mafft
experimental_strct.canonical.aligned.fasta.tree: experimental_strct.canonical.aligned.fasta experimental_strct_in_mltalg.dict
	mafft --retree 2 --treeout $< >$@ ; printf ";" >>$@ ;\
	cat $@ | sed 's/^[0-9]*_//g ; s/-[0-9]_can_sequence//g' >$@.tmp ; \
	bawk '{print $$2, $$1}' $^2 | $(PRJ_ROOT)/local/src/PDB2UNIPROT_tree_idmapping.py $@.tmp >$@ ; \
	rm $@.tmp

TREE=experimental_strct.canonical.aligned.fasta.tree

# realign polypeptides to the canonical structures alignment
experimental_strct.polypeptide.aligned.fasta: experimental_strct.polypeptide.fasta experimental_strct.canonical.aligned.fasta 
	mafft --quiet --anysymbol --add $< --reorder --thread -1 $^2 >$@
else
# for Spielmann version only
# multiple aligment of polypeptides vs existing multialignment (unmasked) of full protein sequences in ORIG_ALIGN
experimental_strct.polypeptide.aligned.fasta: experimental_strct.polypeptide.fasta $(ORIG_ALIGN)/protein_aln_struc.fasta
	mafft --anysymbol --addfragments $< --reorder --thread -1 $^2 >$@
endif
endif

experimental_strct.polypeptide.aligned.infoalign: experimental_strct.polypeptide.aligned.fasta
	infoalign $< -stdout -auto | unhead | cut -f2- >$@

ifeq ($(PFAM),yes)
experimental_strct.polypeptide_only.aligned.infoalign: experimental_strct.polypeptide.aligned.infoalign experimental_strct_in_mltalg.dict
	grep 'poly' $< | tr "-" "\t" | translate -a -d -c -j <(bawk '{print $$PDB, $$PDBmap_id}' $^2) 1 | bawk '{print $$1"-"$$3"-"$$4"-"$$5, $$6~15, $$2}' >$@ 
else
experimental_strct.polypeptide_only.aligned.infoalign: experimental_strct.polypeptide.aligned.infoalign experimental_strct_in_mltalg.dict
	grep 'poly' $< | tr "-" "\t" | translate -a -f 2 -d -c -j <(cat $^2) 1 | bawk '{print $$1"-"$$3"-"$$4"-"$$5, $$6~15, $$2}' >$@ 
endif

.META: *polypeptide_only.aligned.infoalign
	1	Seq_Nam
	2	Seq_Length
	3	AlignLen
	4	Gaps
	5	GapLen
	6	Ident
	7	Similar
	8	Different
	9	%Change
	10	1.000000
	11	Description
	12	CrossRef_Seq_Nam

# find_best does the following: if there are multiple polypeptides from the same structure, same model, and same chain w/t different lengths, pick the longest
merged_poly.XRAY_only: experimental_strct.polypeptide_only.aligned.infoalign $(ORIG_PDB)/xray_only_ids 
	cut -f1 $< | sed 's/>\|_end\|-<Polypeptide_start//g ; s/=\|-/\t/g' \
	| filter_1col 1 $^2 | bawk '{print $$1"-"$$2"-"$$3, $$4, $$5}' \
	| sort -k1,1 -k2,2n | uniq \
	| $(PRJ_ROOT)/local/bin/poly_merge | translate -a -d -j -k <(cut -f1,12 $<) 1 \
	| bawk '{print $$1, $$3, $$4, $$2}' | tr "<" "\t" \
	| find_best -m 1 5 \ 
	| bawk '{print $$1"<"$$2, $$3"<"$$4, $$5, $$6}' \
	| sort | uniq > $@

.META: merged_poly.XRAY_only
	1	Origina_Poly
	2	Merged_Poly
	3	Merged_length
	4	CrossRef_Seq_Nam

ifeq ($(PFAM),yes)
# unsolved issue with /usr/lib64/python2.7/site-packages/scipy/spatial/distance.py , type error when converting distance matrix to distance vector.
experimental_strct_in_mltalg.tree.pdf: $(TREE) 
	../../local/bin/vis_tree -o $@ -p $(EXP_FULL_IDS)  $<

# obtain all tree leaves combination  among different polypeptides
experimental_strct_in_mltalg.tree_leaves_comb.merged_poly: $(TREE) experimental_strct.polypeptide_only.aligned.infoalign merged_poly.XRAY_only experimental_strct_in_mltalg.dict
	$(PRJ_ROOT)/local/bin/tree_dist -l -p $(shell cut -f2 $^4 | sort | uniq | tr '\n' ','  | sed 's/,$$//g') $< | translate -a -d -k -f 2 -j <(cut -f1,12 $^2) 1 \
	| translate -a -d -k -f 2 -j <(cut -f1,12 $^2) 3 | translate -a -d -k -j <(cut -f1,2 $^3) 2 \
	| translate -a -d -k -j <(cut -f1,2 $^3) 5 | bawk '$$1!=$$4 && $$3!=$$6 {print $$3,$$6}' \
	| tr "-" "\t" | bawk '$$1!=$$5 {print $$1"-"$$2"-"$$3"-"$$4, $$5"-"$$6"-"$$7"-"$$8}'| sort | uniq >$@ 

# obtain a sequence of leaves (among different polypeptides) to be compared in the adjoining-leaves algorithm
experimental_strct_in_mltalg.tree_leaves_adjoining.merged_poly: $(TREE) experimental_strct.polypeptide_only.aligned.infoalign merged_poly.XRAY_only experimental_strct_in_mltalg.dict
	cut -f1,12 $^2 | sort -k1,1 | uniq > $@.tmp ; \ 
	$(PRJ_ROOT)/local/bin/tree_dist -a -p $(shell cut -f2 $^4 | sort | uniq | tr '\n' ','  | sed 's/,$$//g') $< | translate -a -d -k -f 2 -j <(cat $@.tmp) 1 \
	| translate -a -d -k -f 2 -j <(cat $@.tmp) 3 | translate -a -d -k -j <(cut -f1,2 $^3) 2 \
	| translate -a -d -k -j <(cut -f1,2 $^3) 5 | bawk '$$1!=$$4 && $$3!=$$6 {print $$3,$$6}' \
	| tr "-" "\t" | bawk '$$1!=$$5 {print $$1"-"$$2"-"$$3"-"$$4, $$5"-"$$6"-"$$7"-"$$8}'| sort | uniq >$@ ; \
	rm $@.tmp
else
# not needed if not estimating the model 
endif

#calculate consensus sequence from multialignment
%.aligned.consensus.fasta: %.aligned.fasta
	cons $< -plurality 0.0 -name  'CONS_SEQ'   -stdout |  sed 's/-//g' | tr '[:lower:]' '[:upper:]' >$@

# search for pfam and prosite domains in consensus sequence
%.aligned.consensus.pfam-ProSite: %.aligned.consensus.fasta
	interproscan.sh -appl Pfam,PROSITEPROFILES -i $< -o $@ -f TSV -T $(BIOINFOROOT)/tmp

%.aligned.consensus.domains.in: %.aligned.consensus.pfam-ProSite
	cut -f 4,5,7,8 $< >$@

# search for all domains in consensus sequence
%.aligned.consensus.all_domains: %.aligned.consensus.fasta
	interproscan.sh -i $< -o $@ -f TSV -T $(BIOINFOROOT)/tmp

experimental_strct.polypeptide_only.aligned.fasta: experimental_strct.polypeptide.aligned.fasta
	cat $< | fasta2tab -s | grep 'poly' | bsort -k1,1 | tab2fasta >$@

ifeq ($(PFAM),yes)
# iterates over aligned polypeptide sequence, obtains chi angles lists and phi, psi values and average bfactor for atoms in rotamer
experimental_strct.polypeptide.mapped.angle_matrix.bfactor.gz: experimental_strct_in_mltalg experimental_strct.polypeptide.aligned.fasta
	cut -f1 $< | xargs -I% --max-args=1 echo "$(ORIG_PDB)/%.cif" | tr '\n' ' ' | $(PRJ_ROOT)/local/bin/non_pairwise.calc_angles -b -P experimental_strct_in_mltalg -p $^2 | gzip >$@				

.META: experimental_strct.polypeptide.mapped.angle_matrix.bfactor.gz
	1	Polypeptide_ID
	2	Structure_ID
	3	Align_site_id
	4	Resname
	5	chi_list
	6	phi_psi
	7	bfactor
	8	PDB_site_id

experimental_strct.polypeptide.mapped.II_struct: experimental_strct.polypeptide.mapped.angle_matrix.bfactor.gz experimental_strct_in_mltalg
	bawk '{print $$Structure_ID, $$PDB_site_id, $$Align_site_id}'  $< | $(PRJ_ROOT)/local/src/assign_II_struct.py  $(ORIG_PDB)/  >$@

.META: experimental_strct.polypeptide.mapped.II_struct
	1	PDB_site_id
	2	Align_site_id
	3	IIary_struct
	4	Structure_ID

experimental_strct.polypeptide.mapped.angle_matrix.bfactor.dstr.pdf: experimental_strct.polypeptide.mapped.angle_matrix.bfactor.gz
	bawk '{print $$Polypeptide_ID";"$$PDB_site_id, $$bfactor}' $< \
	| tr ',' '\t' | cut -f -4 \
	| grep -v 'Disordered' | grep -vw  'NA' \
	| sort | uniq | cut -f 2- | $(PRJ_ROOT)/local/bin/bfactor_dstr -o $@

PDB_site_id.dict: experimental_strct.polypeptide.mapped.angle_matrix.gz
	bawk '{print $$Align_site_id";"$$Structure_ID,$$Resname, $$Polypeptide_ID, $$PDB_site_id}' $< \
	| sort -k1,1 | tr "-" "\t" | bawk '{print $$1";"$$5";"$$2, $$7}' | sort | uniq >$@

experimental_strct.polypeptide.mapped.angle_matrix.XRAY_only.gz: experimental_strct.polypeptide.mapped.angle_matrix.gz $(ORIG_PDB)/xray_only_ids
	zcat  $< | cut -f -6 | filter_1col 2 $^2 | gzip >$@

else
ifeq ($(GENERAL_QUERY),True)
# also non-pairwise
experimental_strct.polypeptide.mapped.angle_matrix.bfactor.gz: experimental_strct_in_mltalg experimental_strct.polypeptide.aligned.fasta
	cut -f1 $< | xargs -I% --max-args=1 echo "$(ORIG_PDB)/%.cif" | tr '\n' ' ' | $(PRJ_ROOT)/local/bin/non_pairwise.calc_angles -b -p $^2 | gzip >$@	

.META: experimental_strct.polypeptide.mapped.angle_matrix.bfactor.gz
	1	Polypeptide_ID
	2	Structure_ID
	3	Align_site_id
	4	Resname
	5	chi_list
	6	phi_psi
	7	bfactor
	8	PDB_site_id

experimental_strct.polypeptide.mapped.II_struct: experimental_strct.polypeptide.mapped.angle_matrix.bfactor.gz experimental_strct_in_mltalg
	bawk '{print $$Structure_ID, $$PDB_site_id, $$Align_site_id}'  $< | $(PRJ_ROOT)/local/src/assign_II_struct.py  $(ORIG_PDB)/  >$@


experimental_strct.polypeptide.mapped.angle_matrix.bfactor.dstr.pdf: experimental_strct.polypeptide.mapped.angle_matrix.bfactor.gz
	bawk '{print $$Polypeptide_ID";"$$PDB_site_id, $$bfactor}' $< \
	| tr ',' '\t' | cut -f -4 \
	| grep -v 'Disordered' | grep -vw  'NA' \
	| sort | uniq | cut -f 2- | $(PRJ_ROOT)/local/bin/bfactor_dstr -o $@

PDB_site_id.dict: experimental_strct.polypeptide.mapped.angle_matrix.gz
	bawk '{print $$Align_site_id";"$$Structure_ID,$$Resname, $$Polypeptide_ID, $$PDB_site_id}' $< \
	| sort -k1,1 | tr "-" "\t" | bawk '{print $$1";"$$5";"$$2, $$7}' | sort | uniq >$@

experimental_strct.polypeptide.mapped.angle_matrix.XRAY_only.gz: experimental_strct.polypeptide.mapped.angle_matrix.gz $(ORIG_PDB)/xray_only_ids
	zcat  $< | cut -f -6 | filter_1col 2 $^2 | gzip >$@

 
else
# pairwise method (i.e. each line refers to a pair residues in different structure)
# non necessary and much slower, included for backward compatibility with Spielmann version
# maps all corresponding residues (structural aligment), obtains chi angles lists and phi, psi values
experimental_strct.polypeptide.mapped.angle_matrix.gz: experimental_strct_in_mltalg experimental_strct.polypeptide.aligned.fasta
	$(PRJ_ROOT)/local/bin/calc_angles -p $^2 $(shell cut -f1 $< | xargs -I% --max-args=1 echo "$(ORIG_PDB)/%.cif" | tr '\n' ' ') | gzip >$@ 

.META: experimental_strct.polypeptide.mapped.angle_matrix.gz
	1	Polypeptide1_ID;Polypeptide2_ID
	2	Structure1_ID
	3	Structure2_ID
	4	site_id
	5	Resname1
	6	Resname2
	7	chi_list1
	8	chi_list2
	9	phi_psi1
	10	phi_psi2
	11	PDB_site_id1
	12	PDB_site_id2

# maps all corresponding residues (structural aligment), obtains chi angles lists and phi, psi values
experimental_strct.polypeptide.mapped.angle_matrix.bfactor.gz: experimental_strct_in_mltalg experimental_strct.polypeptide.aligned.fasta
	$(PRJ_ROOT)/local/bin/calc_angles -b -p $^2 $(shell cut -f1 $< | xargs -I% --max-args=1 echo "$(ORIG_PDB)/%.cif" | tr '\n' ' ') | gzip >$@ 

.META: experimental_strct.polypeptide.mapped.angle_matrix.bfactor.gz
	1	Polypeptide1_ID;Polypeptide2_ID
	2	Structure1_ID
	3	Structure2_ID
	4	site_id
	5	Resname1
	6	Resname2
	7	chi_list1
	8	chi_list2
	9	phi_psi1
	10	phi_psi2
	11	bfactor1
	12	bfactor2
	13	PDB_site_id1
	14	PDB_site_id2

experimental_strct.polypeptide.mapped.angle_matrix.bfactor.dstr.pdf: experimental_strct.polypeptide.mapped.angle_matrix.bfactor.gz
	bawk '{print $$11"\n"$$12}' $< \
	| tr ',' '\t' | cut -f -4 \
	| grep -v 'Disordered' | grep -vw  'NA' \
	| sort | uniq | cut -f 2- | $(PRJ_ROOT)/local/bin/bfactor_dstr -o $@


PDB_site_id.dict: experimental_strct_in_mltalg experimental_strct.polypeptide.aligned.fasta
	$(PRJ_ROOT)/local/bin/non_pairwise.calc_angles $^2 $(shell cut -f1 $< | xargs -I% --max-args=1 echo "$(ORIG_PDB)/%.cif" | tr '\n' ' ') \
	| bawk '{print $$3";"$$2, $$4, $$1, $$6}' | sort -k1,1 | tr "-" "\t" | bawk '{print $$1";"$$5";"$$2, $$7}'>$@

experimental_strct.polypeptide.mapped.angle_matrix.XRAY_only.gz: experimental_strct.polypeptide.mapped.angle_matrix.gz $(ORIG_PDB)/xray_only_ids
	zcat  $< | cut -f -10 | filter_1col 2 $^2 | filter_1col 3 $^2 | gzip >$@
endif
endif

# Dunbrack rotamer library
rotamer_library: $(ORIG_LIBRARY)/ALL.bbdep.rotamers.lib
	cat $<  | grep -v '#'  | sed 's/ \+ /\t/g' | tr ' ' '\t' | cut -f 1-3,5-13 >$@

ifeq ($(PFAM),yes)
# use the non-pairwise angle calculation method to speed up the process
%.chi1_assigned.gz: %.bfactor.gz rotamer_library
	 zcat $< | cut -f -6 | $(PRJ_ROOT)/local/bin/rotamer_assigment -n 4,5,6  $^2 | gzip >$@

%.chi1_assigned.no_0-NA.gz: %.chi1_assigned.gz
	zcat $< | bawk '{print $$3, $$1, $$4, $$7, $$8}' \
	| bawk '$$4!="NA" && $$5!="0.0" { gsub(/-/, "\t", $$2); print }' \
	| bsort -dk1,5 | uniq | bsort -gk1,1 | gzip >$@

# retains GLY and ALA residues
%.chi1_assigned.GLY-ALA.no_0-NA.gz: %.chi1_assigned.gz
	zcat $< | bawk '{print $$3, $$1, $$4, $$7, $$8}' \
	| bawk '{if ($$3=="ALA" || $$3=="GLY" || $$4 != "NA" && $$5!~/'NA'|'0.0'/) print}' \
	| tr '-' '\t' | bsort -dk1,5 | uniq | bsort -gk1,1 | gzip >$@

else
ifeq ($(GENERAL_QUERY),True)
# also non-pairwise
%.chi1_assigned.gz: %.bfactor.gz rotamer_library
	 zcat $< | cut -f -6 | $(PRJ_ROOT)/local/bin/rotamer_assigment -n 4,5,6  $^2 | gzip >$@

%.chi1_assigned.no_0-NA.gz: %.chi1_assigned.gz
	zcat $< | bawk '{print $$3, $$1, $$4, $$7, $$8}' \
	| bawk '$$4!="NA" && $$5!="0.0" { gsub(/-/, "\t", $$2); print }' \
	| bsort -dk1,5 | uniq | bsort -gk1,1 | gzip >$@

# retains GLY and ALA residues
%.chi1_assigned.GLY-ALA.no_0-NA.gz: %.chi1_assigned.gz
	zcat $< | bawk '{print $$3, $$1, $$4, $$7, $$8}' \
	| bawk '{if ($$3=="ALA" || $$3=="GLY" || $$4 != "NA" && $$5!~/'NA'|'0.0'/) print}' \
	| tr '-' '\t' | bsort -dk1,5 | uniq | bsort -gk1,1 | gzip >$@
else
%.chi1_assigned.gz: %.gz rotamer_library
	zcat $< | $(PRJ_ROOT)/local/bin/rotamer_assigment 5,6,7,8,9,10 $^2 | gzip >$@

# no0-NA filters out all disordered residues, first/last residues in polypeptide (No phi/psi), all GLY and ALA and all configs with p=0 in library. 
%.chi1_assigned.no_0-NA.gz: %.chi1_assigned.gz
	zcat $< | cut -f 1,4-6,11-14 | tr ';' '\t' | bawk '{print $$3, $$1, $$4, $$6, $$7 "\n" $$3,$$2,$$5,$$8,$$9}' \
	| bawk '$$4!="NA" && $$5!="0.0" { gsub(/-/, "\t", $$2); print }'  | bsort -dk1,5 \
	| uniq | bsort -gk1,1  | gzip >$@

%.chi1_assigned.GLY-ALA.no_0-NA.gz: %.chi1_assigned.gz
	zcat $< | cut -f 1,4-6,11-14 | tr ';' '\t' | bawk '{print $$3, $$1, $$4, $$6, $$7 "\n" $$3,$$2,$$5,$$8,$$9}' \
	| bawk '{if ($$3=="ALA" || $$3=="GLY" || $$4 != "NA" && $$5!~/'NA'|'0.0'/) print}' \
	| tr '-' '\t' | bsort -dk1,5 | uniq | bsort -gk1,1  | gzip >$@

endif
endif

%.chi1_assigned.no_0-NA.merged_poly.gz: %.chi1_assigned.no_0-NA.gz merged_poly.XRAY_only 
	zcat $< | bawk '{print $$1, $$2"-"$$3"-"$$4"-"$$5~8}' \
	| translate -a -d -j -k <(cat $^2 | cut -f 1,2) 2 \
	| cut -f1,3-6 | bawk '{ gsub(/-/, "\t", $$2); print }' \
	| gzip >$@ 

%.chi1_assigned.GLY-ALA.no_0-NA.merged_poly.gz: %.chi1_assigned.GLY-ALA.no_0-NA.gz merged_poly.XRAY_only
	zcat $< | bawk '{print $$1, $$2"-"$$3"-"$$4"-"$$5~8}' \
	| translate -a -d -j -k <(cat $^2 | cut -f 1,2) 2 \
	| cut -f1,3-6 | bawk '{ gsub(/-/, "\t", $$2); print }' \
	| gzip >$@ 


.META: *.chi1_assigned.*no_0-NA*.gz
	1	site_id
	2	struct_id
	3	model_id
	4	chain_id
	5	Polypeptide_id
	6	resname
	7	chi1_config
	8	chi1_config_p	

ifeq ($(PFAM),yes)
%.no_0-NA.merged_poly.heatmap.in: %.no_0-NA.merged_poly.gz merged_poly.XRAY_only
	bawk '{print $$site_id, $$struct_id"-"$$model_id"-"$$chain_id"-"$$Polypeptide_id, $$resname, $$chi1_config}' $< \
	| translate -k -a -d -j <(bawk 'gsub(/-/, ":", $$4) {print $$2, $$4}' $^2)  2 \	
	| bawk '{print $$1,$$2"-"$$3,$$4,$$5}' | sed 's/\//_/g' | sort -nk1,1 | uniq >$@	

# residue type information only
%.merged_poly.restype-only.heatmap.in: %.mapped.angle_matrix.gz merged_poly.XRAY_only
	bawk '{print $$Align_site_id, $$Polypeptide_ID, $$Resname}' $< \
	| translate -k -a -d -j <(bawk 'gsub(/-/, ":", $$4) {print $$2, $$4}' $^2)  2 \
	| bawk '{print $$1,$$2"-"$$3,$$4}' | sed 's/\//_/g' | sort -nk1,1 | uniq >$@
else
ifeq ($(GENERAL_QUERY),True)
%.no_0-NA.merged_poly.heatmap.in: %.no_0-NA.merged_poly.gz merged_poly.XRAY_only
	bawk '{print $$site_id, $$struct_id"-"$$model_id"-"$$chain_id"-"$$Polypeptide_id, $$resname, $$chi1_config}' $< \
	| translate -k -a -d -j <(bawk '{print $$2, $$4}' $^2)  2 \	
	| bawk '{print $$1,$$2"-"$$3,$$4,$$5}' | sed 's/\//_/g' | sort -nk1,1 | uniq >$@	

# residue type information only
%.merged_poly.restype-only.heatmap.in: %.mapped.angle_matrix.gz merged_poly.XRAY_only
	bawk '{print $$Align_site_id, $$Polypeptide_ID, $$Resname}' $< \
	| translate -k -a -d -j <(bawk 'gsub(/-/, ":", $$4) {print $$2, $$4}' $^2)  2 \
	| bawk '{print $$1,$$2"-"$$3,$$4}' | sed 's/\//_/g' | sort -nk1,1 | uniq >$@
else
# for backward compatibility with Spielmann version
%.no_0-NA.heatmap.in: %.no_0-NA.gz experimental_strct.polypeptide_only.aligned.infoalign
	bawk '$$site_id>2000 && $$site_id<5000 {print $$site_id, $$struct_id"-"$$model_id"-"$$chain_id"-"$$Polypeptide_id, $$resname, $$chi1_config}' $< \
	| translate -k -a -d -j <(bawk '$$2>100 {print $$1, $$12}' $^2)  2 \
	| bawk '{print $$1,$$2"-"$$3,$$4,$$5}' | sort | uniq >$@

%.no_0-NA.merged_poly.heatmap.in: %.no_0-NA.merged_poly.gz merged_poly.XRAY_only
	bawk '$$site_id>2000 && $$site_id<5000 {print $$site_id, $$struct_id"-"$$model_id"-"$$chain_id"-"$$Polypeptide_id, $$resname, $$chi1_config}' $< \
	| translate -k -a -d -j <(bawk '$$3>100 {print $$2, $$4}' $^2)  2 \	
	| bawk '{print $$1,$$2"-"$$3,$$4,$$5}' | sort | uniq >$@	

%.merged_poly.restype-only.heatmap.in: %.mapped.angle_matrix.gz merged_poly.XRAY_only
	zcat $< | tr ";" "\t" | bawk '$$5>2000 && $$5<5000 {print $$5, $$1, $$6"\n"$$2, $$5, $$7}' \
	| translate -k -a -d -j <(bawk '$$3>100 {print $$2, $$4}' $^2)  2 \	
	| bawk '{print $$1,$$2"-"$$3,$$4,$$5}' | sort | uniq >$@	
endif
endif

.META: *.heatmap.in
	1	site_id
	2	struct_id
	3	resname
	4	chi_1_config

.META: *.restype-only.heatmap.in
	1	site_id
	2	struct_id
	3	resname

%.no_0-NA.merged_poly.confP.heatmap.in: %.no_0-NA.merged_poly.gz merged_poly.XRAY_only
	bawk '$$site_id>2000 && $$site_id<5000 {print $$site_id, $$struct_id"-"$$model_id"-"$$chain_id"-"$$Polypeptide_id, $$resname, $$chi1_config_p}' $< \
	| translate -k -a -d -j <(bawk '$$3>100 {print $$2, $$4}' $^2)  2 \	
	| bawk '{print $$1,$$2"-"$$3,$$4,$$5}' >$@	

.META: *.confP.heatmap.in
	1	site_id
	2	struct_id
	3	resname
	4	chi_1_config

.PHONY: %.single-heatmaps %.confP.single-heatmaps *.hydrophobic.single-heatmaps
# creates and heatmap for each Polypeptide sequence. Each cell in the grid corresponds to a site in the alignment. 
# Cells are assigned configuration and color based on chi1 angle value and residue type.
%.single-heatmaps: %.heatmap.in
	mkdir $*.single-heatmaps ; \
	cat $< | $(PRJ_ROOT)/local/bin/vis_single-heatmap -o $@/ $(shell cut -f 1 $< | count) $(shell cut -f 2 $< | count) 

# different script, no residue cetegories considered, only configuartion probability
# deprecated: this probability refers to the FULL configuration (i.e. 1 2 3 4 5) not only to chi1 configuration. 
%.confP.single-heatmaps: %.confP.heatmap.in
	mkdir $*.confP.single-heatmaps ; \
	cat $< | $(PRJ_ROOT)/local/src/vis_single-heatmap.py -o $@/ $(shell cut -f 1 $< | count) $(shell cut -f 2 $< | count)

%.hydrophobic.single-heatmaps: %.heatmap.in
	mkdir $@ ; \
	cat $< | $(PRJ_ROOT)/local/src/vis_single-heatmap.py -y -o $@/ $(shell cut -f 1 $< | count) $(shell cut -f 2 $< | count)

# plots all single heatmaps with the corresponding phylogenetic tree displayed on the y axis
%.single-tree.pdf: $(TREE) %.single-heatmaps
	$(PRJ_ROOT)/local/bin/vis_tree2 -o $@ $^

# An alphabet containing all symbols used to describe chi1 and chi2 configurations for 18 residues.
residue_config.alphabet: rotamer_library experimental_strct.polypeptide.mapped.angle_matrix.chi1_assigned.no_0-NA.gz
	cut -f 1,4,5 $< | sort | uniq | filter_1col 1 <(zcat $^2 | cut -f 6 | sort | uniq) >$@

ifeq ($(PFAM),yes)
residue_config.alphabet.chi1.colors.hues: residue_config.alphabet
	ln -sf ../Rotamers_evo.Spielman/$@ $@

residue_config.alphabet.chi1.colors.uniq: residue_config.alphabet
	ln -sf ../Rotamers_evo.Spielman/$@ $@

residue_config.alphabet.chi1.colors.hydrophobic: residue_config.alphabet
	ln -sf ../Rotamers_evo.Spielman/$@ $@

else

# currently manually created#
# Assign hex colors to symbols. Each residue has a hue, configurations are luminosity-encoded.
#residue_config.alphabet.chi1.colors.hues: residue_config.alphabet
#	cat $< | cut -f-2 | sort | uniq |../../local/bin/assign_color 18,3 1,2 | bawk '{print $$1$$2, $$3}' >$@

# Assign unique hex colors to symbols.
residue_config.alphabet.chi1.colors.uniq: residue_config.alphabet
	cat $< | cut -f-2 | sed 's/\t//g' | sort | uniq | $(PRJ_ROOT)/local/bin/assign_color 53 1 >$@

# currently manually created
# Assign hues to symbols corresponding to hydrophobic residues. 
# residue_config.alphabet.chi1.colors.hydrophobic: residue_config.alphabet
#	cat $< | cut -f-2 | sort | uniq |../../local/bin/assign_color -r ALA,ILE,LEU,PHE,VAL,PRO,GLY 18,3 1,2 | bawk '{print $$1$$2, $$3}' >$@

endif

# plots a stacked barplot for each site displaying symbol count. Use common hue to describe symbols from same residue.
%.stack-barplot.hues: %.heatmap.in residue_config.alphabet.chi1.colors.hues
	mkdir $@ ; \
	cat $< | $(PRJ_ROOT)/local/bin/vis_stack-bar -s -d $@/ $(shell cut -f 1 $< | count) $^2

# plots a stacked barplot for each site displaying symbol count. Use unique colors for symbols.
%.stack-barplot.uniq: %.heatmap.in residue_config.alphabet.chi1.colors.uniq
	mkdir $@ ; \
	cat $< | $(PRJ_ROOT)/local/bin/vis_stack-bar -s -d $@/ $(shell cut -f 1 $< | count) $^2

# plots a stacked barplot for each site. Color only hydrophobic residues, use common hue to describe symbols from same residue.
%.stack-barplot.hydrophobic: %.heatmap.in residue_config.alphabet.chi1.colors.hydrophobic
	mkdir $@ ; \
	cat $< | $(PRJ_ROOT)/local/bin/vis_stack-bar -s -d $@/ $(shell cut -f 1 $< | count) $^2

# plots a stacked barplot for each site. Color only hydrophobic residues, use common hue to describe symbols from same residue.
%.stack-barplot.rgb: %.heatmap.in residue_config.alphabet.chi1.colors.rgb
	mkdir $@ ; \
	cat $< | $(PRJ_ROOT)/local/bin/vis_stack-bar -s -d $@/ $(shell cut -f 1 $< | count) $^2

# plots all single stack barplot with the corresponding phylogenetic tree displayed on the y axis
%.stack.single-tree.pdf: $(TREE) %.stack-barplot.hues
	$(PRJ_ROOT)/local/bin/vis_tree2 -o $@ $^

# plots all single stack barplot with the corresponding phylogenetic tree displayed on the y axis
%.stack.single-tree.hydrophobic.pdf: $(TREE) %.stack-barplot.hydrophobic
	$(PRJ_ROOT)/local/bin/vis_tree2 -o $@ $^

# plots all single stack barplot with the corresponding phylogenetic tree displayed on the y axis
%.stack.single-tree.rgb.pdf: $(TREE) %.stack-barplot.rgb
	$(PRJ_ROOT)/local/bin/vis_tree2 -o $@ $^


# plots a stacked bar plot with all seuences collapsed into a single graph.
%.all_stack-barplot.png: %.heatmap.in residue_config.alphabet.chi1.colors.hues
	cat $<  | $(PRJ_ROOT)/local/src/vis_stack-bar.py -o $@  $(shell cut -f 1 $< | count)  $^2

# plots a stacked bar plot with all sequences collapsed into a single graph. Color scheme only follows chi1 config.
%.all_stack-barplot.rgb.png: %.heatmap.in residue_config.alphabet.chi1.colors.rgb
	cat $<  | $(PRJ_ROOT)/local/src/vis_stack-bar.py -o $@  $(shell cut -f 1 $< | count)  $^2


# contains symbol, residue, configuration and structure count for each site in *heatmap.in
%.heatmap.in.stats: %.heatmap.in
	cat $< | $(PRJ_ROOT)/local/bin/site_stats >$@

.META: *.heatmap.in.stats
	1	align_site
	2	key_count
	3	symbol_count
	4	res_count
	5	config_count
	6	symbols
	7	key	

# if there are multiple mappings of the same align_site to different resnums in the same protein, only one is kept.
site_%: experimental_strct.polypeptide.mapped.angle_matrix.chi1_assigned.no_0-NA.merged_poly.heatmap.in.stats PDB_site_id.dict experimental_strct.polypeptide.mapped.angle_matrix.chi1_assigned.no_0-NA.gz experimental_strct.polypeptide.mapped.angle_matrix.chi1_assigned.gz
	grep '^$*' $< | expandsets 7 | sed 's/:/;/g' | translate -a -d -j  <(cat $^2) 7 \
	| bawk 'gsub(/;/, "\t", $$7)' | bawk '{print $$1~6, $$7";"$$8, $$9~11}' \
	| translate  -d -v -j -a <(zcat $^3 | bawk '{print $$1";"$$2, $$7}') 7 \
	| translate  -d -v -j -a <(bawk 'gsub(/,/,"\t",	$$6) {print $$3";"$$2, $$6}' $^4) 7 \
	| sort | uniq | bawk 'gsub(/[[:digit:]]\.[[:digit:]]/,"0",$$8) gsub(/[[:digit:]]\.[[:digit:]]/,"0",$$9) {print}' \
	| sed 's/-0/0/g' | cut -f6- | bawk 'gsub(/;/, "\t", $$2) {print}' | bawk '{print $$2, $$3, $$1, $$4~9}' | sort -u -k2,2 >$@

.META: site_*
	1	align_site
	2	structure
	3	conserved_chi1_symbols
	4	phi
	5	psi
	6	chi1_config
	7	chain
	8	resname
	9	resnum

# generates an alternate version for all structures at site with the other conserved chi_1 config and an artefact with the non-conserved config 
# calculate interaction energy of residue side chain vs all other atoms in protein. Keep the alternate / artefact structure with the lowest interaction energy.
altered_chi1_structures.site_%: site_% $(ORIG_PDB) rotamer_library
	mkdir $@; \
	cat $< | CHIMERA --nogui --nostatus --script "$(PRJ_ROOT)/local/src/set_chi1-2-3.py -i $^2 -o $@ -f $@.outfile $^3"> $@.log

all_altered_chi1_structures.%: % $(ORIG_PDB) rotamer_library
	mkdir $@; \
	cat $< | CHIMERA --nogui --nostatus --script "$(PRJ_ROOT)/local/src/set_chi1-2-3.py -n -i $^2 -o $@ $^3"> $@.log ;\
	rm outfile 



PHONY: all_altered_chi1_structures.foldex.%
# for each structure in the folder, using foldex freeze modified residue, "minimize" (RepairPDB), then calculate the energy-related terms from Stability
all_altered_chi1_structures.foldex.%: all_altered_chi1_structures.% % $(PRJ_ROOT)/local/share/data/foldx.3to1.dict 
	rm -f $</rotabase.txt $</*.fxout $</*tmp*; \
	(for i in $(shell ls $< | tr ' ' '\n'); do \
	cd $<; ln -sf $(BIOINFO_ROOT)/local/bin/rotabase.txt rotabase.txt ; \
	echo "$$i" | sed 's/\.\|_/\t/g' | cut -f1 | translate <(bawk '{print $$2, $$8, $$7, $$9}' ../$^2) 1 | translate <(cat $^3 | unhead) 1 | sed 's/\t//g' >  $$i.tmp  ; \
	foldx --command=RepairPDB --pdb=$$i --fix-residues-file=$$i.tmp ; \
	foldx --command=Stability --pdb=$$(ls *_Repair.pdb) --output-file=$$i ; \ 
	cat $$i_ST.fxout | bawk -v var="$$i" 'BEGIN {print var, $$2~24}' > $$i.tmp1; \
	mv $$i.tmp1 $$i_ST.fxout ; \
	rm -f rotabase.txt *tmp* *Repair*; \
	cd ..  ; \
	done ;)


stability.stats.foldex.%: all_altered_chi1_structures.%
	(for i in $(shell ls $</*ST.fxout) ; do \
	cat $$i | tr -s '\t' '\t' | tr ' ' '\t' | sed 's/^.\///g' >>$@; done ;) 

.META: stability.stats.foldex.site_*
	1	Pdb 	Pdb file
	2	Total Energy 	This is the predicted overall stability of your protein
	3	Backbone Hbond 	This the contribution of backbone Hbonds
	4	Sidechain Hbond 	This the contribution of sidechain-sidechain and sidechain-backbone Hbonds
	5	Van der Waals 	Contribution of the VanderWaals
	6	Electrostatics 	Electrostatic interactions
	7	Solvation Polar 	Penalization for burying polar groups
	8	Solvation Hydrophobic 	Contribution of hydrophobic groups
	9	Van der Waals clashes 	Energy penalization due to VanderWaals’ clashes (interresidue)
	10	Entropy Side Chain 	Entropy cost of fixing the side chain
	11	Entropy Main Chain 	Entropy cost of fixing the main chain
	12	Sloop Entropy 	ONLY FOR ADVANCED USERS
	13	Mloop Entropy 	ONLY FOR ADVANCED USERS
	14	Cis Bond 	Cost of having a cis peptide bond
	15	Torsional Clash 	VanderWaals’ torsional clashes (intraresidue)
	16	Backbone Clash 	Backbone-backbone VanderWaals. These are not considered in the total
	17	Helix Dipole 	Electrostatic contribution of the helix dipole
	18	Water Bridge 	Contribution of water bridges
	19	Disulfide 	Contribution of disulfide bonds
	20	Electrostatic Kon 	Electrostatic interaction between molecules in the precomplex
	21	Partial Covalent Bonds 	Interactions with bound metals
	22	Energy Ionisation 	Contribution of ionisation energy
	23	Entropy Complex 	Entropy cost of forming a complex
	24	Residue Number 	Number of residues

#AA frequencies computed from multialignment containing Spielman2015 and polypeptides. 
experimental_strct.polypeptide.AA_freq_table: experimental_strct.polypeptide.aligned.fasta
	$(PRJ_ROOT)/local/bin/AA_freqs $< >$@

# computes rotamer sequence information content per site in alignment.
experimental_strct.polypeptide.strct_multialign.chi1_assigned.no_0-NA.sites.info_content: experimental_strct.polypeptide.strct_multialign.chi1_assigned.no_0-NA.gz experimental_strct.polypeptide.AA_freq_table
	zcat $< | bawk '{print $$1, $$2, $$3, $$4, $$5, $$6$$7, $$8}' | $(PRJ_ROOT)/local/bin/rotamer_seq_info_content  1,6,7 $^2 \
	| translate -a -r <(zcat $< | cut -f1 | symbol_count) 1 | bsort -nrk2,3 >$@
	
.META: *.sites.info_content
	1	site_id
	2	info_content
	3	poly_at_site

experimental_strct.polypeptide.strct_multialign.chi1_assigned.no_0-NA.info_content.table: experimental_strct.polypeptide.strct_multialign.chi1_assigned.no_0-NA.gz experimental_strct.polypeptide.strct_multialign.chi1_assigned.no_0-NA.sites.info_content
	zcat $< | translate -a -r <(cat $^2) 1 | bawk '{print $$1, $$2"-"$$3"-"$$4"-"$$5, $$6~10 }' >$@

%.strct_multialign.chi1_assigned.no_0-NA.sites.info_content-domains.pdf: %.strct_multialign.chi1_assigned.no_0-NA.sites.info_content  %.aligned.consensus.domains.in
	cut -f -2 $<| $(PRJ_ROOT)/local/bin/vis_infocont -o $@ $^2

%.strct_multialign.chi1_assigned.no_0-NA.sites.info_content-domains.2k-4k.pdf: %.strct_multialign.chi1_assigned.no_0-NA.sites.info_content  %.aligned.consensus.domains.in
	bawk '$$1>2000 && $$1<4000 {print $$1, $$2}' $<  | $(PRJ_ROOT)/local/bin/vis_infocont -o $@ $^2


.PRECIOUS: experimental_strct.polypeptide.mapped.angle_matrix.chi1_assigned.GLY-ALA.no_0-NA.merged_poly.count_matrix.in

ifeq ($(PFAM),yes)
%.count_matrix.in: %.heatmap.in experimental_strct.polypeptide.mapped.angle_matrix.bfactor.gz merged_poly.XRAY_only
	cat $< | tr "-" "\t" | tr "_" "\t" | bawk '{print $$2"-"$$3"-"$$4"-"$$5"_"$$6"_"$$7";"$$1, $$8"/"$$9, $$10""$$11}' | sed 's/NA$$//g' \
	| translate -a -d -v -e 0.0 -j <(bawk '{print $$Polypeptide_ID,$$Align_site_id, $$bfactor}' $^2 | tr "," "\t"  | grep -v 'Disordered' \
	| translate -a -d -j -k <(cut -f1,2 $^3) 1 | bawk '{print $$2";"$$3, $$4}') 1 | tr ';' '\t' | bawk '{print $$1, $$4, $$2, $$5, $$3}' | uniq  >$@

.META: *.chi1.count_matrix.in
	1       Poly_id
	2       Uniprot_id/mapping
	3       multialign_site
	4       symbol
	5       B_factor	 

%.count_matrix.in.II_struct.noNA: %.count_matrix.in experimental_strct.polypeptide.mapped.II_struct
	cat $< | tr "-" "\t" | bawk '{print $$1"-"$$6, $$7, $$8}' \
	| translate -a -d -j <(bawk '{print $$Structure_ID"-"$$Align_site_id, $$IIary_struct}' $^2) 1 \
	| grep -v "NA" | tr "-" "\t" >$@

else
ifeq ($(GENERAL_QUERY),True)
%.count_matrix.in: %.heatmap.in experimental_strct.polypeptide.mapped.angle_matrix.bfactor.gz merged_poly.XRAY_only
	cat $< | tr "-" "\t" | tr "_" "\t" | bawk '{print $$2"-"$$3"-"$$4"-"$$5"_"$$6"_"$$7";"$$1, $$8, $$9""$$10}' | sed 's/NA$$//g' \
	| translate -a -d -v -e 0.0 -j <(bawk '{print $$Polypeptide_ID,$$Align_site_id, $$bfactor}' $^2 | tr "," "\t"  | grep -v 'Disordered' \
	| translate -a -d -j -k <(cut -f1,2 $^3) 1 | bawk '{print $$2";"$$3, $$4}') 1 | tr ';' '\t' | bawk '{print $$1, $$4, $$2, $$5, $$3}' | uniq  >$@

.META: *.chi1.count_matrix.in
	1       Poly_id
	2       Uniprot_id/mapping
	3       multialign_site
	4       symbol
	5       B_factor	 

%.count_matrix.in.II_struct.noNA: %.count_matrix.in experimental_strct.polypeptide.mapped.II_struct
	cat $< | tr "-" "\t" | bawk '{print $$1"-"$$6, $$7, $$8}' \
	| translate -a -d -j <(bawk '{print $$Structure_ID"-"$$Align_site_id, $$IIary_struct}' $^2) 1 \
	| grep -v "NA" | tr "-" "\t" >$@


else
%.count_matrix.in: %.heatmap.in experimental_strct.polypeptide.mapped.angle_matrix.bfactor.gz merged_poly.XRAY_only
	cat $< | tr "-" "\t" | tr "_" "\t" | bawk '{print $$2"-"$$3"-"$$4"-"$$5"_"$$6"_"$$7";"$$1,$$8"_"$$9"_"$$10"_"$$11,$$12""$$13}' | sed 's/NA$$//g' \
	| translate -a -d -j -v -e 0.0 <(zcat $^2 | tr ";" "\t" | bawk '{print $$1, $$5, $$12"\n"$$2,$$5, $$13}' | tr "," "\t" | grep -v 'Disordered' \
	| translate -a -d -j -k <(cut -f1,2 $^3) 1 | bawk '{print $$2";"$$3, $$4}') 1 | tr ';' '\t' | bawk '{print $$1, $$4, $$2, $$5, $$3}' | uniq >$@
endif
endif

# fake B factor is set at 0.0
experimental_strct.polypeptide.mapped.angle_matrix.chi1_assigned.GLY-ALA.no_0-NA.merged_poly.restype-only.count_matrix.in: experimental_strct.polypeptide.mapped.angle_matrix.chi1_assigned.GLY-ALA.no_0-NA.merged_poly.heatmap.in merged_poly.XRAY_only
	cat $< | sed 's/>-/>\t/' \
	| bawk '{print $$2, $$3, $$1, $$4, "0.0"}' >$@

%.count_matrix: %.count_matrix.in experimental_strct_in_mltalg.tree_leaves_comb.merged_poly
	cat $^2 | $(PRJ_ROOT)/local/bin/tabulate_diffs $< >$@

%.adj_count_matrix: %.count_matrix.in experimental_strct_in_mltalg.tree_leaves_adjoining.merged_poly
	cat $^2 | $(PRJ_ROOT)/local/bin/tabulate_diffs $< >$@ 
# the rotasequence identity cutoff (0.8) is calculated over the number of sites shared (i.e. non-gapped in both) by each pair of sequences not the whole length multialignment
%.adj_shared_count_matrix: %.count_matrix.in experimental_strct_in_mltalg.tree_leaves_adjoining.merged_poly
	cat $^2 | $(PRJ_ROOT)/local/bin/tabulate_diffs -R 0.75 -s $< >$@

%.adj_shared_count_matrix.log: %.count_matrix.in experimental_strct_in_mltalg.tree_leaves_adjoining.merged_poly
	 cat $^2 | $(PRJ_ROOT)/local/bin/tabulate_diffs -R 0.75 -s $< -l $@ >pippo ; rm pippo

#A PHYLIP alignment containing ALL rotasequences (1-character symbols) tabulated in *.adj_count_matrix
# this includes cases of multiple polipeptide sequences (e/a containining this family's domain) from the same uniprot id
# i.e O60684/113:154 and O60684/198:239
%.adj_count_matrix.all.PHYLIP: %.count_matrix.in
	$(PRJ_ROOT)/local/bin/tabulate_diffs -R 0.75 -w $< \
	| $(PRJ_ROOT)/local/src/PHYLIP_codon-2-1.py -C >$@

#A PHYLIP alignment containing UNIQUE rotasequences (unique sequence-wise) tabulated in *.adj_count_matrix
%.adj_count_matrix.uniq.PHYLIP: %.adj_count_matrix.all.PHYLIP
	cat $<| $(PRJ_ROOT)/local/src/PHYLIP_uniq.py >$@

# A PHYLIP alignment containing UNIQUE rotasequences and UNIQUE Uniprot id (no multiple domains)
# if duplicates selects the longest sequence
%.adj_count_matrix.uniq-Uniprot.PHYLIP: %.adj_count_matrix.all.PHYLIP
	cat $<| $(PRJ_ROOT)/local/src/PHYLIP_uniq.py -I >$@

# the masked alignment i.e. an AA sequence alignment
%.masked.PHYLIP: %.PHYLIP
	cat $< | $(PRJ_ROOT)/local/src/PHYLIP_codon-2-1.py -m >$@

%.B50.count_matrix: %.count_matrix.in experimental_strct_in_mltalg.tree_leaves_comb.merged_poly
	cat $^2 | $(PRJ_ROOT)/local/bin/tabulate_diffs -B 50 $< >$@

%.count_matrix.pdf: %.count_matrix.in experimental_strct_in_mltalg.tree_leaves_comb.merged_poly
	cat $^2 | $(PRJ_ROOT)/local/bin/tabulate_diffs -o $@ -r $<

%.adj_count_matrix.pdf:  %.count_matrix.in experimental_strct_in_mltalg.tree_leaves_adjoining.merged_poly
	cat $^2 | $(PRJ_ROOT)/local/bin/tabulate_diffs -o $@ -r $<

%.B50.count_matrix.pdf: %.count_matrix.in experimental_strct_in_mltalg.tree_leaves_comb.merged_poly
	cat $^2 | $(PRJ_ROOT)/local/bin/tabulate_diffs -B 50 -o $@ -r $<

CROP_B?=45
CROP_E?=100
POLY?=1UBD-0-C-<Polypeptide_start=325_end=377>-P25490_325:347
# create a stackbar of site conservation for a specific structure (poly)
single_struct.all_stack-barplot.rgb.pdf: experimental_strct.polypeptide.mapped.angle_matrix.chi1_assigned.no_0-NA.merged_poly.heatmap.in
	cat $< | filter_1col 1 <(bawk '$$1>=$(CROP_B) && $$1<=$(CROP_E) {print}' $< | grep '$(POLY)' | cut -f1) \
	| ../../local/src/vis_stack-bar.py -o $@  29 residue_config.alphabet.chi1.colors.rgb 

# the mafft reconstructed  PFAM tree, pruned so that it has only those leaves also present in *.count_matrix.in
%.count_matrix.in.tree.pdf: $(TREE) %.count_matrix.in experimental_strct.polypeptide_only.aligned.infoalign
	$(PRJ_ROOT)/local/src/vis_tree.py -o $@ -p $(shell cut -f12 experimental_strct.polypeptide_only.aligned.infoalign  | sort | uniq |  tr "\n" "," |  tr ":" "-")  $<

# the mafft reconstructed PFAM tree, pruned so that it has only those leaves also present in *.count_matrix.in, in newick format
%.tree.newick: $(TREE) %.PHYLIP
	$(PRJ_ROOT)/local/src/vis_tree.py -w -p $(shell cut -f1 $*.PHYLIP | unhead | tr "\n" "," | sed "s/,$$// ; s/^,*//g"  | tr ":" "-" )  $< >$@

# the original PFAM tree, pruned
%.ORIG_PFAM_TREE.newick: $(ORIG_TREE) %.PHYLIP
	$(PRJ_ROOT)/local/src/vis_tree.py -w -p $(shell cut -f1 $*.PHYLIP | unhead | tr "\n" "," | sed "s/,$$// ; s/^,*//g"  | tr ":" "-" )  $< >$@
