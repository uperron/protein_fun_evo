extern $(ORIG_FULL)/exp.PDBmap.clan_size
PARALLEL ?= 500
MATRIX_TYPE?=_adj_count_
SUPER_RHO?=0.848097
LGBYFREQ_SUPER_RHO?=0.330188

.PHONY: FILTERED_CLAN_DIRS FILTERED_CLAN_MATRICES FILTERED_CLAN_STATS FILTERED_ADJ_SHARED_CLAN_MATRICES FILTERED_CLAN_IIARY_STRUCT

FILTERED_CLAN_DIRS: $(ORIG_FULL)/exp.PDBmap.clan_size
	bawk '$$3>=$(MIN_PDB) && $$4>=$(MIN_UNIPROT) {print $$1, $$2}' $< \
	| while read i j ; do  mkdir -p $$i ; cd $$i ; \
		echo -e "PFAM=yes\nFULL=True\nRES_LIM=$(RES_LIM)\nFAMILY_ID=$$i\nORIG_PDB=$(ORIG_FULL)/$$i\nFAMILY_ID=$$i\nFAMILY_NAME=$$j\nORIG_ALIGN=$(ORIG_FULL)/$$i" > ../../../local/share/makefiles/makefile_Rotamers_evo.Pfam.$(DATE).$$i ; \
		ln -sf "../../../local/share/makefiles/makefile_Rotamers_evo.Pfam.$(DATE).$$i" "makefile" ; \
		ln -sf "../../../local/share/rules/rules.mk" "rules.mk" ; \
		cd .. ; \
	done
 
FILTERED_CLAN_MATRICES: $(ORIG_FULL)/exp.PDBmap.clan_size
	bawk '$$3>=$(MIN_PDB) && $$4>=$(MIN_UNIPROT) {print $$1}' $< | uniq | xargs -I %  --max-args=1 --max-procs=$(PARALLEL) sh -c 'cd % ; bsub -e err -o out -M 1024 -R "rusage[mem=1024]" bmake experimental_strct.polypeptide.mapped._empiricalangle_matrix.chi1_assigned.GLY-ALA.no_0-NA.merged_poly.count_matrix  experimental_strct.polypeptide.mapped.angle_matrix.chi1_assigned.GLY-ALA.no_0-NA.merged_poly.restype-only.count_matrix CLEAN_ORIG;'

FILTERED_ADJ_CLAN_MATRICES: $(ORIG_FULL)/exp.PDBmap.clan_size
	bawk '$$3>=$(MIN_PDB) && $$4>=$(MIN_UNIPROT) {print $$1}' $< | uniq | xargs -I %  --max-args=1 --max-procs=$(PARALLEL) sh -c 'cd % ; bsub -e err -o out -M 1024 -R "rusage[mem=1024]" bmake experimental_strct.polypeptide.mapped.angle_matrix.chi1_assigned.GLY-ALA.no_0-NA.merged_poly.adj_count_matrix  experimental_strct.polypeptide.mapped.angle_matrix.chi1_assigned.GLY-ALA.no_0-NA.merged_poly.restype-only.adj_count_matrix CLEAN_ORIG;'

FILTERED_ADJ_SHARED_CLAN_MATRICES: $(ORIG_FULL)/exp.PDBmap.clan_size
	bawk '$$3>=$(MIN_PDB) && $$4>=$(MIN_UNIPROT) {print $$1}' $< | uniq | xargs -I %  --max-args=1 sh -c 'cd % ; bsub -e err -o out -M 32000  bmake  experimental_strct.polypeptide.mapped.angle_matrix.chi1_assigned.GLY-ALA.no_0-NA.merged_poly.adj_shared_count_matrix experimental_strct.polypeptide.mapped.angle_matrix.chi1_assigned.GLY-ALA.no_0-NA.merged_poly.restype-only.adj_shared_count_matrix CLEAN_ORIG;'

FILTERED_CLAN_IIARY_STRUCT: $(ORIG_FULL)/exp.PDBmap.clan_size
	bawk '$$3>=$(MIN_PDB) && $$4>=$(MIN_UNIPROT) {print $$1}' $< | uniq | xargs -I %  --max-args=1 sh -c 'cd % ; bsub -e err -o out -M 1024 bmake experimental_strct.polypeptide.mapped.angle_matrix.chi1_assigned.GLY-ALA.no_0-NA.merged_poly.count_matrix.in.II_struct.noNA;'

FILTERED_ADJ_SHARED_CLAN_MATRICES_LOG: $(ORIG_FULL)/exp.PDBmap.clan_size
	bawk '$$3>=$(MIN_PDB) && $$4>=$(MIN_UNIPROT) {print $$1}' $< | uniq | xargs -I %  --max-args=1 sh -c 'cd % ; bsub -e err -o out -M 4000 bmake experimental_strct.polypeptide.mapped.angle_matrix.chi1_assigned.GLY-ALA.no_0-NA.merged_poly.adj_shared_count_matrix.log;'

count_matrix.log.merged:
	printf "Pfam_id\tPoly_id1\tPoly_id2\tuniprot_1\tuniprot_2\talign_site\tsym1\tsym2\n" >$@ ; 
	ls */experimental_strct.polypeptide.mapped.angle_matrix.chi1_assigned.GLY-ALA.no_0-NA.merged_poly.adj_shared_count_matrix.log \
	| xargs -I %  --max-args=1 sh -c 'printf ">%" | tr "/" "\t" | cut -f1 >>$@.tmp; cat % | unhead >>$@.tmp;' ; \
	cat $@.tmp | fasta2tab >>$@ ; rm $@.tmp

count_matrix.log.merged.clean: count_matrix.log.merged
	printf "Pfam_id\tPDB_id1\tPDB_id2\tuniprot_1\tuniprot_2\talign_site\tsym1\tsym2\n" >$@ ;
	cat $< | unhead | sed 's/-[^\t]*>//g ; s/\/[^\t]*//g' >>$@

IIary_struct_tab.merged: 
	printf "struct_id\talign_site\tIIary_struct\trota_state\tBfactor\n" >$@ ; 
	ls */experimental_strct.polypeptide.mapped.angle_matrix.chi1_assigned.GLY-ALA.no_0-NA.merged_poly.count_matrix.in.II_struct.noNA \
	|  xargs -I %  --max-args=1 sh -c 'cat % >>$@;'

Bfactor_tab.merged: 
	ls */experimental_strct.polypeptide.mapped.angle_matrix.chi1_assigned.GLY-ALA.no_0-NA.merged_poly.count_matrix.in \
	| xargs -I %  --max-args=1 sh -c 'cat % >>$@;'

Bfactor_tab.merged.pdf: Bfactor_tab.merged
	cut -f4,5 $< | ../../local/src/vis_Bfactor.py >$@

rotamers_count_matrix.merged:
	ls */experimental_strct.polypeptide.mapped.angle_matrix.chi1_assigned.GLY-ALA.no_0-NA.merged_poly.count_matrix \
	| tr ' ' '\t' | ../../local/bin/merge_matrix $(ROTA_STATES) >$@

restype_count_matrix.merged:
	ls */experimental_strct.polypeptide.mapped.angle_matrix.chi1_assigned.GLY-ALA.no_0-NA.merged_poly.restype-only.count_matrix \
	| tr ' ' '\t' | ../../local/bin/merge_matrix $(RESTYPE_STATES) >$@

rotamers%_count_matrix.merged.freq_norm: rotamers%_count_matrix.merged
	cut -f 2- $< | unhead | ../../local/bin/normalize_matrix.py $(ROTA_STATES) >$@

rotamers_adj_shared_count_matrix.merged:
	ls */experimental_strct.polypeptide.mapped.angle_matrix.chi1_assigned.GLY-ALA.no_0-NA.merged_poly.adj_shared_count_matrix \
	| tr ' ' '\t' | ../../local/bin/merge_matrix $(ROTA_STATES) >$@

restype_adj_shared_count_matrix.merged:
	ls */experimental_strct.polypeptide.mapped.angle_matrix.chi1_assigned.GLY-ALA.no_0-NA.merged_poly.restype-only.adj_shared_count_matrix \
	| tr ' ' '\t' | ../../local/bin/merge_matrix  $(RESTYPE_STATES) >$@

# each block in the rotamer matrix adds to 1, then each cell in the block is multiplied by the corresponding count in the restype matrix (i.e. ARG1-TYR2 / block_total * ARG-TYR)
rotamers_adj_count_matrix.merged.Dayhoff_norm: rotamers_adj_count_matrix.merged restype_adj_count_matrix.merged
	cut -f 2- $< | unhead | ../../local/bin/normalize_matrix.py -f $^2 $(ROTA_STATES) >$@

rotamers_adj_shared_count_matrix.merged.Dayhoff_norm: rotamers_adj_shared_count_matrix.merged restype_adj_shared_count_matrix.merged
	cut -f 2- $< | unhead | ../../local/bin/normalize_matrix.py -f $^2 $(ROTA_STATES) >$@

rotamers%_count_matrix.merged.freq_norm.pdf: rotamers%_count_matrix.merged
	cut -f 2- $< | unhead | ../../local/bin/normalize_matrix.py -r -o $@ $(ROTA_STATES)

rotamers_adj_count_matrix.merged.Dayhoff_norm.pdf: rotamers_adj_count_matrix.merged restype_adj_count_matrix.merged
	cut -f 2- $< | unhead | ../../local/bin/normalize_matrix.py -r -o $@ -f $^2 $(ROTA_STATES)

rotamers_count_matrix.merged.submatrix_norm: rotamers_count_matrix.merged
	cut -f 2- $< | unhead | ../../local/bin/normalize_matrix.py -s $(ROTA_STATES) >$@

# each cell is normalized by the sum of all counts for states i and j
rotamers_count_matrix.merged.submatrix_norm.pdf: rotamers_count_matrix.merged
	cut -f 2- $< | unhead | ../../local/bin/normalize_matrix.py -s -r -o $@ $(ROTA_STATES)

#collapse one axis of the count matrix
rotamers_count_matrix.merged.collapsed: rotamers_count_matrix.merged
	cut -f 2- $< | unhead | ../../local/bin/normalize_matrix.py -c $(ROTA_STATES) >$@

rotamers_count_matrix.merged.collapsed.pdf: rotamers_count_matrix.merged
	cut -f 2- $< | unhead | ../../local/bin/normalize_matrix.py -c -r -o $@ $(ROTA_STATES)

# collapse both axes of the normalized count matrix
rotamers_adj_count_matrix.merged.Dayhoff_norm.COLLAPSED: rotamers_adj_count_matrix.merged.Dayhoff_norm
	cut -f 2- $< | unhead | ../../local/bin/normalize_matrix.py -C $(ROTA_STATES) >$@

FILTERED_CLAN_STATS: 
	ls  */experimental_strct.polypeptide.mapped.angle_matrix.chi1_assigned.GLY-ALA.no_0-NA.merged_poly.adj_shared_count_matrix \
	| sed 's/\/.*$$//g' | xargs -I %  --max-args=1 sh -c 'cd % ; bsub -e err -o out remake experimental_strct_in_mltalg.dict;'

# labelled by Pfam ID
structures_stats: rotamers$(MATRIX_TYPE)matrix.merged FILTERED_CLAN_STATS
	ls */experimental_strct_in_mltalg.dict | xargs -I %  --max-args=1 --max-procs=1 sh -c 'echo ">%" | sed 's/.experimental_strct_in_mltalg.dict//g' >$@.tmp; cut -f 1,3 % >>$@.tmp; cat $@.tmp | fasta2tab >>$@; rm $@.tmp'

.META: structures_stats
	1	Pfam_family_id
	2	Uniprot_entry
	3	PDB_id

# all PFAM families in dataset sited by number of PDBx structures
all_FAMs_tab.tsv: structures_stats $(ORIG_FULL)/all_PFAM_clans.no-UPF
	cut -f1 $< | symbol_count | sort -nrk2,2 | translate -a -r <(cut -f1,5 $^2) 1 >$@



#-----------------------OBTAINING THE SCALED EXCHANGEABILITY MATRIX-----------------------------
#
# calculated using the adjoining-leaves algorithm to compare leaves in each family
rotamers_adj_count_matrix.merged:
	ls */experimental_strct.polypeptide.mapped.angle_matrix.chi1_assigned.GLY-ALA.no_0-NA.merged_poly.adj_count_matrix \
	| tr ' ' '\t' | ../../local/bin/merge_matrix $(ROTA_STATES) >$@

restype_adj_count_matrix.merged:
	ls */experimental_strct.polypeptide.mapped.angle_matrix.chi1_assigned.GLY-ALA.no_0-NA.merged_poly.restype-only.adj_count_matrix \
	| tr ' ' '\t' | ../../local/bin/merge_matrix $(RESTYPE_STATES)  >$@

# calculate the Instantaneous Rates Matrix
rotamers%.IRM: rotamers%
	cut -f 2- $< | unhead | ../../local/bin/normalize_matrix.py -I $(ROTA_STATES) >$@

restype%_count_matrix.merged.IRM: restype%_count_matrix.merged
	cut -f 2- $< | unhead | ../../local/bin/normalize_matrix.py -I $(RESTYPE_STATES)  >$@

# the collapsed IRM
rotamers_adj_count_matrix.merged.Dayhoff_norm.COLLAPSED.IRM: rotamers_adj_count_matrix.merged.Dayhoff_norm.COLLAPSED
	cut -f 2- $< | unhead | ../../local/bin/normalize_matrix.py -I $(RESTYPE_STATES)  >$@

rotamers%.IRM.pdf: rotamers%
	cut -f 2- $< | unhead | ../../local/bin/normalize_matrix.py  -I -r -o $@ $(ROTA_STATES)

# scale the IRM to obtain Q* with a mean substitution rate of 1
rotamers%.IRM.Q-star: rotamers%.IRM rotamers%
	cut -f 2- $< | unhead | ../../local/bin/normalize_matrix.py -a $^2 $(ROTA_STATES) >$@

restype%_count_matrix.merged.IRM.Q-star: restype%_count_matrix.merged.IRM restype%_count_matrix.merged
	cut -f 2- $< | unhead | ../../local/bin/normalize_matrix.py -a $^2 $(RESTYPE_STATES)  >$@

rotamers_adj_count_matrix.merged.Dayhoff_norm.COLLAPSED.IRM.Q-star: rotamers_adj_count_matrix.merged.Dayhoff_norm.COLLAPSED.IRM rotamers_adj_count_matrix.merged.Dayhoff_norm.COLLAPSED
	cut -f 2- $< | unhead | ../../local/bin/normalize_matrix.py -a $^2 $(RESTYPE_STATES)  >$@

# the proper Q* with rows that sum to 0
rotamers%.IRM.Q-star.neg_diag: rotamers%.IRM.Q-star 
	cut -f 2- $< | unhead | ../../local/bin/normalize_matrix.py -n $(ROTA_STATES) >$@

rotamers%.IRM.superQ-star.neg_diag: rotamers%.IRM.superQ-star
	 cut -f 2- $< | unhead | ../../local/bin/normalize_matrix.py -n $(ROTA_STATES) >$@

restype%_count_matrix.merged.IRM.Q-star.neg_diag: restype%_count_matrix.merged.IRM.Q-star
	cut -f 2- $< | unhead | ../../local/bin/normalize_matrix.py -n $(RESTYPE_STATES)  >$@

rotamers_adj_count_matrix.merged.Dayhoff_norm.COLLAPSED.IRM.Q-star.neg_diag: rotamers_adj_count_matrix.merged.Dayhoff_norm.COLLAPSED.IRM.Q-star
	cut -f 2- $< | unhead | ../../local/bin/normalize_matrix.py -n $(RESTYPE_STATES)  >$@

# the unscaled exchangeability matrix: Sij = Qij / Pj
rotamers_adj_count_matrix.merged.Dayhoff_norm.exchangeability: rotamers_adj_count_matrix.merged.Dayhoff_norm.IRM rotamers_adj_count_matrix.merged.Dayhoff_norm
	cut -f 2- $< | unhead | ../../local/bin/normalize_matrix.py -e $^2 $(ROTA_STATES) >$@

restype_adj_count_matrix.merged.exchangeability: restype_adj_count_matrix.merged.IRM restype_adj_count_matrix.merged 
	cut -f 2- $< | unhead | ../../local/bin/normalize_matrix.py -e $^2 $(RESTYPE_STATES) >$@

rotamers_adj_count_matrix.merged.Dayhoff_norm.COLLAPSED.exchangeability: rotamers_adj_count_matrix.merged.Dayhoff_norm.COLLAPSED.IRM rotamers_adj_count_matrix.merged.Dayhoff_norm.COLLAPSED
	cut -f 2- $< | unhead | ../../local/bin/normalize_matrix.py -e $^2 $(RESTYPE_STATES) >$@

# the scaled exchangeability matrix: 
rotamers_adj_count_matrix.merged.Dayhoff_norm.exchangeability.Q-star: rotamers_adj_count_matrix.merged.Dayhoff_norm.IRM.Q-star.neg_diag rotamers_adj_count_matrix.merged.Dayhoff_norm
	cut -f 2- $< | unhead | ../../local/bin/normalize_matrix.py -e $^2 $(ROTA_STATES) >$@

restype%matrix.merged.exchangeability.Q-star: restype%matrix.merged.IRM.Q-star.neg_diag restype%matrix.merged
	cut -f 2- $< | unhead | ../../local/bin/normalize_matrix.py -e $^2 $(RESTYPE_STATES) >$@

rotamers_adj_count_matrix.merged.Dayhoff_norm.COLLAPSED.exchangeability.Q-star: rotamers_adj_count_matrix.merged.Dayhoff_norm.COLLAPSED.IRM.Q-star.neg_diag rotamers_adj_count_matrix.merged.Dayhoff_norm.COLLAPSED
	cut -f 2- $< | unhead | ../../local/bin/normalize_matrix.py -e $^2 $(RESTYPE_STATES) >$@

#-----------------MATRIX ANALYSIS------------------------------------------------------------------------------------------------------------

# calculate the Chi Squared Test Pvalue for each counts submatrix (observed) against a uniform distribution 
rotamers%_count_matrix.merged.Dayhoff_norm.Csq_Pval: rotamers%_count_matrix.merged.Dayhoff_norm
	cut -f 2- $< | unhead | ../../local/src/matrix_stats.py -l $(ROTA_STATES) | bsort -nrk4,4 >$@

.META: *Csq_Pval
	1	label_1
	2	label_2
	3	Pval
	4	Chi_squared_stat
	5	Bonferroni_Pval

rotamers%_count_matrix.merged.Dayhoff_norm.Csq_Pval.pdf: rotamers%_count_matrix.merged.Dayhoff_norm
	cut -f 2- $< | unhead | ../../local/src/matrix_stats.py -r -o $@ $(ROTA_STATES)

rotamers%_count_matrix.merged.Dayhoff_norm.block_association: rotamers%_count_matrix.merged.Dayhoff_norm
	cut -f 2- $< | unhead | ../../local/src/matrix_stats.py -V $(ROTA_STATES) | bsort -nrk3,3 >$@

.META: *.block_association
	1	label_1
	2	label_2
	3	CramersV #https://en.wikipedia.org/wiki/Cram%C3%A9r's_V# (with bias correction)
	4	TschuprowsT #https://en.wikipedia.org/wiki/Tschuprow%27s_T (with bias correction)
	5	diagonal_ratio #i.e. sum of the element on the diagonal / sum total of the block

rotamers%_count_matrix.merged.Dayhoff_norm.block_association.pdf: rotamers%_count_matrix.merged.Dayhoff_norm
	cut -f 2- $< | unhead | ../../local/src/matrix_stats.py -V -s -o $@ $(ROTA_STATES)

rotamers%_count_matrix.merged.Dayhoff_norm.block_association.significance: rotamers%_count_matrix.merged.Dayhoff_norm.Csq_Pval rotamers%_count_matrix.merged.Dayhoff_norm.block_association
	bawk '{print $$label_1";"$$label_2, $$Chi_squared_stat, $$Bonferroni_Pval}' $< \
	| translate -a <(bawk '{print $$label_1";"$$label_2, $$CramersV, $$diagonal_ratio}' $^2) 1 \
	| tr ";" "\t" | bsort -nr -k3,3 >>$@

.META: *block_association.significance
	1	label_1
	2	label_2
	3	CramersV
	4	diagonal_ratio
	5	Chi_squared_stat
	6	Bonferroni_Pval

# random counts matrix (low counts)
rotamers%_rand-low_count_matrix.merged:
	ls */experimental_strct.polypeptide.mapped.angle_matrix.chi1_assigned.GLY-ALA.no_0-NA.merged_poly.adj_count_matrix \
	| tr ' ' '\t' | ../../local/bin/merge_matrix -R 20 $(ROTA_STATES) >$@

# random counts matrix (high counts)
rotamers_adj_rand-high_count_matrix.merged:
	ls */experimental_strct.polypeptide.mapped.angle_matrix.chi1_assigned.GLY-ALA.no_0-NA.merged_poly.adj_count_matrix \
	| tr ' ' '\t' | ../../local/bin/merge_matrix -R max $(ROTA_STATES) >$@

rotamers_adj_rand%matrix.merged.Csq_Pval: rotamers_adj_rand%matrix.merged
	cut -f 2- $< | unhead | ../../local/src/matrix_stats.py -l $(ROTA_STATES) | bsort -nrk4,4 >$@

rotamers_adj_rand%matrix.merged.Csq_Pval.pdf: rotamers_adj_rand%matrix.merged
	cut -f 2- $< | unhead | ../../local/src/matrix_stats.py -r -o $@ $(ROTA_STATES) | bsort -nrk4,4 >$@

rotamers_adj_rand%matrix.merged.block_association: rotamers_adj_rand%matrix.merged
	cut -f 2- $< | unhead | ../../local/src/matrix_stats.py -V $(ROTA_STATES) | bsort -nrk3,3 >$@

rotamers_adj_rand%matrix.merged.block_association.pdf: rotamers_adj_rand%matrix.merged
	cut -f 2- $< | unhead | ../../local/src/matrix_stats.py -V -s -o $@ $(ROTA_STATES)

# Plot Pii(t)
rotamers_adj_count_matrix.merged.IRM.Pii-t.pdf: rotamers_adj_count_matrix.merged.IRM
	cut -f2- $< | unhead | ../../local/src/Pt_matrix.py -o $@ $(ROTA_STATES)

restype_adj_count_matrix.merged.IRM.Pii-t.pdf: restype_adj_count_matrix.merged.IRM
	cut -f2- $< | unhead | ../../local/src/Pt_matrix.py -o $@ $(RESTYPE_STATES)

# --------------------------BENCHMARKING IMPLEMENTATION OF THE MODEL WITH DIFFERENT TOOLS---------------------------------------------------------------------
# using pre-existing tools: CodonPhyML, iqtree, PAML
# map rotamer_states into codons in order to use CodonPhyML with a custom IRM
.META: codons2rotamers.dict
	1	codon
	2	rotamer_state

# a custom lower triangular exchangeability matrix (i.e. PAML-style) using rotamer states rates in a 64x64 "codon" matrix, include state frequency at the bottom
rotamers%.exchangeability.PhyML: rotamers%.exchangeability rotamers%
	cut -f2- $< | unhead | ../../local/src/PhyML_matrix.py -F $^2 $(ROTA_STATES) $(ROTA_AS_CODON) | unhead >$@

#the scaled version
rotamers%.exchangeability.Q-star.PhyML: rotamers%.exchangeability.Q-star rotamers%
	cut -f2- $< | unhead | ../../local/src/PhyML_matrix.py -F $^2 $(ROTA_STATES) $(ROTA_AS_CODON) | unhead >$@

# a custom 61x61 symmetric, scaled exchangeability matrix for iqtree, codon order is alphabetical,  include state frequency at the bottom
rotamers%.exchangeability.Q-star.iqtree: rotamers%.exchangeability.Q-star rotamers%
	cut -f2- $< | unhead | ../../local/src/PhyML_matrix.py -s -F $^2 $(ROTA_STATES) $(IQ_ROTA_AS_CODON) >$@

# custom PAML-style lower triangular scaled exchangeability matrix, 20 AA are mapped to codons
restype%.exchangeability.Q-star.PhyML: restype%.exchangeability.Q-star restype%
	cut -f2- $< | unhead | ../../local/src/PhyML_matrix.py -F $^2 $(RESTYPE_STATES) $(RES_AS_CODON) | unhead >$@

# custom iqtree-style (61x61) symmetric (treated as a CODON matrix), scaled exchangeability matrix, 20 AA are mapped to codons
restype%.exchangeability.Q-star.iqtree: restype%.exchangeability.Q-star restype%
	cut -f2- $< | unhead | ../../local/src/PhyML_matrix.py -s -F $^2 $(RESTYPE_STATES) $(RES_AS_CODON) | unhead >$@


# custom iqtree-style (20x20) lower triangular scaled AA exchangeability matrix, 20 AA are in alphabetical order 
restype%.exchangeability.Q-star.20x20.iqtree: restype%.exchangeability.Q-star restype%
	cut -f2- $< | unhead | ../../local/src/PhyML_matrix.py -F $^2 $(RESTYPE_STATES) $(RESTYPE_STATES) | unhead >$@

# the rotamer states frequencies (with header)
rotamers%matrix.merged.Dayhoff_norm.frequencies: rotamers%matrix.merged.Dayhoff_norm
	cut -f2- $< | unhead | ../../local/src/PhyML_matrix.py -F $< -o $@ $(ROTA_STATES) $(ROTA_STATES)
# the residue frequencies (with header)
restype%matrix.merged.frequencies: restype%matrix.merged
	cut -f2- $< | unhead | ../../local/src/PhyML_matrix.py -F $< -o $@ $(RESTYPE_STATES) $(RESTYPE_STATES)
# the collapsed model frequencies
rotamers_adj_count_matrix.merged.Dayhoff_norm.COLLAPSED.frequencies: rotamers_adj_count_matrix.merged.Dayhoff_norm.COLLAPSED.exchangeability.Q-star rotamers_adj_count_matrix.merged.Dayhoff_norm.COLLAPSED
	cut -f2- $< | unhead | ../../local/src/PhyML_matrix.py -F $^2 -o $@ $(RESTYPE_STATES) $(RESTYPE_STATES)


# a PHYLIP alignment of "codons" mapped to rotamer_states for a given Pfam family
%.rotaseq_align.PHYLIP-PhyML: %/experimental_strct.polypeptide.mapped.angle_matrix.chi1_assigned.GLY-ALA.no_0-NA.merged_poly.count_matrix.in codons2rotamers.dict
	cut -f1 $< | sort | uniq | tr "=" "@" > $@.tmp1 ; \
	cat $< | tr "=" "@" | ../../local/src/PhyML_alignment.py $@.tmp1 $^2 | unhead >$@; \
	rm $@.tmp1

.PRECIOUS: *_codonphyml_tree.txt

# use codonPhyML to infer a phylogenetic tree from a codon alignment and a custom codon IRM
%.rotaseq_align.PHYLIP-PhyML_codonphyml_tree.txt: %.rotaseq_align.PHYLIP-PhyML rotamers_adj_count_matrix.merged.IRM.PhyML 
	cp $^2 usermatrix.ecm ; \
	codonphyml -i $< -d codon -q -m GY -f e --qrates ECMUSR; \
	rm usermatrix.ecm

# use codonphyml to infer a phylogenetic tree from a codon alignment using the Dayhoff-normalized IRM
%.rotaseq_align.phylip-phyml_Dayhoff_norm_codonphyml_tree.txt: %.rotaseq_align.phylip-phyml rotamers_adj_count_matrix.merged.Dayhoff_norm.IRM.PhyML 
	cp $^2 usermatrix.ecm ; cp $< $<_Dayhoff_norm ; \
	codonphyml -i $<_Dayhoff_norm -d codon -q -m GY -f e --qrates ECMUSR; \
	rm usermatrix.ecm $<_Dayhoff_norm


# use codonphyml to infer a phylogenetic tree from a codon alignment and a random codon irm
%.rotaseq_align.phylip-phyml_rand_codonphyml_tree.txt: %.rotaseq_align.phylip-phyml rotamers_adj_rand-high_count_matrix.merged.IRM.phyml
	cp $^2 usermatrix.ecm ; cp $< $<_RAND ; \
	codonphyml -i $<_RAND -d codon -q -m GY -f e --qrates ECMUSR; \
	rm usermatrix.ecm $<_RAND

# use codonPhyML to infer a phylogenetic tree from a AA alignment (restype) using WAG
%.restype_align.PHYLIP-PhyML_WAG_codonphyml_tree.txt: %.restype_align.PHYLIP-PhyML
	codonphyml -i $< -d aa -q -m WAG -f e

# represent codonPhyML trees
%_codonphyml_tree.pdf: %_codonphyml_tree.txt
	../../local/src/vis_tree.py -o $@ $<

# compare codonPhyML tree and a random tree using unweighted Robinson-Foulds distance
%.rotaseq_align.PHYLIP-PhyML_codonphyml_tree.unweighted_RF: %.rotaseq_align.PHYLIP-PhyML_codonphyml_tree.txt %.rotaseq_align.PHYLIP-PhyML_RAND_codonphyml_tree.txt
	../../local/src/compare_tree.py $< $^2 >$@

# simulate sequence evolution using P(t)
sequence_evo_simulation.rotamer-restype.pdf:  rotamers_adj_count_matrix.merged.Dayhoff_norm.IRM restype_adj_count_matrix.merged.IRM codons2rotamers.dict
	cut -f2- $< | unhead | ../../local/src/seq_evo_simulation.py $(ROTA_STATES) $^3 -c $^2 -o $@ 

sequence_evo_simulation.rotamer-restype.Q-star.pdf:  rotamers_adj_count_matrix.merged.Dayhoff_norm.IRM.Q-star restype_adj_count_matrix.merged.IRM.Q-star codons2rotamers.dict
	cut -f2- $< | unhead | ../../local/src/seq_evo_simulation.py $(ROTA_STATES) $^3 -c $^2 -o $@ 

# the simulated sequences for ML estimation, both AA sequences and rotasequences are translated into codons in order for codonPhyML to work
sequence_evo_simulation.rotamer-restype.sequences: rotamers_adj_count_matrix.merged.Dayhoff_norm.IRM.Q-star restype_adj_count_matrix.merged.IRM.Q-star codons2rotamers.dict codons2restype.dict 
	cut -f2- $< | unhead | ../../local/src/seq_evo_simulation.py $(ROTA_STATES) $^3 -u $(RANDOM_SEQ100) -c $^2 -s -d $^4  | unhead | untail >$@ 

# a 500-symbol sequence
sequence500_evo_simulation.rotamer-restype.sequences: rotamers_adj_count_matrix.merged.Dayhoff_norm.IRM.Q-star restype_adj_count_matrix.merged.IRM.Q-star codons2rotamers.dict codons2restype.dict 
	cut -f2- $< | unhead | ../../local/src/seq_evo_simulation.py $(ROTA_STATES) $^3 -u $(RANDOM_SEQ500) -c $^2 -s -d $^4  | unhead >$@ 

# sequences simulated using only the 20x20 model, sequences are NOT translated into codons (1-letter AA)
sequence_evo_simulation.restype-only.sequences: sequence_evo_simulation.rotamer-restype.sequences
	 bawk '$$n_states=="20" {print $$PHYLIP_seq_ID, $$PHYLIP_seq}' $< | ../../local/src/PHYLIP_codon-2-1.py >$@

.META: *sequences
	1	n_states
	2	PHYLIP_seq_ID
	3	PHYLIP_seq

sequence_evo_simulation.restype-only.sequences.3-letter: sequence_evo_simulation.rotamer-restype.sequences
	bawk '$$n_states=="20" {print $$PHYLIP_seq_ID, $$PHYLIP_seq}' $< | ../../local/src/PHYLIP_codon-2-1.py -t >$@

sequence_evo_simulation.rotamer-only.sequences.rotamer_state: sequence_evo_simulation.rotamer-restype.sequences
	bawk '$$n_states=="55" {print $$PHYLIP_seq_ID, $$PHYLIP_seq}' $< | ../../local/src/PHYLIP_codon-2-1.py -r >$@

# the cooordinates for plotting (t, distance from original sequence)
sequence_evo_simulation.rotamer-restype.scatterplot.in: rotamers_adj_count_matrix.merged.Dayhoff_norm.IRM.Q-star restype_adj_count_matrix.merged.IRM.Q-star codons2rotamers.dict
	cut -f2- $< | unhead | ../../local/src/seq_evo_simulation.py $(ROTA_STATES) $^3 -n -u $(RANDOM_SEQ100) -c $^2 | bsort -k2,2 >$@


# the cooordinates for plotting (t, distance from original sequence)
sequence500_evo_simulation.rotamer-restype.scatterplot.in: rotamers_adj_count_matrix.merged.Dayhoff_norm.IRM.Q-star restype_adj_count_matrix.merged.IRM.Q-star codons2rotamers.dict
	cut -f2- $< | unhead | ../../local/src/seq_evo_simulation.py $(ROTA_STATES) $^3 -n -u $(RANDOM_SEQ500) -c $^2 | bsort -k2,2 >$@


NUMALIGN=150
# use codonphyml to infer a phylogenetic tree from a codon alignment of simulated sequences and a "hacked" 64x64 sparsely filled with our 55x55 scaled exchangeability matrix
sequence_evo_simulation.rotamer.sequences.Dayhoff_norm.ML_est_codonphyml_tree.txt: sequence_evo_simulation.rotamer-restype.sequences rotamers_adj_count_matrix.merged.Dayhoff_norm.exchangeability.Q-star.PhyML
	cp $^2 usermatrix.ecm ; bawk '$$n_states=="55" {print $$PHYLIP_seq_ID, $$PHYLIP_seq}' $<  > sequence_evo_simulation.rotamer.sequences.Dayhoff_norm.ML_est ; \
	codonphyml -i sequence_evo_simulation.rotamer.sequences.Dayhoff_norm.ML_est -n $(NUMALIGN) -d codon -m GY  -f e --qrates ECMUSR; \
	rm usermatrix.ecm sequence_evo_simulation.rotamer.sequences.Dayhoff_norm.ML_est

# use codonphyml to infer a phylogenetic tree from a AA alignment of simulated sequences and a "hacked" 64x64 matrix sparsely filled with our 20x20 scaled exchangeability matrix
sequence_evo_simulation.restype.sequences.Dayhoff_norm.ML_est_codonphyml_tree.txt: sequence_evo_simulation.rotamer-restype.sequences restype_adj_count_matrix.merged.exchangeability.Q-star.PhyML 
	cp $^2 usermatrix.ecm ; bawk '$$n_states=="20" {print $$PHYLIP_seq_ID, $$PHYLIP_seq}' $<  > sequence_evo_simulation.restype.sequences.Dayhoff_norm.ML_est ; \
	codonphyml -i sequence_evo_simulation.restype.sequences.Dayhoff_norm.ML_est -n $(NUMALIGN) -d codon -m GY  -f e --qrates ECMUSR; \
	rm usermatrix.ecm sequence_evo_simulation.restype.sequences.Dayhoff_norm.ML_est

# longer sequence
sequence500_evo_simulation.rotamer.sequences.Dayhoff_norm.ML_est_codonphyml_tree.txt: sequence500_evo_simulation.rotamer-restype.sequences rotamers_adj_count_matrix.merged.Dayhoff_norm.exchangeability.Q-star.PhyML
	cp $^2 usermatrix.ecm ; bawk '$$n_states=="55" {print $$PHYLIP_seq_ID, $$PHYLIP_seq}' $<  > sequence500_evo_simulation.rotamer.sequences.Dayhoff_norm.ML_est ; \
	codonphyml -i sequence500_evo_simulation.rotamer.sequences.Dayhoff_norm.ML_est -n $(NUMALIGN) -d codon -m GY  -f e --qrates ECMUSR; \
	rm usermatrix.ecm sequence500_evo_simulation.rotamer.sequences.Dayhoff_norm.ML_est

# longer sequence
sequence500_evo_simulation.restype.sequences.Dayhoff_norm.ML_est_codonphyml_tree.txt: sequence500_evo_simulation.rotamer-restype.sequences restype_adj_count_matrix.merged.exchangeability.Q-star.PhyML 
	cp $^2 usermatrix.ecm ; bawk '$$n_states=="20" {print $$PHYLIP_seq_ID, $$PHYLIP_seq}' $<  > sequence500_evo_simulation.restype.sequences.Dayhoff_norm.ML_est ; \
	codonphyml -i sequence500_evo_simulation.restype.sequences.Dayhoff_norm.ML_est -n $(NUMALIGN) -d codon -m GY  -f e --qrates ECMUSR; \
	rm usermatrix.ecm sequence500_evo_simulation.restype.sequences.Dayhoff_norm.ML_est


# extract time, distances to plot
%.ML_est_codonphyml_tree.distances: %.ML_est_codonphyml_tree.txt
	cat $< | sed 's/\(^(\|);$$\)//g' | sed 's/\(:\|,\)/\t/g' \
	| bawk '{print $$1, $$3, $$4}' | tr "-" "\t" | cut -f 3,5 >$@

# plot the simulated and the ML-estimated distances
sequence_evo_simulation.rotamer-restype.Dayhoff_norm.ML_est_codonphyml.distance_plot.pdf: sequence_evo_simulation.rotamer-restype.scatterplot.in sequence_evo_simulation.rotamer.sequences.Dayhoff_norm.ML_est_codonphyml_tree.distances sequence_evo_simulation.restype.sequences.Dayhoff_norm.ML_est_codonphyml_tree.distances 
	../../local/src/vis_ML-sim.py $^ 55,20 -n 5 -o $@

sequence500_evo_simulation.rotamer-restype.Dayhoff_norm.ML_est_codonphyml.distance_plot.pdf: sequence500_evo_simulation.rotamer-restype.scatterplot.in sequence500_evo_simulation.rotamer.sequences.Dayhoff_norm.ML_est_codonphyml_tree.distances sequence500_evo_simulation.restype.sequences.Dayhoff_norm.ML_est_codonphyml_tree.distances 
	../../local/src/vis_ML-sim.py $^ 55,20 -n 5 -o $@

# use the iqtree wrapper to estimate the distances b/w rotasequences, a "hacked" 64x64 matrix is sparsely filled with our 55x55 scaled exchangeability matrix
sequence_evo_simulation.rotamer.sequences.Dayhoff_norm.ML_est_iqtree_tree: sequence_evo_simulation.rotamer-restype.sequences rotamers_adj_count_matrix.merged.Dayhoff_norm.exchangeability.Q-star.iqtree
	cp $^2 usermatrix.ecm ; bawk '$$n_states=="55" {print $$PHYLIP_seq_ID, $$PHYLIP_seq}' $< | sed 's/^\t$$//g'  > sequence_evo_simulation.rotamer.sequences_Dayhoff_norm.ML_est ; \
	../../local/src/iqtree_wrapper.py usermatrix.ecm sequence_evo_simulation.rotamer.sequences_Dayhoff_norm.ML_est >$@
	rm usermatrix.ecm sequence_evo_simulation.rotamer.sequences_Dayhoff_norm.ML_est

# estimate distances b/w AA sequence, here a "hacked" 64x64 matrix is sparsely filled with our 20x20 scaled exchangeability matrix.
sequence_evo_simulation.restype.sequences.Dayhoff_norm.sparse.ML_est_iqtree_tree: sequence_evo_simulation.rotamer-restype.sequences restype_adj_count_matrix.merged.exchangeability.Q-star.iqtree
	cp $^2 usermatrix.ecm ; bawk '$$n_states=="20" {print $$PHYLIP_seq_ID, $$PHYLIP_seq}' $< | sed 's/^\t$$//g'  > sequence_evo_simulation.restype.sequences_Dayhoff_norm.sparse.ML_est ; \
	../../local/src/iqtree_wrapper.py usermatrix.ecm -d AA sequence_evo_simulation.restype.sequences_Dayhoff_norm.sparse.ML_est >$@
	rm usermatrix.ecm sequence_evo_simulation.restype.sequences_Dayhoff_norm.sparse.ML_est

# use the iqtree wrapper to estimate the distances b/w AA sequence, here a "proper" non-sparse 20x20 matrix is used as input.
sequence_evo_simulation.restype.sequences.Dayhoff_norm.ML_est_iqtree_tree: sequence_evo_simulation.rotamer-restype.sequences restype_adj_count_matrix.merged.exchangeability.Q-star.20x20.iqtree
	cp $^2 usermatrix.ecm ; bawk '$$n_states=="20" {print $$PHYLIP_seq_ID, $$PHYLIP_seq}' $< | sed 's/^\t$$//g'  > sequence_evo_simulation.restype.sequences_Dayhoff_norm.ML_est ; \
	../../local/src/iqtree_wrapper.py usermatrix.ecm -d AA sequence_evo_simulation.restype.sequences_Dayhoff_norm.ML_est >$@
	rm usermatrix.ecm sequence_evo_simulation.restype.sequences_Dayhoff_norm.ML_est

# iqtree with longer rotasequence
sequence500_evo_simulation.rotamer.sequences.Dayhoff_norm.ML_est_iqtree_tree: sequence500_evo_simulation.rotamer-restype.sequences rotamers_adj_count_matrix.merged.Dayhoff_norm.exchangeability.Q-star.iqtree
	cp $^2 usermatrix.ecm ; bawk '$$n_states=="55" {print $$PHYLIP_seq_ID, $$PHYLIP_seq}' $< | sed 's/^\t$$//g'  > sequence_evo_simulation.rotamer.sequences_Dayhoff_norm.ML_est ; \
	../../local/src/iqtree_wrapper.py usermatrix.ecm sequence_evo_simulation.rotamer.sequences_Dayhoff_norm.ML_est >$@
	rm usermatrix.ecm sequence_evo_simulation.rotamer.sequences_Dayhoff_norm.ML_est

# iqtree with longer AA sequence, using the "proper" non-sparse 20x20 matrix
sequence500_evo_simulation.restype.sequences.Dayhoff_norm.ML_est_iqtree_tree: sequence500_evo_simulation.rotamer-restype.sequences restype_adj_count_matrix.merged.exchangeability.Q-star.20x20.iqtree
	cp $^2 usermatrix.ecm ; bawk '$$n_states=="20" {print $$PHYLIP_seq_ID, $$PHYLIP_seq}' $< | sed 's/^\t$$//g' | unhead  > sequence_evo_simulation.restype.sequences_Dayhoff_norm.ML_est ; \
	../../local/src/iqtree_wrapper.py usermatrix.ecm -d AA sequence_evo_simulation.restype.sequences_Dayhoff_norm.ML_est >$@
	rm usermatrix.ecm sequence_evo_simulation.restype.sequences_Dayhoff_norm.ML_est

# use ML extimation (nml) to infer distance b/w two sequences;
# the log likelihood formula is n. 2.4 in chapter 2.3.2 of Yang, Ziheng. Molecular Evolution: A Statistical Approach (p. 41). OUP Oxford. Kindle Edition. 4
sequence_evo_simulation.restype.sequences.ML_est_nlm.distances: sequence_evo_simulation.restype-only.sequences.3-letter restype_adj_count_matrix.merged.IRM.Q-star.neg_diag restype_adj_count_matrix.merged.exchangeability.Q-star.20x20.iqtree
	egrep -v '(^3|^New|^Orig_seq_1)' $< | sed '/^\s*$$/d' | uniq >$@.tmp_orig ; \
	head -n1 $^2 | cut -f2- >  $@.tmp_freq ; tail -n1 $^3 >> $@.tmp_freq ; touch $@.tmp ; \
	egrep -v '(^3|^Orig_seq)' $< | sed '/^\s*$$/d' | enumerate_rows \
	| repeat_group_pipe -s '\
		cp $@.tmp_orig $@.tmp_align ; cut -f2,3 >> $@.tmp_align ; \
		../../local/src/ML_d_est.py $^2 $@.tmp_freq $@.tmp_align $@.tmp ; \	
	' 1 ; cut -f1,2 $@.tmp >$@; rm $@.tmp*

sequence_evo_simulation.rotamer.sequences.ML_est_nlm.distances: sequence_evo_simulation.rotamer-only.sequences.rotamer_state rotamers_adj_count_matrix.merged.Dayhoff_norm.IRM.Q-star.neg_diag rotamers$(MATRIX_TYPE)matrix.merged.Dayhoff_norm.frequencies
	egrep -v '(^3|^New|^Orig_seq_1)' $< | sed '/^\s*$$/d' | uniq >$@.tmp_orig ; \
	head -n1 $^2 | cut -f2- >  $@.tmp_freq ; tail -n1 $^3 >> $@.tmp_freq ; touch $@.tmp ; \
	egrep -v '(^3|^Orig_seq)' $< | sed '/^\s*$$/d' | enumerate_rows \
	| repeat_group_pipe -s '\
		cp $@.tmp_orig $@.tmp_align ; cut -f2,3 >> $@.tmp_align ; \
		../../local/src/ML_d_est.py $^2 $@.tmp_freq $@.tmp_align $@.tmp ; \	
	' 1 ; cut -f1,2 $@.tmp >$@; rm $@.tmp*

# extract time, distances to plot
%.ML_est_iqtree_tree.distances: %.ML_est_iqtree_tree
	grep -v '^$$' $< | sed 's/\(^(\|);$$\)//g' | sed 's/\(:\|,\)/\t/g' \
	| bawk '{print $$1, $$3, $$4}' | tr "-" "\t" | cut -f 3,5 >$@

# plot the simulated and estimated distances
sequence_evo_simulation.rotamer-restype.Dayhoff_norm.ML_est_iqtree.distance_plot.pdf: sequence_evo_simulation.rotamer-restype.scatterplot.in sequence_evo_simulation.rotamer.sequences.Dayhoff_norm.ML_est_iqtree_tree.distances sequence_evo_simulation.restype.sequences.Dayhoff_norm.ML_est_iqtree_tree.distances
	../../local/src/vis_ML-sim.py $^ 55,20 -n 5 -o $@

sequence500_evo_simulation.rotamer-restype.Dayhoff_norm.ML_est_iqtree.distance_plot.pdf: sequence500_evo_simulation.rotamer-restype.scatterplot.in sequence500_evo_simulation.rotamer.sequences.Dayhoff_norm.ML_est_iqtree_tree.distances sequence500_evo_simulation.restype.sequences.Dayhoff_norm.ML_est_iqtree_tree.distances
	../../local/src/vis_ML-sim.py $^ 55,20 -n 5 -o $@

# plot the simulated and estimated (with nlm) distances
sequence_evo_simulation.rotamer-restype.sequences.ML_est_nlm.distance_plot.pdf: sequence_evo_simulation.rotamer-restype.scatterplot.in sequence_evo_simulation.rotamer.sequences.ML_est_nlm.distances sequence_evo_simulation.restype.sequences.ML_est_nlm.distances
	../../local/src/vis_ML-sim.py $^ 55,20 -n 5 -o $@

# compare the estimated distances across iqtree and codonPhyML for the 20x20 model
sequence_evo_simulation.rotamer-restype.Dayhoff_norm.ML_est_iqtree_v_codonPhyML_res.distance_plot.pdf: sequence_evo_simulation.restype.sequences.Dayhoff_norm.ML_est_iqtree_tree.distances sequence_evo_simulation.restype.sequences.Dayhoff_norm.ML_est_codonphyml_tree.distances
	../../local/src/vis_ML-ML.py $^ iqtree_20x20,codonPhyML20x20 -n 5 -o $@

side-chain_conformational_entropy.table: ../../local/share/data/Doig_1993-side-chain_conformational_entropy.table
	unhead -n 5 $< | cut -f 1,2,3 >$@

# 10 pairwise alignments, each of length 10000 AA.
# Each alignment represents 2 sequences evolving under the 20x20 model, separated by divergences of 0.01, 0.025, 0.05, 0.10, 0.25, 0.50, 1.00, 2.50, 5.00 and 10.00 expected replacements per site, respectively.
# add a third sequence (duplicated) to each alignment to satisfy iqtree
PAML_sequence10k_evo_simulation.restype.sequences: ../../local/share/data/PAML_20x20_runs/all.dat
	cat $< | tr "2" "3"  | unhead -n 2 | untail | untail \
	| sed 's/ \+ /\t/g' | sed 's/ //g' | sed 's/^\t//g' | sed s/\r\n/\n/g |  awk '$$1=="Human" {print $$0"\nduplicate\t"$$2;next}{print;}' >$@

# the distances estimated using PAML
PAML_sequence10k_evo_simulation.restype.distances: ../../local/share/data/PAML_20x20_runs/lens.out
	printf "0.01\n0.025\n0.05\n0.10\n0.25\n0.50\n1.00\n2.50\n5.00\n10.00" >$@.tmp_t ; \
	awk '{print $$4}' $< | sed 's/;//g' >$@.tmp_d ; \
	paste $@.tmp_t $@.tmp_d >$@ ; rm $@.tmp*

# the likelihood obatined using PAML
PAML_sequence10k_evo_simulation.restype.LL: ../../local/share/data/PAML_20x20_runs/lens.out
	printf "0.01\n0.025\n0.05\n0.10\n0.25\n0.50\n1.00\n2.50\n5.00\n10.00" >$@.tmp_t ; \
	awk '{print $$9}' $< | sed 's/;//g' >$@.tmp_d ; \
	paste $@.tmp_t $@.tmp_d >$@ ; rm $@.tmp*

# estimate PAML sequence distance using iqtree and the "proper" non-sparse 20x20 matrix. Each alignment represents 2 sequences evolving under 20x20 model, separated by divergences of 0.01, 0.025, 0.05, 0.10, 0.25, 0.50, 1.00, 3.50, 5.00 and 10.00 expected replacements per site, respectively.
PAML_sequence10k_evo_simulation.restype.sequences.ML_est_iqtree_tree: PAML_sequence10k_evo_simulation.restype.sequences restype_adj_count_matrix.merged.exchangeability.Q-star.20x20.iqtree
	cp $^2 usermatrix.ecm ; cat $< > PAML_sequence10k_evo_simulation.restype.sequences.ML_est ; \
	../../local/src/iqtree_wrapper.py -P usermatrix.ecm -d AA PAML_sequence10k_evo_simulation.restype.sequences.ML_est >$@
	rm usermatrix.ecm PAML_sequence10k_evo_simulation.restype.sequences.ML_est

# extract time, distances to plot
PAML_sequence10k%.ML_est_iqtree_tree.distances: PAML_sequence10k%.ML_est_iqtree_tree
	printf "0.01\n0.025\n0.05\n0.10\n0.25\n0.50\n1.00\n2.50\n5.00\n10.00" >$@.tmp1 ;\
	grep -v '^$$' $< | sed 's/\(^(\|);$$\)//g' | sed 's/\(:\|,\)/\t/g' | cut -f6 >$@.tmp2 ;\
	paste $@.tmp1 $@.tmp2 >$@ ; rm $@.tmp1 $@.tmp2 

# extract the likelihoods
PAML_sequence10k%.ML_est_iqtree_tree.LL: PAML_sequence10k_evo_simulation.restype.sequences restype_adj_count_matrix.merged.exchangeability.Q-star.20x20.iqtree
	printf "0.01\n0.025\n0.05\n0.10\n0.25\n0.50\n1.00\n2.50\n5.00\n10.00" >$@.tmp1 ;\
	cp $^2 usermatrix.ecm ; cat $< > PAML_sequence10k_evo_simulation.restype.sequences.ML_est ; \
	../../local/src/iqtree_wrapper.py -P -d AA -o usermatrix.ecm PAML_sequence10k_evo_simulation.restype.sequences.ML_est \
	| grep 'BEST SCORE FOUND :' | awk '{print $$5}' >$@.tmp2 ;\
	paste $@.tmp1 $@.tmp2 >$@ ; rm $@.tmp* usermatrix.ecm PAML_sequence10k_evo_simulation.restype.sequences.ML_est

# from 1-letter AA to codons for codonPhyML
# estimate PAML sequence distance using codonPhyML and the sparse 64x64 matrix. Use the frequecies provided w/t the matrix (no -F e)
PAML_sequence10k_evo_simulation.restype.sequences.ML_est_codonphyml_tree.txt: PAML_sequence10k_evo_simulation.restype.sequences restype_adj_count_matrix.merged.exchangeability.Q-star.PhyML 
	cp $^2 usermatrix.ecm ; cat $< | ../../local/src/PHYLIP_codon-2-1.py -i > PAML_sequence10k_evo_simulation.restype.sequences.ML_est ; \
	codonphyml -i PAML_sequence10k_evo_simulation.restype.sequences.ML_est -n 10 -d codon -m GY --qrates ECMUSR; \
	rm usermatrix.ecm PAML_sequence10k_evo_simulation.restype.sequences.ML_est

# extract time, distances to plot
PAML_sequence10k_evo_simulation.restype.sequences.ML_est_codonphyml_tree.distances: PAML_sequence10k_evo_simulation.restype.sequences.ML_est_codonphyml_tree.txt
	printf "0.01\n0.025\n0.05\n0.10\n0.25\n0.50\n1.00\n2.50\n5.00\n10.00" >$@.tmp1 ;\
	grep -v '^$$' $< | sed 's/\(^(\|);$$\)//g' | sed 's/\(:\|,\)/\t/g' | cut -f6 >$@.tmp2 ;\
	paste $@.tmp1 $@.tmp2 >$@ ; rm $@.tmp1 $@.tmp2

# extract the likelihoods
PAML_sequence10k_evo_simulation.restype.sequences.ML_est_codonphyml_tree.LL: PAML_sequence10k_evo_simulation.restype.sequences.ML_est_codonphyml_tree.txt
	printf "0.01\n0.025\n0.05\n0.10\n0.25\n0.50\n1.00\n2.50\n5.00\n10.00" >$@.tmp1 ;\
	grep -e '\. Log-likelihood:' PAML_sequence10k_evo_simulation.restype.sequences.ML_est_codonphyml_stats.txt | awk '{print $$3}' >$@.tmp2 ;\
	paste $@.tmp1 $@.tmp2 >$@ ; rm $@.tmp*


# use ML extimation (nml) to infer distance b/w two sequences;
# 1-letter AA are converted to 3-letter AA
PAML_sequence10k_evo_simulation.restype.sequences.ML_est_nlm.distances: PAML_sequence10k_evo_simulation.restype.sequences restype_adj_count_matrix.merged.IRM.Q-star.neg_diag restype_adj_count_matrix.merged.exchangeability.Q-star.20x20.iqtree
	printf "0.01\n0.025\n0.05\n0.10\n0.25\n0.50\n1.00\n2.50\n5.00\n10.00" >$@.tmp_t ; \
	cat $< | ../../local/src/PHYLIP_codon-2-1.py -o | egrep -v '(^3|^Chimp|^duplicate)' | sed '/^\s*$$/d' | uniq | enumerate_rows >$@.tmp_orig ; \
	head -n1 $^2 | cut -f2- >  $@.tmp_freq ; tail -n1 $^3 >> $@.tmp_freq ; touch $@.tmp_dist ; \
	cat $< | ../../local/src/PHYLIP_codon-2-1.py -o | egrep -v '(^3|^Human|^duplicate)' | sed '/^\s*$$/d' | enumerate_rows >$@.tmp_new ; \
	awk '{print; getline < "$@.tmp_new"; print}' $@.tmp_orig \
	| repeat_group_pipe -s '\
		cut -f 2,3 > $@.tmp_align ; \
		../../local/src/ML_d_est.py $^2 $@.tmp_freq $@.tmp_align >>$@.tmp_dist ; \
	' 1 ; paste $@.tmp_t $@.tmp_dist | cut -f 1,3 >$@ ; rm $@.tmp*

# extract the likelihoods
PAML_sequence10k_evo_simulation.restype.sequences.ML_est_nlm.LL: PAML_sequence10k_evo_simulation.restype.sequences restype_adj_count_matrix.merged.IRM.Q-star.neg_diag restype_adj_count_matrix.merged.exchangeability.Q-star.20x20.iqtree
	printf "0.01\n0.025\n0.05\n0.10\n0.25\n0.50\n1.00\n2.50\n5.00\n10.00" >$@.tmp_t ; \
	cat $< | ../../local/src/PHYLIP_codon-2-1.py -o | egrep -v '(^3|^Chimp|^duplicate)' | sed '/^\s*$$/d' | uniq | enumerate_rows >$@.tmp_orig ; \
	head -n1 $^2 | cut -f2- >  $@.tmp_freq ; tail -n1 $^3 >> $@.tmp_freq ; touch $@.tmp_dist ; \
	cat $< | ../../local/src/PHYLIP_codon-2-1.py -o | egrep -v '(^3|^Human|^duplicate)' | sed '/^\s*$$/d' | enumerate_rows >$@.tmp_new ; \
	awk '{print; getline < "$@.tmp_new"; print}' $@.tmp_orig \
	| repeat_group_pipe -s '\
		cut -f 2,3 > $@.tmp_align ; \
		../../local/src/ML_d_est.py $^2 $@.tmp_freq $@.tmp_align >>$@.tmp_dist ; \
	' 1 ; paste $@.tmp_t $@.tmp_dist | cut -f 1,4 >$@ ; rm $@.tmp*

# compare the ML-estimated distances b/w the PAML sequences across codonphyml, iqtree and nlm
PAML_sequence10k_evo_simulation.restype.sequences.ML_est_compare.distances.pdf: PAML_sequence10k_evo_simulation.restype.sequences.ML_est_codonphyml_tree.distances PAML_sequence10k_evo_simulation.restype.sequences.ML_est_iqtree_tree.distances PAML_sequence10k_evo_simulation.restype.sequences.ML_est_nlm.distances PAML_sequence10k_evo_simulation.restype.distances
	../../local/src/vis_ML-compare.py $<,$^2,$^3,$^4 20x20_codonPhyML,20x20_iqtree,20x20_nlm,20x20_PAML -o $@

# tabulate the maximum LLs
PAML_sequence10k_evo_simulation.restype.sequences.ML_est.LL_table: PAML_sequence10k_evo_simulation.restype.LL PAML_sequence10k_evo_simulation.restype.sequences.ML_est_codonphyml_tree.LL PAML_sequence10k_evo_simulation.restype.sequences.ML_est_iqtree_tree.LL PAML_sequence10k_evo_simulation.restype.sequences.ML_est_nlm.LL
	printf "t\tPAML\tcodonPhyML\tiqtree\tnlm\n" >$@ ; \
	paste <(cat $<) <(cut -f2 $^2) <(cut -f2 $^3) <(cut -f2 $^4) >>$@

# tabulate the ML-estimated distances b/w the PAML sequences
PAML_sequence10k_evo_simulation.restype.sequences.ML_est_compare.distances.table: PAML_sequence10k_evo_simulation.restype.distances PAML_sequence10k_evo_simulation.restype.sequences.ML_est_codonphyml_tree.distances PAML_sequence10k_evo_simulation.restype.sequences.ML_est_iqtree_tree.distances PAML_sequence10k_evo_simulation.restype.sequences.ML_est_nlm.distances 
	printf "t\tPAML\tcodonPhyML\tiqtree\tnlm\n" >$@ ; \
	paste <(cat $<) <(cut -f2 $^2) <(cut -f2 $^3) <(cut -f2 $^4) >>$@

# iqtree br len estimates and codonPhyML br len estimates on the y-axis, vs. PAML br len estimates on the x-axis
PAML_sequence10k_evo_simulation.restype.sequences.ML_est_compare.distances.PAML_scatter.pdf: PAML_sequence10k_evo_simulation.restype.distances PAML_sequence10k_evo_simulation.restype.sequences.ML_est_codonphyml_tree.distances PAML_sequence10k_evo_simulation.restype.sequences.ML_est_iqtree_tree.distances
	paste <(cut -f2  $<) <(cut -f2 $^2) <(cut -f2 $^3) | ../../local/src/vis_ML-compare_scatter.py 20x20_codonPhyML,20x20_iqtree -o $@ 

# "alternative" PAML results (because of a bug or because of rounding errors?)
PAML_sequence10k_evo_simulation.restype.alt_LL: PAML_sequence10k_evo_simulation.restype.LL ../../local/share/data/PAML_20x20_runs/alt_LL.out
	paste <(cut -f1 $<) <(cat $^2) >$@
PAML_sequence10k_evo_simulation.restype.alt_distances: PAML_sequence10k_evo_simulation.restype.distances ../../local/share/data/PAML_20x20_runs/alt_dists.out
	paste <(cut -f1 $<) <(cat $^2) >$@
PAML_sequence10k_evo_simulation.restype.sequences.ML_est.alt_LL_table: PAML_sequence10k_evo_simulation.restype.alt_LL PAML_sequence10k_evo_simulation.restype.sequences.ML_est.LL_table
	printf "t\talt_PAML\tcodonPhyML\tiqtree\tnlm\n" >$@ ; \
	paste <(cat $<) <(cut -f 3- $^2 | unhead) >>$@
PAML_sequence10k_evo_simulation.restype.sequences.ML_est_compare.alt_distances.table: PAML_sequence10k_evo_simulation.restype.alt_distances PAML_sequence10k_evo_simulation.restype.sequences.ML_est_compare.distances.table
	printf "t\talt_PAML\tcodonPhyML\tiqtree\tnlm\n" >$@ ; \
	paste <(cat $<) <(cut -f 3- $^2 | unhead) >>$@
PAML_sequence10k_evo_simulation.restype.sequences.ML_est_compare.alt_distances.PAML_scatter.pdf: PAML_sequence10k_evo_simulation.restype.alt_distances PAML_sequence10k_evo_simulation.restype.sequences.ML_est_codonphyml_tree.distances PAML_sequence10k_evo_simulation.restype.sequences.ML_est_iqtree_tree.distances
	paste <(cut -f2  $<) <(cut -f2 $^2) <(cut -f2 $^3) | ../../local/src/vis_ML-compare_scatter.py 20x20_codonPhyML,20x20_iqtree -o $@

# PAML_ML_d / codonPhyML_ML_d ratio
PAML_sequence10k_evo_simulation.restype.sequences.ML_est_compare.distances.PAML-codonPhyML_ratio: PAML_sequence10k_evo_simulation.restype.sequences.ML_est_compare.distances.table PAML_sequence10k_evo_simulation.restype.sequences.ML_est_compare.alt_distances.table
	printf "codonPhyML/PAML\tcodonPhyML/altPAML\n" >$@ ; \
	paste <(unhead $< | bawk '{printf("%.6f\n", $$3 / $$2)}') <(unhead $^2 | bawk '{printf("%.6f\n", $$3 / $$2)}') >>$@

.META: PAML_sequence10k*table
	1	time	# i.e. true distance
	2	PAML
	3	codonPhyML
	4	iqtree
	5	nlm

.META: PAML_sequence10k_evo_simulation.restype.sequences.ML_est_compare.distances.PAML-codonPhyML_ratio
	1	PAML
	2	alt_PAML

# generate a 10kAA sequence, symbols are picked according to frequencies
sequence10k_evo_simulation.restype: restype_adj_count_matrix.merged.exchangeability.Q-star.20x20.iqtree
	tail -n1 $< | ../../local/src/seq_gen.py AA 10000 >$@
sequence10k_evo_simulation.rotamer: rotamers$(MATRIX_TYPE)matrix.merged.Dayhoff_norm.frequencies
	tail -n1 $< | ../../local/src/seq_gen.py ROTA 10000 >$@
# convert to 1-letter rotamer state code
sequence10k_evo_simulation.rotamer.1char: sequence10k_evo_simulation.rotamer
	printf "1\t10000\n\n" > $@.tmp ; paste <(printf "seq_1") <(cat $<) >>$@.tmp; \
	cat $@.tmp | ../../local/src/PHYLIP_codon-2-1.py -C >$@ ; rm $@.tmp
sequence10k_evo_simulation.rotamer.1char.seq_only: sequence10k_evo_simulation.rotamer.1char
	unhead -n 4 $< | cut -f2- >$@	

# simulate the evolution of a 10k sequence, only 1 simulation run (10 total alignments)
sequence10k_evo_simulation.restype.sequences: restype_adj_count_matrix.merged.IRM.Q-star codons2restype.dict sequence10k_evo_simulation.restype
	cut -f2- $< | unhead | ../../local/src/seq_evo_simulation.py $(RESTYPE_STATES) $^2 -f $^3  -T 0.01,0.025,0.05,0.075,0.1,0.125,0.15,0.175,0.2,1 -s -r 1 | unhead | untail >$@
sequence10k_evo_simulation.rotamer.sequences: rotamers_adj_count_matrix.merged.Dayhoff_norm.IRM.Q-star codons2rotamers.dict sequence10k_evo_simulation.rotamer
	cut -f2- $< | unhead | ../../local/src/seq_evo_simulation.py $(ROTA_STATES) $^2 -f $^3  -T 0.01,0.025,0.05,0.075,0.1,0.125,0.15,0.175,0.2,1 -s -r 1 | unhead | untail >$@

sequence10k_evo_simulation.restype.sequences.3-letter: sequence10k_evo_simulation.restype.sequences 
	bawk '$$n_states=="20" {print $$PHYLIP_seq_ID, $$PHYLIP_seq}' $< | sed 's/^\t$$//g' | ../../local/src/PHYLIP_codon-2-1.py -t >$@
sequence10k_evo_simulation.rotamer.sequences.rotasequence: sequence10k_evo_simulation.rotamer.sequences
	bawk '$$n_states=="55" {print $$PHYLIP_seq_ID, $$PHYLIP_seq}' $< | sed 's/^\t$$//g' | ../../local/src/PHYLIP_codon-2-1.py -r >$@

# output an allignment compatible w/t PAML: 1-letter AA symbols, no spaces in the sequence
sequence10k_evo_simulation.restype.sequences.1-letter: sequence10k_evo_simulation.restype.sequences
	bawk '$$n_states=="20" {print $$PHYLIP_seq_ID, $$PHYLIP_seq}' $< | sed 's/^\t$$//g' | ../../local/src/PHYLIP_codon-2-1.py -P >$@

.PHONY: PAML_ALIGNS

# split a file containing multiple PHYLIP alignments into signle alignments
PAML_ALIGNS: sequence10k_evo_simulation.restype.sequences.1-letter
	awk -v RS="3\t30000" 'NR > 1 { print RS $$0 > "sequence10k_PAML_align" (NR-1); close("sequence10k_PAML_align" (NR-1)) }' $^

# the simulation scatter plot
sequence10k_evo_simulation.restype.scatterplot.in: restype_adj_count_matrix.merged.IRM.Q-star codons2restype.dict sequence10k_evo_simulation.restype
	cut -f2- $< | unhead | ../../local/src/seq_evo_simulation.py $(RESTYPE_STATES) $^2 -f $^3  -T 0.01,0.025,0.05,0.075,0.1,0.125,0.15,0.175,0.2,1 -n -r 1 | bsort -k2,2 >$@
sequence10k_evo_simulation.rotamer.scatterplot.in: rotamers_adj_count_matrix.merged.Dayhoff_norm.IRM.Q-star codons2rotamers.dict sequence10k_evo_simulation.rotamer
	cut -f2- $< | unhead | ../../local/src/seq_evo_simulation.py $(ROTA_STATES) $^2 -f $^3  -T 0.01,0.025,0.05,0.075,0.1,0.125,0.15,0.175,0.2,1 -n -r 1 | bsort -k2,2 >$@

# use codonphyml to infer branch length
sequence10k_evo_simulation.restype.sequences.ML_est_codonphyml_tree.txt: sequence10k_evo_simulation.restype.sequences  restype_adj_count_matrix.merged.exchangeability.Q-star.PhyML 
	cp $^2 usermatrix.ecm ; bawk '$$n_states=="20" {print $$PHYLIP_seq_ID, $$PHYLIP_seq}' $<  > sequence10k_evo_simulation.restype.sequences.ML_est ; \
	codonphyml -i sequence10k_evo_simulation.restype.sequences.ML_est -n 10 -d codon -m GY --qrates ECMUSR; \
	rm usermatrix.ecm sequence10k_evo_simulation.restype.sequences.ML_est 
sequence10k_evo_simulation.rotamer.sequences.ML_est_codonphyml_tree.txt: sequence10k_evo_simulation.rotamer.sequences rotamers_adj_count_matrix.merged.Dayhoff_norm.exchangeability.Q-star.PhyML
	cp $^2 usermatrix.ecm ; bawk '$$n_states=="55" {print $$PHYLIP_seq_ID, $$PHYLIP_seq}' $<  > sequence10k_evo_simulation.rotamer.sequences.ML_est ; \
	codonphyml -i sequence10k_evo_simulation.rotamer.sequences.ML_est -n 10 -d codon -m GY --qrates ECMUSR; \
	rm usermatrix.ecm sequence10k_evo_simulation.rotamer.sequences.ML_est

# extract the codonPhyML likelihoods
sequence10k_evo_simulation.restype.sequences.ML_est_codonphyml_tree.LL: sequence10k_evo_simulation.restype.sequences.ML_est_codonphyml_tree.txt 
	printf "0.01,0.025,0.05,0.075,0.1,0.125,0.15,0.175,0.2,1" | tr "," "\n" >$@.tmp1 ;\
	grep -e '\. Log-likelihood:' sequence10k_evo_simulation.restype.sequences.ML_est_codonphyml_stats.txt | awk '{print $$3}' >$@.tmp2 ;\
	paste $@.tmp1 $@.tmp2 >$@ ; rm $@.tmp*
sequence10k_evo_simulation.rotamer.sequences.ML_est_codonphyml_tree.LL: sequence10k_evo_simulation.rotamer.sequences.ML_est_codonphyml_tree.txt
	printf "0.01,0.025,0.05,0.075,0.1,0.125,0.15,0.175,0.2,1" | tr "," "\n" >$@.tmp1 ;\
	grep -e '\. Log-likelihood:' sequence10k_evo_simulation.rotamer.sequences.ML_est_codonphyml_stats.txt | awk '{print $$3}' >$@.tmp2 ;\
	paste $@.tmp1 $@.tmp2 >$@ ; rm $@.tmp*

# extract the codonPhyML unconstrained likelihood 
sequence10k_evo_simulation.restype.sequences.ML_est_codonphyml_tree.Unconstrained_LL: sequence10k_evo_simulation.restype.sequences.ML_est_codonphyml_tree.txt
	printf "0.01,0.025,0.05,0.075,0.1,0.125,0.15,0.175,0.2,1" | tr "," "\n" >$@.tmp1 ;\
	grep -e 'Unconstrained Log-likelihood:' sequence10k_evo_simulation.restype.sequences.ML_est_codonphyml_stats.txt | awk '{print $$4}' >$@.tmp2 ;\
	paste $@.tmp1 $@.tmp2 >$@ ; rm $@.tmp*
sequence10k_evo_simulation.rotamer.sequences.ML_est_codonphyml_tree.Unconstrained_LL: sequence10k_evo_simulation.rotamer.sequences.ML_est_codonphyml_tree.txt
	printf "0.01,0.025,0.05,0.075,0.1,0.125,0.15,0.175,0.2,1" | tr "," "\n" >$@.tmp1 ;\
	grep -e 'Unconstrained Log-likelihood:' sequence10k_evo_simulation.rotamer.sequences.ML_est_codonphyml_stats.txt | awk '{print $$4}' >$@.tmp2 ;\
	paste $@.tmp1 $@.tmp2 >$@ ; rm $@.tmp*

# estimate 10k AA sequence distance using iqtree and the "proper" non-sparse 20x20 matrix.
sequence10k_evo_simulation.restype.sequences.ML_est_iqtree_tree: sequence10k_evo_simulation.restype.sequences restype_adj_count_matrix.merged.exchangeability.Q-star.20x20.iqtree
	cp $^2 usermatrix.ecm ; bawk '$$n_states=="20" {print $$PHYLIP_seq_ID, $$PHYLIP_seq}' $< | sed 's/^\t$$//g'  > sequence10k_evo_simulation.restype.sequences.ML_est ; \
	../../local/src/iqtree_wrapper.py usermatrix.ecm -n -d AA sequence10k_evo_simulation.restype.sequences.ML_est >$@
	rm usermatrix.ecm sequence10k_evo_simulation.restype.sequences.ML_est 
# use iqtree w/t the 55x55 model, iqtree requires frequencies >= 0.00001
sequence10k_evo_simulation.rotamer.sequences.ML_est_iqtree_tree: sequence10k_evo_simulation.rotamer.sequences rotamers_adj_count_matrix.merged.Dayhoff_norm.exchangeability.Q-star.iqtree
	cat $^2 | untail > usermatrix.ecm ; tail -n1 $^2 | sed 's/0.0\t/0.00001\t/g;s/0.0$$/0.00001/g' >> usermatrix.ecm ; \ 
	bawk '$$n_states=="55" {print $$PHYLIP_seq_ID, $$PHYLIP_seq}' $< | sed 's/^\t$$//g'  > sequence10k_evo_simulation.rotamer.sequences.ML_est ; \
	../../local/src/iqtree_wrapper.py usermatrix.ecm -n sequence10k_evo_simulation.rotamer.sequences.ML_est >$@
	rm usermatrix.ecm sequence10k_evo_simulation.rotamer.sequences.ML_est

# extract the iqtree likelihoods
sequence10k_evo_simulation.restype.sequences.ML_est_iqtree_tree.LL: sequence10k_evo_simulation.restype.sequences restype_adj_count_matrix.merged.exchangeability.Q-star.20x20.iqtree
	printf "0.01,0.025,0.05,0.075,0.1,0.125,0.15,0.175,0.2,1" | tr "," "\n" >$@.tmp1 ;\
	cp $^2 usermatrix.ecm ; bawk '$$n_states=="20" {print $$PHYLIP_seq_ID, $$PHYLIP_seq}' $< | sed 's/^\t$$//g'  > sequence10k_evo_simulation.restype.sequences.ML_est ; \
	../../local/src/iqtree_wrapper.py -n -d AA -o usermatrix.ecm  sequence10k_evo_simulation.restype.sequences.ML_est \
	| grep 'BEST SCORE FOUND :' | awk '{print $$5}' >$@.tmp2 ;\
	paste $@.tmp1 $@.tmp2 >$@ ; rm $@.tmp* sequence10k_evo_simulation.restype.sequences.ML_est
# use iqtree w/t the 55x55 model, iqtree requires frequencies >= 0.00001
sequence10k_evo_simulation.rotamer.sequences.ML_est_iqtree_tree.LL: sequence10k_evo_simulation.rotamer.sequences rotamers_adj_count_matrix.merged.Dayhoff_norm.exchangeability.Q-star.iqtree
	printf "0.01,0.025,0.05,0.075,0.1,0.125,0.15,0.175,0.2,1" | tr "," "\n" >$@.tmp1 ;\
	cat $^2 | untail >  usermatrix.ecm ; tail -n1 $^2 | sed 's/0.0\t/0.00001\t/g;s/0.0$$/0.00001/g' >> usermatrix.ecm ; \ 
	bawk '$$n_states=="55" {print $$PHYLIP_seq_ID, $$PHYLIP_seq}' $< | sed 's/^\t$$//g'  > sequence10k_evo_simulation.rotamer.sequences.ML_est ; \
	../../local/src/iqtree_wrapper.py usermatrix.ecm -n -o sequence10k_evo_simulation.rotamer.sequences.ML_est \
	| grep 'BEST SCORE FOUND :' | awk '{print $$5}' >$@.tmp2 ;\
	paste $@.tmp1 $@.tmp2 >$@ ; rm usermatrix.ecm sequence10k_evo_simulation.rotamer.sequences.ML_est $@.tmp*

#------------PAIRWISE DISTANCE ESTIMATION USING A CUSTOM SCRIPT------------------------------------------------------------------------------------------------------------
# based on scipy.optimize.minimize

# use ML extimation (nml) to infer distance b/w two sequences
sequence10k_evo_simulation.restype.sequences.ML_est_nlm.distances: sequence10k_evo_simulation.restype.sequences.3-letter restype_adj_count_matrix.merged.IRM.Q-star.neg_diag restype_adj_count_matrix.merged.exchangeability.Q-star.20x20.iqtree
	printf "0.01,0.025,0.05,0.075,0.1,0.125,0.15,0.175,0.2,1" | tr "," "\n" >$@.tmp_t
	egrep -v '(^3|^New|^Orig_seq_1)' $< | sed '/^\s*$$/d' | enumerate_rows >$@.tmp_orig ; \\
	head -n1 $^2 | cut -f2- >  $@.tmp_freq ; tail -n1 $^3 >> $@.tmp_freq ; touch $@.tmp_dist ; \
	egrep -v '(^3|^Orig_seq)' $< | sed '/^\s*$$/d' | enumerate_rows >$@.tmp_new ; \
	awk '{print; getline < "$@.tmp_new"; print}' $@.tmp_orig \
	| repeat_group_pipe -s '\
		cut -f 2,3 > $@.tmp_align ; \
		../../local/src/ML_d_est.py $^2 $@.tmp_freq $@.tmp_align >>$@.tmp_dist ; \	
	' 1 ; paste $@.tmp_t $@.tmp_dist | cut -f 1,3 >$@ ; rm $@.tmp*
sequence10k_evo_simulation.rotamer.sequences.ML_est_nlm.distances: sequence10k_evo_simulation.rotamer.sequences.rotasequence  rotamers_adj_count_matrix.merged.Dayhoff_norm.IRM.Q-star.neg_diag rotamers_adj_count_matrix.merged.Dayhoff_norm.exchangeability.Q-star.iqtree 
	printf "0.01,0.025,0.05,0.075,0.1,0.125,0.15,0.175,0.2,1" | tr "," "\n" >$@.tmp_t
	egrep -v '(^3|^New|^Orig_seq_1)' $< | sed '/^\s*$$/d' | enumerate_rows >$@.tmp_orig ; \\
	echo $(IQ_ROTA_AS_CODON) | tr "," "\t" >  $@.tmp_freq ; tail -n1 $^3 >> $@.tmp_freq ; touch $@.tmp_dist ; \
	egrep -v '(^3|^Orig_seq)' $< | sed '/^\s*$$/d' | enumerate_rows >$@.tmp_new ; \
	awk '{print; getline < "$@.tmp_new"; print}' $@.tmp_orig \
	| repeat_group_pipe -s '\
		cut -f 2,3 > $@.tmp_align ; \
		../../local/src/ML_d_est.py $^2 $@.tmp_freq $@.tmp_align >>$@.tmp_dist ; \	
	' 1 ; paste $@.tmp_t $@.tmp_dist | cut -f 1,3 >$@ ; rm $@.tmp*


# extract the nlm likelihoods
sequence10k_evo_simulation.restype.sequences.ML_est_nlm.distances.LL: sequence10k_evo_simulation.restype.sequences.3-letter restype_adj_count_matrix.merged.IRM.Q-star.neg_diag restype_adj_count_matrix.merged.exchangeability.Q-star.20x20.iqtree
	printf "0.01,0.025,0.05,0.075,0.1,0.125,0.15,0.175,0.2,1" | tr "," "\n" >$@.tmp_t
	egrep -v '(^3|^New|^Orig_seq_1)' $< | sed '/^\s*$$/d' | enumerate_rows >$@.tmp_orig ; \\
	head -n1 $^2 | cut -f2- >  $@.tmp_freq ; tail -n1 $^3 >> $@.tmp_freq ; touch $@.tmp_dist ; \
	egrep -v '(^3|^Orig_seq)' $< | sed '/^\s*$$/d' | enumerate_rows >$@.tmp_new ; \
	awk '{print; getline < "$@.tmp_new"; print}' $@.tmp_orig \
	| repeat_group_pipe -s '\
		cut -f 2,3 > $@.tmp_align ; \
		../../local/src/ML_d_est.py $^2 $@.tmp_freq $@.tmp_align >>$@.tmp_dist ; \	
	' 1 ; paste $@.tmp_t $@.tmp_dist | cut -f 1,4 >$@ ; rm $@.tmp*
sequence10k_evo_simulation.rotamer.sequences.ML_est_nlm.distances.LL: sequence10k_evo_simulation.rotamer.sequences.rotasequence rotamers_adj_count_matrix.merged.Dayhoff_norm.IRM.Q-star.neg_diag rotamers_adj_count_matrix.merged.Dayhoff_norm.exchangeability.Q-star.iqtree
	printf "0.01,0.025,0.05,0.075,0.1,0.125,0.15,0.175,0.2,1" | tr "," "\n" >$@.tmp_t
	egrep -v '(^3|^New|^Orig_seq_1)' $< | sed '/^\s*$$/d' | enumerate_rows >$@.tmp_orig ; \\
	echo $(IQ_ROTA_AS_CODON) | tr "," "\t" >  $@.tmp_freq ; tail -n1 $^3 >> $@.tmp_freq ; touch $@.tmp_dist ; \
	egrep -v '(^3|^Orig_seq)' $< | sed '/^\s*$$/d' | enumerate_rows >$@.tmp_new ; \
	awk '{print; getline < "$@.tmp_new"; print}' $@.tmp_orig \
	| repeat_group_pipe -s '\
		cut -f 2,3 > $@.tmp_align ; \
		../../local/src/ML_d_est.py $^2 $@.tmp_freq $@.tmp_align >>$@.tmp_dist ; \	
	' 1 ; paste $@.tmp_t $@.tmp_dist | cut -f 1,4 >$@ ; rm $@.tmp*


sequence10k_evo_simulation.restype.sequences.ML_est_PAML.LL: ../../local/share/data/PAML_20x20_runs/mysim_out 
	printf "0.01,0.025,0.05,0.075,0.1,0.125,0.15,0.175,0.2,1" | tr "," "\n" >$@.tmp1 ;\
	cut -f2 $< | unhead >$@.tmp2 ;\
	paste $@.tmp1 $@.tmp2 >$@ ; rm $@.tmp* 

sequence10k_evo_simulation.restype.sequences.ML_est_PAML.distances: ../../local/share/data/PAML_20x20_runs/mysim_out
	printf "0.01,0.025,0.05,0.075,0.1,0.125,0.15,0.175,0.2,1" | tr "," "\n" >$@.tmp1 ;\
	cut -f3 $< | unhead >$@.tmp2 ;\
	paste $@.tmp1 $@.tmp2 >$@ ; rm $@.tmp*


# tabulate the maximum LLs
sequence10k_evo_simulation.restype.sequences.ML_est.LL_table: sequence10k_evo_simulation.restype.sequences.ML_est_PAML.LL sequence10k_evo_simulation.restype.sequences.ML_est_codonphyml_tree.LL sequence10k_evo_simulation.restype.sequences.ML_est_iqtree_tree.LL sequence10k_evo_simulation.restype.sequences.ML_est_nlm.distances.LL 	
	printf "t\tPAML\tcodonPhyML\tiqtree\tnlm\n" >$@ ; \
	paste <(cat $<) <(cut -f2 $^2) <(cut -f2 $^3) <(cut -f2 $^4) >>$@
# no PAML data for the rotamer case
sequence10k_evo_simulation.rotamer.sequences.ML_est.LL_table: sequence10k_evo_simulation.rotamer.sequences.ML_est_codonphyml_tree.LL sequence10k_evo_simulation.rotamer.sequences.ML_est_iqtree_tree.LL sequence10k_evo_simulation.rotamer.sequences.ML_est_nlm.distances.LL
	printf "t\tcodonPhyML\tiqtree\tnlm\n" >$@ ; \
	paste <(cat $<) <(cut -f2 $^2) <(cut -f2 $^3) >>$@

# tabulate the ML-estimated distances b/w the sequences
sequence10k_evo_simulation.restype.sequences.ML_est_compare.distances.table: sequence10k_evo_simulation.restype.sequences.ML_est_PAML.distances  sequence10k_evo_simulation.restype.sequences.ML_est_codonphyml_tree.distances sequence10k_evo_simulation.restype.sequences.ML_est_iqtree_tree.distances sequence10k_evo_simulation.restype.sequences.ML_est_nlm.distances 
	printf "t\tPAML\tcodonPhyML\tiqtree\tnlm\n" >$@ ; \
	paste <(cat $<) <(cut -f2 $^2) <(cut -f2 $^3) <(cut -f2 $^4) >>$@
# no PAML data for the rotamer case
sequence10k_evo_simulation.rotamer.sequences.ML_est_compare.distances.table: sequence10k_evo_simulation.rotamer.sequences.ML_est_codonphyml_tree.distances sequence10k_evo_simulation.rotamer.sequences.ML_est_iqtree_tree.distances sequence10k_evo_simulation.rotamer.sequences.ML_est_nlm.distances
	printf "t\tcodonPhyML\tiqtree\tnlm\n" >$@ ; \
	paste <(cat $<) <(cut -f2 $^2) <(cut -f2 $^3) >>$@

# tabulate the unconstrained ML values
sequence10k_evo_simulation.restype.sequences.ML_est.unconstrained_LL_table: ../../local/share/data/PAML_20x20_runs/mysim_out sequence10k_evo_simulation.restype.sequences.ML_est_codonphyml_tree.Unconstrained_LL
	printf "t\tPAML\tcodonPhyM\n" >$@ ; \
	cut -f1 $< | unhead >$@.tmp1 ; cat $^2 >$@.tmp2 ; \ 
	paste $@.tmp2 $@.tmp1 >>$@ ; rm $@.tmp*

# a PHYLIP alignment of 4 1kAA sequences obtained w/t PAML evolver
PAML_4sequences1k_evo_simulation.restype.sequences: ../../local/share/data/PAML_20x20_runs/mc.paml
	cat $< | unhead -n 2 | sed 's/ \+ /\t/g' | sed 's/ //g' | sed 's/^\t//g' | sed s/\r\n/\n/g >$@

PAML_4sequences1k_evo_simulation.restype.tree: ../../local/share/data/PAML_20x20_runs/4seq_test.out
	grep '^((taxonA:' $< >$@

# from 1-letter AA to codons for codonPhyML; here there is only 1 dataset (-n 1)
PAML_4sequences1k_evo_simulation.restype.sequences.ML_est_codonphyml_tree.txt: PAML_4sequences1k_evo_simulation.restype.sequences restype_adj_count_matrix.merged.exchangeability.Q-star.PhyML 
	cp $^2 usermatrix.ecm ; cat $< | ../../local/src/PHYLIP_codon-2-1.py -i > PAML_4sequences1k_evo_simulation.restype.sequences.ML_est ; \
	codonphyml -i PAML_4sequences1k_evo_simulation.restype.sequences.ML_est -n 1 -d codon -m GY --qrates ECMUSR; \
	rm usermatrix.ecm PAML_4sequences1k_evo_simulation.restype.sequences.ML_est

# use iqtree to infer the tree
PAML_4sequences1k_evo_simulation.restype.sequences.ML_est_iqtree_tree: PAML_4sequences1k_evo_simulation.restype.sequences restype_adj_count_matrix.merged.exchangeability.Q-star.20x20.iqtree
	cp $^2 usermatrix.ecm ; cp $<  sequence10k_evo_simulation.restype.sequences.ML_est ; \
	../../local/src/iqtree_wrapper.py usermatrix.ecm -P -o -d AA sequence10k_evo_simulation.restype.sequences.ML_est | tail -n 2 | untail >$@
	rm usermatrix.ecm sequence10k_evo_simulation.restype.sequences.ML_est 

# use nlm to estimate all the pairwise distances b/w the sequences in the multialignment
PAML_4sequences1k_evo_simulation.restype.sequences.combinations: PAML_4sequences1k_evo_simulation.restype.sequences
	cat $< | ../../local/src/PHYLIP_codon-2-1.py -o >$@.tmp ; ../../local/src/PHYLIP_seq_comb.py -e $@.tmp >$@ ; rm $@.tmp
PAML_4sequences1k_evo_simulation.restype.sequences.ML_est_nlm.distances: PAML_4sequences1k_evo_simulation.restype.sequences.combinations restype_adj_count_matrix.merged.IRM.Q-star.neg_diag restype_adj_count_matrix.merged.exchangeability.Q-star.20x20.iqtree
	head -n1 $^2 | cut -f2- >$@.tmp_freq ; tail -n1 $^3 >>$@.tmp_freq ; touch $@.tmp_dist ; \
	cut -f2 $< | awk '{print ; getline ; print $$0";"}' | tr "\n" "-" | tr ";" "\n" | sed 's/^-//g' >$@.tmp.id
	cat $< | repeat_group_pipe -s ' \
		cut -f 2,3 > $@.tmp_align ; \
		../../local/src/ML_d_est.py $^2 $@.tmp_freq $@.tmp_align >>$@.tmp_dist ; \
	' 1 ; paste $@.tmp.id $@.tmp_dist | cut -f 1,3 >$@ ; rm $@.tmp*

# extract the distances from the PAML,iqtree and codonphyml tree
PAML_4sequences1k_evo_simulation.restype.sequences.ML_est_codonphyml_tree.distances: PAML_4sequences1k_evo_simulation.restype.sequences.ML_est_codonphyml_tree.txt
	../../local/src/tree_dist.py $< | bawk '{print $$1"-"$$2, $$3}' | bsort -k1,1 >$@
PAML_4sequences1k_evo_simulation.restype.sequences.ML_est_iqtree_tree.distances: PAML_4sequences1k_evo_simulation.restype.sequences.ML_est_iqtree_tree
	../../local/src/tree_dist.py $< | bawk '{print $$1"-"$$2, $$3}' | bsort -k1,1 >$@
PAML_4sequences1k_evo_simulation.restype.tree.distances: PAML_4sequences1k_evo_simulation.restype.tree
	../../local/src/tree_dist.py $< | bawk '{print $$1"-"$$2, $$3}' | bsort -k1,1 >$@
# tabulate distances
PAML_4sequences1k_evo_simulation.restype.sequences.ML_est.distances.table: PAML_4sequences1k_evo_simulation.restype.tree.distances PAML_4sequences1k_evo_simulation.restype.sequences.ML_est_codonphyml_tree.distances PAML_4sequences1k_evo_simulation.restype.sequences.ML_est_iqtree_tree.distances PAML_4sequences1k_evo_simulation.restype.sequences.ML_est_nlm.distances
	printf "Taxon\tPAML\tcodonPhyML\tiqtree\tnlm\n" >$@ ; \
	paste <(cat $<) <(cut -f2 $^2) <(cut -f2 $^3) <(cut -f2 $^4)>>$@

# create latex tables
%table.tex: %table
	cat $< | sed 's/_/\\_/g; s/\t/\t\&\t/g; s/$$/ \\\\/' >$@

#___________________SYSTEMATIC ANALYSIS ACROSS MULTIPLE MODELS___________________________________________________________________
# all done using the same custom optimizer (Nelder-Mead algorithm), variables are: sequence length, model, frequencies, distance b/w simulated sequences

# simulate sequence evolution at timepoints using the 55x55 model: ancestor is generated every time according to frequencies in database, 100 runs  per timepoint, output rotasequences
sequence%x100_evo_simulation.rotamer.rotasequences: rotamers_adj_count_matrix.merged.Dayhoff_norm.IRM.Q-star  codons2rotamers.dict rotamers$(MATRIX_TYPE)matrix.merged.Dayhoff_norm.frequencies
	cat $^3 | unhead >$@.tmp ; \ 
	cut -f2- $< | unhead | ../../local/src/seq_evo_simulation.py -s -l $* -r 100 -T $(TIMEPOINTS) -F $@.tmp $(ROTA_STATES) $^2 | unhead  \
	| bawk '$$1=="55"{print $$2,$$3}' |  sed 's/^\t//g' | ../../local/src/PHYLIP_codon-2-1.py -r >$@
	rm $@.tmp

# simulate sequence evolution at timepoints using the 20x20 model: ancestor is generated every time according to frequencies in database, 100 runs per timepoint, outpu 3-letter AA seuqnces
sequence%x100_evo_simulation.restype.sequences: restype_adj_count_matrix.merged.IRM.Q-star codons2restype.dict restype_adj_count_matrix.merged.frequencies 
	cat $^3 | unhead >$@.tmp ; \ 
	cut -f2- $< | unhead | ../../local/src/seq_evo_simulation.py -s -l $* -r 100 -T $(TIMEPOINTS) -F $@.tmp $(RESTYPE_STATES) $^2 | unhead  \
	| bawk '$$1=="20"{print $$2,$$3}' |  sed 's/^\t//g' | ../../local/src/PHYLIP_codon-2-1.py -t >$@
	rm $@.tmp


# mask the rotamer states in the rotasequence
sequence%x100_evo_simulation.rotamer.masked_rotasequences: sequence%x100_evo_simulation.rotamer.rotasequences
	bawk '$$2!=$*{ gsub(/[0-9]/, "", $$2); print } {print}' $< | uniq >$@		

# estimate branch lengths using nlm and the 55x55 model
sequence%x100_evo_simulation.rotamer.rotasequences.ML_est_nlm_55x55.distances: sequence%x100_evo_simulation.rotamer.rotasequences rotamers_adj_count_matrix.merged.Dayhoff_norm.IRM.Q-star.neg_diag rotamers$(MATRIX_TYPE)matrix.merged.Dayhoff_norm.frequencies 
	egrep -v '(^3|^New|^Orig_seq_1)' $< | sed '/^\s*$$/d' | enumerate_rows >$@.tmp_orig ; \\
	echo $(ROTA_STATES) | tr "," "\t" >  $@.tmp_freq ; tail -n1 $^3 >> $@.tmp_freq ; touch $@.tmp_dist ; \
	egrep -v '(^3|^Orig_seq)' $< | sed '/^\s*$$/d' | enumerate_rows >$@.tmp_new ; \
	awk '{print; getline < "$@.tmp_new"; print}' $@.tmp_orig \
	| repeat_group_pipe -s '\
		cut -f 2,3 > $@.tmp_align ; cut -f 1 $@.tmp_align | unhead >> $@.tmp_id ;\
		../../local/src/ML_d_est.py $^2 $@.tmp_freq $@.tmp_align >>$@.tmp_dist ; \	
	' 1 ; paste $@.tmp_id $@.tmp_dist | cut -f 1,3 | sed 's/^New_seq-//g' >$@ ; rm $@.tmp*
# simulate infinite sites, estimate branch lengths using nlm and the 55x55 model
sequenceINFx100_evo_simulation.rotamer.rotasequences.ML_est_nlm_55x55.distances: sequence100x100_evo_simulation.rotamer.rotasequences rotamers_adj_count_matrix.merged.Dayhoff_norm.IRM.Q-star.neg_diag rotamers$(MATRIX_TYPE)matrix.merged.Dayhoff_norm.frequencies
	egrep -v '(^3|^New|^Orig_seq_1)' $< | sed '/^\s*$$/d' | enumerate_rows >$@.tmp_orig ; \\
	echo $(ROTA_STATES) | tr "," "\t" >  $@.tmp_freq ; tail -n1 $^3 >> $@.tmp_freq ; touch $@.tmp_dist ; \
	egrep -v '(^3|^Orig_seq)' $< | sed '/^\s*$$/d' | enumerate_rows >$@.tmp_new ; \
	awk '{print; getline < "$@.tmp_new"; print}' $@.tmp_orig \
	| repeat_group_pipe -s '\
		cut -f 2,3 > $@.tmp_align ; cut -f 1 $@.tmp_align | unhead >> $@.tmp_id ;\
		../../local/src/ML_d_est.py -I $$(tail -n1  $@.tmp_id | tr "-" "\t" | cut -f2)  $^2 $@.tmp_freq >> $@.tmp_dist ; \	
	' 1 ; paste $@.tmp_id $@.tmp_dist | cut -f 1,3 | sed 's/^New_seq-//g' >$@ ; rm $@.tmp*
	

# estimate branch lengths using nlm and the 20x20 "Dayhoff-like" model
sequence%x100_evo_simulation.rotamer.rotasequences.ML_est_nlm_20x20.distances: sequence%x100_evo_simulation.rotamer.masked_rotasequences restype_adj_count_matrix.merged.IRM.Q-star.neg_diag restype_adj_count_matrix.merged.frequencies
	egrep -v '(^3|^New|^Orig_seq_1)' $< | sed '/^\s*$$/d' | enumerate_rows >$@.tmp_orig ; \\
	cat $^3 > $@.tmp_freq ; touch $@.tmp_dist ; \
	egrep -v '(^3|^Orig_seq)' $< | sed '/^\s*$$/d' | enumerate_rows >$@.tmp_new ; \
	awk '{print; getline < "$@.tmp_new"; print}' $@.tmp_orig \
	| repeat_group_pipe -s '\
		cut -f 2,3 > $@.tmp_align ; cut -f 1 $@.tmp_align | unhead >> $@.tmp_id ;\
		../../local/src/ML_d_est.py $^2 $@.tmp_freq $@.tmp_align >>$@.tmp_dist ; \	
	' 1 ; paste $@.tmp_id $@.tmp_dist | cut -f 1,3 | sed 's/^New_seq-//g' >$@ ; rm $@.tmp*
# simulate under 20x20, analize under 20x20
sequence%x100_evo_simulation.restype.sequences.ML_est_nlm_20x20.distances: sequence%x100_evo_simulation.restype.sequences restype_adj_count_matrix.merged.IRM.Q-star.neg_diag restype_adj_count_matrix.merged.frequencies
	egrep -v '(^3|^New|^Orig_seq_1)' $< | sed '/^\s*$$/d' | enumerate_rows >$@.tmp_orig ; \
	cat $^3 | tr "," "\t" >  $@.tmp_freq ; touch $@.tmp_dist ; \
	egrep -v '(^3|^Orig_seq)' $< | sed '/^\s*$$/d' | enumerate_rows >$@.tmp_new ; \
	awk '{print; getline < "$@.tmp_new"; print}' $@.tmp_orig \
	| repeat_group_pipe -s '\
		cut -f 2,3 > $@.tmp_align ; cut -f 1 $@.tmp_align | unhead >> $@.tmp_id ;\
		../../local/src/ML_d_est.py $^2 $@.tmp_freq $@.tmp_align >>$@.tmp_dist ; \	
	' 1 ; paste $@.tmp_id $@.tmp_dist | cut -f 1,3 | sed 's/^New_seq-//g' >$@ ; rm $@.tmp*

# infinite sequence
sequenceINFx100_evo_simulation.rotamer.rotasequences.ML_est_nlm_20x20.distances: sequence100x100_evo_simulation.rotamer.masked_rotasequences restype_adj_count_matrix.merged.IRM.Q-star.neg_diag restype_adj_count_matrix.merged.frequencies
	egrep -v '(^3|^New|^Orig_seq_1)' $< | sed '/^\s*$$/d' | enumerate_rows >$@.tmp_orig ; \\
	cat $^3 > $@.tmp_freq ; touch $@.tmp_dist ; \
	egrep -v '(^3|^Orig_seq)' $< | sed '/^\s*$$/d' | enumerate_rows >$@.tmp_new ; \
	awk '{print; getline < "$@.tmp_new"; print}' $@.tmp_orig \
	| repeat_group_pipe -s '\
		cut -f 2,3 > $@.tmp_align ; cut -f 1 $@.tmp_align | unhead >> $@.tmp_id ;\
		../../local/src/ML_d_est.py -I $$(tail -n1  $@.tmp_id | tr "-" "\t" | cut -f2)  $^2 $@.tmp_freq >> $@.tmp_dist ; \	
	' 1 ; paste $@.tmp_id $@.tmp_dist | cut -f 1,3 | sed 's/^New_seq-//g' >$@ ; rm $@.tmp*


# estimate branch lengths using nlm and the 20x20 "collapsed" model
sequence%x100_evo_simulation.rotamer.rotasequences.ML_est_nlm_20x20_COLLAPSED.distances: sequence%x100_evo_simulation.rotamer.masked_rotasequences rotamers_adj_count_matrix.merged.Dayhoff_norm.COLLAPSED.IRM.Q-star.neg_diag rotamers_adj_count_matrix.merged.Dayhoff_norm.COLLAPSED.frequencies
	egrep -v '(^3|^New|^Orig_seq_1)' $< | sed '/^\s*$$/d' | enumerate_rows >$@.tmp_orig ; \\
	cat $^3 > $@.tmp_freq ; touch $@.tmp_dist ; \
	egrep -v '(^3|^Orig_seq)' $< | sed '/^\s*$$/d' | enumerate_rows >$@.tmp_new ; \
	awk '{print; getline < "$@.tmp_new"; print}' $@.tmp_orig \
	| repeat_group_pipe -s '\
		cut -f 2,3 > $@.tmp_align ; cut -f 1 $@.tmp_align | unhead >> $@.tmp_id ;\
		../../local/src/ML_d_est.py $^2 $@.tmp_freq $@.tmp_align >>$@.tmp_dist ; \	
	' 1 ; paste $@.tmp_id $@.tmp_dist | cut -f 1,3 | sed 's/^New_seq-//g' >$@ ; rm $@.tmp*
# infinite sequence
sequenceINFx100_evo_simulation.rotamer.rotasequences.ML_est_nlm_20x20_COLLAPSED.distances: sequence100x100_evo_simulation.rotamer.masked_rotasequences rotamers_adj_count_matrix.merged.Dayhoff_norm.COLLAPSED.IRM.Q-star.neg_diag rotamers_adj_count_matrix.merged.Dayhoff_norm.COLLAPSED.frequencies
	egrep -v '(^3|^New|^Orig_seq_1)' $< | sed '/^\s*$$/d' | enumerate_rows >$@.tmp_orig ; \\
	cat $^3 > $@.tmp_freq ; touch $@.tmp_dist ; \
	egrep -v '(^3|^Orig_seq)' $< | sed '/^\s*$$/d' | enumerate_rows >$@.tmp_new ; \
	awk '{print; getline < "$@.tmp_new"; print}' $@.tmp_orig \
	| repeat_group_pipe -s '\
		cut -f 2,3 > $@.tmp_align ; cut -f 1 $@.tmp_align | unhead >> $@.tmp_id ;\
		../../local/src/ML_d_est.py -I $$(tail -n1  $@.tmp_id | tr "-" "\t" | cut -f2)  $^2 $@.tmp_freq >> $@.tmp_dist ; \	
	' 1 ; paste $@.tmp_id $@.tmp_dist | cut -f 1,3 | sed 's/^New_seq-//g' >$@ ; rm $@.tmp*

# plot the estimated branch length using one boxplot per timepoint
sequence%.distances.boxplot.pdf: sequence%.distances
	cat $< | tr "-" "\t" | cut -f1,3 | bsort -k1,1 | ../../local/src/vis_BL_boxplot.py -o $@

# obtain the 20x20 model IRM from a PAML-style *.dat file
WAG_20x20.IRM: ../../local/share/data/wag.dat
	head -n 21 $< | tail -n1 | tr -s " " "," | sed 's/,[^0-9]//g' >$@.tmp ; \
	head -n 19 $< |  ../../local/src/normalize_matrix.py -R $@.tmp $(RESTYPE_STATES) >$@ ;\
	rm $@.tmp
WAG_20x20.frequencies: ../../local/share/data/wag.dat
	head -n 21 $< | tail -n1 | tr -s " " "," | sed 's/,[^0-9]//g' >$@
LG_20x20.IRM: ../../local/share/data/lg.dat
	head -n 21 $< | tail -n1 | tr -s " " "," | sed 's/,[^0-9]//g' >$@.tmp ; \
	head -n 19 $< |  ../../local/src/normalize_matrix.py -R $@.tmp $(RESTYPE_STATES) >$@
	rm $@.tmp
LG_20x20.frequencies: ../../local/share/data/lg.dat
	head -n 21 $< | tail -n1 | tr -s " " "," | sed 's/,[^0-9]//g' >$@
DAYHOFF_20x20.IRM: ../../local/share/data/dayhoff.dat
	cat $< | unhead | head -n 23 | tail -n3 | tr -s "\r\n" "," | tr -s " " "," | sed 's/,$$//g' >$@.tmp ; \
	head -n 20 $< | unhead |  ../../local/src/normalize_matrix.py -R $@.tmp $(RESTYPE_STATES) >$@ ;\
	rm $@.tmp
DAYHOFF_20x20.frequencies: ../../local/share/data/dayhoff.dat 
	cat $< | unhead | head -n 23 | tail -n3 | tr -s "\r\n" "," | tr -s " " "," | sed 's/,$$//g' >$@
# set the diagonal values so that each row sums to 0
%_20x20.IRM.neg_diag: %_20x20.IRM
	cut -f 2- $< | unhead | ../../local/bin/normalize_matrix.py -n $(RESTYPE_STATES) >$@
# obtain the scaled Q matrix (Q*) from the IRM
%_20x20.IRM.Q-star.neg_diag: %_20x20.IRM.neg_diag %_20x20.frequencies
	cut -f 2- $< | unhead | ../../local/bin/normalize_matrix.py -A $^2 $(RESTYPE_STATES) >$@


# estimate branch lengths using nlm and the WAG  model
sequence%x100_evo_simulation.rotamer.rotasequences.ML_est_nlm_20x20_WAG.distances: sequence%x100_evo_simulation.rotamer.masked_rotasequences WAG_20x20.IRM.Q-star.neg_diag WAG_20x20.frequencies
	egrep -v '(^3|^New|^Orig_seq_1)' $< | sed '/^\s*$$/d' | enumerate_rows >$@.tmp_orig ; \
	printf "$(RESTYPE_STATES)\n"| tr "," "\t" >$@.tmp_freq ; \
	cat $^3 | tr "," "\t" >> $@.tmp_freq ; touch $@.tmp_dist ; \
	egrep -v '(^3|^Orig_seq)' $< | sed '/^\s*$$/d' | enumerate_rows >$@.tmp_new ; \
	awk '{print; getline < "$@.tmp_new"; print}' $@.tmp_orig \
	| repeat_group_pipe -s '\
		cut -f 2,3 > $@.tmp_align ; cut -f 1 $@.tmp_align | unhead >> $@.tmp_id ;\
		../../local/src/ML_d_est.py $^2 $@.tmp_freq $@.tmp_align >>$@.tmp_dist ; \	
	' 1 ; paste $@.tmp_id $@.tmp_dist | cut -f 1,3 | sed 's/^New_seq-//g' >$@ ; rm $@.tmp*
# simulate under 20x20, analize under WAG
sequence%x100_evo_simulation.restype.sequences.ML_est_nlm_20x20_WAG.distances: sequence%x100_evo_simulation.restype.sequences WAG_20x20.IRM.Q-star.neg_diag WAG_20x20.frequencies
	egrep -v '(^3|^New|^Orig_seq_1)' $< | sed '/^\s*$$/d' | enumerate_rows >$@.tmp_orig ; \
	printf "$(RESTYPE_STATES)\n"| tr "," "\t" >$@.tmp_freq ; \
	cat $^3 | tr "," "\t" >> $@.tmp_freq ; touch $@.tmp_dist ; \
	egrep -v '(^3|^Orig_seq)' $< | sed '/^\s*$$/d' | enumerate_rows >$@.tmp_new ; \
	awk '{print; getline < "$@.tmp_new"; print}' $@.tmp_orig \
	| repeat_group_pipe -s '\
		cut -f 2,3 > $@.tmp_align ; cut -f 1 $@.tmp_align | unhead >> $@.tmp_id ;\
		../../local/src/ML_d_est.py $^2 $@.tmp_freq $@.tmp_align >>$@.tmp_dist ; \	
	' 1 ; paste $@.tmp_id $@.tmp_dist | cut -f 1,3 | sed 's/^New_seq-//g' >$@ ; rm $@.tmp*
# estimate branch lengths using nlm and the WAG+F model, frequecies are calculate for each pair of simulated sequences
sequence%x100_evo_simulation.rotamer.rotasequences.ML_est_nlm_20x20_WAG-F.distances: sequence%x100_evo_simulation.rotamer.masked_rotasequences WAG_20x20.IRM.Q-star.neg_diag
	egrep -v '(^3|^New|^Orig_seq_1)' $< | sed '/^\s*$$/d' | enumerate_rows >$@.tmp_orig ; \
	touch $@.tmp_dist ; \
	egrep -v '(^3|^Orig_seq)' $< | sed '/^\s*$$/d' | enumerate_rows >$@.tmp_new ; \
	awk '{print; getline < "$@.tmp_new"; print}' $@.tmp_orig \
	| repeat_group_pipe -s '\
		cut -f 2,3 > $@.tmp_align ; cut -f 1 $@.tmp_align | unhead >> $@.tmp_id ; \
		cut -f2 $@.tmp_align | ../../local/src/seq2freqs.py -s $(RESTYPE_STATES) >$@.tmp_freq ; \
		../../local/src/ML_d_est.py $^2 $@.tmp_freq $@.tmp_align >>$@.tmp_dist ; \
	' 1 ; paste $@.tmp_id $@.tmp_dist | cut -f 1,3 | sed 's/^New_seq-//g' >$@ ; rm $@.tmp*
# simulate under 20x20, analize under WAG-F
sequence%x100_evo_simulation.restype.sequences.ML_est_nlm_20x20_WAG-F.distances:  sequence%x100_evo_simulation.restype.sequences WAG_20x20.IRM.Q-star.neg_diag
	egrep -v '(^3|^New|^Orig_seq_1)' $< | sed '/^\s*$$/d' | enumerate_rows >$@.tmp_orig ; \
	touch $@.tmp_dist ; \
	egrep -v '(^3|^Orig_seq)' $< | sed '/^\s*$$/d' | enumerate_rows >$@.tmp_new ; \
	awk '{print; getline < "$@.tmp_new"; print}' $@.tmp_orig \
	| repeat_group_pipe -s '\
		cut -f 2,3 > $@.tmp_align ; cut -f 1 $@.tmp_align | unhead >> $@.tmp_id ; \
		cut -f2 $@.tmp_align | ../../local/src/seq2freqs.py -s $(RESTYPE_STATES) >$@.tmp_freq ; \
		../../local/src/ML_d_est.py $^2 $@.tmp_freq $@.tmp_align >>$@.tmp_dist ; \
	' 1 ; paste $@.tmp_id $@.tmp_dist | cut -f 1,3 | sed 's/^New_seq-//g' >$@ ; rm $@.tmp*
# simulate under 20x20, analize under WAG with very skewed frequencies
sequence%x100_evo_simulation.restype.sequences.ML_est_nlm_20x20_WAG-F-WRONG.distances: sequence%x100_evo_simulation.restype.sequences WAG_20x20.IRM.Q-star.neg_diag
	egrep -v '(^3|^New|^Orig_seq_1)' $< | sed '/^\s*$$/d' | enumerate_rows >$@.tmp_orig ; \
	touch $@.tmp_dist ; \
	egrep -v '(^3|^Orig_seq)' $< | sed '/^\s*$$/d' | enumerate_rows >$@.tmp_new ; \
	awk '{print; getline < "$@.tmp_new"; print}' $@.tmp_orig \
	| repeat_group_pipe -s '\
		cut -f 2,3 > $@.tmp_align ; cut -f 1 $@.tmp_align | unhead >> $@.tmp_id ; \
		cut -f2 $@.tmp_align | ../../local/src/seq2freqs.py -w -s $(RESTYPE_STATES) >$@.tmp_freq ; \
		../../local/src/ML_d_est.py $^2 $@.tmp_freq $@.tmp_align >>$@.tmp_dist ; \
	' 1 ; paste $@.tmp_id $@.tmp_dist | cut -f 1,3 | sed 's/^New_seq-//g' >$@ ; rm $@.tmp*

# infinite sequence
sequenceINFx100_evo_simulation.rotamer.rotasequences.ML_est_nlm_20x20_WAG.distances: sequence100x100_evo_simulation.rotamer.masked_rotasequences WAG_20x20.IRM.Q-star.neg_diag WAG_20x20.frequencies
	egrep -v '(^3|^New|^Orig_seq_1)' $< | sed '/^\s*$$/d' | enumerate_rows >$@.tmp_orig ; \
	printf "$(RESTYPE_STATES)\n"| tr "," "\t" >$@.tmp_freq ; \
	cat $^3 | tr "," "\t" >> $@.tmp_freq ; touch $@.tmp_dist ; \
	egrep -v '(^3|^Orig_seq)' $< | sed '/^\s*$$/d' | enumerate_rows >$@.tmp_new ; \
	awk '{print; getline < "$@.tmp_new"; print}' $@.tmp_orig \
	| repeat_group_pipe -s '\
		cut -f 2,3 > $@.tmp_align ; cut -f 1 $@.tmp_align | unhead >> $@.tmp_id ;\
		../../local/src/ML_d_est.py -I $$(tail -n1  $@.tmp_id | tr "-" "\t" | cut -f2)  $^2 $@.tmp_freq >> $@.tmp_dist ; \	
	' 1 ; paste $@.tmp_id $@.tmp_dist | cut -f 1,3 | sed 's/^New_seq-//g' >$@ ; rm $@.tmp*
sequenceINFx100_evo_simulation.rotamer.rotasequences.ML_est_nlm_20x20_WAG-F.distances: sequence100x100_evo_simulation.rotamer.masked_rotasequences WAG_20x20.IRM.Q-star.neg_diag WAG_20x20.frequencies
	egrep -v '(^3|^New|^Orig_seq_1)' $< | sed '/^\s*$$/d' | enumerate_rows >$@.tmp_orig ; \
	printf "$(RESTYPE_STATES)\n"| tr "," "\t" >$@.tmp_freq ; touch $@.tmp_dist ; \
	cat $^3 | tr "," "\t" >> $@.tmp_freq ; \
	egrep -v '(^3|^Orig_seq)' $< | sed '/^\s*$$/d' | enumerate_rows >$@.tmp_new ; \
	awk '{print; getline < "$@.tmp_new"; print}' $@.tmp_orig \
	| repeat_group_pipe -s '\
		cut -f 2,3 > $@.tmp_align ; cut -f 1 $@.tmp_align | unhead >> $@.tmp_id ; \
		cut -f2 $@.tmp_align | ../../local/src/seq2freqs.py -s $(RESTYPE_STATES) >$@.tmp_freq ; \
		../../local/src/ML_d_est.py -I $$(tail -n1  $@.tmp_id | tr "-" "\t" | cut -f2)  $^2 $@.tmp_freq >> $@.tmp_dist ; \
	' 1 ; paste $@.tmp_id $@.tmp_dist | cut -f 1,3 | sed 's/^New_seq-//g' >$@ ; rm $@.tmp*


# estimate branch lengths using nlm and the LG model
sequence%x100_evo_simulation.rotamer.rotasequences.ML_est_nlm_20x20_LG.distances: sequence%x100_evo_simulation.rotamer.masked_rotasequences LG_20x20.IRM.Q-star.neg_diag LG_20x20.frequencies
	egrep -v '(^3|^New|^Orig_seq_1)' $< | sed '/^\s*$$/d' | enumerate_rows >$@.tmp_orig ; \
	printf  "$(RESTYPE_STATES)\n" | tr "," "\t" >$@.tmp_freq ; \ 
	cat $^3 | tr "," "\t" >> $@.tmp_freq ; touch $@.tmp_dist ; \
	egrep -v '(^3|^Orig_seq)' $< | sed '/^\s*$$/d' | enumerate_rows >$@.tmp_new ; \
	awk '{print; getline < "$@.tmp_new"; print}' $@.tmp_orig \
	| repeat_group_pipe -s '\
		cut -f 2,3 > $@.tmp_align ; cut -f 1 $@.tmp_align | unhead >> $@.tmp_id ;\
		../../local/src/ML_d_est.py $^2 $@.tmp_freq $@.tmp_align >>$@.tmp_dist ; \	
	' 1 ; paste $@.tmp_id $@.tmp_dist | cut -f 1,3 | sed 's/^New_seq-//g' >$@ ; rm $@.tmp*
# infinite sequence
sequenceINFx100_evo_simulation.rotamer.rotasequences.ML_est_nlm_20x20_LG.distances: sequence100x100_evo_simulation.rotamer.masked_rotasequences LG_20x20.IRM.Q-star.neg_diag LG_20x20.frequencies
	egrep -v '(^3|^New|^Orig_seq_1)' $< | sed '/^\s*$$/d' | enumerate_rows >$@.tmp_orig ; \
	printf  "$(RESTYPE_STATES)\n" | tr "," "\t" >$@.tmp_freq ; \ 
	cat $^3 | tr "," "\t" >> $@.tmp_freq ; touch $@.tmp_dist ; \
	egrep -v '(^3|^Orig_seq)' $< | sed '/^\s*$$/d' | enumerate_rows >$@.tmp_new ; \
	awk '{print; getline < "$@.tmp_new"; print}' $@.tmp_orig \
	| repeat_group_pipe -s '\
		cut -f 2,3 > $@.tmp_align ; cut -f 1 $@.tmp_align | unhead >> $@.tmp_id ;\
		../../local/src/ML_d_est.py -I $$(tail -n1  $@.tmp_id | tr "-" "\t" | cut -f2)  $^2 $@.tmp_freq >>$@.tmp_dist ; \	
	' 1 ; paste $@.tmp_id $@.tmp_dist | cut -f 1,3 | sed 's/^New_seq-//g' >$@ ; rm $@.tmp*

# estimate branch lengths using nlm and the original Dayhoff  model
sequence%x100_evo_simulation.rotamer.rotasequences.ML_est_nlm_20x20_DAYHOFF.distances: sequence%x100_evo_simulation.rotamer.masked_rotasequences DAYHOFF_20x20.IRM.Q-star.neg_diag DAYHOFF_20x20.frequencies
	egrep -v '(^3|^New|^Orig_seq_1)' $< | sed '/^\s*$$/d' | enumerate_rows >$@.tmp_orig ; \
	printf  "$(RESTYPE_STATES)\n" | tr "," "\t" >$@.tmp_freq ; \ 
	cat $^3 | tr "," "\t" >> $@.tmp_freq ; touch $@.tmp_dist ; \
	egrep -v '(^3|^Orig_seq)' $< | sed '/^\s*$$/d' | enumerate_rows >$@.tmp_new ; \
	awk '{print; getline < "$@.tmp_new"; print}' $@.tmp_orig \
	| repeat_group_pipe -s '\
		cut -f 2,3 > $@.tmp_align ; cut -f 1 $@.tmp_align | unhead >> $@.tmp_id ;\
		../../local/src/ML_d_est.py $^2 $@.tmp_freq $@.tmp_align >>$@.tmp_dist ; \	
	' 1 ; paste $@.tmp_id $@.tmp_dist | cut -f 1,3 | sed 's/^New_seq-//g' >$@ ; rm $@.tmp*
# infinite sequence
sequenceINFx100_evo_simulation.rotamer.rotasequences.ML_est_nlm_20x20_DAYHOFF.distances: sequence100x100_evo_simulation.rotamer.masked_rotasequences DAYHOFF_20x20.IRM.Q-star.neg_diag DAYHOFF_20x20.frequencies
	egrep -v '(^3|^New|^Orig_seq_1)' $< | sed '/^\s*$$/d' | enumerate_rows >$@.tmp_orig ; \
	printf  "$(RESTYPE_STATES)\n" | tr "," "\t" >$@.tmp_freq ; \
	cat $^3 | tr "," "\t" >> $@.tmp_freq ; touch $@.tmp_dist ; \
	egrep -v '(^3|^Orig_seq)' $< | sed '/^\s*$$/d' | enumerate_rows >$@.tmp_new ; \
	awk '{print; getline < "$@.tmp_new"; print}' $@.tmp_orig \
	| repeat_group_pipe -s '\
		cut -f 2,3 > $@.tmp_align ; cut -f 1 $@.tmp_align | unhead >> $@.tmp_id ;\
		../../local/src/ML_d_est.py -I $$(tail -n1  $@.tmp_id | tr "-" "\t" | cut -f2)  $^2 $@.tmp_freq >> $@.tmp_dist ; \	
	' 1 ; paste $@.tmp_id $@.tmp_dist | cut -f 1,3 | sed 's/^New_seq-//g' >$@ ; rm $@.tmp*

# aggregate by model across different sequence lengths
ALL_sequence_x100_evo_simulation.rotamer.rotasequences.ML_est_nlm_%.distances: $(addsuffix x100_evo_simulation.rotamer.rotasequences.ML_est_nlm_%.distances, $(addprefix sequence, $(SEQ_LEN))) 
	for i in $^; do printf ">$$i" | sed 's/x100_.*/\n/g; s/sequence//g' >>$@.tmp ; cat $$i | tr "-" "\t" | cut -f1,3 | bsort -k1,1 >>$@.tmp; done ; \ 
	cat $@.tmp | fasta2tab >$@ ; rm $@.tmp

.META: ALL_sequence_x100_evo_simulation.rotamer.rotasequences.ML_est_nlm_*.distances
	1	seq_len # i.e. N
	2	true_dist # used in simulation
	3	ML_est_dist

# plot bias over sequence length
ALL_sequence_x100_evo_simulation.rotamer.rotasequences.ML_est_nlm_%.distances.bias.pdf: ALL_sequence_x100_evo_simulation.rotamer.rotasequences.ML_est_nlm_%.distances
	bawk '{print $$seq_len, $$true_dist, $$ML_est_dist - $$true_dist}' $< | ../../local/src/ML_est_plot_bias.py -o $@




#------------LIBPLL IMPLEMENTATION-----------------------------------------------------------------------------------------------------------

# generate a map for libpll. This translated ASCII symbols into a positive integer number. This number directly dictates how the CLV for a particular state is going to be set. 
55_states_map: rotamers_2_1-character.dict
	cut -f3 $< | ../../local/src/create_map.py >$@
# generate a map for libpll that handles rotamer ambiguity (i.e. AA sequences can be analysed using a 55x55 model)
55_states_ambiguity_map: rotamers_2_1-character.dict rotamers_ambiguity.dict
	cut -f3 $< | ../../local/src/create_map.py -a $^2 >$@
# generate a PHYLIP parsing map for libpll, this sets legal, fatal and stripped symbols.
55_states_PHYLIP_parsing-map: rotamers_2_1-character.dict rotamers_ambiguity.dict
	cut -f 3 $< >$@.tmp ; cut -f 1 $^2 >>$@.tmp ; \ 
	cat $@.tmp | ../../local/src/create_map.py -p >$@ ;\
	rm $@.tmp

# generate an array containing the lower triangular scaled exchangeabilities, ordered column-wise
55_states_rates: rotamers_adj_count_matrix.merged.Dayhoff_norm.exchangeability.Q-star
	printf "const double pll_rates_55x55[1485] =\n {\n" >$@ ; \
	cut -f 2- $< | unhead | awk '{for(i=NR;i<=NF;i++) $$i="";print}' | unhead | tr -s " " "\t" \
	| transpose | tr "\t" "," | sed 's/^,*//g' | tr "\n" "," | sed 's/,$$//g' | sed -r 's/([0-9.e-]*,){25}/&\n/g' \
	| awk '{print "  ", $$0}' | sed 's/,/& /g' >>$@ ; \
	printf " };\n" >>$@
20_states_rates: restype_adj_count_matrix.merged.exchangeability.Q-star
	printf "const double pll_rates_20x20[190] =\n {\n" >$@ ; \
	cut -f 2- $< | unhead | awk '{for(i=NR;i<=NF;i++) $$i="";print}' | unhead | tr -s " " "\t" \
	| transpose | tr "\t" "," | sed 's/^,*//g' | tr "\n" "," | sed 's/,$$//g' | sed -r 's/([0-9.e-]*,){25}/&\n/g' \
	| awk '{print "  ", $$0}' | sed 's/,/& /g' >>$@ ; \
	printf " };\n" >>$@

rotamers%count_matrix.merged.Dayhoff_norm.exchangeability.superQ-star.rates: rotamers%matrix.merged.Dayhoff_norm.exchangeability.superQ-star 
	printf "const double pll_rates_55x55[1485] =\n {\n" >$@ ; \
	cut -f 2- $< | unhead | awk '{for(i=NR;i<=NF;i++) $$i="";print}' | unhead | tr -s " " "\t" \
	| transpose | tr "\t" "," | sed 's/^,*//g' | tr "\n" "," | sed 's/,$$//g' | sed -r 's/([0-9.e-]*,){25}/&\n/g' \
	| awk '{print "  ", $$0}' | sed 's/,/& /g' >>$@ ; \
	printf " };\n" >>$@

restype%matrix.merged.exchangeability.Q-star.rates: restype%matrix.merged.exchangeability.Q-star
	printf "const double pll_rates_20x20[190] =\n {\n" >$@ ; \
	cut -f 2- $< | unhead | awk '{for(i=NR;i<=NF;i++) $$i="";print}' | unhead | tr -s " " "\t" \
	| transpose | tr "\t" "," | sed 's/^,*//g' | tr "\n" "," | sed 's/,$$//g' | sed -r 's/([0-9.e-]*,){25}/&\n/g' \
	| awk '{print "  ", $$0}' | sed 's/,/& /g' >>$@ ; \
	printf " };\n" >>$@

# same order but in the format accepted by raxml-ng (i.e. plain space-separated numbers)
%_states_rates.ng: %_states_rates
	cat $< | unhead -n 2 | untail | tr -d ',' >$@

# generate a mock exchangeability matrix with uniform rates for testing
55MOCK_states_rates: 55_states_rates
	printf "const double pll_MOCK-rates_55x55[1485] =\n {\n" >$@ ; \
	cat $< | unhead -n 2 | untail | sed 's/[0-9]*\.[0-9]*/0.09274/g' >>$@ ; \ 
	printf " };\n" >>$@

#generate an array containing the frequencies of the 55x55 model ordered alphabetically (ALA, ARG1 ... VAL3)
rotamers%matrix.merged.Dayhoff_norm_states_freqs: rotamers%matrix.merged.Dayhoff_norm.frequencies
	printf "const double 55_states_freqs[55] =\n {\n" >$@ ; \
	unhead $< | tr "\t" "," |  sed -r 's/([0-9.e-]*,){5}/&\n/g' | awk '{print "  ", $$0}' | sed 's/,/& /g' >>$@ ; \ 
	printf " };\n" >>$@
restype%matrix.merged_states_freqs: restype%matrix.merged.frequencies 
	printf "const double 20_states_freqs[20] =\n {\n" >$@ ; \
	unhead $< | tr "\t" "," |  sed -r 's/([0-9.e-]*,){5}/&\n/g' | awk '{print "  ", $$0}' | sed 's/,/& /g' >>$@ ; \ 
	printf " };\n" >>$@

# same order but in the format accepted by raxml-ng (i.e. plain space-separated numbers)
%_states_freqs.ng: %_states_freqs
	cat $< | unhead -n 2 | untail | tr -d ',' >$@

# compare the original simulated tree vs inferred trees
ALIG_LEN?=200
MODEL?=55x55#LG, LG4X
REPLICATES?=100
TAXA?=16#32, 64, 128
BRANCHES_LEN?=_short-branches#_intermediate-branches, ''
UNIFORM?=#uniform-
# generate a random Newick tree of specified size, default branch length is in range 0.01,2
%_leaves.newick: 
	../../local/src/tree_gen.py $* >$@

# represent any newick tre s horizontal dendrogram
%.newick.pdf: %.newick
	../../local/src/vis_tree.py $< >$@

# trees with shorter branches
%_leaves_short-branches.newick:
	../../local/src/tree_gen.py -b 0.01,0.5 $* >$@

# tree with intermediate branches
%_leaves_intermediate-branches.newick:
	../../local/src/tree_gen.py -b 0.01,1 $* >$@

# tree with uniform branches
%_leaves_uniform-branches.newick:
	../../local/src/tree_gen.py -b 0.5,0.5 $* >$@

SCALING_FACTORS?=0.1,0.2,0.5,0.7,1.2,1.5,1.7,2,2.5,3
GAMMA?=1

# scale a tree and analyse eanch alignment replicate from the scaled tree using the 55x55 model, then compare the estimated tree to the scaled tree
alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55.1char_rotasequence.RF_compare.pre: %_leaves_short-branches.newick rotamers_adj_count_matrix.merged.Dayhoff_norm.IRM.Q-star rotamers$(MATRIX_TYPE)matrix.merged.Dayhoff_norm.frequencies 55_states_rates.ng rotamers_adj_count_matrix.merged.Dayhoff_norm_states_freqs.ng
	mkdir -p $@ ; \
	cat $^3 >$@/freqs.tmp ; \
	for j in  $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
		cat $< | ../../local/src/scale_tree.py $$(printf $$j) >$@/scaled_$$(printf $$j).newick.tmp ; \
		cut -f2- $^2  | unhead | ../../local/src/tree_sim.py -l $(shell printf $(ALIG_LEN)) -r $(shell printf $(REPLICATES)) $@/scaled_$$(printf $$j).newick.tmp  $(ROTA_AS_1CHAR) -F $@/freqs.tmp -o $@/alignment_$$(printf $$j).tmp; \ 
	done ; \
	for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
		for i in $$(ls $$(printf $@/alignment_$$(printf $$j).tmp_\*)) ; do \
			cat $$(printf $$i) | ../../local/src/PHYLIP_codon-2-1.py -m >$$(printf $$i".masked") ; \
			bsub -e err -o out -M 500 -R "rusage[mem=500]" raxml-ng --opt-model off --msa $$(printf $$i) --model MULTI55_GTR{$^4}+FU{$^5}+M{ABCDEFGHIJKLMNOPQRSTUVWYXZabcdefghijklmnopqrstuvwxyz012} --seed 1 --threads 2 ; \
			done ; \
		done
#use the superscaled 55x55 model
alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled.1char_rotasequence.RF_compare.pre: %_leaves_short-branches.newick rotamers$(MATRIX_TYPE)matrix.merged.Dayhoff_norm.IRM.superQ-star rotamers$(MATRIX_TYPE)matrix.merged.Dayhoff_norm.frequencies rotamers$(MATRIX_TYPE)matrix.merged.Dayhoff_norm.exchangeability.superQ-star.rates.ng rotamers$(MATRIX_TYPE)matrix.merged.Dayhoff_norm_states_freqs.ng
	mkdir -p $@ ; \
	cat $^3 >$@/freqs.tmp ; \
	for j in  $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
		cat $< | ../../local/src/scale_tree.py $$(printf $$j) >$@/scaled_$$(printf $$j).newick.tmp ; \
		cut -f2- $^2  | unhead | ../../local/src/tree_sim.py -l $(shell printf $(ALIG_LEN)) -r $(shell printf $(REPLICATES)) $@/scaled_$$(printf $$j).newick.tmp  $(ROTA_AS_1CHAR) -F $@/freqs.tmp -o $@/alignment_$$(printf $$j).tmp; \ 
	done ; \
	for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
		for i in $$(ls $$(printf $@/alignment_$$(printf $$j).tmp_\*)) ; do \
			cat $$(printf $$i) | ../../local/src/PHYLIP_codon-2-1.py -m >$$(printf $$i".masked") ; \
			bsub -e err -o out -m ebi6-068 -n 1 -M 500 -R "rusage[mem=500]" raxml-ng --opt-model off --msa $$(printf $$i) --model MULTI55_GTR{$^4}+FU{$^5}+M{ABCDEFGHIJKLMNOPQRSTUVWYXZabcdefghijklmnopqrstuvwxyz012} --seed 1 --threads 1 --site-repeats on; \
			done ; \
		done
#----------GAMMA SIMULATION--------------------------------------------------------------------------------------
#use the superscaled 55x55 model and a gamma distribution to simulate and analyse
alignment%x$(ALIG_LEN)x$(REPLICATES)_$(GAMMA)_scaled-branches_evo_simulation.55x55_superscaled_plusG.1char_rotasequence.RF_compare.pre: %_leaves_short-branches.newick rotamers$(MATRIX_TYPE)matrix.merged.Dayhoff_norm.IRM.superQ-star rotamers$(MATRIX_TYPE)matrix.merged.Dayhoff_norm.frequencies rotamers$(MATRIX_TYPE)matrix.merged.Dayhoff_norm.exchangeability.superQ-star.rates.ng rotamers$(MATRIX_TYPE)matrix.merged.Dayhoff_norm_states_freqs.ng
	mkdir -p $@ ; \
	cat $^3 >$@/freqs.tmp ; \
	for j in  $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
		cat $< | ../../local/src/scale_tree.py $$(printf $$j) >$@/scaled_$$(printf $$j).newick.tmp ; \
		cut -f2- $^2  | unhead | ../../local/src/tree_sim.py -l $(shell printf $(ALIG_LEN)) -r $(shell printf $(REPLICATES)) -g $(shell printf $(GAMMA)) $@/scaled_$$(printf $$j).newick.tmp  $(ROTA_AS_1CHAR) -F $@/freqs.tmp -o $@/alignment_$$(printf $$j).tmp; \ 
	done ; \
	for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
		for i in $$(ls $$(printf $@/alignment_$$(printf $$j).tmp_\*)) ; do \
			cat $$(printf $$i) | ../../local/src/PHYLIP_codon-2-1.py -m >$$(printf $$i".masked") ; \
			bsub -e err -o out -n 2 -M 500 -R "rusage[mem=500]" raxml-ng  --msa $$(printf $$i) --model MULTI55_GTR{$^4}+FU{$^5}+M{ABCDEFGHIJKLMNOPQRSTUVWYXZabcdefghijklmnopqrstuvwxyz012}+G --seed 1 --threads 2 ; \
			done ; \
		done
# analyse alignments simulated using 55x55plusG with 55x55 (no G)
alignment%x$(ALIG_LEN)x$(REPLICATES)_$(GAMMA)_scaled-branches_evo_simulation.55x55_superscaled_plusGv55x55.1char_rotasequence.RF_compare.pre: alignment%x$(ALIG_LEN)x$(REPLICATES)_$(GAMMA)_scaled-branches_evo_simulation.55x55_superscaled_plusG.1char_rotasequence.RF_compare.pre rotamers$(MATRIX_TYPE)matrix.merged.Dayhoff_norm.exchangeability.superQ-star.rates.ng rotamers$(MATRIX_TYPE)matrix.merged.Dayhoff_norm_states_freqs.ng
	for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
		for i in $$(ls $$(printf $</alignment_$$(printf $$j).tmp_\*) | grep -v masked | grep -v raxml) ; do \
			bsub -e err -o out -n 2 -M 500 -R "rusage[mem=500]" raxml-ng  --msa $$(printf $$i) --model MULTI55_GTR{$^2}+FU{$^3}+M{ABCDEFGHIJKLMNOPQRSTUVWYXZabcdefghijklmnopqrstuvwxyz012} --seed 1 --threads 2 --prefix $$(printf $$i".55x55noG") ; \
			done ; \
		done
# analyse alignments simulated using 55x55plusG with 20x20+G
alignment%x$(ALIG_LEN)x$(REPLICATES)_$(GAMMA)_scaled-branches_evo_simulation.55x55_superscaled_plusGv20x20plusG.1char_rotasequence.RF_compare.pre: alignment%x$(ALIG_LEN)x$(REPLICATES)_$(GAMMA)_scaled-branches_evo_simulation.55x55_superscaled_plusG.1char_rotasequence.RF_compare.pre  restype$(MATRIX_TYPE)matrix.merged.exchangeability.Q-star.rates.ng restype$(MATRIX_TYPE)matrix.merged_states_freqs.ng
	for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
		for i in $$(ls $$(printf $</alignment_$$(printf $$j).tmp_\*.masked)) ; do \
			bsub -e err -o out -n 2 -M 500 -R "rusage[mem=500]" raxml-ng  --msa $$(printf $$i) --model MULTI20_GTR{$^2}+FU{$^3}+M{ARNDCQEGHILKMFPSTWYV}+G --seed 1 --threads 2 --prefix $$(printf $$i".masked_20x20plusG") ; \
			done ; \
		done
# analyse alignments simulated using 55x55plusG with LG+G
alignment%x$(ALIG_LEN)x$(REPLICATES)_$(GAMMA)_scaled-branches_evo_simulation.55x55_superscaled_plusGvLGplusG.1char_rotasequence.RF_compare.pre: alignment%x$(ALIG_LEN)x$(REPLICATES)_$(GAMMA)_scaled-branches_evo_simulation.55x55_superscaled_plusG.1char_rotasequence.RF_compare.pre
	for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
		for i in $$(ls $$(printf $</alignment_$$(printf $$j).tmp_\*.masked)) ; do \
			bsub -e err -o out -n 2 -M 500 -R "rusage[mem=500]" raxml-ng  --msa $$(printf $$i) --model LG+G -seed 1 --threads 2 --prefix $$(printf $$i".masked_LGplusG") ; \
			done ; \
		done
# analyse alignments simulated using 55x55plusG with LGbyfreq-exp (55x55 freqs)+ G 
alignment%x$(ALIG_LEN)x$(REPLICATES)_$(GAMMA)_scaled-branches_evo_simulation.55x55_superscaled_plusGvLGbyfreq-exp55x55freqsplusG.1char_rotasequence.RF_compare.pre: alignment%x$(ALIG_LEN)x$(REPLICATES)_$(GAMMA)_scaled-branches_evo_simulation.55x55_superscaled_plusG.1char_rotasequence.RF_compare.pre LG_55x55.byfreq-exp.exchangeability.superQ-star.rates.ng  rotamers_adj_count_matrix.merged.Dayhoff_norm_states_freqs.ng
	for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
		for i in $$(ls $$(printf $</alignment_$$(printf $$j).tmp_\*) | grep -v masked | grep -v raxml) ; do \
			bsub -e err -o out -n 2 -M 500 -R "rusage[mem=500]" raxml-ng  --msa $$(printf $$i) --model MULTI55_GTR{$^2}+FU{$^3}+M{ABCDEFGHIJKLMNOPQRSTUVWYXZabcdefghijklmnopqrstuvwxyz012}+G --seed 1 --threads 2 --prefix $$(printf $$i".LGbyfreq-exp55x55freqsplusG") ; \
		done ; \
	done
#check whether raxml is recovering the correct value for alpha
alignment%x$(ALIG_LEN)x$(REPLICATES)_$(GAMMA)_scaled-branches_evo_simulation.55x55_superscaled_plusGvALL.1char_rotasequence.alpha_compare.pre: alignment%x$(ALIG_LEN)x$(REPLICATES)_$(GAMMA)_scaled-branches_evo_simulation.55x55_superscaled_plusG.1char_rotasequence.RF_compare.pre
	for i in $$(ls $$(printf $</alignment_\*.log) | grep -v masked | grep -v LG | grep -v 20x20 | grep -v 55x55noG) ; do \
		grep 'Rate heterogeneity:' $$(printf $$i)  >>$@.tmp.ORIG ; \
	done; \
	for m in 55x55noG 20x20plusG LGplusG LGbyfreq-exp55x55freqsplusG; do \
		grep 'Rate heterogeneity:' $</*$$(printf $$m)*log >>$@.tmp.$$(printf $$m) ; \
	done; \
	paste <(cat $@.tmp.ORIG) <(cat $@.tmp.55x55noG) <(cat $@.tmp.20x20plusG) <(cat $@.tmp.LGplusG) <(cat $@.tmp.LGbyfreq-exp55x55freqsplusG) >$@ ; \
	rm $@.tmp.*

#--------------------------------------------------------------------------------------------
# use the superscaled 55x55 model AND the scaled tree as a guide for inference
# this allows to mantain the internal node names to identify branches and compare them
alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_AND_guide-tree.1char_rotasequence.RF_compare.pre: %_leaves_short-branches.newick rotamers_adj_count_matrix.merged.Dayhoff_norm.IRM.superQ-star rotamers$(MATRIX_TYPE)matrix.merged.Dayhoff_norm.frequencies rotamers_adj_count_matrix.merged.Dayhoff_norm.exchangeability.superQ-star.rates.ng rotamers_adj_count_matrix.merged.Dayhoff_norm_states_freqs.ng
	mkdir -p $@ ; \
	cat $^3 >$@/freqs.tmp ; \
	for j in  $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
		cat $< | ../../local/src/scale_tree.py $$(printf $$j) >$@/scaled_$$(printf $$j).newick.tmp ; \
		cut -f2- $^2  | unhead | ../../local/src/tree_sim.py -l $(shell printf $(ALIG_LEN)) -r $(shell printf $(REPLICATES)) $@/scaled_$$(printf $$j).newick.tmp  $(ROTA_AS_1CHAR) -F $@/freqs.tmp -o $@/alignment_$$(printf $$j).tmp; \
	done ; \
	for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
		for i in $$(ls $$(printf $@/alignment_$$(printf $$j).tmp_\*)) ; do \
			cat $$(printf $$i) | ../../local/src/PHYLIP_codon-2-1.py -m >$$(printf $$i".masked") ; \
			bsub -e err -o out -n 2 -M 500 -R "rusage[mem=500]" raxml-ng --tree $@/scaled_$$(printf $$j).newick.tmp --opt-model off --msa $$(printf $$i) --model MULTI55_GTR{$^4}+FU{$^5}+M{ABCDEFGHIJKLMNOPQRSTUVWYXZabcdefghijklmnopqrstuvwxyz012} --seed 1 --threads 2 ; \
			done ; \
		done

# scale a tree and analyse eanch alignment replicate from the scaled tree using the 20x20 model, then compare the estimated tree to the scaled tree
alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.20x20.1char_AAsequence.RF_compare.pre: %_leaves_short-branches.newick restype_adj_count_matrix.merged.IRM.Q-star restype_adj_count_matrix.merged.frequencies restype$(MATRIX_TYPE)matrix.merged.exchangeability.Q-star.rates.ng restype$(MATRIX_TYPE)matrix.merged_states_freqs.ng 
	mkdir $@ ; \
	cat $^3 | unhead >$@/freqs.tmp ; \
	for j in  $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
		cat $< | ../../local/src/scale_tree.py $$(printf $$j) >$@/scaled_$$(printf $$j).newick.tmp ; \
		cut -f2- $^2  | unhead | ../../local/src/tree_sim.py -l $(shell printf $(ALIG_LEN)) -r $(shell printf $(REPLICATES)) $@/scaled_$$(printf $$j).newick.tmp  $(RES_AS_1CHAR) -F $@/freqs.tmp -o $@/alignment_$$(printf $$j).tmp; \ 
	done ; \
	for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
		for i in $$(ls $$(printf $@/alignment_$$(printf $$j).tmp_\*)) ; do \
			bsub -e err -o out -n 2 -M 500 -R "rusage[mem=500]" raxml-ng --opt-model off --msa $$(printf $$i) --model MULTI20_GTR{$^4}+FU{$^5}+M{ARNDCQEGHILKMFPSTWYV} --seed 1 --threads 2 ; \
			done ; \
		done
# uniform tree branches
alignment%x$(ALIG_LEN)x$(REPLICATES)_uniform-scaled-branches_evo_simulation.55x55.1char_rotasequence.RF_compare.pre: %_leaves_uniform-branches.newick rotamers_adj_count_matrix.merged.Dayhoff_norm.IRM.Q-star rotamers$(MATRIX_TYPE)matrix.merged.Dayhoff_norm.frequencies 55_states_rates.ng rotamers_adj_count_matrix.merged.Dayhoff_norm_states_freqs.ng
	mkdir $@ ; \
	cat $^3 | unhead >$@/freqs.tmp ; \
	for j in  $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
		cat $< | ../../local/src/scale_tree.py $$(printf $$j) >$@/scaled_$$(printf $$j).newick.tmp ; \
		cut -f2- $^2  | unhead | ../../local/src/tree_sim.py -l $(shell printf $(ALIG_LEN)) -r $(shell printf $(REPLICATES)) $@/scaled_$$(printf $$j).newick.tmp  $(ROTA_AS_1CHAR) -F $@/freqs.tmp -o $@/alignment_$$(printf $$j).tmp; \ 
	done ; \
	for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
		for i in $$(ls $$(printf $@/alignment_$$(printf $$j).tmp_\*)) ; do \
			cat $$(printf $$i) | ../../local/src/PHYLIP_codon-2-1.py -m >$$(printf $$i".masked") ; \
			bsub -e err -o out -n 2 -M 500 -R "rusage[mem=500]" raxml-ng --opt-model off --msa $$(printf $$i) --model MULTI55_GTR{$^4}+FU{$^5}+M{ABCDEFGHIJKLMNOPQRSTUVWYXZabcdefghijklmnopqrstuvwxyz012} --seed 1 --threads 2 ; \
			done ; \
		done

# analyse each (masked) alignment replicate from the scaled tree using the 20x20 model
alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55v20x20.RF_compare.pre: alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55.1char_rotasequence.RF_compare.pre restype$(MATRIX_TYPE)matrix.merged.exchangeability.Q-star.rates.ng restype$(MATRIX_TYPE)matrix.merged_states_freqs.ng
	for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
		for i in $$(ls $$(printf $</alignment_$$(printf $$j).tmp_\*.masked)) ; do \
			bsub -e err -o out -n 2 -M 500 -R "rusage[mem=500]" raxml-ng --opt-model off --msa $$(printf $$i) --model MULTI20_GTR{$^2}+FU{$^3}+M{ARNDCQEGHILKMFPSTWYV} --seed 1 --threads 2 --prefix $$(printf $$i".masked_20x20") ; \
			done ; \
		done
# uniform tree branches
alignment%x$(ALIG_LEN)x$(REPLICATES)_uniform-scaled-branches_evo_simulation.55x55v20x20.1char_rotasequence.RF_compare.pre: alignment%x$(ALIG_LEN)x$(REPLICATES)_uniform-scaled-branches_evo_simulation.55x55.1char_rotasequence.RF_compare.pre restype$(MATRIX_TYPE)matrix.merged.exchangeability.Q-star.rates.ng restype$(MATRIX_TYPE)matrix.merged_states_freqs.ng
	for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
		for i in $$(ls $$(printf $</alignment_$$(printf $$j).tmp_\*.masked)) ; do \
			bsub -e err -o out -n 2 -M 500 -R "rusage[mem=500]" raxml-ng --opt-model off --msa $$(printf $$i) --model MULTI20_GTR{$^2}+FU{$^3}+M{ARNDCQEGHILKMFPSTWYV} --seed 1 --threads 2 --prefix $$(printf $$i".masked_20x20") ; \
			done ; \
		done
#20x20 model vs 55x55_superscaled
alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledv20x20.1char_rotasequence.RF_compare.pre:  alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled.1char_rotasequence.RF_compare.pre restype$(MATRIX_TYPE)matrix.merged.exchangeability.Q-star.rates.ng restype$(MATRIX_TYPE)matrix.merged_states_freqs.ng
	for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
		for i in $$(ls $$(printf $</alignment_$$(printf $$j).tmp_\*.masked)) ; do \
			bsub -e err -o out -m ebi6-068 -n 1 -M 500 -R "rusage[mem=500]" raxml-ng --opt-model off --msa $$(printf $$i) --model MULTI20_GTR{$^2}+FU{$^3}+M{ARNDCQEGHILKMFPSTWYV} --seed 1 --threads 1 --site-repeats on --prefix $$(printf $$i".masked_20x20") ; \
			done ; \
		done
#20x20 model vs 55x55_superscaled using a guide-tree
alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_AND_guide-treev20x20.1char_rotasequence.RF_compare.pre: alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_AND_guide-tree.1char_rotasequence.RF_compare.pre restype$(MATRIX_TYPE)matrix.merged.exchangeability.Q-star.rates.ng restype$(MATRIX_TYPE)matrix.merged_states_freqs.ng
	for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
		for i in $$(ls $$(printf $</alignment_$$(printf $$j).tmp_\*.masked)) ; do \
			bsub -e err -o out -n 2 -M 500 -R "rusage[mem=500]" raxml-ng --tree $</scaled_$$(printf $$j).newick.tmp  --opt-model off --msa $$(printf $$i) --model MULTI20_GTR{$^2}+FU{$^3}+M{ARNDCQEGHILKMFPSTWYV} --seed 1 --threads 2 --prefix $$(printf $$i".masked_20x20") ; \
			done ; \
		done

# analyse each (masked) alignment replicate from the scaled tree using the LG model
alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55vLG.1char_rotasequence.RF_compare.pre: alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55.1char_rotasequence.RF_compare.pre
	for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
		for i in $$(ls $$(printf $</alignment_$$(printf $$j).tmp_\*.masked)) ; do \
			bsub -e err -o out -n 2 -M 500 -R "rusage[mem=500]" raxml-ng --opt-model off --msa $$(printf $$i) --model LG --seed 1 --threads 2 --prefix $$(printf $$i".masked_LG") ; \
			done ; \
		done
# the superscaled 55x55 vs LG
alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvLG.1char_rotasequence.RF_compare.pre: alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled.1char_rotasequence.RF_compare.pre
	for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
		for i in $$(ls $$(printf $</alignment_$$(printf $$j).tmp_\*.masked)) ; do \
			bsub -e err -o out -n 1 -M 500 -m ebi6-068 -R "rusage[mem=500]" raxml-ng --opt-model off --msa $$(printf $$i) --model LG --seed 1 --threads 1 --site-repeats on --prefix $$(printf $$i".masked_LG") ; \
			done ; \
		done
# the superscaled 55x55 vs LGplusF
alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvLGplusF.1char_rotasequence.RF_compare.pre: alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled.1char_rotasequence.RF_compare.pre
	for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
		for i in $$(ls $$(printf $</alignment_$$(printf $$j).tmp_\*.masked)) ; do \
			bsub -e err -o out -n 2 -M 500 -R "rusage[mem=500]" raxml-ng --msa $$(printf $$i) --model LG+FO --seed 1 --threads 2 --prefix $$(printf $$i".masked_LGplusF") ; \
			done ; \
		done

# the superscaled 55x55 vs LG using a guide-tree
alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_AND_guide-treevLG.1char_rotasequence.RF_compare.pre: alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_AND_guide-tree.1char_rotasequence.RF_compare.pre
	for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
		for i in $$(ls $$(printf $</alignment_$$(printf $$j).tmp_\*.masked)) ; do \
			bsub -e err -o out -n 2 -M 500 -R "rusage[mem=500]" raxml-ng --tree $</scaled_$$(printf $$j).newick.tmp --opt-model off --msa $$(printf $$i) --model LG --seed 1 --threads 2 --prefix $$(printf $$i".masked_LG") ; \
			done ; \
		done

# use he expanded version of LG in order to compare LL across 20-state and 55-states models
alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvLGexp.1char_rotasequence.RF_compare.pre: alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled.1char_rotasequence.RF_compare.pre LG_55x55.uniform-exp.exchangeability.superQ-star.rates.ng LG_55x55.uniform-exp_states_freqs.ng
	for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
		for i in $$(ls $$(printf $</alignment_$$(printf $$j).tmp_\*) | grep -v masked | grep -v raxml) ; do \
			bsub -e err -o out -n 2 -M 500 -R "rusage[mem=500]" raxml-ng --opt-model off --msa $$(printf $$i) --model MULTI55_GTR{$^2}+FU{$^3}+M{ABCDEFGHIJKLMNOPQRSTUVWYXZabcdefghijklmnopqrstuvwxyz012} --seed 1 --threads 2 --prefix $$(printf $$i".LGexp") ; \
			done ; \
		done
# the expanded version of LG using a guide-tree
alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_AND_guide-treevLGexp.1char_rotasequence.RF_compare.pre: alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_AND_guide-tree.1char_rotasequence.RF_compare.pre LG_55x55.uniform-exp.exchangeability.superQ-star.rates.ng LG_55x55.uniform-exp_states_freqs.ng
	for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
		for i in $$(ls $$(printf $</alignment_$$(printf $$j).tmp_\*) | grep -v masked | grep -v raxml) ; do \
			bsub -e err -o out -n 2 -M 500 -R "rusage[mem=500]" raxml-ng --tree $</scaled_$$(printf $$j).newick.tmp  --opt-model off --msa $$(printf $$i) --model  MULTI55_GTR{$^2}+FU{$^3}+M{ABCDEFGHIJKLMNOPQRSTUVWYXZabcdefghijklmnopqrstuvwxyz012} --seed 1 --threads 2 --prefix $$(printf $$i".LGexp") ; \
		done ; \
	done

# use this expanded version of LG and the 55x55 in order to compare LL across 20-state and 55-states models
# this accounts for the relative rarity of certain rotamers
alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvLGexp55x55freqs.1char_rotasequence.RF_compare.pre: alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled.1char_rotasequence.RF_compare.pre LG_55x55.uniform-exp.exchangeability.superQ-star.rates.ng rotamers_adj_count_matrix.merged.Dayhoff_norm_states_freqs.ng
	for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
		for i in $$(ls $$(printf $</alignment_$$(printf $$j).tmp_\*) | grep -v masked | grep -v raxml) ; do \
			bsub -e err -o out -n 2 -M 500 -R "rusage[mem=500]" raxml-ng --opt-model off --msa $$(printf $$i) --model MULTI55_GTR{$^2}+FU{$^3}+M{ABCDEFGHIJKLMNOPQRSTUVWYXZabcdefghijklmnopqrstuvwxyz012} --seed 1 --threads 2 --prefix $$(printf $$i".LGexp55x55freqs") ; \
			done ; \
		done

# use this expanded version of LG and the 55x55 in order to compare LL across 20-state and 55-states models
# this accounts for the relative rarity of certain rotamers
alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvLGbyfreq-exp55x55freqs.1char_rotasequence.RF_compare.pre: alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled.1char_rotasequence.RF_compare.pre LG_55x55.byfreq-exp.exchangeability.superQ-star.rates.ng  rotamers_adj_count_matrix.merged.Dayhoff_norm_states_freqs.ng
	for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
		for i in $$(ls $$(printf $</alignment_$$(printf $$j).tmp_\*) | grep -v masked | grep -v raxml) ; do \
			bsub -e err -o out -n 2 -M 500 -R "rusage[mem=500]" raxml-ng --opt-model off --msa $$(printf $$i) --model MULTI55_GTR{$^2}+FU{$^3}+M{ABCDEFGHIJKLMNOPQRSTUVWYXZabcdefghijklmnopqrstuvwxyz012} --seed 1 --threads 2 --prefix $$(printf $$i".LGbyfreq-exp55x55freqs") ; \
			done ; \
		done
#LGbyfreq-exp55x55freqs vs 55x55_superscaled using a guide-tree
alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_AND_guide-treevLGbyfreq-exp55x55freqs.1char_rotasequence.RF_compare.pre: alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_AND_guide-tree.1char_rotasequence.RF_compare.pre LG_55x55.byfreq-exp.exchangeability.superQ-star.rates.ng  rotamers_adj_count_matrix.merged.Dayhoff_norm_states_freqs.ng
	for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
		for i in $$(ls $$(printf $</alignment_$$(printf $$j).tmp_\*) | grep -v masked | grep -v raxml) ; do \
			bsub -e err -o out -n 2 -M 500 -R "rusage[mem=500]" raxml-ng --tree $</scaled_$$(printf $$j).newick.tmp  --opt-model off --msa $$(printf $$i) --model  MULTI55_GTR{$^2}+FU{$^3}+M{ABCDEFGHIJKLMNOPQRSTUVWYXZabcdefghijklmnopqrstuvwxyz012} --seed 1 --threads 2 --prefix $$(printf $$i".LGbyfreq-exp55x55freqs") ; \ 
			done ; \
		done

alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55vLGman.1char_rotasequence.RF_compare.pre: alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55.1char_rotasequence.RF_compare.pre LG_20x20.libpll.exchangeabilities.ng LG_20x20.libpll.frequencies.ng
	for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
		for i in $$(ls $$(printf $</alignment_$$(printf $$j).tmp_\*.masked)) ; do \
			bsub -n 2 -e err -o out -M 500 -R "rusage[mem=500]" raxml-ng --opt-model off --msa $$(printf $$i) --model MULTI20_GTR{$^2}+FU{$^3}+M{ARNDCQEGHILKMFPSTWYV} --seed 1 --threads 2 --prefix $$(printf $$i".masked_LGman") ; \
			done ; \
		done
alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.20x20vLG.1char_AAsequence.RF_compare.pre: alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.20x20.1char_AAsequence.RF_compare.pre
	for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
		for i in $$(ls $$(printf $</alignment_$$(printf $$j).tmp_\*| grep -v 'raxml' )) ; do \
			bsub -e err -o out -n 2 -M 500 -R "rusage[mem=500]" raxml-ng --opt-model off --msa $$(printf $$i) --model LG --seed 1 --threads 2 --prefix $$(printf $$i".LG") ; \
			done ; \
		done
# uniform tree branches
alignment%x$(ALIG_LEN)x$(REPLICATES)_uniform-scaled-branches_evo_simulation.55x55vLG.1char_rotasequence.RF_compare.pre: alignment%x$(ALIG_LEN)x$(REPLICATES)_uniform-scaled-branches_evo_simulation.55x55.1char_rotasequence.RF_compare.pre
	for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
		for i in $$(ls $$(printf $</alignment_$$(printf $$j).tmp_\*.masked)) ; do \
			bsub -e err -o out -n 2 -M 500 -R "rusage[mem=500]" raxml-ng --opt-model off --msa $$(printf $$i) --model LG --seed 1 --threads 2 --prefix $$(printf $$i".masked_LG") ; \
			done ; \
		done

#  use he expanded version of WAG in order to compare LL across 20-state and 55-states models
alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55vWAGexp.1char_rotasequence.RF_compare.pre: alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55.1char_rotasequence.RF_compare.pre WAG_55x55.expanded.exchangeabilities.ng WAG_55x55.expanded.frequencies.ng
	for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
		for i in $$(ls $$(printf $</alignment_$$(printf $$j).tmp_\*.masked)) ; do \
			bsub -e err -o out -n 2 -M 500 -R "rusage[mem=500]" raxml-ng --opt-model off --msa $$(printf $$i) --model MULTI55_GTR{$^2}+FU{$^3}+M{ABCDEFGHIJKLMNOPQRSTUVWYXZabcdefghijklmnopqrstuvwxyz012} --seed 1 --threads 2 --prefix $$(printf $$i".masked_20x20") ; \
			done ; \
		done

# analyse each (masked) alignment replicate from the scaled tree using the LG4X model
alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55vLG4X.1char_rotasequence.RF_compare.pre: alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55.1char_rotasequence.RF_compare.pre
	for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
		for i in $$(ls $$(printf $</alignment_$$(printf $$j).tmp_\*.masked)) ; do \
			bsub -e err -o out -n 2 -M 500 -R "rusage[mem=500]" raxml-ng --opt-model off --msa $$(printf $$i) --model LG4X --seed 1 --threads 2 --prefix $$(printf $$i".masked_LG4X") ; \
			done ; \
		done
# uniform tree branches
alignment%x$(ALIG_LEN)x$(REPLICATES)_uniform-scaled-branches_evo_simulation.55x55vLG4X.1char_rotasequence.RF_compare.pre: alignment%x$(ALIG_LEN)x$(REPLICATES)_uniform-scaled-branches_evo_simulation.55x55.1char_rotasequence.RF_compare.pre
	for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
		for i in $$(ls $$(printf $</alignment_$$(printf $$j).tmp_\*.masked)) ; do \
			bsub -e err -o out -n 2 -M 500 -R "rusage[mem=500]" raxml-ng --opt-model off --msa $$(printf $$i) --model LG4X --seed 1 --threads 2 --prefix $$(printf $$i".masked_LG4X") ; \
			done ; \
		done


# compare trees estimated from the same scaled tree and using the same model
alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55vALL.1char_rotasequence.RF_compare: alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55.1char_rotasequence.RF_compare.pre
	for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
		../../local/src/compare_tree.py -o $</scaled_$$(printf $$j).newick.tmp  $$(ls $</alignment_$$(printf $$j).tmp_*.raxml.bestTree | grep -v 'masked' | grep -v exp | bsort -nr | tr '\n' ' ')  >>$@.55x55.tmp ; \
		for i in 20x20 LG LG4X; do \
			../../local/src/compare_tree.py -o $</scaled_$$(printf $$j).newick.tmp  $$(ls $</alignment_$$(printf $$j).tmp_*.masked_$$(printf $$i).raxml.bestTree | bsort -nr | tr '\n' ' ')  >>$@.$$(printf $$i).tmp ; \
		done ; \
	done ; paste <(cut -f1,3 $@.55x55.tmp | tr "/" "\t" | cut -f2,3 | sed 's/scaled_//g ; s/.newick.tmp//g') <(cut -f3 $@.20x20.tmp) <(cut -f3 $@.LG.tmp) <(cut -f3 $@.LG4X.tmp) >>$@  ; \
	rm $@.*.tmp
alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55vLG-LGman.1char_rotasequence.RF_compare: alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55.1char_rotasequence.RF_compare.pre
	for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
		for i in LG LGman ; do \
			../../local/src/compare_tree.py -o $</scaled_$$(printf $$j).newick.tmp  $$(ls $</alignment_$$(printf $$j).tmp_*.masked_$$(printf $$i).raxml.bestTree | bsort -nr | tr '\n' ' ')  >>$@.$$(printf $$i).tmp ; \
		done ; \
	done ; paste <(cut -f1,3 $@.LG.tmp | tr "/" "\t" | cut -f2,3 | sed 's/scaled_//g ; s/.newick.tmp//g') <(cut -f3 $@.LGman.tmp) >>$@  ; \
	rm $@.*.tmp
alignment%x$(ALIG_LEN)x$(REPLICATES)_uniform-scaled-branches_evo_simulation.55x55vALL.1char_rotasequence.RF_compare: alignment%x$(ALIG_LEN)x$(REPLICATES)_uniform-scaled-branches_evo_simulation.55x55.1char_rotasequence.RF_compare.pre
	for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
		../../local/src/compare_tree.py -o $</scaled_$$(printf $$j).newick.tmp  $$(ls $</alignment_$$(printf $$j).tmp_*.raxml.bestTree | grep -v 'masked' | grep -v exp | bsort -nr | tr '\n' ' ')  >>$@.55x55.tmp ; \
		for i in 20x20 LG LG4X ; do \
			../../local/src/compare_tree.py -o $</scaled_$$(printf $$j).newick.tmp  $$(ls $</alignment_$$(printf $$j).tmp_*.masked_$$(printf $$i).raxml.bestTree | bsort -nr | tr '\n' ' ')  >>$@.$$(printf $$i).tmp ; \
		done ; \
	done ; paste <(cut -f1,3 $@.55x55.tmp | tr "/" "\t" | cut -f2,3 | sed 's/scaled_//g ; s/.newick.tmp//g') <(cut -f3 $@.20x20.tmp) <(cut -f3 $@.LG.tmp) <(cut -f3 $@.LG4X.tmp) >>$@  ; \
	rm $@.*.tmp

.META: *55x55vALL.1char_rotasequence.RF_compare
	1	scaling_factor
	2	55x55_RF
	3	20x20_RF
	4	LG_RF
	5	LG4X_RF
	6	LGman_RF

# compare raxmlng's built-in LG implementation with manual input LG (i.e. LG as as custom 20x20 matrix)
alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.LGvLGman.1char_rotasequence.RF_compare: alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55.1char_rotasequence.RF_compare.pre
	for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
		for i in LG LGman; do \
			../../local/src/compare_tree.py -o $</scaled_$$(printf $$j).newick.tmp  $$(ls $</alignment_$$(printf $$j).tmp_*.masked_$$(printf $$i).raxml.bestTree | bsort -nr | tr '\n' ' ')  >>$@.$$(printf $$i).tmp ; \
		done ; \
	done ; paste <(cut -f1,3 $@.LG.tmp | tr "/" "\t" | cut -f2,3 | sed 's/scaled_//g ; s/.newick.tmp//g') <(cut -f3 $@.LGman.tmp) >>$@  ; \
	rm $@.*.tmp

.META: *LGvLGman.1char_rotasequence.RF_compare
	1	scaling_factor
	2	LG_RF
	3	LGman_RF

alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.20x20vLG.1char_AAsequence.RF_compare: alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.20x20.1char_AAsequence.RF_compare.pre
	for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
		../../local/src/compare_tree.py -o $</scaled_$$(printf $$j).newick.tmp  $$(ls $</alignment_$$(printf $$j).tmp_*.raxml.bestTree | grep -v 'LG' | bsort -nr | tr '\n' ' ')  >>$@.20x20.tmp ; \
		for i in LG; do \
			../../local/src/compare_tree.py -o $</scaled_$$(printf $$j).newick.tmp  $$(ls $</alignment_$$(printf $$j).tmp_*.$$(printf $$i).raxml.bestTree | bsort -nr | tr '\n' ' ')  >>$@.$$(printf $$i).tmp ; \
		done ; \
	done ; paste <(cut -f1,3 $@.20x20.tmp | tr "/" "\t" | cut -f2,3 | sed 's/scaled_//g ; s/.newick.tmp//g') <(cut -f3 $@.LG.tmp) >>$@  ; \
	rm $@.*.tmp

.META: *20x20vLG.1char_AAsequence.RF_compare
	1	scaling_factor
	2	20x20_RF
	3	LG_RF




# compare the ML values
alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55vALL.1char_rotasequence.ML_compare: alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55.1char_rotasequence.RF_compare.pre
	for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
		grep -H 'logLikelihood:' $$(ls $</alignment_$$(printf $$j).tmp_*.raxml.log | grep -v 'masked' | grep -v exp | bsort -nr | tr '\n' ' ')  | tr ' ' '\t' >>$@.55x55.tmp ; \
		for i in LGexp WAGexp; do \
			grep -H 'logLikelihood:' $$(ls $</alignment_$$(printf $$j).tmp_*.masked_$$(printf $$i).raxml.log | bsort -nr | tr '\n' ' ')  | tr ' ' '\t' >>$@.$$(printf $$i).tmp ; \
		done ; \
	done ;  paste <(cut -f1,7 $@.55x55.tmp | tr "/" "\t" | cut -f2,3 |  sed 's/alignment_//g; s/.tmp_.*]//g') <(cut -f7 $@.WAGexp.tmp) <(cut -f7 $@.LGexp.tmp) >>$@ ; \
	rm $@.*.tmp
alignment%x$(ALIG_LEN)x$(REPLICATES)_uniform-scaled-branches_evo_simulation.55x55vALL.1char_rotasequence.ML_compare: alignment%x$(ALIG_LEN)x$(REPLICATES)_uniform-scaled-branches_evo_simulation.55x55.1char_rotasequence.RF_compare.pre
	for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
		grep -H 'logLikelihood:' $$(ls $</alignment_$$(printf $$j).tmp_*.raxml.log | grep -v 'masked' | grep -v exp | bsort -nr | tr '\n' ' ')  | tr ' ' '\t' >>$@.55x55.tmp ; \
		for i in 20x20 LG LG4X ; do \
			grep -H 'logLikelihood:' $$(ls $</alignment_$$(printf $$j).tmp_*.masked_$$(printf $$i).raxml.log | bsort -nr | tr '\n' ' ')  | tr ' ' '\t' >>$@.$$(printf $$i).tmp ; \
		done ; \
	done ;  paste <(cut -f1,7 $@.55x55.tmp | tr "/" "\t" | cut -f2,3 |  sed 's/alignment_//g; s/.tmp_.*]//g') <(cut -f7 $@.20x20.tmp) <(cut -f7 $@.LG.tmp) <(cut -f7 $@.LG4X.tmp) >>$@ ; \
	rm $@.*.tmp
.META: *55x55vALL.1char_rotasequence.ML_compare
	1	scaling_factor
	2	55x55_ML
	3	WAGexp
	4	LGexp

alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55vALL.1char_rotasequence.RF_compare.pdf: alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55vALL.1char_rotasequence.RF_compare 
	bawk '$$2!="" && $$3!="" && $$4!="" && $$5!="" {print}'  $<  | ../../local/src/vis_RF-compare_scatter.py 55x55,20x20,LG,LG4X -b -o $@
alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.20x20vLG.1char_AAsequence.RF_compare.pdf: alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.20x20vLG.1char_AAsequence.RF_compare
	bawk '$$2!="" && $$3!="" {print}'  $<  | ../../local/src/vis_RF-compare_scatter.py 20x20,LG -b -o $@
alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55vALL.1char_rotasequence.RF_compare.variance.pdf: alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55vALL.1char_rotasequence.RF_compare
	cut -f1,2,3,4,5 $<  | ../../local/src/vis_RF-compare_scatter.py 55x55,20x20,LG,LG4X -v -o $@
alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55vALL.1char_rotasequence.RF_compare.stackplot.pdf: alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55vALL.1char_rotasequence.RF_compare
	bawk '$$2!="" && $$3!="" && $$4!="" && $$5!="" {print}'  $<  | ../../local/src/vis_RF-compare_scatter.py 55x55,20x20,LG,LG4X,LGman -s -o $@
# x = 55x55 RF distance, y= other model RF distance, one point per simulated alignment
alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55vALL.1char_rotasequence.RF_compare.scatter.pdf: alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55vALL.1char_rotasequence.RF_compare
	bawk '$$2!="" && $$3!="" && $$4!="" && $$5!="" {print}' $<  | ../../local/src/vis_RF-compare_scatter.py 55x55,20x20,LG,LG4X,LGman -o $@


alignment%x$(ALIG_LEN)x$(REPLICATES)_uniform-scaled-branches_evo_simulation.55x55vALL.1char_rotasequence.RF_compare.pdf: alignment%x$(ALIG_LEN)x$(REPLICATES)_uniform-scaled-branches_evo_simulation.55x55vALL.1char_rotasequence.RF_compare 
	bawk '$$2!="" && $$3!="" && $$4!="" && $$5!="" {print}'  $<  | ../../local/src/vis_RF-compare_scatter.py 55x55,20x20,LG,LG4X -b -o $@
alignment%x$(ALIG_LEN)x$(REPLICATES)_uniform-scaled-branches_evo_simulation.55x55vALL.1char_rotasequence.RF_compare.variance.pdf: alignment%x$(ALIG_LEN)x$(REPLICATES)_uniform-scaled-branches_evo_simulation.55x55vALL.1char_rotasequence.RF_compare
	bawk '$$2!="" && $$3!="" && $$4!="" && $$5!="" {print}'  $<  | ../../local/src/vis_RF-compare_scatter.py 55x55,20x20,LG,LG4X -v -o $@
alignment%x$(ALIG_LEN)x$(REPLICATES)_uniform-scaled-branches_evo_simulation.55x55vALL.1char_rotasequence.RF_compare.stackplot.pdf: alignment%x$(ALIG_LEN)x$(REPLICATES)_uniform-scaled-branches_evo_simulation.55x55vALL.1char_rotasequence.RF_compare
	bawk '$$2!="" && $$3!="" && $$4!="" && $$5!="" {print}'  $< | ../../local/src/vis_RF-compare_scatter.py 55x55,20x20,LG,LG4X -s -o $@
# x = 55x55 RF distance, y= other model RF distance, one point per simulated alignment
alignment%x$(ALIG_LEN)x$(REPLICATES)_uniform-scaled-branches_evo_simulation.55x55vALL.1char_rotasequence.RF_compare.scatter.pdf: alignment%x$(ALIG_LEN)x$(REPLICATES)_uniform-scaled-branches_evo_simulation.55x55vALL.1char_rotasequence.RF_compare
	bawk '$$2!="" && $$3!="" && $$4!="" && $$5!="" {print}'  $<  | ../../local/src/vis_RF-compare_scatter.py 55x55,20x20,LG,LG4X -o $@

%_evo_simulation.55x55vALL.1char_rotasequence.ML_compare.mean.pdf: %_evo_simulation.55x55vALL.1char_rotasequence.ML_compare
	bawk '$$2!="" && $$3!="" && $$4!="" && $$5!="" {print}'  $<  | ../../local/src/vis_ML-compare_scatter.py 55x55,20x20,LG,LG4X -m -o $@

# generate a 55x55 version of a 20x20 model (PAML-style) by uniformly expanding the rates into 3x3 (or 3x1,3x2 etc.) submatrices 
# i.e. expanded_matrix[i][j] = orig_matrix[orig_col_index, orig_row_index] / submatrix_size
WAG_55x55.expanded.exchangeabilities.ng: ../../local/share/data/wag.dat
	head -n 19 $< |  ../../local/src/expand_PAML_matrix.py $(RESTYPE_STATES) $(ROTA_STATES) \
	| cut -f 2- | unhead | awk '{for(i=NR;i<=NF;i++) $$i="";print}' | unhead | tr -s " " "\t" \ 
	|  transpose | tr "\t" "," | sed 's/^,*//g' | tr "\n" "," | sed 's/,$$//g' | sed -r 's/([0-9.e-]*,){25}/&\n/g' \
	|  awk '{print "  ", $$0}' | sed 's/,/& /g'  | tr -d ',' >$@
WAG_55x55.expanded.frequencies.ng: ../../local/share/data/wag.dat
	head -n 21 $< | tail -n1 | tr -s " " "," | sed 's/,[^0-9]//g' >$@.tmp ;\
	head -n 19 $< |  ../../local/src/expand_PAML_matrix.py -f $(shell printf $@.tmp) $(RESTYPE_STATES) $(ROTA_STATES) | tail -n1 \
	|  tr "\t" "," | sed -r 's/([0-9.e-]*,){5}/&\n/g' | awk '{print "  ", $$0}' | sed 's/,/& /g' | tr -d ',' >$@ ;\
	rm $@.tmp
DAYHOFF_55x55.expanded.exchangeabilities.ng: ../../local/share/data/dayhoff.dat
	head -n 20 $< | unhead |  ../../local/src/expand_PAML_matrix.py $(RESTYPE_STATES) $(ROTA_STATES) \
	| cut -f 2- | unhead | awk '{for(i=NR;i<=NF;i++) $$i="";print}' | unhead | tr -s " " "\t" \ 
	|  transpose | tr "\t" "," | sed 's/^,*//g' | tr "\n" "," | sed 's/,$$//g' | sed -r 's/([0-9.e-]*,){25}/&\n/g' \
	|  awk '{print "  ", $$0}' | sed 's/,/& /g'  | tr -d ',' >$@
DAYHOFF_20x20.frequencies.ng: ../../local/share/data/dayhoff.dat 
	cat $< | unhead | head -n 23 | tail -n3 | tr -s "\r\n" "," | tr -s " " "," | sed 's/,$$//g' >$@.tmp ; \
	head -n 20 $< | unhead |  ../../local/src/expand_PAML_matrix.py -f $(shell printf $@.tmp) $(RESTYPE_STATES) $(ROTA_STATES) | tail -n1 \
	|  tr "\t" "," | sed -r 's/([0-9.e-]*,){5}/&\n/g' | awk '{print "  ", $$0}' | sed 's/,/& /g' | tr -d ',' >$@ ;\
	rm $@.tmp

#LG rates and freqs from https://github.com/xflouris/libpll-2/blob/master/src/maps.c in order to manually input into raxmlng
LG_20x20.libpll.exchangeabilities.ng: ../../local/share/data/pll_aa_rates_lg
	cat $< | unhead -n 3 | untail | tr -d "," >$@
LG_20x20.libpll.frequencies.ng: ../../local/share/data/pll_aa_freq_lg
	cat $< | unhead -n 2 | untail | tr -d "," >$@


# the LG-normalized model works on the same principle as the Dayhoff_norm model
# LG-normalized (using recontructed LG counts obtained from the LG exchangeabilities) counts
rotamers_adj_shared_count_matrix.merged.LG_norm: rotamers_adj_shared_count_matrix.merged ../../local/share/data/LG_model_correct.full.tsv
	cut -f 2- $< | unhead | ../../local/bin/normalize_matrix.py -f $^2 $(ROTA_STATES) >$@

# LG counts  are reconstructed assuming total number of counts = 10^9
# this 20x20 matrix is then expanded by UNIFORMLY distributing the counts in a single 20x20 cell
# across the corresponding submatrix in the expanded 55x55 matrix 
LG_55x55.uniform-exp.counts: ../../local/share/data/LG_model_correct.full.tsv
	cut -f 2- $< | unhead | ../../local/src/expand_counts_matrix.py $(RESTYPE_STATES) $(ROTA_STATES) > $@
LG_55x55.orig-exp.counts: ../../local/share/data/LG_model_correct.full.tsv
	cut -f 2- $< | unhead | ../../local/src/expand_counts_matrix.py -o $(RESTYPE_STATES) $(ROTA_STATES) > $@

# compute the IRM, then scale it and superscale it
LG_55x55.%-exp.IRM.superQ-star: LG_55x55.%-exp.counts  rotamers$(MATRIX_TYPE)matrix.merged.Dayhoff_norm.frequencies
	cut -f 2- $< | unhead | ../../local/bin/normalize_matrix.py -I $(ROTA_STATES) \
	| cut -f 2- | unhead | ../../local/src/scale_55_matrix.py -r $^2 $(ROTA_STATES) >$@

# calculate the IRM, then the rates
LG_55x55.%-exp.exchangeability.superQ-star: LG_55x55.%-exp.IRM.superQ-star
	cut -f 2- $< | unhead |  ../../local/bin/normalize_matrix.py -e $< $(ROTA_STATES) >$@

LG_55x55.%-exp.frequencies: LG_55x55.%-exp.exchangeability.superQ-star LG_55x55.uniform-exp.counts
	cut -f 2- $< | unhead | ../../local/src/PhyML_matrix.py -F $^2 -o $@ $(ROTA_STATES) $(ROTA_STATES) 

LG_55x55.%-exp.exchangeability.superQ-star.rates: LG_55x55.%-exp.exchangeability.superQ-star
	printf "const double pll_rates_55x55[1485] =\n {\n" >$@ ; \
	cut -f 2- $< | unhead | awk '{for(i=NR;i<=NF;i++) $$i="";print}' | unhead | tr -s " " "\t" \
	| transpose | tr "\t" "," | sed 's/^,*//g' | tr "\n" "," | sed 's/,$$//g' | sed -r 's/([0-9.e-]*,){25}/&\n/g' \
	| awk '{print "  ", $$0}' | sed 's/,/& /g' >>$@ ; \
	printf " };\n" >>$@

LG_55x55.%-exp.exchangeability.superQ-star.rates.ng: LG_55x55.%-exp.exchangeability.superQ-star.rates
	cat $< | unhead -n 2 | untail | tr -d ',' >$@

LG_55x55.%-exp_states_freqs: LG_55x55.%-exp.frequencies
	printf "const double 55_states_freqs[55] =\n {\n" >$@ ; \
	unhead $< | tr "\t" "," |  sed -r 's/([0-9.e-]*,){5}/&\n/g' | awk '{print "  ", $$0}' | sed 's/,/& /g' >>$@ ; \ 
	printf " };\n" >>$@


# LG counts  are reconstructed assuming total number of counts = 10^9
# this 20x20 matrix is then expanded by distributing the counts in a single 20x20 cell 
# across the corresponding submatrix in the expanded 55x55 matrix ACCORDING TO THE FREQUENCIES OF STATES i and j
LG_55x55.byfreq-exp.counts: ../../local/share/data/LG_model_correct.full.tsv rotamers$(MATRIX_TYPE)matrix.merged.Dayhoff_norm.frequencies
	cut -f 2- $< | unhead | ../../local/src/expand_counts_matrix.py $(RESTYPE_STATES) $(ROTA_STATES) -F $^2 >$@


# superscaled IRM
# the scaling factor RHO  is calculated over all rates outside the main diagonal
# -> results in 1 ROTAMER STATE change on avg per unit of time at equilibrium
# this isn't comparable with 20x20 models' brlens
# therefore the scaling factor SUPER_RHO is calculated over the ALREADY RHO-SCALED rates where residue types are != (i.e. all excluding the "superdiagonal")
# -> results in 1 AA STATE change per unit of time at equilibrium
# both scaling factors are applied to the original, unscaled, IRM
rotamers_%.IRM.superQ-star: rotamers_%.IRM rotamers_%.frequencies
	cut -f 2- $< | unhead | ../../local/src/scale_55_matrix.py -r $^2 $(ROTA_STATES) >$@

# SUPER_RHO is calculated over the UNSCALED RATES (from the IRM) where residue types are != (i.e. all excluding the "superdiagonal")
rotamers_%.IRM.superQ: rotamers_%.IRM rotamers_%.frequencies
	cut -f 2- $< | unhead | ../../local/src/scale_55_matrix.py  $^2 $(ROTA_STATES) >$@

# plot an IRM (55x55 superQ-star) as an heatmap with two grids superimposed
%.superQ-star.heatmap-grid.pdf: %.superQ-star
	cat $< | ../../local/src/vis_heatmap_grid.py >$@

# plot an IRM (20x20 Q-star) as an heatmap with one grid superimposed
%.Q-star.heatmap-grid.pdf: %.Q-star
	cat $< | ../../local/src/vis_heatmap_grid.py >$@
# plot the uniformely expanded LG matrix as an heatmap with one grid
LG_55x55.uniform-exp.IRM.heatmap-grid.pdf: LG_55x55.uniform-exp.IRM.superQ-star
	cat $< | ../../local/src/vis_heatmap_grid.py >$@

# plot a pre-scaled 20x20 matrix as heatmap with grid (i.e. WAG or LG IRM reconstructed from PAML .dat files)
%.IRM.heatmap-grid.pdf: %.IRM
	cat $< | ../../local/src/vis_heatmap_grid.py >$@

# the "superscaled" exchangeability matrix: 
rotamers_%.exchangeability.superQ-star: rotamers_%.IRM.superQ-star rotamers_%
	cut -f 2- $< | unhead | ../../local/bin/normalize_matrix.py -e $^2 $(ROTA_STATES) >$@

# the superscaled rates as a lower triangular scaled exchangeabilities, ordered column-wise
rotamers_%.exchangeability.superQ-star.rates: rotamers_%.exchangeability.superQ-star
	printf "const double pll_rates_55x55[1485] =\n {\n" >$@ ; \
	cut -f 2- $< | unhead | awk '{for(i=NR;i<=NF;i++) $$i="";print}' | unhead | tr -s " " "\t" \
	| transpose | tr "\t" "," | sed 's/^,*//g' | tr "\n" "," | sed 's/,$$//g' | sed -r 's/([0-9.e-]*,){25}/&\n/g' \
	| awk '{print "  ", $$0}' | sed 's/,/& /g' >>$@ ; \
	printf " };\n" >>$@

rotamers_%.exchangeability.superQ-star.rates.ng: rotamers_%.exchangeability.superQ-star.rates
	cat $< | unhead -n 2 | untail | tr -d ',' >$@

restype%matrix.merged.exchangeability.Q-star.rates.ng:  restype%matrix.merged.exchangeability.Q-star.rates
	cat $< | unhead -n 2 | untail | tr -d ',' >$@

# the rotamer states frequencies (with header)
rotamers_adj_shared_count_matrix.merged.LG_norm.frequencies: rotamers_adj_shared_count_matrix.merged.IRM rotamers_adj_shared_count_matrix.merged.LG_norm
	cut -f2- $< | unhead | ../../local/src/PhyML_matrix.py -F $^2 -o $@ $(ROTA_STATES) $(ROTA_STATES)

# generate an array containing the LG_norm  scaled exchangeabilities, ordered column-wise
55LG_norm_states_rates: rotamers_adj_shared_count_matrix.merged.LG_norm.exchangeability.superQ-star
	printf "const double pll_rates_55x55[1485] =\n {\n" >$@ ; \
	cut -f 2- $< | unhead | awk '{for(i=NR;i<=NF;i++) $$i="";print}' | unhead | tr -s " " "\t" \
	| transpose | tr "\t" "," | sed 's/^,*//g' | tr "\n" "," | sed 's/,$$//g' | sed -r 's/([0-9.e-]*,){25}/&\n/g' \
	| awk '{print "  ", $$0}' | sed 's/,/& /g' >>$@ ; \
	printf " };\n" >>$@

#generate an array containing the frequencies of the 55x55 model ordered alphabetically (ALA, ARG1 ... VAL3)
55LG_norm_states_freqs: rotamers_adj_shared_count_matrix.merged.LG_norm.frequencies 
	printf "const double 55_states_freqs[55] =\n {\n" >$@ ; \
	unhead $< | tr "\t" "," |  sed -r 's/([0-9.e-]*,){5}/&\n/g' | awk '{print "  ", $$0}' | sed 's/,/& /g' >>$@ ; \ 
	printf " };\n" >>$@

.PRECIOUS: 55LG_norm_states_freqs 55LG_norm_states_rates.ng rotamers_adj_shared_count_matrix.merged.IRM.superQ-star rotamers_adj_shared_count_matrix.merged.LG_norm.frequencies

# scale a tree and analyse eanch alignment replicate from the scaled tree using the 55x55LG_norm model, then compare the estimated tree to the scaled tree
alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55LG_norm.1char_rotasequence.RF_compare.pre: %_leaves_short-branches.newick rotamers_adj_shared_count_matrix.merged.IRM.superQ-star rotamers_adj_shared_count_matrix.merged.LG_norm.frequencies 55LG_norm_states_rates.ng  55LG_norm_states_freqs.ng
	mkdir -p $@ ; \
	cat $^3 | unhead >$@/freqs.tmp ; \
	for j in  $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
		cat $< | ../../local/src/scale_tree.py $$(printf $$j) >$@/scaled_$$(printf $$j).newick.tmp ; \
		cut -f2- $^2  | unhead | ../../local/src/tree_sim.py -l $(shell printf $(ALIG_LEN)) -r $(shell printf $(REPLICATES)) $@/scaled_$$(printf $$j).newick.tmp  $(ROTA_AS_1CHAR) -F $@/freqs.tmp -o $@/alignment_$$(printf $$j).tmp; \ 
	done ; \
	for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
		for i in $$(ls $$(printf $@/alignment_$$(printf $$j).tmp_\*)) ; do \
			cat $$(printf $$i) | ../../local/src/PHYLIP_codon-2-1.py -m >$$(printf $$i".masked") ; \
			bsub -e err -o out -n 2 -M 500 -R "rusage[mem=500]" raxml-ng --opt-model off --msa $$(printf $$i) --model MULTI55_GTR{$^4}+FU{$^5}+M{ABCDEFGHIJKLMNOPQRSTUVWYXZabcdefghijklmnopqrstuvwxyz012} --seed 1 --threads 2 ; \
			done ; \
		done

# analyse each (masked) alignment replicate from the scaled tree using the LG model
alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55LG_normvLG.1char_rotasequence.RF_compare.pre: alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55LG_norm.1char_rotasequence.RF_compare.pre
	for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
		for i in $$(ls $$(printf $</alignment_$$(printf $$j).tmp_\*.masked)) ; do \
			bsub -e err -o out -n 2 -M 500 -R "rusage[mem=500]" raxml-ng --opt-model off --msa $$(printf $$i) --model LG --seed 1 --threads 2 --prefix $$(printf $$i".masked_LG") ; \
			done ; \
		done
# use weighted RF as distance from the reference tree
alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55LG_normvLG.1char_rotasequence.weightedRF_compare: alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55LG_norm.1char_rotasequence.RF_compare.pre
	for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
		../../local/src/compare_tree.py -w -o $</scaled_$$(printf $$j).newick.tmp  $$(ls $</alignment_$$(printf $$j).tmp_*.raxml.bestTree | grep -v 'masked' | grep -v exp | bsort -nr | tr '\n' ' ')  >>$@.55x55.tmp ; \
		../../local/src/compare_tree.py -w -o $</scaled_$$(printf $$j).newick.tmp  $$(ls $</alignment_$$(printf $$j).tmp_*.masked_LG.raxml.bestTree | bsort -nr | tr '\n' ' ')  >>$@.LG.tmp ; \
	done ; paste <(cut -f1,3 $@.55x55.tmp | tr "/" "\t" | cut -f2,3 | sed 's/scaled_//g ; s/.newick.tmp//g') <(cut -f3 $@.LG.tmp) >>$@  ; \
	rm $@.*.tmp
# compare trees estimated from the same scaled tree and using the same model using unweighted RF
alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55LG_normvLG.1char_rotasequence.RF_compare: alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55LG_norm.1char_rotasequence.RF_compare.pre
	for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
		../../local/src/compare_tree.py -o $</scaled_$$(printf $$j).newick.tmp  $$(ls $</alignment_$$(printf $$j).tmp_*.raxml.bestTree | grep -v 'masked' | grep -v exp | bsort -nr | tr '\n' ' ')  >>$@.55x55.tmp ; \
		../../local/src/compare_tree.py -o $</scaled_$$(printf $$j).newick.tmp  $$(ls $</alignment_$$(printf $$j).tmp_*.masked_LG.raxml.bestTree | bsort -nr | tr '\n' ' ')  >>$@.LG.tmp ; \
	done ; paste <(cut -f1,3 $@.55x55.tmp | tr "/" "\t" | cut -f2,3 | sed 's/scaled_//g ; s/.newick.tmp//g') <(cut -f3 $@.LG.tmp) >>$@  ; \
	rm $@.*.tmp
# compare the total tree length
alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55LG_normvLG.1char_rotasequence.tree-len_compare: alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55LG_norm.1char_rotasequence.RF_compare.pre
	for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
		../../local/src/compare_tree.py -l -o $</scaled_$$(printf $$j).newick.tmp  $$(ls $</alignment_$$(printf $$j).tmp_*.raxml.bestTree | grep -v 'masked' | grep -v exp | bsort -nr | tr '\n' ' ')  >>$@.55x55.tmp ; \
		../../local/src/compare_tree.py -l -o $</scaled_$$(printf $$j).newick.tmp  $$(ls $</alignment_$$(printf $$j).tmp_*.masked_LG.raxml.bestTree | bsort -nr | tr '\n' ' ')  >>$@.LG.tmp ; \
	done ; paste <(cut -f1,3,4 $@.55x55.tmp | tr "/" "\t" | cut -f2,3,4| sed 's/scaled_//g ; s/.newick.tmp//g') <(cut -f4 $@.LG.tmp) >$@  ; \
	rm $@.*.tmp
alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55vORIG.1char_rotasequence.tree-len_compare: alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55.1char_rotasequence.RF_compare.pre
	for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
		../../local/src/compare_tree.py -l -o $</scaled_$$(printf $$j).newick.tmp  $$(ls $</alignment_$$(printf $$j).tmp_*.raxml.bestTree | grep -v 'masked' | grep -v exp | bsort -nr | tr '\n' ' ')  >>$@.55x55.tmp ; \
	done ; cut -f1,3,4 $@.55x55.tmp | tr "/" "\t" | cut -f2,3,4| sed 's/scaled_//g ; s/.newick.tmp//g'>$@  ; \
	rm $@.*.tmp
alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55vALL.1char_rotasequence.tree-len_compare: alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55.1char_rotasequence.RF_compare.pre
	for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
		../../local/src/compare_tree.py -l -o $</scaled_$$(printf $$j).newick.tmp  $$(ls $</alignment_$$(printf $$j).tmp_*.raxml.bestTree | grep -v 'masked' | grep -v exp | bsort -nr | tr '\n' ' ')  >>$@.55x55.tmp ; \
		for i in 20x20 LG LG4X; do \
			../../local/src/compare_tree.py -l -o $</scaled_$$(printf $$j).newick.tmp  $$(ls $</alignment_$$(printf $$j).tmp_*.masked_$$(printf $$i).raxml.bestTree | bsort -nr | tr '\n' ' ')  >>$@.$$(printf $$i).tmp ; \
		done ; \
	done ; paste <(cut -f1,3,4 $@.55x55.tmp | tr "/" "\t" | cut -f2,3,4 | sed 's/scaled_//g ; s/.newick.tmp//g') <(cut -f4 $@.20x20.tmp) <(cut -f4 $@.LG.tmp) <(cut -f4 $@.LG4X.tmp) >>$@  ; \
	rm $@.*.tmp


alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55LG_normvLG.1char_rotasequence.RF_compare.pdf: alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55LG_normvLG.1char_rotasequence.RF_compare	
	bawk '$$2!="" && $$3!="" {print}'  $<  | ../../local/src/vis_RF-compare_scatter.py 55x55LG_norm,LG -b -o $@
alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55LG_normvLG.1char_rotasequence.weightedRF_compare.pdf: alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55LG_normvLG.1char_rotasequence.weightedRF_compare
	bawk '$$2!="" && $$3!="" {print}'  $<  | ../../local/src/vis_RF-compare_scatter.py 55x55LG_norm,LG -b -l weighted-RF -o $@
alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55LG_normvLG.1char_rotasequence.tree-len_compare.pdf: alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55LG_normvLG.1char_rotasequence.tree-len_compare
	bawk '$$2!="" && $$3!="" {print}'  $<  | ../../local/src/vis_RF-compare_scatter.py REFERENCE,55x55LG_norm,LG -b -l "Tree length" -o $@
alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55vORIG.1char_rotasequence.tree-len_compare.pdf: alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55vORIG.1char_rotasequence.tree-len_compare
	bawk '$$2!="" && $$3!="" {print}'  $<  | ../../local/src/vis_RF-compare_scatter.py REFERENCE,55x55 -b -S -l "Tree length" -o $@
alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55vALL.1char_rotasequence.tree-len_compare.pdf: alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55vALL.1char_rotasequence.tree-len_compare
	bawk '$$2!="" && $$3!="" {print}'  $<  | ../../local/src/vis_RF-compare_scatter.py REFERENCE,55x55,20x20,LG,LG4X -b -l "Tree length" -o $@
	

# generate an alignment by simulating the evolution of an original sequence using pyvolve and a guide tree 
alignment%x10000x1_pyvolve_evo_simulation.55x55.1char_rotasequence.seq_compare: %_leaves_short-branches.newick rotamers_adj_count_matrix.merged.Dayhoff_norm.IRM.Q-star.neg_diag rotamers$(MATRIX_TYPE)matrix.merged.Dayhoff_norm.frequencies sequence10k_evo_simulation.rotamer.1char.seq_only
	cut -f2- $^2  | unhead | ../../local/src/tree_sim.py -l 10000 -r 1 -p $<  $(ROTA_AS_1CHAR) -F $^3 -f $^4 | unhead >$@ ; \
	rm custom_matrix_frequencies.txt
# generate an alignment by simulating the evoution of an original sequence using np.random.choice  and a guide tree 
alignment%x10000x1_rchoice_evo_simulation.55x55.1char_rotasequence.seq_compare: %_leaves_short-branches.newick rotamers_adj_count_matrix.merged.Dayhoff_norm.IRM.Q-star rotamers$(MATRIX_TYPE)matrix.merged.Dayhoff_norm.frequencies sequence10k_evo_simulation.rotamer.1char.seq_only
	cut -f2- $^2  | unhead | ../../local/src/tree_sim.py -l 10000 -r 1 $<  $(ROTA_AS_1CHAR) -F $^3 -f $^4 >$@ 
	rm custom_matrix_frequencies.txt
# tabulate the state frequencies in different simulations
alignment%x10000x1_ALL_evo_simulation.55x55.1char_rotasequence.seq_compare.tab: alignment%x10000x1_pyvolve_evo_simulation.55x55.1char_rotasequence.seq_compare alignment%x10000x1_rchoice_evo_simulation.55x55.1char_rotasequence.seq_compare rotamers$(MATRIX_TYPE)matrix.merged.Dayhoff_norm.frequencies
	printf $(ROTA_STATES) | tr "," "\t" >$@.tmp ; printf "\n" >>$@.tmp ; \
	cat $< | tr -s " " "\t" | cut -f2- | unhead | sed 's/./& /g; s/ $$//' | tr "\n" " " | ../../local/src/seq2freqs.py -s $(ROTA_AS_1CHAR) | unhead >>$@.tmp ; \
	cut -f2- $^2 | unhead -n 2 | sed 's/./& /g; s/ $$//' | tr "\n" " " | ../../local/src/seq2freqs.py -s $(ROTA_AS_1CHAR) | unhead >>$@.tmp ; \
	unhead $^3 >>$@.tmp ; \
	paste <(printf "\npyvolve\nrchoice\n55x55Dayhoff_norm") <(cat $@.tmp) >$@ ; rm $@.tmp
	
# compare frequencies
alignment%x10000x1_ALL_evo_simulation.55x55.1char_rotasequence.seq_compare.pdf: alignment%x10000x1_ALL_evo_simulation.55x55.1char_rotasequence.seq_compare.tab
	cat $< | ../../local/src/vis_freq-compare.py >$@
# infer a tree using the original tree as guide and the alignment generated with pyvolve
alignment%x10000x1_pyvolve_evo_simulation.55x55.1char_rotasequence.seq_compare.55x55.tree_guided.raxml.bestTree: alignment%x10000x1_pyvolve_evo_simulation.55x55.1char_rotasequence.seq_compare 55_states_rates.ng rotamers_adj_count_matrix.merged.Dayhoff_norm_states_freqs.ng %_leaves_short-branches.newick 
	cp $< $<.55x55.tree_guided ; \
	raxml-ng --opt-model off --msa $<.55x55.tree_guided --model MULTI55_GTR{$^2}+FU{$^3}+M{ABCDEFGHIJKLMNOPQRSTUVWYXZabcdefghijklmnopqrstuvwxyz012} --seed 1 --threads 2 --tree $^4 ; \
	rm alignment$*x10000x1_pyvolve*log alignment$*x10000x1_pyvolve*rba alignment$*x10000x1_pyvolve*startTree alignment$*x10000x1_pyvolve*bestModel $<.55x55.tree_guided
# infer a tree using the original tree as guide and the alignment generated with rchoice
alignment%x10000x1_rchoice_evo_simulation.55x55.1char_rotasequence.seq_compare.55x55.tree_guided.raxml.bestTree: alignment%x10000x1_rchoice_evo_simulation.55x55.1char_rotasequence.seq_compare 55_states_rates.ng rotamers_adj_count_matrix.merged.Dayhoff_norm_states_freqs.ng %_leaves_short-branches.newick 
	cp $< $<.55x55.tree_guided ; \
	raxml-ng --opt-model off --msa $<.55x55.tree_guided --model MULTI55_GTR{$^2}+FU{$^3}+M{ABCDEFGHIJKLMNOPQRSTUVWYXZabcdefghijklmnopqrstuvwxyz012} --seed 1 --threads 2 --tree $^4 ; \
	rm alignment$*x10000x1_rchoice*log alignment$*x10000x1_rchoice*rba alignment$*x10000x1_rchoice*startTree alignment$*x10000x1_rchoice*bestModel $<.55x55.tree_guided
# compare tree lengths for tree estimated (with or without guide tree) from a pyvolve or rchoice-generate alignment
alignment%x10000x1_rchoicevpyvolve_evo_simulation.55x55.1char_rotasequence.tree-len_compare: alignment%x10000x1_rchoice_evo_simulation.55x55.1char_rotasequence.seq_compare.55x55.tree_guided.raxml.bestTree alignment%x10000x1_pyvolve_evo_simulation.55x55.1char_rotasequence.seq_compare.55x55.tree_guided.raxml.bestTree alignment%x10000x1_rchoice_evo_simulation.55x55.1char_rotasequence.seq_compare.55x55.raxml.bestTree alignment%x10000x1_pyvolve_evo_simulation.55x55.1char_rotasequence.seq_compare.55x55.raxml.bestTree 
	for i in $^ ; do \
		../../local/src/compare_tree.py -l -o $*_leaves_short-branches.newick $$(printf $$i) | sed 's/alignment.*x10000x1_//g ; s/evo_simulation.55x55\.1char_rotasequence\.seq_compare.55x55\.//g ; s/\.raxml\.bestTree//g' >>$@; \
	done ;


#----------------SIMULATION BENCHMARK---------------------------------------------------------------------------------------------------------------
# compare 55x55-estimated vs ORIGINAL tree lengths across different tree sizes (taxa), alignment length is constant
TAXA_NUM?=4 8 16 32 64 
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55vORIG.1char_rotasequence.tree-len_compare: $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM)))
	for i in $^ ; do \
		for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
			../../local/src/compare_tree.py -l -o $$(printf $$i)/scaled_$$(printf $$j).newick.tmp  $$(ls $$(printf $$i)/alignment_$$(printf $$j).tmp_*.raxml.bestTree | grep -v 'masked' | grep -v exp | bsort -nr | tr '\n' ' ')  \
			| cut -f1,3,4 | tr "/" "\t" | cut -f2,3,4 | sed 's/scaled_//g ; s/.newick.tmp//g' >>$@.$$(printf $$i).55x55.tmp ; \
		done ; \
	done ; paste <(cat $@.$<.55x55.tmp) <(cut -f 2- $@.$^2.55x55.tmp) <(cut -f 2- $@.$^3.55x55.tmp) <(cut -f 2- $@.$^4.55x55.tmp) <(cut -f 2- $@.$^5.55x55.tmp) >$@ ; rm $@.*.tmp
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55vORIG.1char_rotasequence.RF_compare: $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM)))
	for i in $^ ; do \
		for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
			../../local/src/compare_tree.py -o $$(printf $$i)/scaled_$$(printf $$j).newick.tmp  $$(ls $$(printf $$i)/alignment_$$(printf $$j).tmp_*.raxml.bestTree | grep -v 'masked' | grep -v exp | bsort -nr | tr '\n' ' ')  \
			| cut -f1,3 | tr "/" "\t" | cut -f2,3,4 | sed 's/scaled_//g ; s/.newick.tmp//g' >>$@.$$(printf $$i).55x55.tmp ; \
		done ; \
	done ; paste <(cat $@.$<.55x55.tmp) <(cut -f 2- $@.$^2.55x55.tmp) <(cut -f 2- $@.$^3.55x55.tmp) <(cut -f 2- $@.$^4.55x55.tmp) <(cut -f 2- $@.$^5.55x55.tmp) >$@ ; rm $@.*.tmp

# 55x55 vs LG in terms of tree length
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55vLG.1char_rotasequence.tree-len_compare: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55vORIG.1char_rotasequence.tree-len_compare $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM)))
	for i in $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM))) ; do \
		for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
			../../local/src/compare_tree.py -l -o $$(printf $$i)/scaled_$$(printf $$j).newick.tmp  $$(ls $$(printf $$i)/alignment_$$(printf $$j).tmp_*.masked_LG.raxml.bestTree | bsort -nr | tr '\n' ' ')  >>$@.$$(printf $$i).LG.tmp ; \	
		done ; \
	done ; paste <(cat $<) <(cut -f 4 $@.$^2.LG.tmp) <(cut -f 4 $@.$^3.LG.tmp) <(cut -f 4 $@.$^4.LG.tmp) <(cut -f 4 $@.$^5.LG.tmp) <(cut -f 4 $@.$^6.LG.tmp) >$@ ; rm $@.*.tmp
# 55x55 vs LG in terms of unweighted RF
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55vLG.1char_rotasequence.RF_compare: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55vORIG.1char_rotasequence.RF_compare $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM)))
	for i in $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM))) ; do \
		for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
			../../local/src/compare_tree.py -o $$(printf $$i)/scaled_$$(printf $$j).newick.tmp  $$(ls $$(printf $$i)/alignment_$$(printf $$j).tmp_*.masked_LG.raxml.bestTree | bsort -nr | tr '\n' ' ')  >>$@.$$(printf $$i).LG.tmp ; \	
		done ; \
	done ; paste <(cat $<) <(cut -f 3 $@.$^2.LG.tmp) <(cut -f 3 $@.$^3.LG.tmp) <(cut -f 3 $@.$^4.LG.tmp) <(cut -f 3 $@.$^5.LG.tmp) <(cut -f 3 $@.$^6.LG.tmp) >$@ ; rm $@.*.tmp

#SUPERSCALED COMPARISON ACROSS SCALED TREES
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvORIG.1char_rotasequence.tree-len_compare: $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM)))
	for i in $^ ; do \
		for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
			for k in $$(ls $$(printf $$i)/alignment_$$(printf $$j).tmp_*.raxml.bestTree | grep -v 'masked' | grep -v exp | bsort -nr | tr '\n' ' '); do \
				cat $$k | ../../local/src/scale_tree.py $(SUPER_RHO) >$$k.rescaled ; \		
				../../local/src/compare_tree.py -l -o $$(printf $$i)/scaled_$$(printf $$j).newick.tmp  $$k.rescaled  \
				| cut -f1,3,4 | tr "/" "\t" | cut -f2,3,4 | sed 's/scaled_//g ; s/.newick.tmp//g' >>$@.$$(printf $$i).55x55.tmp ; \
			done; \
		done ; \
	done ; paste <(cat $@.$<.55x55.tmp) <(cut -f 2- $@.$^2.55x55.tmp) <(cut -f 2- $@.$^3.55x55.tmp) <(cut -f 2- $@.$^4.55x55.tmp) <(cut -f 2- $@.$^5.55x55.tmp) >$@ ; rm $@.*.tmp
#GUIDED SUPERSCALED SINGLE BRALEN COMPARISON ACROSS SCALED TREES
# rescaling a posteriori using SUPER_RHO to account for raxml's rescaling of user rates
# rxml ends up using rates * SUPER_RHO after its rescaling of user-submitted rates.
# smaller rates result in longer branches (and vice versa)
# therefore bralen * SUPER_RHO = true_brlen
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_AND_guide-treevORIG.1char_rotasequence.single-brlen_compare: $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_AND_guide-tree.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM))) 
	for i in $^ ; do \
		for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
			../../local/src/compare_tree.py -s -o $$(printf $$i)/scaled_$$(printf $$j).newick.tmp  $$(ls $$(printf $$i)/alignment_$$(printf $$j).tmp_*.raxml.bestTree | grep -v 'masked' | grep -v exp | bsort -nr | tr '\n' ' ')  \
			| cut -f1,3- | tr "/" "\t" | cut -f2- | sed 's/scaled_//g ; s/.newick.tmp//g' >>$@.$$(printf $$i).tmp ; \
		done ; \
	done ; paste <(bawk '{print $$1~5, $$6 * $(SUPER_RHO), $$7 * $(SUPER_RHO), $$8 * $(SUPER_RHO), $$9 * $(SUPER_RHO)}' $@.$<.tmp) <(bawk '{print $$2~5, $$6 * $(SUPER_RHO), $$7 * $(SUPER_RHO), $$8 * $(SUPER_RHO), $$9 * $(SUPER_RHO)}' $@.$^2.tmp) <(bawk '{print $$2~5, $$6 * $(SUPER_RHO), $$7 * $(SUPER_RHO), $$8 * $(SUPER_RHO), $$9 * $(SUPER_RHO)}' $@.$^3.tmp) <(bawk '{print $$2~5, $$6 * $(SUPER_RHO), $$7 * $(SUPER_RHO), $$8 * $(SUPER_RHO), $$9 * $(SUPER_RHO)}' $@.$^4.tmp) <(bawk '{print $$2~5, $$6 * $(SUPER_RHO), $$7 * $(SUPER_RHO), $$8 * $(SUPER_RHO), $$9 * $(SUPER_RHO)}' $@.$^5.tmp) >$@ ; rm $@.*.tmp

.META: alignmentALL*_scaled-branches_evo_simulation.55x55_superscaled_AND_guide-treevORIG.1char_rotasequence.single-brlen_compare
	1	scale
	2	4_taxa_ref_int_sec-short
	3	4_taxa_ref_int_sec-long
	4	4_taxa_ref_lf_sec-short
	5	4_taxa_ref_lf_sec-long
	6	4_taxa_new_int_sec-short
	7	4_taxa_new_int_sec-long
	8	4_taxa_new_lf_sec-short
	9	4_taxa_new_lf_sec-long
	10	8_taxa_ref_int_sec-short
	11	8_taxa_ref_int_sec-long
	12	8_taxa_ref_lf_sec-short
	13	8_taxa_ref_lf_sec-long
	14	8_taxa_new_int_sec-shor
	15	8_taxa_new_int_sec-long
	16	8_taxa_new_lf_sec-short
	17	8_taxa_new_lf_sec-long
	18	16_taxa_ref_int_sec-shor
	19	16_taxa_ref_int_sec-long
	20	16_taxa_ref_lf_sec-short
	21	16_taxa_ref_lf_sec-long
	22	16_taxa_new_int_sec-shor
	23	16_taxa_new_int_sec-long
	24	16_taxa_new_lf_sec-short
	25	16_taxa_new_lf_sec-long
	26	32_taxa_ref_int_sec-short
	27	32_taxa_ref_int_sec-long
	28	32_taxa_ref_lf_sec-short
	29	32_taxa_ref_lf_sec-long
	30	32_taxa_new_int_sec-shor
	31	32_taxa_new_int_sec-long
	32	32_taxa_new_lf_sec-short
	33	32_taxa_new_lf_sec-long
	34	64_taxa_ref_int_sec-short
	35	64_taxa_ref_int_sec-long
	36	64_taxa_ref_lf_sec-short
	37	64_taxa_ref_lf_sec-long
	38	64_taxa_new_int_sec-shor
	39	64_taxa_new_int_sec-long
	40	64_taxa_new_lf_sec-short
	41	64_taxa_new_lf_sec-long

alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvORIG.1char_rotasequence.RF_compare: $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM)))
	for i in $^ ; do \
		for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
			../../local/src/compare_tree.py -o $$(printf $$i)/scaled_$$(printf $$j).newick.tmp  $$(ls $$(printf $$i)/alignment_$$(printf $$j).tmp_*.raxml.bestTree | grep -v 'masked' | grep -v exp | bsort -nr | tr '\n' ' ')  \
			| cut -f1,3 | tr "/" "\t" | cut -f2,3,4 | sed 's/scaled_//g ; s/.newick.tmp//g' >>$@.$$(printf $$i).55x55.tmp ; \
		done ; \
	done ; paste <(cat $@.$<.55x55.tmp) <(cut -f 2- $@.$^2.55x55.tmp) <(cut -f 2- $@.$^3.55x55.tmp) <(cut -f 2- $@.$^4.55x55.tmp) <(cut -f 2- $@.$^5.55x55.tmp) >$@ ; rm $@.*.tmp
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvORIG.1char_rotasequence.euclidean_compare: $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM)))
	for i in $^ ; do \
		for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
			for k in $$(ls $$(printf $$i)/alignment_$$(printf $$j).tmp_*.raxml.bestTree | grep -v 'masked' | grep -v exp | bsort -nr | tr '\n' ' ');  do \
				cat $$k | ../../local/src/scale_tree.py $(SUPER_RHO) >$$k.rescaled ; \
				../../local/src/compare_tree.py -e -o $$(printf $$i)/scaled_$$(printf $$j).newick.tmp  $$k.rescaled  \
				| cut -f1,3 | tr "/" "\t" | cut -f2,3,4 | sed 's/scaled_//g ; s/.newick.tmp//g' >>$@.$$(printf $$i).55x55.tmp ; \
			done ; \
		done ; \
	done ; paste <(cat $@.$<.55x55.tmp) <(cut -f 2- $@.$^2.55x55.tmp) <(cut -f 2- $@.$^3.55x55.tmp) <(cut -f 2- $@.$^4.55x55.tmp) <(cut -f 2- $@.$^5.55x55.tmp) >$@ ; rm $@.*.tmp
# on a single (empirical) tree
alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvORIG.1char_rotasequence.euclidean_compare: alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled.1char_rotasequence.RF_compare.pre
	for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
		for k in $$(ls $</alignment_$$(printf $$j).tmp_*.raxml.bestTree | grep -v 'masked' | grep -v exp | bsort -nr | tr '\n' ' ');  do \
			cat $$k | ../../local/src/scale_tree.py $(SUPER_RHO) >$$k.rescaled ; \
			../../local/src/compare_tree.py -e -o $</scaled_$$(printf $$j).newick.tmp  $$k.rescaled  \
			| cut -f1,3 | tr "/" "\t" | cut -f2,3,4 | sed 's/scaled_//g ; s/.newick.tmp//g' >>$@; \
		done ; \
	done ; \

alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvORIG.1char_rotasequence.adapted-Ln_compare: $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM)))
	for i in $^ ; do \
		for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
			grep 'Final LogLikelihood:'  $$(ls $$(printf $$i)/alignment_$$(printf $$j).tmp_*.log | grep -v 'masked' | grep -v exp | grep -v 'exp' | bsort -nr | tr '\n' ' ' ) \
			| tr "/" "\t" | cut -f2 | sed 's/alignment_//g  ; s/.tmp.*: /\t/g' >>$@.$$(printf $$i).55x55.tmp ; \
		done ; \
	done ; paste <(cat $@.$<.55x55.tmp) <(cut -f 2- $@.$^2.55x55.tmp) <(cut -f 2- $@.$^3.55x55.tmp) <(cut -f 2- $@.$^4.55x55.tmp) <(cut -f 2- $@.$^5.55x55.tmp) >$@ ; rm $@.*.tmp
# compare run-time
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvORIG.1char_rotasequence.run-time_compare: $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM)))
	for i in $^ ; do \
		for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
			grep 'Elapsed time:' $$(ls $$(printf $$i)/alignment_$$(printf $$j).tmp_*.log | grep -v 'masked' | grep -v 'exp' | bsort -nr | tr '\n' ' ' ) \
			| sed 's/ seconds//g'  | tr "/" "\t" | cut -f2 | sed 's/alignment_//g  ; s/.tmp.*: /\t/g' >>$@.$$(printf $$i).55x55.tmp ; \
		done ; \
	done ; paste <(cat $@.$<.55x55.tmp) <(cut -f 2- $@.$^2.55x55.tmp) <(cut -f 2- $@.$^3.55x55.tmp) <(cut -f 2- $@.$^4.55x55.tmp) <(cut -f 2- $@.$^5.55x55.tmp) >$@ ; rm $@.*.tmp
# 55x55 superscaled with gammma (one file per alpha value)
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_plusGvORIG.1char_rotasequence.RF_compare: $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_plusG.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM)))
	for i in $^ ; do \
		for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
			../../local/src/compare_tree.py -o $$(printf $$i)/scaled_$$(printf $$j).newick.tmp  $$(ls $$(printf $$i)/alignment_$$(printf $$j).tmp_*.raxml.bestTree | grep -v 'masked' | grep -v exp | bsort -nr | tr '\n' ' ')  \
			| cut -f1,3 | tr "/" "\t" | cut -f2,3,4 | sed 's/scaled_//g ; s/.newick.tmp//g' >>$@.$$(printf $$i).55x55.tmp ; \
		done ; \
	done ; paste <(cat $@.$<.55x55.tmp) <(cut -f 2- $@.$^2.55x55.tmp) <(cut -f 2- $@.$^3.55x55.tmp) <(cut -f 2- $@.$^4.55x55.tmp) <(cut -f 2- $@.$^5.55x55.tmp) >$@ ; rm $@.*.tmp
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_plusGvORIG.1char_rotasequence.adapted-Ln_compare: $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_plusG.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM)))
	for i in $^ ; do \
		for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
			grep 'Final LogLikelihood:'  $$(ls $$(printf $$i)/alignment_$$(printf $$j).tmp_*.log | grep -v 'masked' | grep -v exp | grep -v 'exp' | bsort -nr | tr '\n' ' ' ) \
			| tr "/" "\t" | cut -f2 | sed 's/alignment_//g  ; s/.tmp.*: /\t/g' >>$@.$$(printf $$i).55x55.tmp ; \
		done ; \
	done ; paste <(cat $@.$<.55x55.tmp) <(cut -f 2- $@.$^2.55x55.tmp) <(cut -f 2- $@.$^3.55x55.tmp) <(cut -f 2- $@.$^4.55x55.tmp) <(cut -f 2- $@.$^5.55x55.tmp) >$@ ; rm $@.*.tmp



# superscaled 55x55 vs LG in terms of tree length
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvLG.1char_rotasequence.tree-len_compare: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvORIG.1char_rotasequence.tree-len_compare $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM)))
	for i in $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM))) ; do \
		for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
			../../local/src/compare_tree.py -l -o $$(printf $$i)/scaled_$$(printf $$j).newick.tmp  $$(ls $$(printf $$i)/alignment_$$(printf $$j).tmp_*.masked_LG.raxml.bestTree | bsort -nr | tr '\n' ' ')  >>$@.$$(printf $$i).LG.tmp ; \	
		done ; \
	done ; paste <(cat $<) <(cut -f 4 $@.$^2.LG.tmp) <(cut -f 4 $@.$^3.LG.tmp) <(cut -f 4 $@.$^4.LG.tmp) <(cut -f 4 $@.$^5.LG.tmp) <(cut -f 4 $@.$^6.LG.tmp) >$@ ; rm $@.*.tmp
# superscaled 55x55 vs LG in terms of unweighted RF
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvLG.1char_rotasequence.RF_compare: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvORIG.1char_rotasequence.RF_compare $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM)))
	for i in $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM))) ; do \
		for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
			../../local/src/compare_tree.py -o $$(printf $$i)/scaled_$$(printf $$j).newick.tmp  $$(ls $$(printf $$i)/alignment_$$(printf $$j).tmp_*.masked_LG.raxml.bestTree | bsort -nr | tr '\n' ' ')  >>$@.$$(printf $$i).LG.tmp ; \	
		done ; \
	done ; paste <(cat $<) <(cut -f 3 $@.$^2.LG.tmp) <(cut -f 3 $@.$^3.LG.tmp) <(cut -f 3 $@.$^4.LG.tmp) <(cut -f 3 $@.$^5.LG.tmp) <(cut -f 3 $@.$^6.LG.tmp) >$@ ; rm $@.*.tmp

# superscaled 55x55 vs LG in terms of unweighted RF
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvLG.1char_rotasequence.euclidean_compare: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvORIG.1char_rotasequence.euclidean_compare $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM)))
	for i in $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM))) ; do \
		for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
			../../local/src/compare_tree.py -e -o $$(printf $$i)/scaled_$$(printf $$j).newick.tmp  $$(ls $$(printf $$i)/alignment_$$(printf $$j).tmp_*.masked_LG.raxml.bestTree | bsort -nr | tr '\n' ' ')  >>$@.$$(printf $$i).LG.tmp ; \	
		done ; \
	done ; paste <(cat $<) <(cut -f 3 $@.$^2.LG.tmp) <(cut -f 3 $@.$^3.LG.tmp) <(cut -f 3 $@.$^4.LG.tmp) <(cut -f 3 $@.$^5.LG.tmp) <(cut -f 3 $@.$^6.LG.tmp) >$@ ; rm $@.*.tmp
alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvLG.1char_rotasequence.euclidean_compare: alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvORIG.1char_rotasequence.euclidean_compare alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled.1char_rotasequence.RF_compare.pre
	for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
		../../local/src/compare_tree.py -e -o $^2/scaled_$$(printf $$j).newick.tmp  $$(ls $^2/alignment_$$(printf $$j).tmp_*.masked_LG.raxml.bestTree | bsort -nr | tr '\n' ' ')  >>$@.tmp ; \
	done ; paste <(cat $<) <(cut -f3 $@.tmp) >$@ ; rm $@.tmp


# superscaled 55x55 vs LGexp in terms of tree length
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvLGexp.1char_rotasequence.tree-len_compare: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvORIG.1char_rotasequence.tree-len_compare $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM)))
	for i in $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM))) ; do \
		for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
			../../local/src/compare_tree.py -l -o $$(printf $$i)/scaled_$$(printf $$j).newick.tmp  $$(ls $$(printf $$i)/alignment_$$(printf $$j).tmp_*.LGexp.raxml.bestTree | bsort -nr | tr '\n' ' ')  >>$@.$$(printf $$i).LGexp.tmp ; \	
		done ; \
	done ; paste <(cat $<) <(cut -f 4 $@.$^2.LGexp.tmp) <(cut -f 4 $@.$^3.LGexp.tmp) <(cut -f 4 $@.$^4.LGexp.tmp) <(cut -f 4 $@.$^5.LGexp.tmp) <(cut -f 4 $@.$^6.LGexp.tmp) >$@ ; rm $@.*.tmp
# superscaled 55x55 vs LGexp in terms of unweighted RF
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvLGexp.1char_rotasequence.RF_compare: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvORIG.1char_rotasequence.RF_compare $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM)))
	for i in $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM))) ; do \
		for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
			../../local/src/compare_tree.py -o $$(printf $$i)/scaled_$$(printf $$j).newick.tmp  $$(ls $$(printf $$i)/alignment_$$(printf $$j).tmp_*.LGexp.raxml.bestTree | bsort -nr | tr '\n' ' ')  >>$@.$$(printf $$i).LGexp.tmp ; \	
		done ; \
	done ; paste <(cat $<) <(cut -f 3 $@.$^2.LGexp.tmp) <(cut -f 3 $@.$^3.LGexp.tmp) <(cut -f 3 $@.$^4.LGexp.tmp) <(cut -f 3 $@.$^5.LGexp.tmp) <(cut -f 3 $@.$^6.LGexp.tmp) >$@ ; rm $@.*.tmp



# superscaled 55x55 vs LGexp55x55freqs in terms of tree length
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvLGexp55x55freqs.1char_rotasequence.tree-len_compare: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvORIG.1char_rotasequence.tree-len_compare $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM)))
	for i in $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM))) ; do \
		for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
			../../local/src/compare_tree.py -l -o $$(printf $$i)/scaled_$$(printf $$j).newick.tmp  $$(ls $$(printf $$i)/alignment_$$(printf $$j).tmp_*.LGexp55x55freqs.raxml.bestTree | bsort -nr | tr '\n' ' ')  >>$@.$$(printf $$i).LGexp55x55freqs.tmp ; \	
		done ; \
	done ; paste <(cat $<) <(cut -f 4 $@.$^2.LGexp55x55freqs.tmp) <(cut -f 4 $@.$^3.LGexp55x55freqs.tmp) <(cut -f 4 $@.$^4.LGexp55x55freqs.tmp) <(cut -f 4 $@.$^5.LGexp55x55freqs.tmp) <(cut -f 4 $@.$^6.LGexp55x55freqs.tmp) >$@ ; rm $@.*.tmp
# superscaled 55x55 vs LGexp55x55freqs in terms of unweighted RF
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvLGexp55x55freqs.1char_rotasequence.RF_compare: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvORIG.1char_rotasequence.RF_compare $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM)))
	for i in $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM))) ; do \
		for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
			../../local/src/compare_tree.py -o $$(printf $$i)/scaled_$$(printf $$j).newick.tmp  $$(ls $$(printf $$i)/alignment_$$(printf $$j).tmp_*.LGexp55x55freqs.raxml.bestTree | bsort -nr | tr '\n' ' ')  >>$@.$$(printf $$i).LGexp55x55freqs.tmp ; \	
		done ; \
	done ; paste <(cat $<) <(cut -f 3 $@.$^2.LGexp55x55freqs.tmp) <(cut -f 3 $@.$^3.LGexp55x55freqs.tmp) <(cut -f 3 $@.$^4.LGexp55x55freqs.tmp) <(cut -f 3 $@.$^5.LGexp55x55freqs.tmp) <(cut -f 3 $@.$^6.LGexp55x55freqs.tmp) >$@ ; rm $@.*.tmp



# superscaled 55x55 vs LGbyfreq-exp55x55freqs in terms of tree length
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvLGbyfreq-exp55x55freqs.1char_rotasequence.tree-len_compare: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvORIG.1char_rotasequence.tree-len_compare $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM)))
	for i in $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM))) ; do \
		for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
			../../local/src/compare_tree.py -l -o $$(printf $$i)/scaled_$$(printf $$j).newick.tmp  $$(ls $$(printf $$i)/alignment_$$(printf $$j).tmp_*.LGbyfreq-exp55x55freqs.raxml.bestTree | bsort -nr | tr '\n' ' ')  >>$@.$$(printf $$i).LGbyfreq-exp55x55freqs.tmp ; \	
		done ; \
	done ; paste <(cat $<) <(cut -f 4 $@.$^2.LGbyfreq-exp55x55freqs.tmp) <(cut -f 4 $@.$^3.LGbyfreq-exp55x55freqs.tmp) <(cut -f 4 $@.$^4.LGbyfreq-exp55x55freqs.tmp) <(cut -f 4 $@.$^5.LGbyfreq-exp55x55freqs.tmp) <(cut -f 4 $@.$^6.LGbyfreq-exp55x55freqs.tmp) >$@ ; rm $@.*.tmp
# superscaled 55x55 vs LGbyfreq-exp55x55freqs in terms of unweighted RF
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvLGbyfreq-exp55x55freqs.1char_rotasequence.RF_compare: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvORIG.1char_rotasequence.RF_compare $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM)))
	for i in $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM))) ; do \
		for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
			../../local/src/compare_tree.py -o $$(printf $$i)/scaled_$$(printf $$j).newick.tmp  $$(ls $$(printf $$i)/alignment_$$(printf $$j).tmp_*.LGbyfreq-exp55x55freqs.raxml.bestTree | bsort -nr | tr '\n' ' ')  >>$@.$$(printf $$i).LGbyfreq-exp55x55freqs.tmp ; \	
		done ; \
	done ; paste <(cat $<) <(cut -f 3 $@.$^2.LGbyfreq-exp55x55freqs.tmp) <(cut -f 3 $@.$^3.LGbyfreq-exp55x55freqs.tmp) <(cut -f 3 $@.$^4.LGbyfreq-exp55x55freqs.tmp) <(cut -f 3 $@.$^5.LGbyfreq-exp55x55freqs.tmp) <(cut -f 3 $@.$^6.LGbyfreq-exp55x55freqs.tmp) >$@ ; rm $@.*.tmp




# superscaled 55x55 vs 20x20 in terms of tree length
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledv20x20.1char_rotasequence.tree-len_compare: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvORIG.1char_rotasequence.tree-len_compare $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM)))
	for i in $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM))) ; do \
		for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
			../../local/src/compare_tree.py -l -o $$(printf $$i)/scaled_$$(printf $$j).newick.tmp  $$(ls $$(printf $$i)/alignment_$$(printf $$j).tmp_*.masked_20x20.raxml.bestTree | bsort -nr | tr '\n' ' ')  >>$@.$$(printf $$i).20x20.tmp ; \	
		done ; \
	done ; paste <(cat $<) <(cut -f 4 $@.$^2.20x20.tmp) <(cut -f 4 $@.$^3.20x20.tmp) <(cut -f 4 $@.$^4.20x20.tmp) <(cut -f 4 $@.$^5.20x20.tmp) <(cut -f 4 $@.$^6.20x20.tmp) >$@ ; rm $@.*.tmp
# superscaled 55x55 vs 20x20 in terms of unweighted RF
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledv20x20.1char_rotasequence.RF_compare: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvORIG.1char_rotasequence.RF_compare $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM)))
	for i in $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM))) ; do \
		for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
			../../local/src/compare_tree.py -o $$(printf $$i)/scaled_$$(printf $$j).newick.tmp  $$(ls $$(printf $$i)/alignment_$$(printf $$j).tmp_*.masked_20x20.raxml.bestTree | bsort -nr | tr '\n' ' ')  >>$@.$$(printf $$i).20x20.tmp ; \	
		done ; \
	done ; paste <(cat $<) <(cut -f 3 $@.$^2.20x20.tmp) <(cut -f 3 $@.$^3.20x20.tmp) <(cut -f 3 $@.$^4.20x20.tmp) <(cut -f 3 $@.$^5.20x20.tmp) <(cut -f 3 $@.$^6.20x20.tmp) >$@ ; rm $@.*.tmp
# superscaled 55x55 vs 20x20 in terms of euclidean distance
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledv20x20.1char_rotasequence.euclidean_compare: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvORIG.1char_rotasequence.euclidean_compare $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM)))
	for i in $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM))) ; do \
		for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
			../../local/src/compare_tree.py -e -o $$(printf $$i)/scaled_$$(printf $$j).newick.tmp  $$(ls $$(printf $$i)/alignment_$$(printf $$j).tmp_*.masked_20x20.raxml.bestTree | bsort -nr | tr '\n' ' ')  >>$@.$$(printf $$i).20x20.tmp ; \	
		done ; \
	done ; paste <(cat $<) <(cut -f 3 $@.$^2.20x20.tmp) <(cut -f 3 $@.$^3.20x20.tmp) <(cut -f 3 $@.$^4.20x20.tmp) <(cut -f 3 $@.$^5.20x20.tmp) <(cut -f 3 $@.$^6.20x20.tmp) >$@ ; rm $@.*.tmp
alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledv20x20.1char_rotasequence.euclidean_compare: alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvORIG.1char_rotasequence.euclidean_compare alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled.1char_rotasequence.RF_compare.pre
	for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
		../../local/src/compare_tree.py -e -o $^2/scaled_$$(printf $$j).newick.tmp  $$(ls $^2/alignment_$$(printf $$j).tmp_*.masked_20x20.raxml.bestTree | bsort -nr | tr '\n' ' ')  >>$@.tmp ; \	
	done ; paste <(cat $<) <(cut -f3 $@.tmp) >$@ ; rm $@.tmp 

# superscaled 55x55 vs 20x20 in terms of adapted LogLikelihood (Whelan et al. 2015, PMID25209223)
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledv20x20.1char_rotasequence.adapted-Ln_compare:  alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvORIG.1char_rotasequence.adapted-Ln_compare $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM)))
	for i in $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM))) ; do \
		for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
			for k in $(shell seq 0 99 | tr "\n" " ") ; do \
				../../local/src/LL_adapter.py $$(printf $$i)/alignment_$$(printf $$j).tmp_$$(printf $$k) $$(printf $$i)/alignment_$$(printf $$j).tmp_$$(printf $$k).masked $$(printf $$i)/alignment_$$(printf $$j).tmp_$$(printf $$k).masked.masked_20x20.raxml.log | tr "/" "\t" | cut -f 2-  | sed 's/alignment_//g ;  s/.tmp_.*log//g' | cut -f 1,3 >>$@.$$(printf $$i).20x20.tmp ; \
			done ; \
		done ; \
	done ; paste <(cat $<) <(cut -f 2- $@.$^2.20x20.tmp) <(cut -f 2- $@.$^3.20x20.tmp) <(cut -f 2- $@.$^4.20x20.tmp) <(cut -f 2- $@.$^5.20x20.tmp) <(cut -f 2- $@.$^6.20x20.tmp) >$@ ; rm $@.*.tmp
# compare run-time vs 55x55
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledv20x20.1char_rotasequence.run-time_compare: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvORIG.1char_rotasequence.run-time_compare $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM)))
	for i in $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM))); do \
		for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
			grep 'Elapsed time:' $$(ls $$(printf $$i)/alignment_$$(printf $$j).tmp_*.masked.masked_20x20.raxml.log | bsort -nr | tr '\n' ' ' ) \
			| sed 's/ seconds//g'  | tr "/" "\t" | cut -f2 | sed 's/alignment_//g  ; s/.tmp.*: /\t/g' >>$@.$$(printf $$i).20x20.tmp ; \
		done ; \
	done ; paste <(cat $<) <(cut -f 2- $@.$^2.20x20.tmp) <(cut -f 2- $@.$^3.20x20.tmp) <(cut -f 2- $@.$^4.20x20.tmp) <(cut -f 2- $@.$^5.20x20.tmp)  <(cut -f 2- $@.$^6.20x20.tmp) >$@ ; rm $@.*.tmp
# 55x55noG on alignments simulated with 55x55+G, compare in terms of unweighted RF
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_plusGv55x55.1char_rotasequence.RF_compare: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_plusGvORIG.1char_rotasequence.RF_compare $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_plusG.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM)))
	for i in $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_plusG.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM))) ; do \
		for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
			../../local/src/compare_tree.py -o $$(printf $$i)/scaled_$$(printf $$j).newick.tmp  $$(ls $$(printf $$i)/alignment_$$(printf $$j).tmp_*55x55noG.raxml.bestTree | bsort -nr | tr '\n' ' ')  >>$@.$$(printf $$i).55x55noG.tmp ; \	
		done ; \
	done ; paste <(cat $<) <(cut -f 3 $@.$^2.55x55noG.tmp) <(cut -f 3 $@.$^3.55x55noG.tmp) <(cut -f 3 $@.$^4.55x55noG.tmp) <(cut -f 3 $@.$^5.55x55noG.tmp) <(cut -f 3 $@.$^6.55x55noG.tmp) >$@ ; rm $@.*.tmp
# superscaled 55x55+G vs 55x55noG in terms of adapted LogLikelihood (Whelan et al. 2015, PMID25209223)
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_plusGv55x55.1char_rotasequence.adapted-Ln_compare: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_plusGvORIG.1char_rotasequence.adapted-Ln_compare $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_plusG.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM)))
	for i in $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_plusG.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM))) ; do \
		for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
			for k in $(shell seq 0 99 | tr "\n" " ") ; do \
				../../local/src/LL_adapter.py $$(printf $$i)/alignment_$$(printf $$j).tmp_$$(printf $$k) $$(printf $$i)/alignment_$$(printf $$j).tmp_$$(printf $$k).masked $$(printf $$i)/alignment_$$(printf $$j).tmp_$$(printf $$k).masked.masked_55x55noG.raxml.log | tr "/" "\t" | cut -f 2-  | sed 's/alignment_//g ;  s/.tmp_.*log//g' | cut -f 1,3 >>$@.$$(printf $$i).55x55noG.tmp ; \
			done ; \
		done ; \
	done ; paste <(cat $<) <(cut -f 2- $@.$^2.55x55nog.tmp) <(cut -f 2- $@.$^3.55x55nog.tmp) <(cut -f 2- $@.$^4.55x55nog.tmp) <(cut -f 2- $@.$^5.55x55nog.tmp) <(cut -f 2- $@.$^6.55x55nog.tmp) >$@ ; rm $@.*.tmp



# superscaled 55x55 vs LG in terms of adapted LogLikelihood (Whelan et al. 2015, PMID25209223)
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvLG.1char_rotasequence.adapted-Ln_compare:  alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvORIG.1char_rotasequence.adapted-Ln_compare $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM)))
	for i in $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM))) ; do \
		for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
			for k in $(shell seq 0 99 | tr "\n" " ") ; do \
				../../local/src/LL_adapter.py $$(printf $$i)/alignment_$$(printf $$j).tmp_$$(printf $$k) $$(printf $$i)/alignment_$$(printf $$j).tmp_$$(printf $$k).masked $$(printf $$i)/alignment_$$(printf $$j).tmp_$$(printf $$k).masked.masked_LG.raxml.log | tr "/" "\t" | cut -f 2-  | sed 's/alignment_//g ;  s/.tmp_.*log//g' | cut -f 1,3 >>$@.$$(printf $$i).LG.tmp ; \
			done ; \
		done ; \
	done ; paste <(cat $<) <(cut -f 2- $@.$^2.LG.tmp) <(cut -f 2- $@.$^3.LG.tmp) <(cut -f 2- $@.$^4.LG.tmp) <(cut -f 2- $@.$^5.LG.tmp) <(cut -f 2- $@.$^6.LG.tmp) >$@ ; rm $@.*.tmp

# superscaled 55x55 vs LGplusF in terms of adapted LogLikelihood (Whelan et al. 2015, PMID25209223)
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvLGplusF.1char_rotasequence.adapted-Ln_compare:  alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvORIG.1char_rotasequence.adapted-Ln_compare $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM)))
	for i in $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM))) ; do \
		for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
			for k in $(shell seq 0 99 | tr "\n" " ") ; do \
				../../local/src/LL_adapter.py $$(printf $$i)/alignment_$$(printf $$j).tmp_$$(printf $$k) $$(printf $$i)/alignment_$$(printf $$j).tmp_$$(printf $$k).masked $$(printf $$i)/alignment_$$(printf $$j).tmp_$$(printf $$k).masked.masked_LGplusF.raxml.log | tr "/" "\t" | cut -f 2-  | sed 's/alignment_//g ;  s/.tmp_.*log//g' | cut -f 1,3 >>$@.$$(printf $$i).LGplusF.tmp ; \
			done ; \
		done ; \
	done ; paste <(cat $<) <(cut -f 2- $@.$^2.LGplusF.tmp) <(cut -f 2- $@.$^3.LGplusF.tmp) <(cut -f 2- $@.$^4.LGplusF.tmp) <(cut -f 2- $@.$^5.LGplusF.tmp) <(cut -f 2- $@.$^6.LGplusF.tmp) >$@ ; rm $@.*.tmp


# compare LG run-time vs 55x55's
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvLG.1char_rotasequence.run-time_compare: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvORIG.1char_rotasequence.run-time_compare $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM)))
	for i in $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM))); do \
		for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
			grep 'Elapsed time:' $$(ls $$(printf $$i)/alignment_$$(printf $$j).tmp_*.masked.masked_LG.raxml.log | bsort -nr | tr '\n' ' ' ) \
			| sed 's/ seconds//g'  | tr "/" "\t" | cut -f2 | sed 's/alignment_//g  ; s/.tmp.*: /\t/g' >>$@.$$(printf $$i).LG.tmp ; \
		done ; \
	done ; paste <(cat $<) <(cut -f 2- $@.$^2.LG.tmp) <(cut -f 2- $@.$^3.LG.tmp) <(cut -f 2- $@.$^4.LG.tmp) <(cut -f 2- $@.$^5.LG.tmp) <(cut -f 2- $@.$^6.LG.tmp) >$@ ; rm $@.*.tmp


# LGplusG on alignments simulated with 55x55+G, compare in terms of unweighted RF
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_plusGvLGplusG.1char_rotasequence.RF_compare: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_plusGvORIG.1char_rotasequence.RF_compare $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_plusG.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM)))
	for i in $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_plusG.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM))) ; do \
		for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
			../../local/src/compare_tree.py -o $$(printf $$i)/scaled_$$(printf $$j).newick.tmp  $$(ls $$(printf $$i)/alignment_$$(printf $$j).tmp_*LGplusG.raxml.bestTree | bsort -nr | tr '\n' ' ')  >>$@.$$(printf $$i).LGplusG.tmp ; \	
		done ; \
	done ; paste <(cat $<) <(cut -f 3 $@.$^2.LGplusG.tmp) <(cut -f 3 $@.$^3.LGplusG.tmp) <(cut -f 3 $@.$^4.LGplusG.tmp) <(cut -f 3 $@.$^5.LGplusG.tmp) <(cut -f 3 $@.$^6.LGplusG.tmp) >$@ ; rm $@.*.tmp
# superscaled 55x55+G vs LGplusG in terms of adapted LogLikelihood (Whelan et al. 2015, PMID25209223)
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_plusGvLGplusG.1char_rotasequence.adapted-Ln_compare: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_plusGvORIG.1char_rotasequence.adapted-Ln_compare $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_plusG.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM)))
	for i in $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_plusG.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM))) ; do \
		for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
			for k in $(shell seq 0 99 | tr "\n" " ") ; do \
				../../local/src/LL_adapter.py $$(printf $$i)/alignment_$$(printf $$j).tmp_$$(printf $$k) $$(printf $$i)/alignment_$$(printf $$j).tmp_$$(printf $$k).masked $$(printf $$i)/alignment_$$(printf $$j).tmp_$$(printf $$k).masked.masked_LGplusG.raxml.log | tr "/" "\t" | cut -f 2-  | sed 's/alignment_//g ;  s/.tmp_.*log//g' | cut -f 1,3 >>$@.$$(printf $$i).LGplusG.tmp ; \
			done ; \
		done ; \
	done ; paste <(cat $<) <(cut -f 2- $@.$^2.LGplusG.tmp) <(cut -f 2- $@.$^3.LGplusG.tmp) <(cut -f 2- $@.$^4.LGplusG.tmp) <(cut -f 2- $@.$^5.LGplusG.tmp) <(cut -f 2- $@.$^6.LGplusG.tmp) >$@ ; rm $@.*.tmp

# 20x20plusG on alignments simulated with 55x55+G, compare in terms of unweighted RF
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_plusGv20x20plusG.1char_rotasequence.RF_compare: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_plusGvORIG.1char_rotasequence.RF_compare $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_plusG.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM)))
	for i in $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_plusG.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM))) ; do \
		for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
			../../local/src/compare_tree.py -o $$(printf $$i)/scaled_$$(printf $$j).newick.tmp  $$(ls $$(printf $$i)/alignment_$$(printf $$j).tmp_*20x20plusG.raxml.bestTree | bsort -nr | tr '\n' ' ')  >>$@.$$(printf $$i).20x20plusG.tmp ; \	
		done ; \
	done ; paste <(cat $<) <(cut -f 3 $@.$^2.20x20plusG.tmp) <(cut -f 3 $@.$^3.20x20plusG.tmp) <(cut -f 3 $@.$^4.20x20plusG.tmp) <(cut -f 3 $@.$^5.20x20plusG.tmp) <(cut -f 3 $@.$^6.20x20plusG.tmp) >$@ ; rm $@.*.tmp
# superscaled 55x55+G vs 20x20plusG in terms of adapted LogLikelihood (Whelan et al. 2015, PMID25209223)
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_plusGv20x20plusG.1char_rotasequence.adapted-Ln_compare: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_plusGvORIG.1char_rotasequence.adapted-Ln_compare $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_plusG.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM)))
	for i in $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_plusG.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM))) ; do \
		for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
			for k in $(shell seq 0 99 | tr "\n" " ") ; do \
				../../local/src/LL_adapter.py $$(printf $$i)/alignment_$$(printf $$j).tmp_$$(printf $$k) $$(printf $$i)/alignment_$$(printf $$j).tmp_$$(printf $$k).masked $$(printf $$i)/alignment_$$(printf $$j).tmp_$$(printf $$k).masked.masked_20x20plusG.raxml.log | tr "/" "\t" | cut -f 2-  | sed 's/alignment_//g ;  s/.tmp_.*log//g' | cut -f 1,3 >>$@.$$(printf $$i).20x20plusG.tmp ; \
			done ; \
		done ; \
	done ; paste <(cat $<) <(cut -f 2- $@.$^2.20x20plusG.tmp) <(cut -f 2- $@.$^3.20x20plusG.tmp) <(cut -f 2- $@.$^4.20x20plusG.tmp) <(cut -f 2- $@.$^5.20x20plusG.tmp) <(cut -f 2- $@.$^6.20x20plusG.tmp) >$@ ; rm $@.*.tmp

# LGbyfreq-exp55x55freqsplusG on alignments simulated with 55x55+G, compare in terms of unweighted RF
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_plusGvLGbyfreq-exp55x55freqsplusG.1char_rotasequence.RF_compare: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_plusGvORIG.1char_rotasequence.RF_compare $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_plusG.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM)))
	for i in $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_plusG.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM))) ; do \
		for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
			../../local/src/compare_tree.py -o $$(printf $$i)/scaled_$$(printf $$j).newick.tmp  $$(ls $$(printf $$i)/alignment_$$(printf $$j).tmp_*LGbyfreq-exp55x55freqsplusG.raxml.bestTree | bsort -nr | tr '\n' ' ')  >>$@.$$(printf $$i).LGbyfreq-exp55x55freqsplusG.tmp ; \	
		done ; \
	done ; paste <(cat $<) <(cut -f 3 $@.$^2.LGbyfreq-exp55x55freqsplusG.tmp) <(cut -f 3 $@.$^3.LGbyfreq-exp55x55freqsplusG.tmp) <(cut -f 3 $@.$^4.LGbyfreq-exp55x55freqsplusG.tmp) <(cut -f 3 $@.$^5.LGbyfreq-exp55x55freqsplusG.tmp) <(cut -f 3 $@.$^6.LGbyfreq-exp55x55freqsplusG.tmp) >$@ ; rm $@.*.tmp
# superscaled 55x55+G vs LGbyfreq-exp55x55freqsplusG in terms of adapted LogLikelihood (Whelan et al. 2015, PMID25209223)
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_plusGvLGbyfreq-exp55x55freqsplusG.1char_rotasequence.adapted-Ln_compare: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_plusGvORIG.1char_rotasequence.adapted-Ln_compare $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_plusG.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM)))
	for i in $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_plusG.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM))) ; do \
		for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
			for k in $(shell seq 0 99 | tr "\n" " ") ; do \
				../../local/src/LL_adapter.py $$(printf $$i)/alignment_$$(printf $$j).tmp_$$(printf $$k) $$(printf $$i)/alignment_$$(printf $$j).tmp_$$(printf $$k).masked $$(printf $$i)/alignment_$$(printf $$j).tmp_$$(printf $$k).masked.masked_LGbyfreq-exp55x55freqsplusG.raxml.log | tr "/" "\t" | cut -f 2-  | sed 's/alignment_//g ;  s/.tmp_.*log//g' | cut -f 1,3 >>$@.$$(printf $$i).LGbyfreq-exp55x55freqsplusG.tmp ; \
			done ; \
		done ; \
	done ; paste <(cat $<) <(cut -f 2- $@.$^2.LGbyfreq-exp55x55freqsplusG.tmp) <(cut -f 2- $@.$^3.LGbyfreq-exp55x55freqsplusG.tmp) <(cut -f 2- $@.$^4.LGbyfreq-exp55x55freqsplusG.tmp) <(cut -f 2- $@.$^5.LGbyfreq-exp55x55freqsplusG.tmp) <(cut -f 2- $@.$^6.LGbyfreq-exp55x55freqsplusG.tmp) >$@ ; rm $@.*.tmp



# superscaled 55x55 vs LGexp in terms of LogLikelihood (not adapted since the model is already expanded)
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvLGexp.1char_rotasequence.Ln_compare:  alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvORIG.1char_rotasequence.adapted-Ln_compare $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM)))
	for i in $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM))) ; do \
		for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
			grep 'Final LogLikelihood:'  $$(ls $$(printf $$i)/alignment_$$(printf $$j).tmp_*.LGexp.raxml.log | bsort -nr | tr '\n' ' ' ) \
			| tr "/" "\t" | cut -f2 | sed 's/alignment_//g  ; s/.tmp.*: /\t/g' >>$@.$$(printf $$i).LGexp.tmp ; \
		done ; \
	done ; paste <(cat $<) <(cut -f 2- $@.$^2.LGexp.tmp) <(cut -f 2- $@.$^3.LGexp.tmp) <(cut -f 2- $@.$^4.LGexp.tmp) <(cut -f 2- $@.$^5.LGexp.tmp) <(cut -f 2- $@.$^6.LGexp.tmp) >$@ ; rm $@.*.tmp


# superscaled 55x55 vs LGexp55x55freqs in terms of LogLikelihood (not adapted since the model is already expanded)
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvLGexp55x55freqs.1char_rotasequence.Ln_compare:  alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvORIG.1char_rotasequence.adapted-Ln_compare $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM)))
	for i in $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM))) ; do \
		for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
			grep 'Final LogLikelihood:'  $$(ls $$(printf $$i)/alignment_$$(printf $$j).tmp_*.LGexp55x55freqs.raxml.log | bsort -nr | tr '\n' ' ' ) \
			| tr "/" "\t" | cut -f2 | sed 's/alignment_//g  ; s/.tmp.*: /\t/g' >>$@.$$(printf $$i).LGexp55x55freqs.tmp ; \
		done ; \
	done ; paste <(cat $<) <(cut -f 2- $@.$^2.LGexp55x55freqs.tmp) <(cut -f 2- $@.$^3.LGexp55x55freqs.tmp) <(cut -f 2- $@.$^4.LGexp55x55freqs.tmp) <(cut -f 2- $@.$^5.LGexp55x55freqs.tmp) <(cut -f 2- $@.$^6.LGexp55x55freqs.tmp) >$@ ; rm $@.*.tmp


# superscaled 55x55 vs LGbyfreq-exp55x55freqs in terms of LogLikelihood (not adapted since the model is already expanded)
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvLGbyfreq-exp55x55freqs.1char_rotasequence.Ln_compare:  alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvORIG.1char_rotasequence.adapted-Ln_compare $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM)))
	for i in $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM))) ; do \
		for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
			grep 'Final LogLikelihood:'  $$(ls $$(printf $$i)/alignment_$$(printf $$j).tmp_*.LGbyfreq-exp55x55freqs.raxml.log | bsort -nr | tr '\n' ' ' ) \
			| tr "/" "\t" | cut -f2 | sed 's/alignment_//g  ; s/.tmp.*: /\t/g' >>$@.$$(printf $$i).LGbyfreq-exp55x55freqs.tmp ; \
		done ; \
	done ; paste <(cat $<) <(cut -f 2- $@.$^2.LGbyfreq-exp55x55freqs.tmp) <(cut -f 2- $@.$^3.LGbyfreq-exp55x55freqs.tmp) <(cut -f 2- $@.$^4.LGbyfreq-exp55x55freqs.tmp) <(cut -f 2- $@.$^5.LGbyfreq-exp55x55freqs.tmp) <(cut -f 2- $@.$^6.LGbyfreq-exp55x55freqs.tmp) >$@ ; rm $@.*.tmp

# superscaled 55x55 vs LG in terms of single branch length, uses a guide tree
# compares second-shortest and second-longest branch in internal and leaf branches
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_AND_guide-treevLG.1char_rotasequence.single-brlen_compare: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_AND_guide-treevORIG.1char_rotasequence.single-brlen_compare $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_AND_guide-tree.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM)))
	for i in $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_AND_guide-tree.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM))) ; do \
		for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
			../../local/src/compare_tree.py -s -o $$(printf $$i)/scaled_$$(printf $$j).newick.tmp  $$(ls $$(printf $$i)/alignment_$$(printf $$j).tmp*LG.raxml.bestTree | bsort -nr | tr '\n' ' ')  >>$@.$$(printf $$i).LG.tmp ; \
		done ; \
	done ; printf 'scale\t4_taxa_REF_int_sec-short\t4_taxa_REF_int_sec-long\t4_taxa_REF_lf_sec-short\t4_taxa_REF_lf_sec-long\t4_taxa_55x55_int_sec-short\t4_taxa_55x55_int_sec-long\t4_taxa_55x55_lf_sec-short\t4_taxa_55x55_lf_sec-long\t8_taxa_REF_int_sec-short\t8_taxa_REF_int_sec-long\t8_taxa_REF_lf_sec-short\t8_taxa_REF_lf_sec-long\t8_taxa_55x55_int_sec-short\t8_taxa_55x55_int_sec-long\t8_taxa_55x55_lf_sec-short\t8_taxa_55x55_lf_sec-long\t16_taxa_REF_int_sec-short\t16_taxa_REF_int_sec-long\t16_taxa_REF_lf_sec-short\t16_taxa_REF_lf_sec-long\t16_taxa_55x55_int_sec-short\t16_taxa_55x55_int_sec-long\t16_taxa_55x55_lf_sec-short\t16_taxa_55x55_lf_sec-long\t32_taxa_REF_int_sec-short\t32_taxa_REF_int_sec-long\t32_taxa_REF_lf_sec-short\t32_taxa_REF_lf_sec-long\t32_taxa_55x55_int_sec-short\t32_taxa_55x55_int_sec-long\t32_taxa_55x55_lf_sec-short\t32_taxa_55x55_lf_sec-long\t64_taxa_REF_int_sec-short\t64_taxa_REF_int_sec-long\t64_taxa_REF_lf_sec-short\t64_taxa_REF_lf_sec-long\t64_taxa_55x55_int_sec-short\t64_taxa_55x55_int_sec-long\t64_taxa_55x55_lf_sec-short\t64_taxa_55x55_lf_sec-long\t4_taxa_LG_int_sec-short\t4_taxa_LG_int_sec-long\t4_taxa_LG_lf_sec-short\t4_taxa_LG_lf_sec-long\t8_taxa_LG_int_sec-short\t8_taxa_LG_int_sec-long\t8_taxa_LG_lf_sec-short\t8_taxa_LG_lf_sec-long\t16_taxa_LG_int_sec-short\t16_taxa_LG_int_sec-long\t16_taxa_LG_lf_sec-short\t16_taxa_LG_lf_sec-long\t32_taxa_LG_int_sec-short\t32_taxa_LG_int_sec-long\t32_taxa_LG_lf_sec-short\t32_taxa_LG_lf_sec-long\t64_taxa_LG_int_sec-short\t64_taxa_LG_int_sec-long\t64_taxa_LG_lf_sec-short\t64_taxa_LG_lf_sec-long\n' >$@ ; \ 
	paste <(cat $<) <(cut -f 7- $@.$^2.LG.tmp) <(cut -f 7- $@.$^3.LG.tmp) <(cut -f 7- $@.$^4.LG.tmp) <(cut -f 7- $@.$^5.LG.tmp) <(cut -f 7- $@.$^6.LG.tmp) >>$@ ; rm $@.*.tmp


# superscaled 55x55 vs 20x20 in terms of single branch length, uses a guide tree
# compares second-shortest and second-longest branch in internal and leaf branches
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_AND_guide-treev20x20.1char_rotasequence.single-brlen_compare: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_AND_guide-treevORIG.1char_rotasequence.single-brlen_compare $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_AND_guide-tree.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM)))
	for i in $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_AND_guide-tree.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM))) ; do \
		for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
			../../local/src/compare_tree.py -s -o $$(printf $$i)/scaled_$$(printf $$j).newick.tmp  $$(ls $$(printf $$i)/alignment_$$(printf $$j).tmp_*.20x20.raxml.bestTree | bsort -nr | tr '\n' ' ')  >>$@.$$(printf $$i).20x20.tmp ; \
		done ; \
	done ; printf 'scale\t4_taxa_REF_int_sec-short\t4_taxa_REF_int_sec-long\t4_taxa_REF_lf_sec-short\t4_taxa_REF_lf_sec-long\t4_taxa_55x55_int_sec-short\t4_taxa_55x55_int_sec-long\t4_taxa_55x55_lf_sec-short\t4_taxa_55x55_lf_sec-long\t8_taxa_REF_int_sec-short\t8_taxa_REF_int_sec-long\t8_taxa_REF_lf_sec-short\t8_taxa_REF_lf_sec-long\t8_taxa_55x55_int_sec-short\t8_taxa_55x55_int_sec-long\t8_taxa_55x55_lf_sec-short\t8_taxa_55x55_lf_sec-long\t16_taxa_REF_int_sec-short\t16_taxa_REF_int_sec-long\t16_taxa_REF_lf_sec-short\t16_taxa_REF_lf_sec-long\t16_taxa_55x55_int_sec-short\t16_taxa_55x55_int_sec-long\t16_taxa_55x55_lf_sec-short\t16_taxa_55x55_lf_sec-long\t32_taxa_REF_int_sec-short\t32_taxa_REF_int_sec-long\t32_taxa_REF_lf_sec-short\t32_taxa_REF_lf_sec-long\t32_taxa_55x55_int_sec-short\t32_taxa_55x55_int_sec-long\t32_taxa_55x55_lf_sec-short\t32_taxa_55x55_lf_sec-long\t64_taxa_REF_int_sec-short\t64_taxa_REF_int_sec-long\t64_taxa_REF_lf_sec-short\t64_taxa_REF_lf_sec-long\t64_taxa_55x55_int_sec-short\t64_taxa_55x55_int_sec-long\t64_taxa_55x55_lf_sec-short\t64_taxa_55x55_lf_sec-long\t4_taxa_20x20_int_sec-short\t4_taxa_20x20_int_sec-long\t4_taxa_20x20_lf_sec-short\t4_taxa_20x20_lf_sec-long\t8_taxa_20x20_int_sec-short\t8_taxa_20x20_int_sec-long\t8_taxa_20x20_lf_sec-short\t8_taxa_20x20_lf_sec-long\t16_taxa_20x20_int_sec-short\t16_taxa_20x20_int_sec-long\t16_taxa_20x20_lf_sec-short\t16_taxa_20x20_lf_sec-long\t32_taxa_20x20_int_sec-short\t32_taxa_20x20_int_sec-long\t32_taxa_20x20_lf_sec-short\t32_taxa_20x20_lf_sec-long\t64_taxa_20x20_int_sec-short\t64_taxa_20x20_int_sec-long\t64_taxa_20x20_lf_sec-short\t64_taxa_20x20_lf_sec-long\n' >$@ ; \ 
	paste <(cat $<) <(cut -f 7- $@.$^2.20x20.tmp) <(cut -f 7- $@.$^3.20x20.tmp) <(cut -f 7- $@.$^4.20x20.tmp) <(cut -f 7- $@.$^5.20x20.tmp) <(cut -f 7- $@.$^6.20x20.tmp) >>$@ ; rm $@.*.tmp


# superscaled 55x55 vs LGexp in terms of single branch length, uses a guide tree
# compares second-shortest and second-longest branch in internal and leaf branches
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_AND_guide-treevLGexp.1char_rotasequence.single-brlen_compare: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_AND_guide-treevORIG.1char_rotasequence.single-brlen_compare $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_AND_guide-tree.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM)))
	for i in $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_AND_guide-tree.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM))) ; do \
		for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
			../../local/src/compare_tree.py -s -o $$(printf $$i)/scaled_$$(printf $$j).newick.tmp  $$(ls $$(printf $$i)/alignment_$$(printf $$j).tmp_*.LGexp.raxml.bestTree | bsort -nr | tr '\n' ' ')  >>$@.$$(printf $$i).LGexp.tmp ; \
		done ; \
	done ; printf 'scale\t4_taxa_REF_int_sec-short\t4_taxa_REF_int_sec-long\t4_taxa_REF_lf_sec-short\t4_taxa_REF_lf_sec-long\t4_taxa_55x55_int_sec-short\t4_taxa_55x55_int_sec-long\t4_taxa_55x55_lf_sec-short\t4_taxa_55x55_lf_sec-long\t8_taxa_REF_int_sec-short\t8_taxa_REF_int_sec-long\t8_taxa_REF_lf_sec-short\t8_taxa_REF_lf_sec-long\t8_taxa_55x55_int_sec-short\t8_taxa_55x55_int_sec-long\t8_taxa_55x55_lf_sec-short\t8_taxa_55x55_lf_sec-long\t16_taxa_REF_int_sec-short\t16_taxa_REF_int_sec-long\t16_taxa_REF_lf_sec-short\t16_taxa_REF_lf_sec-long\t16_taxa_55x55_int_sec-short\t16_taxa_55x55_int_sec-long\t16_taxa_55x55_lf_sec-short\t16_taxa_55x55_lf_sec-long\t32_taxa_REF_int_sec-short\t32_taxa_REF_int_sec-long\t32_taxa_REF_lf_sec-short\t32_taxa_REF_lf_sec-long\t32_taxa_55x55_int_sec-short\t32_taxa_55x55_int_sec-long\t32_taxa_55x55_lf_sec-short\t32_taxa_55x55_lf_sec-long\t64_taxa_REF_int_sec-short\t64_taxa_REF_int_sec-long\t64_taxa_REF_lf_sec-short\t64_taxa_REF_lf_sec-long\t64_taxa_55x55_int_sec-short\t64_taxa_55x55_int_sec-long\t64_taxa_55x55_lf_sec-short\t64_taxa_55x55_lf_sec-long\t4_taxa_LGexp_int_sec-short\t4_taxa_LGexp_int_sec-long\t4_taxa_LGexp_lf_sec-short\t4_taxa_LGexp_lf_sec-long\t8_taxa_LGexp_int_sec-short\t8_taxa_LGexp_int_sec-long\t8_taxa_LGexp_lf_sec-short\t8_taxa_LGexp_lf_sec-long\t16_taxa_LGexp_int_sec-short\t16_taxa_LGexp_int_sec-long\t16_taxa_LGexp_lf_sec-short\t16_taxa_LGexp_lf_sec-long\t32_taxa_LGexp_int_sec-short\t32_taxa_LGexp_int_sec-long\t32_taxa_LGexp_lf_sec-short\t32_taxa_LGexp_lf_sec-long\t64_taxa_LGexp_int_sec-short\t64_taxa_LGexp_int_sec-long\t64_taxa_LGexp_lf_sec-short\t64_taxa_LGexp_lf_sec-long\n' >$@ ; \ 
	paste <(cat $<) <(cut -f 7- $@.$^2.LGexp.tmp) <(cut -f 7- $@.$^3.LGexp.tmp) <(cut -f 7- $@.$^4.LGexp.tmp) <(cut -f 7- $@.$^5.LGexp.tmp) <(cut -f 7- $@.$^6.LGexp.tmp) >>$@ ; rm $@.*.tmp



# superscaled 55x55 vs LGbyfreq-exp55x55freqs in terms of single branch length, uses a guide tree
# compares second-shortest and second-longest branch in internal and leaf branches
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_AND_guide-treevLGbyfreq-exp55x55freqs.1char_rotasequence.single-brlen_compare: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_AND_guide-treevORIG.1char_rotasequence.single-brlen_compare $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_AND_guide-tree.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM)))
	for i in $(addsuffix x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_AND_guide-tree.1char_rotasequence.RF_compare.pre, $(addprefix alignment, $(TAXA_NUM))) ; do \
		for j in $(shell printf $(SCALING_FACTORS) | tr "," " ") ; do \
			../../local/src/compare_tree.py -s -o $$(printf $$i)/scaled_$$(printf $$j).newick.tmp  $$(ls $$(printf $$i)/alignment_$$(printf $$j).tmp_*.LGbyfreq-exp55x55freqs.raxml.bestTree | bsort -nr | tr '\n' ' ')  >>$@.$$(printf $$i).LGbyfreq-exp55x55freqs.tmp ; \
		done ; \
	done ; printf 'scale\t4_taxa_REF_int_sec-short\t4_taxa_REF_int_sec-long\t4_taxa_REF_lf_sec-short\t4_taxa_REF_lf_sec-long\t4_taxa_55x55_int_sec-short\t4_taxa_55x55_int_sec-long\t4_taxa_55x55_lf_sec-short\t4_taxa_55x55_lf_sec-long\t8_taxa_REF_int_sec-short\t8_taxa_REF_int_sec-long\t8_taxa_REF_lf_sec-short\t8_taxa_REF_lf_sec-long\t8_taxa_55x55_int_sec-short\t8_taxa_55x55_int_sec-long\t8_taxa_55x55_lf_sec-short\t8_taxa_55x55_lf_sec-long\t16_taxa_REF_int_sec-short\t16_taxa_REF_int_sec-long\t16_taxa_REF_lf_sec-short\t16_taxa_REF_lf_sec-long\t16_taxa_55x55_int_sec-short\t16_taxa_55x55_int_sec-long\t16_taxa_55x55_lf_sec-short\t16_taxa_55x55_lf_sec-long\t32_taxa_REF_int_sec-short\t32_taxa_REF_int_sec-long\t32_taxa_REF_lf_sec-short\t32_taxa_REF_lf_sec-long\t32_taxa_55x55_int_sec-short\t32_taxa_55x55_int_sec-long\t32_taxa_55x55_lf_sec-short\t32_taxa_55x55_lf_sec-long\t64_taxa_REF_int_sec-short\t64_taxa_REF_int_sec-long\t64_taxa_REF_lf_sec-short\t64_taxa_REF_lf_sec-long\t64_taxa_55x55_int_sec-short\t64_taxa_55x55_int_sec-long\t64_taxa_55x55_lf_sec-short\t64_taxa_55x55_lf_sec-long\t4_taxa_LGbyfreq-exp55x55freqs_int_sec-short\t4_taxa_LGbyfreq-exp55x55freqs_int_sec-long\t4_taxa_LGbyfreq-exp55x55freqs_lf_sec-short\t4_taxa_LGbyfreq-exp55x55freqs_lf_sec-long\t8_taxa_LGbyfreq-exp55x55freqs_int_sec-short\t8_taxa_LGbyfreq-exp55x55freqs_int_sec-long\t8_taxa_LGbyfreq-exp55x55freqs_lf_sec-short\t8_taxa_LGbyfreq-exp55x55freqs_lf_sec-long\t16_taxa_LGbyfreq-exp55x55freqs_int_sec-short\t16_taxa_LGbyfreq-exp55x55freqs_int_sec-long\t16_taxa_LGbyfreq-exp55x55freqs_lf_sec-short\t16_taxa_LGbyfreq-exp55x55freqs_lf_sec-long\t32_taxa_LGbyfreq-exp55x55freqs_int_sec-short\t32_taxa_LGbyfreq-exp55x55freqs_int_sec-long\t32_taxa_LGbyfreq-exp55x55freqs_lf_sec-short\t32_taxa_LGbyfreq-exp55x55freqs_lf_sec-long\t64_taxa_LGbyfreq-exp55x55freqs_int_sec-short\t64_taxa_LGbyfreq-exp55x55freqs_int_sec-long\t64_taxa_LGbyfreq-exp55x55freqs_lf_sec-short\t64_taxa_LGbyfreq-exp55x55freqs_lf_sec-long\n' >$@ ; \ 
	paste <(cat $<) <(cut -f 7- $@.$^2.LGbyfreq-exp55x55freqs.tmp) <(cut -f 7- $@.$^3.LGbyfreq-exp55x55freqs.tmp) <(cut -f 7- $@.$^4.LGbyfreq-exp55x55freqs.tmp) <(cut -f 7- $@.$^5.LGbyfreq-exp55x55freqs.tmp) <(cut -f 7- $@.$^6.LGbyfreq-exp55x55freqs.tmp) >>$@ ; rm $@.*.tmp




.META: alignmentALL*_scaled-branches_evo_simulation.55x55_superscaled_AND_guide-treev*.1char_rotasequence.single-brlen_compare
	1	scale
	2	4_taxa_REF_int_sec-short
	3	4_taxa_REF_int_sec-long
	4	4_taxa_REF_lf_sec-short
	5	4_taxa_REF_lf_sec-long
	6	4_taxa_55x55_int_sec-short
	7	4_taxa_55x55_int_sec-long
	8	4_taxa_55x55_lf_sec-short
	9	4_taxa_55x55_lf_sec-long
	10	8_taxa_REF_int_sec-short
	11	8_taxa_REF_int_sec-long
	12	8_taxa_REF_lf_sec-short
	13	8_taxa_REF_lf_sec-long
	14	8_taxa_55x55_int_sec-short
	15	8_taxa_55x55_int_sec-long
	16	8_taxa_55x55_lf_sec-short
	17	8_taxa_55x55_lf_sec-long
	18	16_taxa_REF_int_sec-short
	19	16_taxa_REF_int_sec-long
	20	16_taxa_REF_lf_sec-short
	21	16_taxa_REF_lf_sec-long
	22	16_taxa_55x55_int_sec-short
	23	16_taxa_55x55_int_sec-long
	24	16_taxa_55x55_lf_sec-short
	25	16_taxa_55x55_lf_sec-long
	26	32_taxa_REF_int_sec-short
	27	32_taxa_REF_int_sec-long
	28	32_taxa_REF_lf_sec-short
	29	32_taxa_REF_lf_sec-long
	30	32_taxa_55x55_int_sec-short
	31	32_taxa_55x55_int_sec-long
	32	32_taxa_55x55_lf_sec-short
	33	32_taxa_55x55_lf_sec-long
	34	64_taxa_REF_int_sec-short
	35	64_taxa_REF_int_sec-long
	36	64_taxa_REF_lf_sec-short
	37	64_taxa_REF_lf_sec-long
	38	64_taxa_55x55_int_sec-short
	39	64_taxa_55x55_int_sec-long
	40	64_taxa_55x55_lf_sec-short
	41	64_taxa_55x55_lf_sec-long
	42	4_taxa_LG_int_sec-short
	43	4_taxa_LG_int_sec-long
	44	4_taxa_LG_lf_sec-short
	45	4_taxa_LG_lf_sec-long
	46	8_taxa_LG_int_sec-short
	47	8_taxa_LG_int_sec-long
	48	8_taxa_LG_lf_sec-short
	49	8_taxa_LG_lf_sec-long
	50	16_taxa_LG_int_sec-short
	51	16_taxa_LG_int_sec-long
	52	16_taxa_LG_lf_sec-short
	53	16_taxa_LG_lf_sec-long
	54	32_taxa_LG_int_sec-short
	55	32_taxa_LG_int_sec-long
	56	32_taxa_LG_lf_sec-short
	57	32_taxa_LG_lf_sec-long
	58	64_taxa_LG_int_sec-short
	59	64_taxa_LG_int_sec-long
	60	64_taxa_LG_lf_sec-short
	61	64_taxa_LG_lf_sec-long

alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55vORIG.1char_rotasequence.tree-len_compare.pdf: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55vORIG.1char_rotasequence.tree-len_compare 
	bawk '$$11!="" && $$10!="" {print}'  $<  | ../../local/src/vis_RF-compare_scatter.py REF_TX4,TX4,REF_TX8,TX8,REF_TX16,TX16,REF_TX32,TX32,REF_TX64,TX64 -S -b -l "Tree length" -o $@
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55vORIG.1char_rotasequence.RIGHTvWRONG_tree-len_compare.pdf: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55vORIG.1char_rotasequence.tree-len_compare alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55vORIG.1char_rotasequence.RF_compare
	paste <(bawk 'NF==11' $<) <(cut -f 2- $^2 | bawk 'NF=5') | bawk 'NF==16' | ../../local/src/vis_RF-compare_scatter.py REF_TX4,55x55_TX4,REF_TX8,55x55_TX8,REF_TX16,55x55_TX16,REF_TX32,55x55_TX32,REF_TX64,55x55_TX64 -S -b -l "Tree length" -r TX4,TX8,TX16,TX32,TX64  -o $@ 

# brlens are rescaled (in $<) using SUPER_RHO to account for raxml's default rescaling of user-defined rates
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvORIG.1char_rotasequence.tree-len_compare.pdf: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvORIG.1char_rotasequence.tree-len_compare
	bawk 'NF==11{print}' $<  | ../../local/src/vis_RF-compare_scatter.py REF_TX4,55x55_TX4,REF_TX8,55x55_TX8,REF_TX16,55x55_TX16,REF_TX32,55x55_TX32,REF_TX64,55x55_TX64 -S  -b -l "Tree length" -a 55x55 -o $@
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvORIG.1char_rotasequence.RF_compare.pdf: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvORIG.1char_rotasequence.RF_compare
	 bawk 'NF==6' $<  | ../../local/src/vis_RF-compare_multiplot.py 55x55_TX4,55x55_TX8,55x55_TX16,55x55_TX32,55x55_TX64 55x55 4,8,16,32,64 >$@


alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55vLG.1char_rotasequence.tree-len_compare.pdf: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55vLG.1char_rotasequence.tree-len_compare 
	bawk 'NF==16' $<  | ../../local/src/vis_RF-compare_scatter.py REF_TX4,55x55_TX4,REF_TX8,55x55_TX8,REF_TX16,55x55_TX16,REF_TX32,55x55_TX32,REF_TX64,55x55_TX64,LG_TX4,LG_TX8,LG_TX16,LG_TX32,LG_TX64 -S -b -l "Tree length" -a 55x55,LG -o $@
#scale LG resultsa by multiplyng LG tree length by RHO / RHO* = 1.17911
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55vLG.1char_rotasequence.tree-len_compare_wLGref.pdf: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55vLG.1char_rotasequence.tree-len_compare 
	bawk 'NF==16{print $$1~11, $$12*1.17911, $$13*1.17911, $$14*1.17911,$$15*1.17911, $$16*1.17911}' $< | ../../local/src/vis_RF-compare_scatter.py REF_TX4,55x55_TX4,REF_TX8,55x55_TX8,REF_TX16,55x55_TX16,REF_TX32,55x55_TX32,REF_TX64,55x55_TX64,LG_TX4,LG_TX8,LG_TX16,LG_TX32,LG_TX64 -S 1 -b -l "Tree length" -a 55x55,LG -o $@
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55vLG.1char_rotasequence.RF_compare.pdf: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55vLG.1char_rotasequence.RF_compare 
	bawk 'NF==11' $<  | ../../local/src/vis_RF-compare_scatter.py 55x55_TX4,55x55_TX8,55x55_TX16,55x55_TX32,55x55_TX64,LG_TX4,LG_TX8,LG_TX16,LG_TX32,LG_TX64 -S  -b -l "Unweighted RF distance" -a 55x55,LG -o $@
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvLG.1char_rotasequence.tree-len_compare.pdf: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvLG.1char_rotasequence.tree-len_compare
	bawk 'NF==16{print}' $<  | grep -vP '\t\t'  | ../../local/src/vis_RF-compare_scatter.py REF_TX4,55x55_TX4,REF_TX8,55x55_TX8,REF_TX16,55x55_TX16,REF_TX32,55x55_TX32,REF_TX64,55x55_TX64,LG_TX4,LG_TX8,LG_TX16,LG_TX32,LG_TX64 -S  -b -l "Tree length" -a 55x55,LG -o $@
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvLG.1char_rotasequence.RF_compare.pdf: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvLG.1char_rotasequence.RF_compare
	 bawk 'NF==11' $<  | ../../local/src/vis_RF-compare_multiplot.py 55x55_TX4,55x55_TX8,55x55_TX16,55x55_TX32,55x55_TX64,LG_TX4,LG_TX8,LG_TX16,LG_TX32,LG_TX64 55x55,LG 4,8,16,32,64 >$@
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvLG.1char_rotasequence.RF_compare.histo.pdf: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvLG.1char_rotasequence.RF_compare
	bawk 'NF==11' $<  | ../../local/src/vis_RFashisto-compare_multiplot.py  -c 2 55x55_TX4,55x55_TX8,55x55_TX16,55x55_TX32,55x55_TX64,LG_TX4,LG_TX8,LG_TX16,LG_TX32,LG_TX64 55x55,LG 8,16,32,64 >$@
alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvLG.1char_rotasequence.RF_compare.histo.pdf: alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvLG.1char_rotasequence.RF_compare
	bawk 'NF==3' $<  | ../../local/src/vis_RFashisto-compare_multiplot.py  -c 2 55x55_TX$*,LG_TX$* 55x55,LG $*4 >$@


alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvLGexp.1char_rotasequence.tree-len_compare.pdf: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvLGexp.1char_rotasequence.tree-len_compare
	bawk 'NF==16{print}' $<  | grep -vP '\t\t'  | ../../local/src/vis_RF-compare_scatter.py REF_TX4,55x55_TX4,REF_TX8,55x55_TX8,REF_TX16,55x55_TX16,REF_TX32,55x55_TX32,REF_TX64,55x55_TX64,LGexp_TX4,LGexp_TX8,LGexp_TX16,LGexp_TX32,LGexp_TX64 -S  -b -l "Tree length" -a 55x55,LGexp -o $@
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvLGexp.1char_rotasequence.RF_compare.pdf: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvLGexp.1char_rotasequence.RF_compare
	 bawk 'NF==11' $<  | ../../local/src/vis_RF-compare_multiplot.py 55x55_TX4,55x55_TX8,55x55_TX16,55x55_TX32,55x55_TX64,LGexp_TX4,LGexp_TX8,LGexp_TX16,LGexp_TX32,LGexp_TX64 55x55,LGexp 4,8,16,32,64 >$@



alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvLGexp55x55freqs.1char_rotasequence.tree-len_compare.pdf: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvLGexp55x55freqs.1char_rotasequence.tree-len_compare
	bawk 'NF==16{print $$1, $$2, $$3 * $(SUPER_RHO), $$4, $$5 * $(SUPER_RHO), $$6, $$7 * $(SUPER_RHO), $$8, $$9 * $(SUPER_RHO), $$10, $$11 * $(SUPER_RHO), $$12~16}' $<  | grep -vP '\t\t'  | ../../local/src/vis_RF-compare_scatter.py REF_TX4,55x55_TX4,REF_TX8,55x55_TX8,REF_TX16,55x55_TX16,REF_TX32,55x55_TX32,REF_TX64,55x55_TX64,LGexp55x55freqs_TX4,LGexp55x55freqs_TX8,LGexp55x55freqs_TX16,LGexp55x55freqs_TX32,LGexp55x55freqs_TX64 -S  -b -l "Tree length" -a 55x55,LGexp55x55freqs -o $@
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvLGexp55x55freqs.1char_rotasequence.RF_compare.pdf: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvLGexp55x55freqs.1char_rotasequence.RF_compare
	 bawk 'NF==11' $<  | ../../local/src/vis_RF-compare_multiplot.py 55x55_TX4,55x55_TX8,55x55_TX16,55x55_TX32,55x55_TX64,LGexp55x55freqs_TX4,LGexp55x55freqs_TX8,LGexp55x55freqs_TX16,LGexp55x55freqs_TX32,LGexp55x55freqs_TX64 55x55,LGexp55x55freqs 4,8,16,32,64 >$@


alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvLGbyfreq-exp55x55freqs.1char_rotasequence.tree-len_compare.pdf: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvLGbyfreq-exp55x55freqs.1char_rotasequence.tree-len_compare
	bawk 'NF==16{print $$1, $$2, $$3 * $(SUPER_RHO), $$4, $$5 * $(SUPER_RHO), $$6, $$7 * $(SUPER_RHO), $$8, $$9 * $(SUPER_RHO), $$10, $$11 * $(SUPER_RHO), $$12~16}' $<  | grep -vP '\t\t'  | ../../local/src/vis_RF-compare_scatter.py REF_TX4,55x55_TX4,REF_TX8,55x55_TX8,REF_TX16,55x55_TX16,REF_TX32,55x55_TX32,REF_TX64,55x55_TX64,LGbyfreq-exp55x55freqs_TX4,LGbyfreq-exp55x55freqs_TX8,LGbyfreq-exp55x55freqs_TX16,LGbyfreq-exp55x55freqs_TX32,LGbyfreq-exp55x55freqs_TX64 -S  -b -l "Tree length" -a 55x55,LGbyfreq-exp55x55freqs -o $@
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvLGbyfreq-exp55x55freqs.1char_rotasequence.RF_compare.pdf: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvLGbyfreq-exp55x55freqs.1char_rotasequence.RF_compare
	 bawk 'NF==11' $<  | ../../local/src/vis_RF-compare_multiplot.py 55x55_TX4,55x55_TX8,55x55_TX16,55x55_TX32,55x55_TX64,LGbyfreq-exp55x55freqs_TX4,LGbyfreq-exp55x55freqs_TX8,LGbyfreq-exp55x55freqs_TX16,LGbyfreq-exp55x55freqs_TX32,LGbyfreq-exp55x55freqs_TX64 55x55,LGbyfreq-exp55x55freqs 4,8,16,32,64 >$@



# single brlen comparison plots
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_AND_guide-treevLG.1char_rotasequence.single-brlen_compare.pdf: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_AND_guide-treevLG.1char_rotasequence.single-brlen_compare
	bawk 'NF==61' $< | ../../local/src/vis_single-brlen-compare_multiplot.py  -c 2 '55x55,LG' 'int_sec-short,int_sec-long,lf_sec-short,lf_sec-long' '4,8,16,32,64' >$@ 
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_AND_guide-treev20x20.1char_rotasequence.single-brlen_compare.pdf: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_AND_guide-treev20x20.1char_rotasequence.single-brlen_compare
	bawk 'NF==61' $< | ../../local/src/vis_single-brlen-compare_multiplot.py  '55x55,20x20' 'int_sec-short,int_sec-long,lf_sec-short,lf_sec-long' '4,8,16,32,64' >$@ 
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_AND_guide-treevLGbyfreq-exp55x55freqs.1char_rotasequence.single-brlen_compare.pdf: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_AND_guide-treevLGbyfreq-exp55x55freqs.1char_rotasequence.single-brlen_compare
	bawk 'NF==61' $< | ../../local/src/vis_single-brlen-compare_multiplot.py  '55x55,LGbyfreq-exp55x55freqs' 'int_sec-short,int_sec-long,lf_sec-short,lf_sec-long' '4,8,16,32,64' >$@ 
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_AND_guide-treevLGexp.1char_rotasequence.single-brlen_compare.pdf: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaled_AND_guide-treevLGexp.1char_rotasequence.single-brlen_compare
	bawk 'NF==61' $< | ../../local/src/vis_single-brlen-compare_multiplot.py  '55x55,LGexp' 'int_sec-short,int_sec-long,lf_sec-short,lf_sec-long' '4,8,16,32,64' >$@ 


alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledv20x20.1char_rotasequence.tree-len_compare.pdf: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledv20x20.1char_rotasequence.tree-len_compare
	bawk 'NF==16{print}' $<  | grep -vP '\t\t'  | ../../local/src/vis_RF-compare_scatter.py REF_TX4,55x55_TX4,REF_TX8,55x55_TX8,REF_TX16,55x55_TX16,REF_TX32,55x55_TX32,REF_TX64,55x55_TX64,20x20_TX4,20x20_TX8,20x20_TX16,20x20_TX32,20x20_TX64 -S  -b -l "Tree length" -a 55x55,20x20 -o $@
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledv20x20.1char_rotasequence.RF_compare.pdf: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledv20x20.1char_rotasequence.RF_compare
	bawk 'NF==11' $<  | ../../local/src/vis_RF-compare_multiplot.py 55x55_TX4,55x55_TX8,55x55_TX16,55x55_TX32,55x55_TX64,20x20_TX4,20x20_TX8,20x20_TX16,20x20_TX32,20x20_TX64 55x55,20x20 4,8,16,32,64 >$@
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledv20x20.1char_rotasequence.RF_compare.histo.pdf: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledv20x20.1char_rotasequence.RF_compare
	bawk 'NF==11' $<  | ../../local/src/vis_RFashisto-compare_multiplot.py  -c 1  55x55_TX4,55x55_TX8,55x55_TX16,55x55_TX32,55x55_TX64,20x20_TX4,20x20_TX8,20x20_TX16,20x20_TX32,20x20_TX64 55x55,20x20 8,16,32,64 >$@
alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledv20x20.1char_rotasequence.RF_compare.histo.pdf: alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledv20x20.1char_rotasequence.RF_compare
	bawk 'NF==3' $<  | ../../local/src/vis_RFashisto-compare_multiplot.py  -c 1  55x55_TX$*,20x20_TX$* 55x55,20x20 $* >$@





alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledv20x20.1char_rotasequence.adapted-Ln_compare.pdf: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledv20x20.1char_rotasequence.adapted-Ln_compare
	bawk 'NF==11' $<  | grep -v NA | ../../local/src/vis_Ln-compare_multiplot.py 55x55_TX4,55x55_TX8,55x55_TX16,55x55_TX32,55x55_TX64,20x20_TX4,20x20_TX8,20x20_TX16,20x20_TX32,20x20_TX64 55x55,20x20 4,8,16,32,64 >$@ 
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvLG.1char_rotasequence.adapted-Ln_compare.pdf: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvLG.1char_rotasequence.adapted-Ln_compare
	bawk 'NF==11' $<  | grep -v NA | ../../local/src/vis_Ln-compare_multiplot.py 55x55_TX4,55x55_TX8,55x55_TX16,55x55_TX32,55x55_TX64,LG_TX4,LG_TX8,LG_TX16,LG_TX32,LG_TX64 55x55,LG 4,8,16,32,64 >$@ 
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvLGexp.1char_rotasequence.Ln_compare.pdf: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvLGexp.1char_rotasequence.Ln_compare
	bawk 'NF==11' $<  | grep -v NA | ../../local/src/vis_Ln-compare_multiplot.py 55x55_TX4,55x55_TX8,55x55_TX16,55x55_TX32,55x55_TX64,LGexp_TX4,LGexp_TX8,LGexp_TX16,LGexp_TX32,LGexp_TX64 55x55,LGexp 4,8,16,32,64 >$@ 
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvLGexp55x55freqs.1char_rotasequence.Ln_compare.pdf: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvLGexp55x55freqs.1char_rotasequence.Ln_compare
	bawk 'NF==11' $<  | grep -v NA | ../../local/src/vis_Ln-compare_multiplot.py 55x55_TX4,55x55_TX8,55x55_TX16,55x55_TX32,55x55_TX64,LGexp55x55freqs_TX4,LGexp55x55freqs_TX8,LGexp55x55freqs_TX16,LGexp55x55freqs_TX32,LGexp55x55freqs_TX64 55x55,LGexp55x55freqs 4,8,16,32,64 >$@ 

alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvLGbyfreq-exp55x55freqs.1char_rotasequence.Ln_compare.pdf: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvLGbyfreq-exp55x55freqs.1char_rotasequence.Ln_compare
	bawk 'NF==11' $<  | grep -v NA | ../../local/src/vis_Ln-compare_multiplot.py 55x55_TX4,55x55_TX8,55x55_TX16,55x55_TX32,55x55_TX64,LGbyfreq-exp55x55freqs_TX4,LGbyfreq-exp55x55freqs_TX8,LGbyfreq-exp55x55freqs_TX16,LGbyfreq-exp55x55freqs_TX32,LGbyfreq-exp55x55freqs_TX64 55x55,LGbyfreq-exp55x55freqs 4,8,16,32,64 >$@ 

#putting all LL plots together, include corrected LL and expanded models
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvALL.1char_rotasequence.Ln_compare.pdf: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledv20x20.1char_rotasequence.adapted-Ln_compare alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvLG.1char_rotasequence.adapted-Ln_compare  alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvLGexp.1char_rotasequence.Ln_compare alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvLGbyfreq-exp55x55freqs.1char_rotasequence.Ln_compare
	paste <(bawk 'NF==11' $<  ) <(bawk 'NF==11' $^2  | cut -f 7- ) <(bawk 'NF==11' $^3   | cut -f 7- ) <(bawk 'NF==11' $^4  | cut -f 7- ) \
	|  ../../local/src/vis_Ln-compare_multiplot.py 55x55_TX4,55x55_TX8,55x55_TX16,55x55_TX32,55x55_TX64,20x20_TX4,20x20_TX8,20x20_TX16,20x20_TX32,20x20_TX64,LG_TX4,LG_TX8,LG_TX16,LG_TX32,LG_TX64,LGexp_TX4,LGexp_TX8,LGexp_TX16,LGexp_TX32,LGexp_TX64,LGbyfreq-exp55x55freqs_TX4,LGbyfreq-exp55x55freqs_TX8,LGbyfreq-exp55x55freqs_TX16,LGbyfreq-exp55x55freqs_TX32,LGbyfreq-exp55x55freqs_TX64 55x55,20x20,LG,LGexp,LGbyfreq-exp55x55freqs 16,32,64 >$@ 


alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvALL.1char_rotasequence.AIC_compare: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledv20x20.1char_rotasequence.adapted-Ln_compare alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvLG.1char_rotasequence.adapted-Ln_compare  alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvLGexp.1char_rotasequence.Ln_compare alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvLGbyfreq-exp55x55freqs.1char_rotasequence.Ln_compare alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvLGplusF.1char_rotasequence.adapted-Ln_compare
	printf "scale,55x55_TX4,55x55_TX8,55x55_TX16,55x55_TX32,55x55_TX64,20x20_TX4,20x20_TX8,20x20_TX16,20x20_TX32,20x20_TX64,LG_TX4,LG_TX8,LG_TX16,LG_TX32,LG_TX64,LGexp_TX4,LGexp_TX8,LGexp_TX16,LGexp_TX32,LGexp_TX64,LGbyfreq-exp55x55freqs_TX4,LGbyfreq-exp55x55freqs_TX8,LGbyfreq-exp55x55freqs_TX16,LGbyfreq-exp55x55freqs_TX32,LGbyfreq-exp55x55freqs_TX64,LGplusF_TX4,LGplusF_TX8,LGplusF_TX16,LGplusF_TX32,LGplusF_TX64\n" | tr "," "\t" >$@ ;
	paste <(bawk 'NF==11' $<  ) <(bawk 'NF==11' $^2  | cut -f 7- ) <(bawk 'NF==11' $^3   | cut -f 7- ) <(bawk 'NF==11' $^4  | cut -f 7- ) <(bawk 'NF==11' $^5  | cut -f 7-) \
	|  awk '{FS=OFS="\t"} {printf "%2.1f", $$1};{printf "\t"};{for (i=2;i<=NF;i++)  {v = sprintf("%06.3f\t", $$i* -2); printf v}};{printf "\n"}' \
	| sed 's/\t$$//g' | bawk '{print $$1~26, $$27 + 19*2, $$28 + 19*2, $$29 + 19*2, $$30 + 19*2, $$31 + 19*2}' | sed 's/\t\t/\tNA\t/g  ; s/\t38\t/\tNA\t/g' >>$@

#putting all AIC plots together, include corrected LL and expanded models
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvALL.1char_rotasequence.AIC_compare.pdf: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvALL.1char_rotasequence.AIC_compare 
	cat $< | unhead \
	| ../../local/src/vis_Ln-compare_multiplot.py 55x55_TX4,55x55_TX8,55x55_TX16,55x55_TX32,55x55_TX64,20x20_TX4,20x20_TX8,20x20_TX16,20x20_TX32,20x20_TX64,LG_TX4,LG_TX8,LG_TX16,LG_TX32,LG_TX64,LGexp_TX4,LGexp_TX8,LGexp_TX16,LGexp_TX32,LGexp_TX64,LGbyfreq-exp55x55freqs_TX4,LGbyfreq-exp55x55freqs_TX8,LGbyfreq-exp55x55freqs_TX16,LGbyfreq-exp55x55freqs_TX32,LGbyfreq-exp55x55freqs_TX64,LGplusF_TX4,LGplusF_TX8,LGplusF_TX16,LGplusF_TX32,LGplusF_TX64 55x55,20x20,LG,LGexp,LGbyfreq-exp55x55freqs,LGplusF 16,32,64 -y "AIC" >$@ 


# plotting euclidean distances for 55x55, 20x20, LG as means
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvALL.1char_rotasequence.euclidean_compare.pdf: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledv20x20.1char_rotasequence.euclidean_compare alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvLG.1char_rotasequence.euclidean_compare
	paste <(bawk 'NF==11' $<  ) <(bawk 'NF==11' $^2  | cut -f 7- ) \
	|  ../../local/src/vis_Ln-compare_multiplot.py 55x55_TX4,55x55_TX8,55x55_TX16,55x55_TX32,55x55_TX64,20x20_TX4,20x20_TX8,20x20_TX16,20x20_TX32,20x20_TX64,LG_TX4,LG_TX8,LG_TX16,LG_TX32,LG_TX64 55x55,20x20,LG 16,32,64 -y "Euclidean distance" >$@


# plotting tree lengths for 55x55, 20x20, LG as means
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvALL.1char_rotasequence.tree-len_compare.pdf: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledv20x20.1char_rotasequence.tree-len_compare alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvLG.1char_rotasequence.tree-len_compare
	paste <(bawk 'NF==16{print}' $<) <(bawk 'NF==16{print}' $^2 | cut -f 12-)  | grep -vP '\t\t'  | ../../local/src/vis_Ln-compare_multiplot.py REF_TX4,55x55_TX4,REF_TX8,55x55_TX8,REF_TX16,55x55_TX16,REF_TX32,55x55_TX32,REF_TX64,55x55_TX64,20x20_TX4,20x20_TX8,20x20_TX16,20x20_TX32,20x20_TX64,LG_TX4,LG_TX8,LG_TX16,LG_TX32,LG_TX64 REF,55x55,20x20,LG 16,32,64 -y "Tree length" >$@

# plotting run-times for 55x55, 20x20, LG as means
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvALL.1char_rotasequence.run-time_compare.pdf: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledv20x20.1char_rotasequence.run-time_compare alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvLG.1char_rotasequence.run-time_compare
	paste <(bawk 'NF==11{print}' $<) <(bawk 'NF==11{print}' $^2 | cut -f 7-)  | grep -vP '\t\t'  | ../../local/src/vis_Ln-compare_multiplot.py 55x55_TX4,55x55_TX8,55x55_TX16,55x55_TX32,55x55_TX64,20x20_TX4,20x20_TX8,20x20_TX16,20x20_TX32,20x20_TX64,LG_TX4,LG_TX8,LG_TX16,LG_TX32,LG_TX64 55x55,20x20,LG 16,32,64 -y "Run time" >$@



# plotting euclidean distance for 55x55 and LG as horizontal histograms with medians
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvLG.1char_rotasequence.euclidean_compare.histo.pdf: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvLG.1char_rotasequence.euclidean_compare
	bawk 'NF==11' $<  | ../../local/src/vis_RFashisto-compare_multiplot.py -r -b 10 -c 2 55x55_TX4,55x55_TX8,55x55_TX16,55x55_TX32,55x55_TX64,LG_TX4,LG_TX8,LG_TX16,LG_TX32,LG_TX64 55x55,LG 8,16,32,64 -y "Euclidean distance" >$@
# plotting euclidean distance for 55x55 and 20x20  as horizontal histograms with medians
alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledv20x20.1char_rotasequence.euclidean_compare.histo.pdf: alignmentALLx$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledv20x20.1char_rotasequence.euclidean_compare
	bawk 'NF==11' $<  | ../../local/src/vis_RFashisto-compare_multiplot.py -r -b 10 -c 1 55x55_TX4,55x55_TX8,55x55_TX16,55x55_TX32,55x55_TX64,20x20_TX4,20x20_TX8,20x20_TX16,20x20_TX32,20x20_TX64 55x55,20x20 8,16,32,64 -y "Euclidean distance" >$@

#for a single (empirical) tree
# plotting euclidean distance for 55x55 and LG as horizontal histograms with medians
alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvLG.1char_rotasequence.euclidean_compare.histo.pdf: alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledvLG.1char_rotasequence.euclidean_compare
	bawk 'NF==3' $<  | ../../local/src/vis_RFashisto-compare_multiplot.py -r -b 10 -c 2  55x55_TX$*,LG_TX$* 55x55,LG $* -y "Euclidean distance" >$@
# plotting euclidean distance for 55x55 and 20x20  as horizontal histograms with medians
alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledv20x20.1char_rotasequence.euclidean_compare.histo.pdf: alignment%x$(ALIG_LEN)x$(REPLICATES)_scaled-branches_evo_simulation.55x55_superscaledv20x20.1char_rotasequence.euclidean_compare
	bawk 'NF==3' $<  | ../../local/src/vis_RFashisto-compare_multiplot.py -r -b 10 -c 1  55x55_TX$*,20x20_TX$* 55x55,20x20 $* -y "Euclidean distance" >$@


# EMPIRICAL BENCHMARK
#------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# Empirical benchmark on a PFAM family included in the full-lax version
# Here the alignment used is the uniq-Uniprot version
# i.e. one rotasequence per uniprot id (picks the longest domain rotasequence for e/a uniprot ID)
# For this alignment rotaequence identity thereshold is set at .75, Bfactor thereshold at 30
# the PFAM_tree i obtained by using mafft --retree 2 (UPGMA) on the original PFAM alignment for $(FAM)
ALIGN_TYPE?=uniq-Uniprot
GENERAL_QUERY?=True
#FAM?=PF00514
#FAM?=PF07714
FAM?=Rubisco
#PF00514

#PF08659$
ALPHA?=1
# FAM ALPHA from raxml-ng opt-model ON 
# PF07714 1.872495
# Rubisco 4.805095
# PF00514 2.317909
ifeq ($(GENERAL_QUERY),False)
FAMDIR=../Rotamers_evo.Pfam.full-lax/$(FAM)
else
FAMDIR=../Rotamers_evo.$(FAM)
endif

# this is a phony target that generates UNIQ_UNIPROT_ALIGN, UNIQ_UNIPROT_MASKED_ALIGN and UNIQ_UNIPROT_TREE
extern=$(FAMDIR)/PRE_ANALYSIS

UNIQ_UNIPROT_ALIGN=experimental_strct.polypeptide.mapped.angle_matrix.chi1_assigned.GLY-ALA.no_0-NA.merged_poly.adj_count_matrix.$(ALIGN_TYPE).PHYLIP
UNIQ_UNIPROT_MASKED_ALIGN=experimental_strct.polypeptide.mapped.angle_matrix.chi1_assigned.GLY-ALA.no_0-NA.merged_poly.adj_count_matrix.$(ALIGN_TYPE).masked.PHYLIP
UNIQ_UNIPROT_TREE=experimental_strct.polypeptide.mapped.angle_matrix.chi1_assigned.GLY-ALA.no_0-NA.merged_poly.adj_count_matrix.$(ALIGN_TYPE).tree.newick

.PHONY: EMPIRICAL_ANALYSIS EMPIRICAL_ANALYSIS_PLOTS
EMPIRICAL_ANALYSIS: alignment$(FAM)_empirical.55x55_superscaled.1char_rotasequence.RF_compare.pre alignment$(FAM)_empirical.55x55_superscaledv20x20.1char_rotasequence.RF_compare.pre alignment$(FAM)_empirical.55x55_superscaledvLG.1char_rotasequence.RF_compare.pre alignment$(FAM)_empirical.55x55_superscaledvWAG.1char_rotasequence.RF_compare.pre alignment$(FAM)_empirical.55x55_superscaledvLGbyfreq-exp55x55freqs.1char_rotasequence.RF_compare.pre alignment$(FAM)_empirical.55x55_superscaledvNJ.1char_rotasequence.RF_compare.pre alignment$(FAM)_empirical.55x55_superscaledvUPGMA.1char_rotasequence.RF_compare.pre

EMPIRICAL_ANALYSIS_PLOTS: alignment$(FAM)_empirical.55x55_superscaledvALL.1char_rotasequence.Ln_compare alignment$(FAM)_empirical.55x55_superscaledvALL.1char_rotasequence.tree-len_compare alignment$(FAM)_empirical.55x55_superscaledALLvsALL.1char_rotasequence.RF_AND_weigthedRF_compare.tex alignment$(FAM)_empirical.55x55_superscaledALLvsALL.1char_rotasequence.RF_AND_weigthedRF_compare.pdf alignment$(FAM)_empirical.55x55_superscaledvALL.1char_rotasequence.ALL_compare.tex


# add {-} to handle gaps
alignment$(FAM)_empirical.55x55_superscaled.1char_rotasequence.RF_compare.pre: $(FAMDIR)/$(UNIQ_UNIPROT_ALIGN) rotamers_adj_count_matrix.merged.Dayhoff_norm.exchangeability.superQ-star.rates.ng rotamers_adj_count_matrix.merged.Dayhoff_norm_states_freqs.ng
	mkdir -p $@ ; \
	cat $<  >$@/$(FAM).$(ALIGN_TYPE).rotaseq.PHYLIP ; \
	raxml-ng --opt-model off --msa $@/$(FAM).$(ALIGN_TYPE).rotaseq.PHYLIP --model MULTI55_GTR{$^2}+FU{$^3}+M{ABCDEFGHIJKLMNOPQRSTUVWYXZabcdefghijklmnopqrstuvwxyz012}{-} --seed 1 --threads 2 --prefix $@/$(FAM).55x55

# gamma distribution and op-model ON
alignment$(FAM)_empirical.55x55_superscaled_plusG.1char_rotasequence.RF_compare.pre: alignment$(FAM)_empirical.55x55_superscaled.1char_rotasequence.RF_compare.pre $(FAMDIR)/$(UNIQ_UNIPROT_ALIGN) rotamers_adj_count_matrix.merged.Dayhoff_norm.exchangeability.superQ-star.rates.ng rotamers_adj_count_matrix.merged.Dayhoff_norm_states_freqs.ng
	cat $^2  >$</$(FAM).$(ALIGN_TYPE).rotaseq.PHYLIP ; \
	raxml-ng --msa $</$(FAM).$(ALIGN_TYPE).rotaseq.PHYLIP --model MULTI55_GTR{$^3}+FU{$^4}+M{ABCDEFGHIJKLMNOPQRSTUVWYXZabcdefghijklmnopqrstuvwxyz012}{-}+G --seed 1 --threads 2 --prefix $</$(FAM).55x55plusG_optON

# gamma distribution and empirical sequences, opt-on
alignment$(FAM)_empirical.55x55_superscaled_plusGplusF.1char_rotasequence.RF_compare.pre: alignment$(FAM)_empirical.55x55_superscaled.1char_rotasequence.RF_compare.pre $(FAMDIR)/$(UNIQ_UNIPROT_ALIGN) rotamers_adj_count_matrix.merged.Dayhoff_norm.exchangeability.superQ-star.rates.ng rotamers_adj_count_matrix.merged.Dayhoff_norm_states_freqs.ng
	cat $^2  >$</$(FAM).$(ALIGN_TYPE).rotaseq.PHYLIP ; \
	raxml-ng --msa $</$(FAM).$(ALIGN_TYPE).rotaseq.PHYLIP --model MULTI55_GTR{$^3}+M{ABCDEFGHIJKLMNOPQRSTUVWYXZabcdefghijklmnopqrstuvwxyz012}{-}+G+F --seed 1 --threads 2 --prefix $</$(FAM).55x55plusGplusF_optON

# 55x55 with empirical frequencies only
alignment$(FAM)_empirical.55x55_superscaled_plusF.1char_rotasequence.RF_compare.pre: alignment$(FAM)_empirical.55x55_superscaled.1char_rotasequence.RF_compare.pre $(FAMDIR)/$(UNIQ_UNIPROT_ALIGN) rotamers_adj_count_matrix.merged.Dayhoff_norm.exchangeability.superQ-star.rates.ng rotamers_adj_count_matrix.merged.Dayhoff_norm_states_freqs.ng
	cat $^2  >$</$(FAM).$(ALIGN_TYPE).rotaseq.PHYLIP ; \
	raxml-ng --msa $</$(FAM).$(ALIGN_TYPE).rotaseq.PHYLIP --model MULTI55_GTR{$^3}+M{ABCDEFGHIJKLMNOPQRSTUVWYXZabcdefghijklmnopqrstuvwxyz012}{-}+F --seed 1 --threads 2 --prefix $</$(FAM).55x55plusF_optON

# 55x55 with ML-estimated frequencies
alignment$(FAM)_empirical.55x55_superscaled_plusFO.1char_rotasequence.RF_compare.pre: alignment$(FAM)_empirical.55x55_superscaled.1char_rotasequence.RF_compare.pre $(FAMDIR)/$(UNIQ_UNIPROT_ALIGN) rotamers_adj_count_matrix.merged.Dayhoff_norm.exchangeability.superQ-star.rates.ng rotamers_adj_count_matrix.merged.Dayhoff_norm_states_freqs.ng
	cat $^2  >$</$(FAM).$(ALIGN_TYPE).rotaseq.PHYLIP ; \
	raxml-ng --msa $</$(FAM).$(ALIGN_TYPE).rotaseq.PHYLIP --model MULTI55_GTR{$^3}+M{ABCDEFGHIJKLMNOPQRSTUVWYXZabcdefghijklmnopqrstuvwxyz012}{-}+FO --seed 1 --threads 2 --prefix $</$(FAM).55x55plusFO_optON

# 55x55 with ML-estimated frequencies and gamma (4 cat)
alignment$(FAM)_empirical.55x55_superscaled_plusGplusFO.1char_rotasequence.RF_compare.pre: alignment$(FAM)_empirical.55x55_superscaled.1char_rotasequence.RF_compare.pre $(FAMDIR)/$(UNIQ_UNIPROT_ALIGN) rotamers_adj_count_matrix.merged.Dayhoff_norm.exchangeability.superQ-star.rates.ng rotamers_adj_count_matrix.merged.Dayhoff_norm_states_freqs.ng
	cat $^2  >$</$(FAM).$(ALIGN_TYPE).rotaseq.PHYLIP ; \
	raxml-ng --msa $</$(FAM).$(ALIGN_TYPE).rotaseq.PHYLIP --model MULTI55_GTR{$^3}+M{ABCDEFGHIJKLMNOPQRSTUVWYXZabcdefghijklmnopqrstuvwxyz012}{-}+G+FO --seed 1 --threads 2 --prefix $</$(FAM).55x55plusGplusFO_optON



# gamma distribution and op-model OFF
alignment$(FAM)_empirical.55x55_superscaled_plusGplusA.1char_rotasequence.RF_compare.pre: alignment$(FAM)_empirical.55x55_superscaled.1char_rotasequence.RF_compare.pre $(FAMDIR)/$(UNIQ_UNIPROT_ALIGN) rotamers_adj_count_matrix.merged.Dayhoff_norm.exchangeability.superQ-star.rates.ng rotamers_adj_count_matrix.merged.Dayhoff_norm_states_freqs.ng
	cat $^2  >$</$(FAM).$(ALIGN_TYPE).rotaseq.PHYLIP ; \
	raxml-ng --opt-model off -msa $</$(FAM).$(ALIGN_TYPE).rotaseq.PHYLIP --model MULTI55_GTR{$^3}+FU{$^4}+M{ABCDEFGHIJKLMNOPQRSTUVWYXZabcdefghijklmnopqrstuvwxyz012}{-}+G4{$(ALPHA)} --seed 1 --threads 2 --prefix $</$(FAM).55x55plusG_optOFF

alignment$(FAM)_empirical.55x55_superscaledv20x20.1char_rotasequence.RF_compare.pre: alignment$(FAM)_empirical.55x55_superscaled.1char_rotasequence.RF_compare.pre $(FAMDIR)/$(UNIQ_UNIPROT_MASKED_ALIGN) restype$(MATRIX_TYPE)matrix.merged.exchangeability.Q-star.rates.ng restype$(MATRIX_TYPE)matrix.merged_states_freqs.ng
	cat $^2 >$</$(FAM).$(ALIGN_TYPE).masked.PHYLIP ; \
	raxml-ng --opt-model off --msa $</$(FAM).$(ALIGN_TYPE).masked.PHYLIP --model MULTI20_GTR{$^3}+FU{$^4}+M{ARNDCQEGHILKMFPSTWYV}{-} --seed 1 --threads 2 --prefix $</$(FAM).masked_20x20

alignment$(FAM)_empirical.55x55_superscaledvLG.1char_rotasequence.RF_compare.pre: alignment$(FAM)_empirical.55x55_superscaled.1char_rotasequence.RF_compare.pre $(FAMDIR)/$(UNIQ_UNIPROT_MASKED_ALIGN) 
	cat $^2 > $</$(FAM).$(ALIGN_TYPE).masked.PHYLIP ; \
	raxml-ng --opt-model off --msa $</$(FAM).$(ALIGN_TYPE).masked.PHYLIP --model LG --seed 1 --threads 2 --prefix $</$(FAM).masked_LG
# LG+G and op-model OFF (alpha estimated using a raxml-ng run w/t opt-model on)
alignment$(FAM)_empirical.55x55_superscaledvLGplusGplusA.1char_rotasequence.RF_compare.pre: alignment$(FAM)_empirical.55x55_superscaled.1char_rotasequence.RF_compare.pre $(FAMDIR)/$(UNIQ_UNIPROT_MASKED_ALIGN)
	cat $^2 > $</$(FAM).$(ALIGN_TYPE).masked.PHYLIP ; \
	raxml-ng --opt-model off --msa $</$(FAM).$(ALIGN_TYPE).masked.PHYLIP --model LG+G4{$(ALPHA)} --seed 1 --threads 2 --prefix $</$(FAM).masked_LGplusG_optON

# LG with ML-estimated freqs
alignment$(FAM)_empirical.55x55_superscaledvLGplusFO.1char_rotasequence.RF_compare.pre: alignment$(FAM)_empirical.55x55_superscaled.1char_rotasequence.RF_compare.pre $(FAMDIR)/$(UNIQ_UNIPROT_MASKED_ALIGN)
	cat $^2 > $</$(FAM).$(ALIGN_TYPE).masked.PHYLIP ; \
	raxml-ng --msa $</$(FAM).$(ALIGN_TYPE).masked.PHYLIP --model LG+FO --seed 1 --threads 2 --prefix $</$(FAM).masked_LGplusFO_optON


# LG with gamma (4 categories) 	and ML-estimated freqs
alignment$(FAM)_empirical.55x55_superscaledvLGplusGplusFO.1char_rotasequence.RF_compare.pre: alignment$(FAM)_empirical.55x55_superscaled.1char_rotasequence.RF_compare.pre $(FAMDIR)/$(UNIQ_UNIPROT_MASKED_ALIGN)
	cat $^2 > $</$(FAM).$(ALIGN_TYPE).masked.PHYLIP ; \
	raxml-ng --msa $</$(FAM).$(ALIGN_TYPE).masked.PHYLIP --model LG+G+FO --seed 1 --threads 2 --prefix $</$(FAM).masked_LGplusGplusFO_optON

alignment$(FAM)_empirical.55x55_superscaledvLG4X.1char_rotasequence.RF_compare.pre: alignment$(FAM)_empirical.55x55_superscaled.1char_rotasequence.RF_compare.pre $(FAMDIR)/$(UNIQ_UNIPROT_MASKED_ALIGN)
	cat $^2 > $</$(FAM).$(ALIGN_TYPE).masked.PHYLIP ; \
	raxml-ng --msa $</$(FAM).$(ALIGN_TYPE).masked.PHYLIP --model LG4X --seed 1 --threads 2 --prefix $</$(FAM).masked_LG4X

alignment$(FAM)_empirical.55x55_superscaledvWAG.1char_rotasequence.RF_compare.pre: alignment$(FAM)_empirical.55x55_superscaled.1char_rotasequence.RF_compare.pre $(FAMDIR)/$(UNIQ_UNIPROT_MASKED_ALIGN)
	cat $^2 > $</$(FAM).$(ALIGN_TYPE).masked.PHYLIP ; \
	raxml-ng --opt-model off --msa $</$(FAM).$(ALIGN_TYPE).masked.PHYLIP --model WAG --seed 1 --threads 2 --prefix $</$(FAM).masked_WAG

# uniformely expanded version of LG, uses 55x55  rotamer state frequencies
alignment$(FAM)_empirical.55x55_superscaledvLGexp55x55freqs.1char_rotasequence.RF_compare.pre: alignment$(FAM)_empirical.55x55_superscaled.1char_rotasequence.RF_compare.pre $(FAMDIR)/$(UNIQ_UNIPROT_ALIGN) LG_55x55.uniform-exp.exchangeability.superQ-star.rates.ng rotamers_adj_count_matrix.merged.Dayhoff_norm_states_freqs.ng
	cat $^2 >$</$(FAM).$(ALIGN_TYPE).rotaseq.PHYLIP ; \
	raxml-ng --opt-model off --msa $</$(FAM).$(ALIGN_TYPE).rotaseq.PHYLIP --model MULTI55_GTR{$^3}+FU{$^4}+M{ABCDEFGHIJKLMNOPQRSTUVWYXZabcdefghijklmnopqrstuvwxyz012}{-} --seed 1 --threads 2 --prefix $</$(FAM).LGexp55x55freqs 

# expanded version of LG according to the rotamer state frequencies
# this accounts for rotamer rarity
# also it uses the "right" 55x55 frequencies to infer the tree
alignment$(FAM)_empirical.55x55_superscaledvLGbyfreq-exp55x55freqs.1char_rotasequence.RF_compare.pre: alignment$(FAM)_empirical.55x55_superscaled.1char_rotasequence.RF_compare.pre $(FAMDIR)/$(UNIQ_UNIPROT_ALIGN) LG_55x55.byfreq-exp.exchangeability.superQ-star.rates.ng rotamers_adj_count_matrix.merged.Dayhoff_norm_states_freqs.ng
	cat $^2  >$</$(FAM).$(ALIGN_TYPE).rotaseq.PHYLIP ; \
	raxml-ng --opt-model off --msa $</$(FAM).$(ALIGN_TYPE).rotaseq.PHYLIP --model MULTI55_GTR{$^3}+FU{$^4}+M{ABCDEFGHIJKLMNOPQRSTUVWYXZabcdefghijklmnopqrstuvwxyz012}{-} --seed 1 --threads 2 --prefix $</$(FAM).LGbyfreq-exp55x55freqs 

# NON-ML methods for comparison
# they work on the *.masked_reduced_ version of the masked alignment 
# gaps-only columns have been removed (same as what happens with raxml-ng)
# Neighbor Joining tree from the masked alignment (based on LG distance matrix)
alignment$(FAM)_empirical.55x55_superscaledvNJ.1char_rotasequence.RF_compare.pre: alignment$(FAM)_empirical.55x55_superscaled.1char_rotasequence.RF_compare.pre $(FAMDIR)/$(UNIQ_UNIPROT_MASKED_ALIGN)
	awk 'NR>1{gsub(/./, "& ",$$2); print};NR==1{print}' $</$(FAM).masked_20x20.raxml.reduced.phy > $</$(FAM).masked.PHYLIP ; \
	../../local/src/phangorn_tree.MP.R $</$(FAM).masked.PHYLIP NJ | tail -n 1 >$</$(FAM).masked_NJ.bestTree

# UPGMA tree  from the masked alignment (based on LG distance matrix)
alignment$(FAM)_empirical.55x55_superscaledvUPGMA.1char_rotasequence.RF_compare.pre: alignment$(FAM)_empirical.55x55_superscaled.1char_rotasequence.RF_compare.pre $(FAMDIR)/$(UNIQ_UNIPROT_MASKED_ALIGN)
	awk 'NR>1{gsub(/./, "& ",$$2); print};NR==1{print}' $</$(FAM).masked_20x20.raxml.reduced.phy > $</$(FAM).masked.PHYLIP ; \
	../../local/src/phangorn_tree.MP.R $</$(FAM).masked.PHYLIP UPGMA | tail -n 1 >$</$(FAM).masked_UPGMA.bestTree

# Maximum Parsimony tree from the masked alignment (Parsimony Ratchet algorithm)
alignment$(FAM)_empirical.55x55_superscaledvMP.1char_rotasequence.RF_compare.pre: alignment$(FAM)_empirical.55x55_superscaled.1char_rotasequence.RF_compare.pre $(FAMDIR)/$(UNIQ_UNIPROT_MASKED_ALIGN)
	awk 'NR>1{gsub(/./, "& ",$$2); print};NR==1{print}' $</$(FAM).masked_20x20.raxml.reduced.phy > $</$(FAM).masked.PHYLIP ; \
	../../local/src/phangorn_tree.MP.R $</$(FAM).masked.PHYLIP MP | tail -n 1 >$</$(FAM).masked_MP.bestTree

#compare LL or corrected LL (modelOmatic-style)
alignment$(FAM)_empirical.55x55_superscaledvALL.1char_rotasequence.Ln_compare: alignment$(FAM)_empirical.55x55_superscaled.1char_rotasequence.RF_compare.pre $(FAMDIR)/$(UNIQ_UNIPROT_ALIGN) $(FAMDIR)/$(UNIQ_UNIPROT_MASKED_ALIGN)
	printf "model\tLL\tcorrected_LL\n" >$@
	grep "Final LogLikelihood:"  $(shell ls $</*.log | grep -v masked | tr '\n' ' ' ) \
	| tr "/" "\t" | cut -f2 | sed 's/$(FAM)\.//g ; s/\.raxml.log:Final LogLikelihood: /\t/g' >$@.tmp ; \
	grep -v -e '^$$' $^2 >$</$(FAM).rotaseq.PHYLIP ;  grep -v -e '^$$' $^3  >$</$(FAM).masked.PHYLIP ; \
	for k in $(shell ls $</*masked*log | tr "\n" " ") ; do \
		../../local/src/LL_adapter.py $</$(FAM).rotaseq.PHYLIP $</$(FAM).masked.PHYLIP $$(printf $$k) | tr "/" "\t" | cut -f 2-  | sed 's/$(FAM).masked_//g ; s/\.raxml.log//g' >>$@.tmp ; \
	done ; cat $@.tmp | bawk '$$3==""{print $$0, "NA"};$$3!=""{print}' >>$@ ; rm $@.tmp

# compare models using AIC or corrected AIC; AIC = 2k-2ln({L})
alignment$(FAM)_empirical.55x55_superscaledvALL.1char_rotasequence.Ln-AIC_compare: alignment$(FAM)_empirical.55x55_superscaledvALL.1char_rotasequence.Ln_compare
	printf "model\tLL\tcorrected_LL\tk\tAIC\n" >$@
	bawk '{print $$0, 0}' $< | unhead  \
	| bawk '{if ($$1 ~ /55.*plusFO/) {print $$1~3, $$4+54} else {print}}' \
	| bawk '{if ($$1 ~ /LG4X/) {print $$1~3, $$4+6} else {print}}' \
	| bawk '{if ($$1 ~ /(LG|WAG|20).*plusFO/) {print $$1~3, $$4+19} else {print}}' \
	| bawk '{if ($$1 ~ /plusG/) {print $$1~3, $$4+1} else {print}}' \
	| bawk '$$3=="NA"{print $$0, $$4 * 2 - $$2 * 2};$$3!="NA"{print $$0, $$4 * 2 - $$3 * 2}' | bsort -n -k5,5 >>$@

# \num[round-precision=4,round-mode=figures]{}
alignment$(FAM)_empirical.55x55_superscaledvALL.1char_rotasequence.Ln-AIC_compare.tex: alignment$(FAM)_empirical.55x55_superscaledvALL.1char_rotasequence.Ln-AIC_compare
	cat $< | unhead | grep -v "optOFF" \
	| sed 's/_opt\(ON\|OFF\)//g ; s/-exp55x55freqs//g ; s/plus/\+/g; s/^55x55/RAM55/g ; s/20x20/RUM20/g ; s/FO/F/g' \
	| bawk '$$3=="NA"{print $$1, $$2, $$2, $$4, $$5};$$3!="NA"{print $$0}' \
	| bawk '{print $$1, "\\num[round-precision=6,round-mode=figures]{"$$2"}", "\\num[round-precision=6,round-mode=figures]{"$$3"}", $$4, "\\num[round-precision=6,round-mode=figures]{"$$5"}"}' \ 
	| sed 's/_/\\_/g; s/\t/\t\&\t/g; s/$$/ \\\\/g' >$@

ifeq ($(GENERAL_QUERY),False)
# compare tree length across all models plus the mafft estimated tree based on PFAM alignment (appropriately pruned)
# also scale the 55x55 tree to remove raxml's rate rescaling effect
alignment$(FAM)_empirical.55x55_superscaledvALL.1char_rotasequence.tree-len_compare: alignment$(FAM)_empirical.55x55_superscaled.1char_rotasequence.RF_compare.pre $(FAMDIR)/$(UNIQ_UNIPROT_TREE) 
	for i in $(shell ls $</$(FAM).55x55*bestTree); do \
		cat $$i | ../../local/src/scale_tree.py $(SUPER_RHO) >$$i.rescaled ; \
	done ; \
	cat $</$(FAM).LGbyfreq-exp55x55freqs.raxml.bestTree | ../../local/src/scale_tree.py $(LGBYFREQ_SUPER_RHO)  >$</$(FAM).LGbyfreq-exp55x55freqs.raxml.bestTree.rescaled ; \
	printf "model\ttree_len\n" >$@ ; \
	../../local/src/compare_tree.py -l -o $(shell ls $</*bestTree*) | tr "/" "\t" | cut -f 4-  | sed 's/$(FAM)\.//g ; s/masked_//g  ; s/\.bestTree//g ; s/\.raxml//g' \
	| awk '/55/ && /rescaled/{print};!/55/{print}' | sed 's/\.rescaled//g' >$@.tmp ; \
	cut -f1,3 $@.tmp >>$@ ; rm $@.tmp

# unweighted RF comparison vs the original PFAM-estimated tree (appropriately pruned)
alignment$(FAM)_empirical.55x55_superscaledvALL.1char_rotasequence.RF_compare: alignment$(FAM)_empirical.55x55_superscaled.1char_rotasequence.RF_compare.pre $(FAMDIR)/$(UNIQ_UNIPROT_TREE)
	printf "model\tRF\n" >$@ ; cat $^2 >$</$(FAM).tree ; \
	../../local/src/compare_tree.py -o $</$(FAM).tree $(shell ls $</*bestTree) | tr "/" "\t" | cut -f 4-  | sed 's/$(FAM)\.//g ; s/masked_//g  ; s/\.bestTree//g ; s/\.raxml//g'  >>$@
# weighted RF comparison vs the original PFAM-estimated tree (appropriately pruned)
alignment$(FAM)_empirical.55x55_superscaledvALL.1char_rotasequence.weigthedRF_compare: alignment$(FAM)_empirical.55x55_superscaled.1char_rotasequence.RF_compare.pre $(FAMDIR)/$(UNIQ_UNIPROT_TREE)
	printf "model\tweighted_RF\n" >$@ ; cat $^2 >$</$(FAM).tree ; \
	for i in $(shell ls $</$(FAM).55x55*bestTree); do \
		cat $$i | ../../local/src/scale_tree.py $(SUPER_RHO) >$$i.rescaled ; \
	done ; \
	cat $</$(FAM).LGbyfreq-exp55x55freqs.raxml.bestTree | ../../local/src/scale_tree.py $(LGBYFREQ_SUPER_RHO)  >$</$(FAM).LGbyfreq-exp55x55freqs.raxml.bestTree.rescaled ; \
	../../local/src/compare_tree.py -w -o $</$(FAM).tree $(shell ls $</*bestTree*) | tr "/" "\t" | cut -f 4-  | sed 's/$(FAM)\.//g ; s/masked_//g  ; s/\.bestTree//g ; s/\.raxml//g' \
	| awk '/55/ && /rescaled/{print};!/55/{print}'  | sed 's/\.rescaled//g' >>$@
#  unweighted RF comparison ALL vs ALL
alignment$(FAM)_empirical.55x55_superscaledALLvsALL.1char_rotasequence.RF_compare: alignment$(FAM)_empirical.55x55_superscaled.1char_rotasequence.RF_compare.pre
	../../local/src/compare_tree.py $(shell ls $</*bestTree) | tr "/" "\t" | cut -f2,4,5 | sed 's/$(FAM)\.//g ; s/masked_//g  ; s/\.bestTree//g ; s/\.raxml//g' >$@
# weighted RF comparison ALL vs ALL
alignment$(FAM)_empirical.55x55_superscaledALLvsALL.1char_rotasequence.weigthedRF_compare: alignment$(FAM)_empirical.55x55_superscaled.1char_rotasequence.RF_compare.pre
	for i in $(shell ls $</$(FAM).55x55*bestTree); do \
		cat $$i | ../../local/src/scale_tree.py $(SUPER_RHO) >$$i.rescaled ; \
	done ; \
	cat $</$(FAM).LGbyfreq-exp55x55freqs.raxml.bestTree | ../../local/src/scale_tree.py $(LGBYFREQ_SUPER_RHO)  >$</$(FAM).LGbyfreq-exp55x55freqs.raxml.bestTree.rescaled ; \
	../../local/src/compare_tree.py -w  $(shell ls $</*bestTree*) | tr "/" "\t" | cut -f2,4,5 | sed 's/$(FAM)\.//g ; s/masked_//g  ; s/\.bestTree//g ; s/\.raxml//g' \
	| awk '/55/ && /rescaled/{print};!/55/{print}' | sed 's/\.rescaled//g' >$@


else
#For single term query versions ignore canonical sequence alignment tree
# scale the 55x55 tree to remove raxml's rate rescaling effect
alignment$(FAM)_empirical.55x55_superscaledvALL.1char_rotasequence.tree-len_compare: alignment$(FAM)_empirical.55x55_superscaled.1char_rotasequence.RF_compare.pre $(FAMDIR)/$(UNIQ_UNIPROT_TREE) 
	for i in $(shell ls $</$(FAM).55x55*bestTree); do \
		cat $$i | ../../local/src/scale_tree.py $(SUPER_RHO) >$$i.rescaled ; \
	done ; \
	cat $</$(FAM).LGbyfreq-exp55x55freqs.raxml.bestTree | ../../local/src/scale_tree.py $(LGBYFREQ_SUPER_RHO)  >$</$(FAM).LGbyfreq-exp55x55freqs.raxml.bestTree.rescaled ; \
	printf "model\ttree_len\nPFAM_tree\t" >$@ ; \
	../../local/src/compare_tree.py -l -o $(shell ls $</*bestTree*) | tr "/" "\t" | cut -f 4-  | sed 's/$(FAM)\.//g ; s/masked_//g  ; s/\.bestTree//g ; s/\.raxml//g' \
	| awk '/55/ && /rescaled/{print};!/55/{print}' | sed 's/\.rescaled//g' >$@.tmp ; \
	cut -f2 $@.tmp | head -n1 >>$@ ; cut -f1,3 $@.tmp >>$@ ; rm $@.tmp

#  unweighted RF comparison ALL vs ALL
alignment$(FAM)_empirical.55x55_superscaledALLvsALL.1char_rotasequence.RF_compare: alignment$(FAM)_empirical.55x55_superscaled.1char_rotasequence.RF_compare.pre $(FAMDIR)/$(UNIQ_UNIPROT_TREE)
	../../local/src/compare_tree.py $(shell ls $</*bestTree) | tr "/" "\t" | cut -f2,4,5 | sed 's/$(FAM)\.//g ; s/tree/PFAM_UPGMA/g ; s/masked_//g  ; s/\.bestTree//g ; s/\.raxml//g' >$@
# weighted RF comparison ALL vs ALL
alignment$(FAM)_empirical.55x55_superscaledALLvsALL.1char_rotasequence.weigthedRF_compare: alignment$(FAM)_empirical.55x55_superscaled.1char_rotasequence.RF_compare.pre $(FAMDIR)/$(UNIQ_UNIPROT_TREE)
	for i in $(shell ls $</$(FAM).55x55*bestTree); do \
		cat $$i | ../../local/src/scale_tree.py $(SUPER_RHO) >$$i.rescaled ; \
	done ; \
	cat $</$(FAM).LGbyfreq-exp55x55freqs.raxml.bestTree | ../../local/src/scale_tree.py $(LGBYFREQ_SUPER_RHO)  >$</$(FAM).LGbyfreq-exp55x55freqs.raxml.bestTree.rescaled ; \
	../../local/src/compare_tree.py -w  $(shell ls $</*bestTree*) | tr "/" "\t" | cut -f2,4,5 | sed 's/$(FAM)\.//g ; s/tree/PFAM_UPGMA/g ; s/masked_//g  ; s/\.bestTree//g ; s/\.raxml//g' \
	|  awk '/55/ && /rescaled/{print};!/55/{print}' | sed 's/\.rescaled//g' >$@

endif
# compare RF and weighted RF in one table (ALL vs ALL)
alignment$(FAM)_empirical.55x55_superscaledALLvsALL.1char_rotasequence.RF_AND_weigthedRF_compare.tex: alignment$(FAM)_empirical.55x55_superscaledALLvsALL.1char_rotasequence.RF_compare alignment$(FAM)_empirical.55x55_superscaledALLvsALL.1char_rotasequence.weigthedRF_compare
	../../local/src/vis_RF_matrix.py $^ >$@
alignment$(FAM)_empirical.55x55_superscaledALLvsALL.1char_rotasequence.RF_AND_weigthedRF_compare.pdf: alignment$(FAM)_empirical.55x55_superscaledALLvsALL.1char_rotasequence.RF_compare alignment$(FAM)_empirical.55x55_superscaledALLvsALL.1char_rotasequence.weigthedRF_compare
	../../local/src/vis_RF_matrix.py -H $^ >$@

alignment$(FAM)_empirical.55x55_superscaledvALL.1char_rotasequence.Ln_compare.tex: alignment$(FAM)_empirical.55x55_superscaledvALL.1char_rotasequence.Ln_compare
	cat $< | sed 's/_/\\_/g; s/\t/\t\&\t/g; s/$$/ \\\\ \\hline/' >$@
alignment$(FAM)_empirical.55x55_superscaledvALL.1char_rotasequence.ALL_compare.tex: alignment$(FAM)_empirical.55x55_superscaledvALL.1char_rotasequence.Ln_compare alignment$(FAM)_empirical.55x55_superscaledvALL.1char_rotasequence.tree-len_compare 
	paste <(cat $<) <(egrep  'x|LG|WAG|len' $^2 | cut -f2) >$@.tmp ; \
	grep  -v LG  $^2 | unhead | grep -v x  | grep -v WAG | bawk '{print $$1,"NA\tNA",$$2}'  >> $@.tmp ; \
	cat $@.tmp | awk 'NR>1{printf "%s\t%4.3e\t%4.3e\t%.3f\n", $$1, $$2, $$3, $$4};NR==1{print}' | sed 's/0.000e+00/NA/g ; s/plus/+/g' \
	| sed 's/_/\\_/g; s/\t/\t\&\t/g; s/$$/ \\\\ \\hline/' >$@ ; rm $@.tmp  
