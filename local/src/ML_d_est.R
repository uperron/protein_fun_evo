#!/usr/bin/env Rscript


library(Matrix)
library(methods)

args = commandArgs(trailingOnly=TRUE)

# test if there is at least one argument: if not, return an error
if (length(args) <4) {
	  stop("Four arguments must be supplied: rate matrix, frequencies, alignment and output file", call.=FALSE)
}

# read matrix
IRM <- as.matrix(read.table(args[1], header=TRUE, sep = "\t", row.names = 1, as.is=TRUE))


# read the frequencies
eq_freqs <- read.csv(file=args[2], header=TRUE, sep="\t")

# read the states
states <- rownames(IRM)

if (args[3]!="INF") {
	# read the sequences
	aln  <- read.table(file=args[3], header=FALSE, sep="\t")
	orig_seq <- aln[1,2]
	new_seq <- aln[2,2]
	# extract the true distance (i.e. t used in simulation) from the sequence name
	true_dist <- as.numeric(substr(aln[2,1], 9, 16))
	# compute all Nij, the number of coresponding sites occupied by AA i and j in the two sequences
	site_counts <- matrix(0L, nrow=length(states), ncol=length(states))
	dimnames(site_counts) = list(states, states)
	orig_seq_split <- as.list(strsplit(as.character(orig_seq), " ")[[1]])
	new_seq_split <- as.list(strsplit(as.character(new_seq), " ")[[1]])
	seq_length <- length(orig_seq_split)
	for (position in 1:seq_length){
		state1 <- as.character(orig_seq_split[position])
		state2 <- as.character(new_seq_split[position])
		site_counts[state1, state2] = site_counts[state1, state2] + 1
		}
	# calculate the proportion of changed sites as a starting estimate for the optimization
	p <- (sum(site_counts) - sum(diag(site_counts))) / seq_length
} else {
	# in the case of an infinitely long sequence and a finite amount of time assume p ~ 0
	p <- 0
	#  aln  <- read.table(file=args[5], header=FALSE, sep="\t")
	# extract the true distance (i.e. t used in simulation) from the sequence name
	#true_dist <- as.numeric(substr(aln[2,1], 9, 16))
}


# define likelihood function
log_likelihood <- function(t){
	# matrix exponentiation
	Pt <- expm(t*IRM)
	colnames(Pt) = rownames(Pt)  <- states
	sum_matrix <- matrix(0L, nrow=length(states), ncol=length(states))
	colnames(sum_matrix) = rownames(sum_matrix)  <- states
	for (i in states) {
		for (j in states) {
			if (args[3]!="INF") {
				# The log likelihood function is (2.4) from Chapter 2.3 of 
				# Yang, Ziheng. Molecular Evolution: A Statistical Approach (p. 41). OUP Oxford. Kindle Edition. 
				if (site_counts[i,j]!=0) {sum_matrix[i, j] = site_counts[i,j]*log(eq_freqs[1,i]*Pt[i, j])}
			} else {
				# In the case of an entremely long (or infinite) sequence Ni,j could be replaced by 
				#the probability of observation i,j (eq_freqs[1,i]*Pt[i, j]) times the number of sites
				sum_matrix[i, j] = eq_freqs[1,i]*Pt[i, j]*log(eq_freqs[1,i]*Pt[i, j])
			}}	
		}
	LL <- sum(sum_matrix)
	# return minus the log likelihood to be minimized
	return(-LL)
}

# calculate distance by minimizing -LL
# the maximum step for each iteration is set to 1
out <- nlm(log_likelihood, p, stepmax=1)
ML_est_d <- out$estimate
ML <- -out$minimum
nlm_code <- out$code

sink(args[4], append = TRUE)
cat(p, "\t", ML_est_d, "\t", ML, "\t", nlm_code, "\n")
sink()
