#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
#from random import shuffle
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader

import random

def main():
	usage = format_usage('''
		%prog < STDIN
	''')
	parser = OptionParser(usage=usage)
	parser.add_option('-s', '--states', default=False, help='set of states states, comma-separated  [default: %default]')
	parser.add_option('-w', '--wrong_freqs', default=False, action='store_true', help='use a very skewed frequency set  [default: %default]')
	options, args = parser.parse_args()
	
	if len(args) != 0:
		exit('Unexpected argument number.')

	all_seq = []
	for line in stdin:
		all_seq.extend(safe_rstrip(line).split())
	
	if options.states:
		states = options.states.split(',')	
	else:
		states = list(set(sorted(all_seq)))
	print '\t'.join(map(str, states))

	l = len(all_seq)
	
	if options.wrong_freqs:
		A = random.uniform(0.99, 1)
		freqs = [(1 - A) / 19] * 19 + [A]
		random.shuffle(freqs)
	else:
		freqs = []
		for s in states:
			freqs.append(float(all_seq.count(s))/l)
	
	print '\t'.join(map(str, freqs))

if __name__ == '__main__':
	main()

