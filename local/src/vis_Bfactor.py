#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr, stdout
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
#from random import shuffle
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader

import matplotlib
# force matplotlib  not to use Xwi
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker 
import matplotlib.patches as mpatches
import pandas as pd
import numpy as np
import seaborn as sns

def main():
	usage = format_usage('''
		%prog <STDIN
	STDIN = ROTA_STATE\tBFACTOR
	''')
	parser = OptionParser(usage=usage)
	
	parser.add_option('-o', '--outfile', dest='outfile', default='outfile', help='output file [default: %default]')
	options, args = parser.parse_args()
	

	fig = plt.figure(figsize=(12,12))
	ax = fig.add_subplot(111)
	plt.style.use(['seaborn-white', 'seaborn-paper'])
	matplotlib.rc("font", serif="Helvetica Neue")


	# parse stdin
	data = pd.read_csv(stdin, sep="\t", header=None, names=['state', 'bfactor'])
	# group by rotamer state
	grouped = data.groupby('state', sort=True)
	# plot kernel density
	for key in grouped.groups.keys():
		sns.kdeplot(grouped.get_group(key)['bfactor'].values, bw=0.5, ax=ax, label=key)
	
	plt.axvline(x=30, linestyle="--", color="k")
	ax.set_xticks(range(0,100,10))
	plt.xlabel('B factor', size='large')
	plt.ylabel('Kernel density estimate', size='large')
	ax.set_xlim(left=0, right=100)
	ax.set_ylim(bottom=0, top=0.3)
	plt.legend(fontsize="small", ncol=3)
	plt.legend()
	plt.savefig(stdout, format='pdf', bbox_inches='tight', dpi=fig.dpi)

if __name__ == '__main__':
	main()

