#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
#from random import shuffle
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader

import matplotlib
# force matplotlib  not to use Xwi
matplotlib.use('Agg')
import matplotlib.colors as colors

import matplotlib.pyplot as plt
import matplotlib.figure as fg
import matplotlib.ticker as ticker

import numpy as np
import itertools

def main():
	usage = format_usage('''
		%prog SIMULATION_COORDS MODEL_1_COORDS MODEL_2_COORDS NSTATES1,NSTATES2
	''')
	parser = OptionParser(usage=usage)
	
	parser.add_option('-o', '--outfile', dest='outfile', default='outfile', help='output file [default: %default]')
	parser.add_option('-n', '--nruns', dest='nruns', type=int, default=5, help='number of simulation runs [default: %default]')
	options, args = parser.parse_args()
	
	if len(args) != 4:
		exit('Unexpected argument number.')
	
	nruns = options.nruns

	sim_file, rota_file, res_file, labels = args
	label1, label2 = [int(x) for x in labels.split(",")]
	time_sim = []
	d_55 = []
	d_20 = []
	d_ML_55 = []
	d_ML_20 = []
	
	# parse the simulation coordinates
	with file(sim_file, 'r') as fd:
		for line in fd:
			n_states, t, d = safe_rstrip(line).split('\t')
			if int(n_states) == label1:
				d_55.append(float(d))
				time_sim.append(float(t))
			elif int(n_states) == label2:
				d_20.append(float(d))
	
	def calc_means(time, dists, nruns):
		means = []
		d = {}
		for k,v in zip(time, dists):
			d.setdefault(k, []).append(v)
		# means order corresponds to uniq_time
		for k,v in sorted(d.items()):
			means.append(np.mean(v))
		return means
	
	uniq_time = sorted(list(set(time_sim)))

	fig = plt.figure(figsize=(8,8))
	plt.scatter(time_sim, d_55, alpha=0.5, label='$sim %dx%d$' % (label1, label1), color='r')
	plt.scatter(time_sim, d_20, alpha=0.5, label='$sim %dx%d$' % (label2, label2), color='b')

	# calculate and plot the mean distance for each timepoint of each model
	plt.plot(uniq_time, calc_means(time_sim, d_55, nruns), linestyle='--', marker='x', color='r')
	plt.plot(uniq_time, calc_means(time_sim, d_20, nruns), linestyle='--', marker='x', color='b')
	
	# a different time axis because simulation and ML files are sorted differently
	time_ML = []

	# ML estimate for model1
	with file(rota_file, 'r') as fd:
		for line in fd:
			t, d = safe_rstrip(line).split('\t')
			time_ML.append(float(t))
			d_ML_55.append(float(d))
	
	plt.scatter(time_ML, d_ML_55, alpha=0.5, label='$ML %dx%d$' % (label1, label1), color='#810500')
	plt.plot(uniq_time, calc_means(time_ML, d_ML_55, nruns), linestyle='--', marker='x', color='#810500')
	
	# ML extimate for model 2
	with file(res_file,  'r') as fd:
		for line in fd:
			t, d = safe_rstrip(line).split('\t')
			d_ML_20.append(float(d))
	plt.scatter(time_ML, d_ML_20, alpha=0.5, label='$ML %dx%d$' % (label2, label2), color='#060096')
	plt.plot(uniq_time, calc_means(time_ML, d_ML_20, nruns), linestyle='--', marker='x', color='#060096')

	# bisector
	axes = plt.gca()
	axes.set_ylim([0,3])
	plt.plot(np.linspace(0,axes.get_xlim()[1]), np.linspace(0,axes.get_xlim()[1]), '--', color='k')
	
	plt.xlabel('t')
	plt.ylabel('d')
	plt.legend()
	plt.savefig(options.outfile, format='pdf', bbox_inches='tight', dpi=fig.dpi)

if __name__ == '__main__':
	main()

