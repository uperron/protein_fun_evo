#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
#from random import shuffle
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader
import warnings


def main():
	usage = format_usage('''
		%prog < STDIN
		.META: STDIN
		1	ID
		2	START
		3	END
		STDIN must be sorted
	''')
	parser = OptionParser(usage=usage)
	

	options, args = parser.parse_args()
	
	if len(args) != 0:
		exit('Unexpected argument number.')

	
	#for id, sample, raw, norm in Reader(stdin, '0u,1s,2i,3f', False):
	current_ID = None
	for line in stdin:
		inverted = False
		tokens = safe_rstrip(line).split('\t')
		try:
			ID = tokens[0]
			start = int(tokens[1])
			end = int(tokens[2])
		except:
			warnings.warn('Error while parsing %s, skipping...' %  (line))
			continue
		if start > end:
			inverted = True
			start = int(tokens[2])
			end = int(tokens[1])
		
		if current_ID is None:
			current_ID = ID
			current_start = start
			current_end = end
			previous_poly = []
		
		else:
			if current_ID == ID:
				if start >= current_end:
					previous_poly.append((current_start, current_end))
					if inverted:
						previous_poly.append((end, start))
					else:	
						previous_poly.append((start, end))
					current_end = end
				else:	
					length = end - start				
					if inverted:
						print "%s-<Polypeptide_start=%d_end=%d>\t%s-<Polypeptide_start=%d_end=%d>\t%d" % (ID, end, start, ID, end, start, length)
					else:
						print "%s-<Polypeptide_start=%d_end=%d>\t%s-<Polypeptide_start=%d_end=%d>\t%d" % (ID, start, end, ID, start, end, length)
			elif current_ID != ID:
				if previous_poly != []:
					for duo in previous_poly:
						length = current_end - current_start
						print "%s-<Polypeptide_start=%d_end=%d>\t%s-<MergedPoly_start=%d_end=%d>\t%d" % (current_ID, duo[0], duo[1], current_ID, current_start, current_end, length)
				elif previous_poly == []:
						length = current_end - current_start
						print "%s-<Polypeptide_start=%d_end=%d>\t%s-<Polypeptide_start=%d_end=%d>\t%d" % (current_ID, current_start, current_end, current_ID, current_start, current_end, length)
				current_ID = ID
				current_start = start
				current_end = end
				previous_poly = []
	length = current_end - current_start
	print "%s-<Polypeptide_start=%d_end=%d>\t%s-<Polypeptide_start=%d_end=%d>\t%d" % (current_ID, current_start, current_end, current_ID, current_start, current_end, length)


if __name__ == '__main__':
	main()

