#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
#from random import shuffle
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader
import itertools
import numpy as np
import re

def main():
	usage = format_usage('''
		%prog FREQ_FILE ORIG_LABELS < NON_SCALED_IRM
		Super scale a 55x55 matrix so that, at equilibrium,
		it will result on avg in 1 AA change per unit of time.
	''')
	parser = OptionParser(usage=usage)
	parser.add_option('-r', '--rho', action="store_true", default=False, help='also scale so that avg exchange rate at equilibrium = 1 [default: %default]')	
	options, args = parser.parse_args()
	
	if len(args) != 2:
		exit('Unexpected argument number.')
	
	ORIG_LABELS = args[1].split(",")
	collapsed_labels = [re.sub(re.compile('[0-9]$'), '', x) for x in ORIG_LABELS]
	orig_size = len(ORIG_LABELS)
	orig_matrix = np.zeros((orig_size,orig_size), dtype=float)	
	norm_matrix = np.zeros((orig_size,orig_size), dtype=float)
	
	# parse the IRM
	for i,line in enumerate(stdin):
		values = [float(x) for x in safe_rstrip(line).split("\t")]
		for j,value in enumerate(values):
			if i != j:
				orig_matrix[i,j] = value
	
	# parse frequencies:
	with open(args[0], "r") as fd:
		lines = fd.readlines()
		freqs = [float(x) for x in safe_rstrip(lines[1]).split("\t")]
	
	fd.close()

	if options.rho:
		rho_matrix = np.zeros((orig_size,orig_size), dtype=float)
		# compute rho
		rho = .0
		for i,row_label in enumerate(ORIG_LABELS):
			for j,col_label in enumerate(ORIG_LABELS):
				if row_label != col_label:
					rho = rho + (orig_matrix[i,j] * freqs[i])
		# apply the rho scaling
		for i,row_label in enumerate(ORIG_LABELS):
			for j,col_label in enumerate(ORIG_LABELS):
				rho_matrix[i][j] = orig_matrix[i][j] / rho
		orig_matrix = rho_matrix

	#compute super-rho
	super_rho = .0
	for i,collapsed_row_label in enumerate(collapsed_labels):
		# from label to LABEL(s) in the orig_matrix by searching LABELS elements for matches
		r_rgx = re.compile('%s.*'%re.escape(collapsed_row_label))
		# obtain lists of indices that identify a block (submatrix) in the rotamer counts matrix (i.e. a 1x1, 1x2, 1x3, 2x2, 2x3 or 3x3 block)
		col_INDECES = [ORIG_LABELS.index(m.group(0)) for l in ORIG_LABELS for m in [r_rgx.search(l)] if m]
		for j,col_label in enumerate(ORIG_LABELS):
			if j not in col_INDECES:
				super_rho = super_rho + (orig_matrix[i,j] * freqs[i])
	
	# apply the super-rho scaling
	# this esures that at equlibium we have on avg 1 AA substitution per unit of time
	for i,row_label in enumerate(ORIG_LABELS):
		for j,col_label in enumerate(ORIG_LABELS):
			norm_matrix[i][j] = orig_matrix[i][j] / super_rho

	# print the expanded matrix including header and row labels
	if options.rho:
		print  '\t'.join(map(str, ['rho=%f;super-rho=%f' % (rho, super_rho)] + ORIG_LABELS))
	else:
		print  '\t'.join(map(str, ['super-rho=%f' % (super_rho)] + ORIG_LABELS))
	for idx, symbol in enumerate(ORIG_LABELS):
		row = [symbol]
		row.extend([x for x in norm_matrix[idx,:]])
		print '\t'.join(map(str, row))

if __name__ == '__main__':
	main()

