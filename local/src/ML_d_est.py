#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
#from random import shuffle
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader
from math import log
import numpy as np
import scipy.linalg
from scipy.optimize import minimize


def main():
	usage = format_usage('''
		%prog IRM FREQS ALIGNMENT
	''')
	parser = OptionParser(usage=usage)
	
	parser.add_option('-I', '--Infinite_seq', default=False, help='Simulate an infinite sequence, option argument should be the true distance used in sequence simulation [default: %default]')
	parser.add_option('-t', '--true_dist', action="store_true", default=False, help='Use the t value used in simulation (i.e. the "true" distance) as initial guess for the optimizer [default: %default]')
	
	options, args = parser.parse_args()
	
	if len(args) != 3 and not options.Infinite_seq:
		exit('Unexpected argument number.')
	elif len(args) != 2 and options.Infinite_seq:
		exit('Unexpected argument number.')
	
	# parse the IRM.Q-star.neg_diag stripping the header and first column
	with open(args[0], 'r') as fs:
		rows = fs.readlines()[1:]
		states = [safe_rstrip(row).split('\t')[0] for row in rows]
		n_states = len(states)
		IRM = np.zeros((n_states, n_states), dtype=float)
		for i in range(n_states):
			IRM[i,:] = safe_rstrip(rows[i]).split('\t')[1:]
		fs.close()
	# parse the frequencies, again strip header
	with open(args[1], 'r') as fs:
		row = fs.readlines()[1]
		eq_freqs = [float(x) for x in safe_rstrip(row).split('\t')]
		fs.close()
	
	if not options.Infinite_seq:
		# read two sequences
		sequences =[]
		names = []
		with open(args[2], 'r') as fs:
			for line in fs:
				seq_name, sequence = safe_rstrip(line).split('\t')
				names.append(seq_name)
				sequences.append(sequence.split(' '))
			fs.close()

		# extract the "true distance" from sequence 2's name
		true_dist = float(''.join(list(names[1])[8:15]))
		# compute all Nij, the number of coresponding sites occupied by AA i and j in the two sequences
		site_counts = np.zeros((n_states, n_states), dtype=float)
		for position in range(len(sequences[0])):
			state1 = sequences[0][position]
			state2 = sequences[1][position]
			i = states.index(state1)
			j = states.index(state2)
			site_counts[i, j] = site_counts[i, j] + 1

		# calculate the proportion of changed sites as a starting estimate for the optimization
		p = (np.sum(site_counts) - np.sum(np.diag(site_counts))) / len(sequences[0])
	else:
		true_dist = float(options.Infinite_seq)
	

	# define the likelihood	function
	def log_likelihood(t):
		# matrix exponentiation
		Pt = scipy.linalg.expm(t* IRM)
		sum_matrix = np.zeros((n_states, n_states), dtype=float)
		for i in range(n_states):
			for j in range(n_states):
				
				if not options.Infinite_seq:
					# redundant when frequencies are calculated on current sequences
					if site_counts[i, j] > 0 and eq_freqs[i] != 0.0:
					# The log likelihood function is (2.4) from Chapter 2.3 of
					# Yang, Ziheng. Molecular Evolution: A Statistical Approach (p. 41). OUP Oxford.
						sum_matrix[i, j] = site_counts[i,j]*log(eq_freqs[i]*Pt[i,j])
					else:
						continue
				else:
					# In the case of an entremely long (or infinite) sequence Ni,j could be replaced by
					#the probability of observation i,j (eq_freqs[i]*Pt[i, j]) times the number of sites
					#note that the first Pt[i,j] (outside the _log_) should use the _t_ that is the time value used in simulation, aka the timepoint
					if Pt[i, j] * eq_freqs[i] != 0.0:
						Pt_true = scipy.linalg.expm(true_dist* IRM)
						sum_matrix[i, j] = eq_freqs[i] * Pt_true[i, j] * log(eq_freqs[i] * Pt[i, j])
					else:
						continue
		LL = np.sum(sum_matrix)
		return -LL
	
	if options.true_dist:
		guess = true_dist
	elif options.Infinite_seq:
		guess = true_dist*0.7
	else:
		guess = p
	
	if guess == 0.0:
		# the sequences are identical, therefore output an inferred distance of 0
		print "0.0\t0.0\tThe sequences are identical"	
	else:
		# extimate branch lengths by minimizing -LL
		out = minimize(log_likelihood, guess, method='Nelder-Mead', options={'maxiter': 500, 'xatol': 0.0001, 'fatol': 0.0001})
		print "%f\t%f\t%s" % (guess, out.x, out.message)

if __name__ == '__main__':
	main()

