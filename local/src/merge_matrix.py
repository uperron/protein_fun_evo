#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
#from random import shuffle
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader

import numpy as np
import matplotlib
# force matplotlib  not to use Xwi
matplotlib.use('Agg')

import matplotlib.pyplot as plt
import matplotlib.figure as fg
import numpy as np
import matplotlib.ticker as ticker

import random
import re
import os

def plot_heatmap(matrix, label, outfile):
        fig = plt.figure(figsize=matrix.shape)
        ax = plt.subplot(2, 1, 1)

        # set ticks format on axes
        majorLocator = ticker.LinearLocator(numticks=len(label))
        majorFormatter = ticker.FormatStrFormatter('%s')
        ax.xaxis.set_major_locator(majorLocator)
        ax.xaxis.set_major_formatter(majorFormatter)
        ax.yaxis.set_major_locator(majorLocator)
        ax.yaxis.set_major_formatter(majorFormatter)

        # plot the matrix with no interpolation
        ax.imshow(matrix, interpolation=None, cmap='binary', aspect='auto')


        #label ticks on axes
        ax.set_yticklabels(label)

	plt.savefig(outfile, format='pdf', bbox_inches='tight', dpi=fig.dpi)
        plt.close(fig)


def main():
	usage = format_usage('''
		%prog LABELS < FILENAMES
	''')
	parser = OptionParser(usage=usage)

	parser.add_option('-R', '--Rand', dest='Rand', type=str,  default=False, help='generate random counts matrix w/t specified max number of counts per cell [default: %default]')
	parser.add_option('-r', '--rep', dest='rep', action='store_true', default=False, help='represent the matrix as an  heatmap [default: %default]')	
	parser.add_option('-o', '--outfile', dest='outfile', default='outfile', help='output file [default: %default]', metavar='FILE')

	options, args = parser.parse_args()
	
	if len(args) != 1:
		exit('Unexpected argument number.')
	
	labels = args[0].split(",")
	merged_matrix = np.zeros((len(labels),len(labels)), dtype=np.float64)	
	
	if options.Rand is not False and options.Rand != 'max':
		# create a random matrix, min is set to 5 in order to always allow Cramer's V and T-tests
		rand_matrix = np.random.random_integers(5, high=int(options.Rand), size=(len(labels),len(labels)))
		# generate a symmetric random matrix using the lower triangular matrix
		merged_matrix = np.tril(rand_matrix) + np.tril(rand_matrix, -1).T

	else:
		# parse filenames in stdin (\n separated), avoid 'Argument list too long'
		for line in stdin:
			filename = safe_rstrip(line)
			# skip empty files
			if os.stat(filename).st_size == 0:
				continue 
			else:
				with open(filename, 'r') as f:
					lines = f.readlines()
					# obtain th subset of labels used in current file
					current_col_labels = re.sub('\n', '', lines[0]).split("\t")[1:]
					# from cell to label
					for line in lines[1:]:
						current_line_label = line.split("\t")[0]
						for i,value in enumerate([float(x) for x in line.split("\t")[1:]]):
							current_col_label = current_col_labels[i]
							# sum current cell to corresponding cell in merged matrix
							line_label = labels.index(current_line_label)
							col_label = labels.index(current_col_label)	
							merged_matrix[line_label][col_label] = merged_matrix[line_label][col_label] + value

		if options.Rand == 'max':
			#create a random, symmetric matrix, high value is max value in merged matrix
			rand_matrix = np.random.random_integers(5, high=np.amax(merged_matrix), size=(len(labels),len(labels)))
			merged_matrix = np.tril(rand_matrix) + np.tril(rand_matrix, -1).T
		
	# print matrix as TSV with header and row labels or represent as heatmap
	if options.rep == False:
		header = ['X']
		header.extend(labels)
		print  '\t'.join(map(str, header))
		for idx, symbol in enumerate(labels):
			row = [symbol]
			row.extend([x for x in merged_matrix[idx]])
			print '\t'.join(map(str, row))
	else:
		plot_heatmap(merged_matrix, labels, options.outfile)		


if __name__ == '__main__':
	main()
