# Contains all methods for preparing input files, cleaning pdbs, and so on
#!/usr/bin/env python
import math
import os
import numpy as np
#serv_home = os.getenv("INT_SERV_HOME")
#working_space = os.getenv("INT_SERV_WORK")

RES_3=['ALA','CYS','ASP','GLU','PHE','GLY','HIS','ILE','LYS','LEU','MET','ASN','PRO','GLN','ARG','SER','THR','VAL','TRP','TYR']
RES_1=['A','C','D','E','F','G','H','I','K','L','M','N','P','Q','R','S','T','V','W','Y']
atoms_in_residues=[ ]
atoms_in_residues.append(['N','CA','C','O','CB'])
atoms_in_residues.append(['N','CA','C','O','CB','SG'])
atoms_in_residues.append(['N','CA','C','O','CB','CG','OD1','OD2'])
atoms_in_residues.append(['N','CA','C','O','CB','CG','CD','OE1','OE2'])
atoms_in_residues.append(['N','CA','C','O','CB','CG','CD1','CD2','CE1','CE2','CZ'])
atoms_in_residues.append(['N','CA','C','O',])
atoms_in_residues.append(['N','CA','C','O','CB','CG','ND1','CD2','CE1','NE2'])
atoms_in_residues.append(['N','CA','C','O','CB','CG1','CG2','CD1'])
atoms_in_residues.append(['N','CA','C','O','CB','CG','CD','CE','NZ'])
atoms_in_residues.append(['N','CA','C','O','CB','CG','CD1','CD2'])
atoms_in_residues.append(['N','CA','C','O','CB','CG','SD','CE'])
atoms_in_residues.append(['N','CA','C','O','CB','CG','OD1','ND2'])
atoms_in_residues.append(['N','CA','C','O','CB','CG','CD'])
atoms_in_residues.append(['N','CA','C','O','CB','CG','CD','OE1','NE2'])
atoms_in_residues.append(['N','CA','C','O','CB','CG','CD','NE','CZ','NH1','NH2'])
atoms_in_residues.append(['N','CA','C','O','CB','OG'])
atoms_in_residues.append(['N','CA','C','O','CB','OG1','CG2'])
atoms_in_residues.append(['N','CA','C','O','CB','CG1','CG2'])
atoms_in_residues.append(['N','CA','C','O','CB','CG','CD1','CD2','NE1','CE2','CE3','CZ2','CZ3','CH2'])
atoms_in_residues.append(['N','CA','C','O','CB','CG','CD1','CD2','CE1','CE2','CZ','OH'])

##Dictionary of dictionaries (one for each chi rotamer) of lists (atoms in that chi rotamer.
chi_atoms = dict(
        chi1=dict(
            ARG=['N', 'CA', 'CB', 'CG'],
            ASN=['N', 'CA', 'CB', 'CG'],
            ASP=['N', 'CA', 'CB', 'CG'],
            CYS=['N', 'CA', 'CB', 'SG'],
            GLN=['N', 'CA', 'CB', 'CG'],
            GLU=['N', 'CA', 'CB', 'CG'],
            HIS=['N', 'CA', 'CB', 'CG'],
            ILE=['N', 'CA', 'CB', 'CG1'],
            LEU=['N', 'CA', 'CB', 'CG'],
            LYS=['N', 'CA', 'CB', 'CG'],
            MET=['N', 'CA', 'CB', 'CG'],
            PHE=['N', 'CA', 'CB', 'CG'],
            PRO=['N', 'CA', 'CB', 'CG'],
            SER=['N', 'CA', 'CB', 'OG'],
            THR=['N', 'CA', 'CB', 'OG1'],
            TRP=['N', 'CA', 'CB', 'CG'],
            TYR=['N', 'CA', 'CB', 'CG'],
            VAL=['N', 'CA', 'CB', 'CG1'],
        ),
        altchi1=dict(
            VAL=['N', 'CA', 'CB', 'CG2'],
        ),
        chi2=dict(
            ARG=['CA', 'CB', 'CG', 'CD'],
            ASN=['CA', 'CB', 'CG', 'OD1'],
            ASP=['CA', 'CB', 'CG', 'OD1'],
            GLN=['CA', 'CB', 'CG', 'CD'],
            GLU=['CA', 'CB', 'CG', 'CD'],
            HIS=['CA', 'CB', 'CG', 'ND1'],
            ILE=['CA', 'CB', 'CG1', 'CD1'],
            LEU=['CA', 'CB', 'CG', 'CD1'],
            LYS=['CA', 'CB', 'CG', 'CD'],
            MET=['CA', 'CB', 'CG', 'SD'],
            PHE=['CA', 'CB', 'CG', 'CD1'],
            PRO=['CA', 'CB', 'CG', 'CD'],
            TRP=['CA', 'CB', 'CG', 'CD1'],
            TYR=['CA', 'CB', 'CG', 'CD1'],
        ),
        altchi2=dict(
            ASP=['CA', 'CB', 'CG', 'OD2'],
            LEU=['CA', 'CB', 'CG', 'CD2'],
            PHE=['CA', 'CB', 'CG', 'CD2'],
            TYR=['CA', 'CB', 'CG', 'CD2'],
        ),
        chi3=dict(
            ARG=['CB', 'CG', 'CD', 'NE'],
            GLN=['CB', 'CG', 'CD', 'OE1'],
            GLU=['CB', 'CG', 'CD', 'OE1'],
            LYS=['CB', 'CG', 'CD', 'CE'],
            MET=['CB', 'CG', 'SD', 'CE'],
        ),
        chi4=dict(
            ARG=['CG', 'CD', 'NE', 'CZ'],
            LYS=['CG', 'CD', 'CE', 'NZ'],
        ),
        chi5=dict(
            ARG=['CD', 'NE', 'CZ', 'NH1'],
        ),
    )

class HETATOM:
        def __init__(self,a_name,a_number,x,y,z,marker,chain_ref,r_name,r_number):
                self.a_name = a_name
                self.a_number = a_number
                self.x = x
                self.y = y
                self.z = z
                self.a_marker = marker
                self.chain_ref = chain_ref
                self.r_name = r_name
                self.r_number = r_number

class ATOM:
        def __init__(self,name,number,x,y,z,marker):  #nb marker is usually A or B, to show ambiguities in PDB
                self.a_name = name
                self.a_number = number
                self.x = x
                self.y = y
                self.z = z
                self.a_marker = marker
                self.modes = [ ]
        def add_mode(self,modetoadd):
                self.modes.append(modetoadd)
        def get_modes(self):
                #print self.modes
                return self.modes
        def get_mass(self):
                if (self.a_name[0] == 'H'):
                        return 1.007825
                if (self.a_name[0] == 'C'):
                        return 12.0
                if (self.a_name[0] == 'O'):
                        return 15.9994
                if (self.a_name[0] == 'N'):
                        return 14.0067
                if (self.a_name[0] == 'S'):
                        return 31.972071

class RESIDUE:
        def __init__(self,atoms_list, name, number):
                self.atoms = [ ]
                for x in atoms_list:
                        self.atoms.append(x)
                self.r_name = name
                self.r_number = number
                self.interface = 0
                self.chi_angles = 0

        def set_as_interface(self):
                self.interface = 1
        def unset_as_interface(self):
                self.interface = 0
        def check_CA(self): #checks whether it has alpha carbon, returns 0 if it doesn't, or 1 if it does.
                for atm in self.atoms:
                        if (atm.a_name == 'CA'):
                                return 1
                return 0
        def check_backbone(self): #checks whether it has all backbone atoms, returns 0 if it doesn't, or 1 if it does.
                got_N = 0
                got_CA = 0
                got_C = 0
                got_O = 0
                for atm in self.atoms:
                        if (atm.a_name == 'N'):
                                got_N = 1
                        if (atm.a_name == 'CA'):
                                got_CA = 1
                        if (atm.a_name == 'C'):
                                got_C = 1
                        if (atm.a_name == 'O'):
                                got_O = 1
                if ( (got_N == 1) and (got_CA == 1) and (got_C == 1) and (got_O == 1)):
                        return 1
                else:
                        return 0
        def check_standard_residue(self): #checks to see if residue is standard, returns 0 if it isn't, or 1 if it is.
                ref=9999
                for x in range(len(RES_3)):
                        if (self.r_name == RES_3[x]):
                                ref=x
                if (ref==9999):
                        return 0
                else:
                        return 1
        def check_standard_atoms(self): #returns a list of non-standard atoms. #NOTE: doesn't charmm charmm ILE CD or OT1/OT2 to normal CD1 or O/OXT here. ONLY CALL IF ITS A STANDARD RESIDUE, otherwise will return empty list
                bad_atms = []
                ref = 999
                for x in range(len(RES_3)):
                        if (self.r_name == RES_3[x]):
                                ref=x
                if (ref == 999):
                        return bad_atms
                counter = 0
                for atm in self.atoms:
                        inlist = 0
                        for x in range(len(atoms_in_residues[ref])):
                                if (atm.a_name == atoms_in_residues[ref][x]):
                                        inlist = 1
                        if (inlist == 0):
                                bad_atms.append(counter)
                        counter += 1
                return bad_atms
        def check_duplicate_atoms(self): #returns all atoms that are duplicates, not including the first instance of that atom. Returns empty list if not a standard residue
                ref = 999
                dup_atms = []
                for x in range(len(RES_3)):
                        if (self.r_name == RES_3[x]):
                                ref=x
                if (ref == 999):
                        return dup_atms
                countvec=[ ]
                for x in atoms_in_residues[ref]:
                        countvec.append(0)
                counter = 0
                for atm in self.atoms:
                        for x in range(len(atoms_in_residues[ref])):
                                if (atm.a_name == atoms_in_residues[ref][x]):
                                        inlist = 1
                                        countvec[x] += 1
                                        if (countvec[x] > 1):
                                                dup_atms.append(counter)
                        counter+=1
                return dup_atms
        def check_missing_atoms(self): #returns 1 if there are missing atoms, 0 otherwise. Returns 0 if non-standard residue
                ref = 999
                for x in range(len(RES_3)):
                        if (self.r_name == RES_3[x]):
                                ref=x
                if (ref == 999):
                        return 0
                for atm in self.atoms:
                        inlist = 0
                        for x in range(len(atoms_in_residues[ref])):
                                if (atm.a_name == atoms_in_residues[ref][x]):
                                        inlist = 1
                        if (inlist == 0):
                                return 1
                return 0


        def check_atoms(self):
                ref=9999
                for x in range(len(RES_3)):
                        if (self.r_name == RES_3[x]):
                                ref=x
                countvec=[ ]
                for x in atoms_in_residues[ref]:
                        countvec.append(0)
                for atm in self.atoms:
                        inlist = 0
                        for x in range(len(atoms_in_residues[ref])):
                                if (atm.a_name == atoms_in_residues[ref][x]):
                                        inlist = 1
                                        countvec[x] += 1
                        if (inlist == 0):
                                print("RESIDUE %s%i HAS NONSTANDARD ATOM %s" % (self.r_name,self.r_number,atm.a_name))
                errors=0
                for x in range(len(countvec)):
                        if (countvec[x] > 1):
                                print('RESIDUE %s%i HAS DUPLICATE %s' % (self.r_name,self.r_number,atoms_in_residues[ref][x]))
                                errors += 1
                        if (countvec[x] == 0):
                                print('RESIDUE %s%i HAS MISSING %s' % (self.r_name,self.r_number,atoms_in_residues[ref][x]))
                                errors += 1
                if (errors == 0):
                        ordermismatch=0
                        for x in range(len(atoms_in_residues[ref])):
                                if (atoms_in_residues[ref][x] != self.atoms[x].a_name):
                                        ordermismatch += 1
                        if (ordermismatch != 0):
                                print('RESIDUE %s%i HAS CORRECT ATOMS, BUT MISORDERED, REORDERING' % (self.r_name,self.r_number))
                                self.reorder_atoms()

               

        def reorder_atoms(self):
                for x in range(len(RES_3)):
                        if (self.r_name == RES_3[x]):
                                ref=x
                tempatoms=[ ]
                for x in self.atoms:
                        an_atom=x
                        tempatoms.append(an_atom)
                self.atoms=[ ]
                for x in atoms_in_residues[ref]:
                        for y in tempatoms:
                                if (x == y.a_name):
                                        self.atoms.append(y)
        def calculate_chi(self):
                self.reorder_atoms()
                chi_angles={}
                for rotamer in chi_atoms.keys():
                        rotamer_coords = []
                        rotamer_atms = chi_atoms.get(rotamer, {}).get(self.r_name)
                        if rotamer_atms is not None:
                                for atm in rotamer_atms:
                                         for atm2 in self.atoms:
                                                 if atm == atm2.a_name:
                                                         rotamer_coords.append([atm2.x,atm2.y,atm2.z])
                                                 continue
                                if len(rotamer_coords) == 4:
                                        rotamer_array = np.array(rotamer_coords)
                                        chi_angles[rotamer] =  self.dihedral_angle(rotamer_array)
                                else:
                                        chi_angles[rotamer] = "NA"
                        else:
                                chi_angles[rotamer] = "NA"
                #alt_lst = ('altchi1', 'altchi2')
                #chi_lst = ('chi1', 'chi2')
                #for alt, chi in alt_lst, chi_lst:
                #        min_angle = min(chi_angles[alt], chi_angles[chi] 
                #        chi_angles[chi] = 
                self.chi_angles = [value for(key, value) in sorted(chi_angles.items())]


#Calculates dihedral angles with formula from Wikipedia "Dihedral angle" article https://en.wikipedia.org/wiki/Dihedral_angle.
        def dihedral_angle(self,points):
                p0 = points[0]
                p1 = points[1]
                p2 = points[2]
                p3 = points[3]

                b0 = -1.0*(p1 - p0)
                b1 = p2 - p1
                b2 = p3 - p2

                b0xb1 = np.cross(b0, b1)
                b1xb2 = np.cross(b2, b1)

                b0xb1_x_b1xb2 = np.cross(b0xb1, b1xb2)

                y = np.dot(b0xb1_x_b1xb2, b1)*(1.0/np.linalg.norm(b1))
                x = np.dot(b0xb1, b1xb2)

                return np.degrees(np.arctan2(y, x)) 

 
       

class CHAIN:
        def __init__(self,residues,chain_ref):
                self.residues = [ ]
                for x in residues:
                        self.residues.append(x)
                self.chain_id = chain_ref
                self.sequence = [ ]
                for x in residues:
                        found=0
                        for y in range(len(RES_3)):
                                if (x.r_name == RES_3[y]):
                                        found=1
                                        self.sequence.append(RES_1[y])
                        if (found == 0):
                                self.sequence.append('X')

        def change_chain_id(self,newid):
                self.chain_id = newid
        def CENTRE_OF_MASS(self):
                sum_masses = 0
                sum_x = 0
                sum_y = 0
                sum_z = 0
                for b in self.residues:
                        for c in b.atoms:
                                x = c.x
                                y = c.y
                                z = c.z
                                mass = c.get_mass()
                                sum_masses += mass
                                sum_x += mass * x
                                sum_y += mass * y
                                sum_z += mass * z
                x = sum_x/sum_masses
                y = sum_y/sum_masses
                z = sum_z/sum_masses
                mytupel = (x,y,z)
                return mytupel

        def get_one_letter_code(self):
                olc=[ ]
                for x in self.residues:
                        ref=999
                        for y in range(len(RES_3)):
                                if (x.r_name == RES_3[y]):
                                        ref=y
                        if (ref==999):
                                olc.append('X')
                        else:
                                olc.append(RES_1[ref])
                return olc
        def check_contiguous(self):                     # Checks inter-CA distances to determine contiguousness.
                start = 1
                found=0
                for x in self.residues[0].atoms:
                        if (x.a_name == 'C'):
                                found = 1
                                C1x = x.x
                                C1y = x.y
                                C1z = x.z
                                rnme1 = self.residues[0].r_name
                                rno1 = self.residues[0].r_number
                if (found==0):
#                       print 'ERROR: CHAIN %s FIRST RESIDUE HAS NO CA, checking second'%self.chain_id
#                       for x in self.residues[1].atoms:
#                               if (x.a_name == 'CA'):
#                                       found = 1
#                                       CA1x = x.x
#                                       CA1y = x.y
#                                       CA1z = x.z
#                                       rnme1 = self.residues[1].r_name
#                                       rno1 = self.residues[1].r_number
#                       start += 1
#               if (found==0):
                        print('ERROR: CHAIN %s FIRST RESIDUE HAS NO C, not checking for contiguity'%self.chain_id)
                else:
                        for x in range(start,len(self.residues)):
                                found = 0
                                found2 = 0
                                for y in self.residues[x].atoms:
                                        if (y.a_name == 'N'):
                                                found = 1
                                                N2x = y.x
                                                N2y = y.y
                                                N2z = y.z
                                                rnme2 = self.residues[x].r_name
                                                rno2 = self.residues[x].r_number
                                        if (y.a_name == 'C'):
                                                found2 = 1
                                                C2x = y.x
                                                C2y = y.y
                                                C2z = y.z
                                                nCnme = self.residues[x].r_name
                                                nCrno = self.residues[x].r_number
                                if (found == 0):
                                        print('MISSING N IN RESIDUE %s%i'%(self.residues[x].r_name,self.residues[x].r_number))
                                else:
                                        dx = C1x-N2x
                                        dy = C1y-N2y
                                        dz = C1z-N2z
                                        distance=math.sqrt((dx*dx)+(dy*dy)+(dz*dz))
                                        if (distance > 2.0):
                                                print('%s%i C to %s%i N distance is %f - TOO BIG FOR CONTIGUOUS CHAIN'%(rnme1,rno1,rnme2,rno2,distance))
                                                if (rno2-rno1 == 2):
                                                        print('LOOKHERE')
                                if (found2 == 0):
                                        print('MISSING C IN RESIDUE %s%i'%(self.residues[x].r_name,self.residues[x].r_number))
                                else:
                                        C1x = C2x
                                        C1y = C2y
                                        C1z = C2z
                                        rnme1 = nCnme
                                        rno1 = nCrno
class PDB:
        def __init__(self,filename):
                self.chains = [ ]
                self.hetatms = [ ]
                pdb = open(filename).readlines()
                firstres = 1
                currentresidues = [ ]
                currentatoms = [ ]
                newchain = 0
                for line in pdb:
                        if (len(line) > 2):
                                if (line[:3] == 'TER'):
                                        newchain = 1
                        if (len(line) > 6):
                                if (line[:6] == 'HETATM'):
                                        h_chain = line[21]
                                        h_resno = int(line[22:26].strip())
                                        h_resname = line[17:20]
                                        h_atomname = line[13:16].strip()
                                        h_atomno = int(line[7:11])
                                        h_marker = line[16]
                                        h_x = float(line[30:38].strip())
                                        h_y = float(line[38:46].strip())
                                        h_z = float(line[46:54].strip())
                                        a_hetatm=HETATOM(h_atomname,h_atomno,h_x,h_y,h_z,h_marker,h_chain,h_resname,h_resno)
                                        self.hetatms.append(a_hetatm)
                                if (line[:4] == 'ATOM'):
                                        current_chain = line[21]
                                        current_resno = int(line[22:26].strip())
                                        current_resname = line[17:20]
                                        current_atomname = line[13:16].strip()
                                        if (current_resname == 'ILE' and current_atomname == 'CD'): #convert CHARMM type to pdb type
                                                current_atomname='CD1'
                                        if (current_atomname == 'OT1' or current_atomname == 'OT2' or current_atomname == 'OXT'): #convert CHARMM type to pdb type, and rename OXTs
                                                current_atomname='O'
                                        current_atomno = int(line[7:11])
                                        current_marker = line[16]
                                        current_x = float(line[30:38].strip())
                                        current_y = float(line[38:46].strip())
                                        current_z = float(line[46:54].strip())
                                        if (firstres==1):
                                                old_resno = current_resno
                                                old_chain = current_chain
                                                old_resname = current_resname
                                                firstres=0
                                        if ((old_resno != current_resno) or (old_chain != current_chain) or (old_resname != current_resname)):
                                                if (old_chain != current_chain):
                                                        newchain = 1
                                                a_residue=RESIDUE(currentatoms,old_resname,old_resno)
                                                #a_residue.repair_for_capri16()
                                                currentresidues.append(a_residue)
                                                currentatoms = [ ]
                                        if (newchain == 1):
                                                a_chain = CHAIN(currentresidues,old_chain)
                                                self.chains.append(a_chain)
                                                currentresidues = [ ]
                                                newchain = 0
                                        an_atom=ATOM(current_atomname,current_atomno,current_x,current_y,current_z,current_marker)
                                        currentatoms.append(an_atom)
                                        old_resname = current_resname
                                        old_chain = current_chain
                                        old_resno = current_resno
                a_residue=RESIDUE(currentatoms,old_resname,old_resno)
                #a_residue.repair_for_capri16()
                currentresidues.append(a_residue)
                a_chain = CHAIN(currentresidues,old_chain)
                self.chains.append(a_chain)
        def CLEAN(self):
                for a in range(len(self.chains)):
                        delreslist = []
                        for b in range(len(self.chains[a].residues)):
                                if (self.chains[a].residues[b].check_standard_residue() == 0):
                                        delreslist.append(b)
                                        print('Deleting non-standard residue: %s%i'%(self.chains[a].residues[b].r_name,self.chains[a].residues[b].r_number))
                                if (self.chains[a].residues[b].check_backbone() == 0):
                                        delreslist.append(b)
                                        print('Deleting residue with incomplete backbone: %s%i'%(self.chains[a].residues[b].r_name,self.chains[a].residues[b].r_number))
                        delreslist.reverse()
                        for b in delreslist:
                                del self.chains[a].residues[b]
                        for b in range(len(self.chains[a].residues)):
                                delatlist = self.chains[a].residues[b].check_duplicate_atoms()
                                delatlist.reverse()
                                for c in delatlist:
                                        print('Deleting duplicate atom %s%i %s'%(self.chains[a].residues[b].r_name,self.chains[a].residues[b].r_number,self.chains[a].residues[b].atoms[c].a_name))
                                        del self.chains[a].residues[b].atoms[c]
                                        self.chains[a].residues[b].reorder_atoms()
                                        
        def CENTRE_OF_MASS(self):
                sum_masses = 0
                sum_x = 0
                sum_y = 0
                sum_z = 0
                for a in self.chains:
                        for b in a.residues:
                                for c in b.atoms:
                                        x = c.x
                                        y = c.y
                                        z = c.z
                                        mass = c.get_mass()
                                        sum_masses += mass
                                        sum_x += mass * x
                                        sum_y += mass * y
                                        sum_z += mass * z
                x = sum_x/sum_masses
                y = sum_y/sum_masses
                z = sum_z/sum_masses
                mytupel = (x,y,z)
                return mytupel

def WRITEPDB(pdb,filename):             #NB does not write out HETATMs
        outfile=open(filename,'w')
        for chns in pdb.chains:
                for resi in chns.residues:
                        for atm in resi.atoms:
                                outfile.write('ATOM   %s  %s%s%s %s%s    %8.3f%8.3f%8.3f\n'%(str(atm.a_number).rjust(4),str(atm.a_name).ljust(3),str(atm.a_marker),resi.r_name,chns.chain_id,str(resi.r_number).rjust(4)[:4],atm.x,atm.y,atm.z))
                outfile.write('TER\n')
        outfile.close()

def WRITEPDB_CLEAN(pdb,chainID,filename):             #NB does not write out HETATMs
        outfile=open(filename,'w')
        rescounter = 0
        for chns in pdb.chains:
                for resi in chns.residues:
                        rescounter += 1
                        for atm in resi.atoms:
                                outfile.write('ATOM   %s  %s%s%s %s%s    %8.3f%8.3f%8.3f\n'%(str(atm.a_number).rjust(4),str(atm.a_name).ljust(3),str(atm.a_marker),resi.r_name,chainID,str(rescounter).rjust(4)[:4],atm.x,atm.y,atm.z))
#                outfile.write('TER\n')
        outfile.close()

def WRITE_COMPLEX(p1,p2,cID1,cID2,filename):
        outfile=open(filename,'w')
        rescounter = 0
        for chns in p1.chains:
                for resi in chns.residues:
                        rescounter += 1
                        for atm in resi.atoms:
                                outfile.write('ATOM   %s  %s%s%s %s%s    %8.3f%8.3f%8.3f\n'%(str(atm.a_number).rjust(4),str(atm.a_name).ljust(3),str(atm.a_marker),resi.r_name,cID1,str(rescounter).rjust(4)[:4],atm.x,atm.y,atm.z))
#                outfile.write('TER\n')
        for chns in p2.chains:
                for resi in chns.residues:
                        rescounter += 1
                        for atm in resi.atoms:
                                outfile.write('ATOM   %s  %s%s%s %s%s    %8.3f%8.3f%8.3f\n'%(str(atm.a_number).rjust(4),str(atm.a_name).ljust(3),str(atm.a_marker),resi.r_name,cID2,str(rescounter).rjust(4)[:4],atm.x,atm.y,atm.z))
        outfile.close()

