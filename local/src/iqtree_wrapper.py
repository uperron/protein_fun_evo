#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
#from random import shuffle
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader

import os
import subprocess
import shutil

codon_AA_dict = {'ACC': 'GLN', 'ATG': 'PRO', 'AAG': 'ASN', 'AAA': 'ALA', 'ATC': 'PHE', 'AAC': 'ARG', 'ATA': 'MET', 'AGG': 'LEU', 'CCT': 'NA', 'CTC': 'NA', 'AGC': 'ILE', 'ACA': 'CYS', 'AGA': 'HIS', 'CAT': 'VAL', 'AAT': 'ASP', 'ATT': 'SER', 'CTG': 'NA', 'CTA': 'NA', 'ACT': 'GLY', 'CAC': 'TRP', 'ACG': 'GLU', 'CAA': 'THR', 'AGT': 'LYS', 'CCA': 'NA', 'CCG': 'NA', 'CCC': 'NA', 'TAT': 'NA', 'GGT': 'NA', 'TGT': 'NA', 'CGA': 'NA', 'CAG': 'TYR', 'CGC': 'NA', 'GAT': 'NA', 'CGG': 'NA', 'CTT': 'NA', 'TGC': 'NA', 'GGG': 'NA', 'TAG': 'NA', 'GGA': 'NA', 'TAA': 'NA', 'GGC': 'NA', 'TAC': 'NA', 'GAG': 'NA', 'TCG': 'NA', 'TTA': 'NA', 'TTT': 'NA', 'GAC': 'NA', 'CGT': 'NA', 'GAA': 'NA', 'TCA': 'NA', 'GCA': 'NA', 'GTA': 'NA', 'GCC': 'NA', 'GTC': 'NA', 'GCG': 'NA', 'GTG': 'NA', 'TTC': 'NA', 'GTT': 'NA', 'GCT': 'NA', 'TGA': 'NA', 'TTG': 'NA', 'TCC': 'NA', 'TGG': 'NA', 'TCT': 'NA'}

three_to_one = {'CYS': 'C', 'ASP': 'D', 'SER': 'S', 'GLN': 'Q', 'LYS': 'K', 'TRP': 'W', 'PRO': 'P', 'THR': 'T', 'ILE': 'I', 'ALA': 'A', 'PHE': 'F', 'GLY': 'G', 'HIS': 'H', 'LEU': 'L', 'ARG': 'R', 'MET': 'M', 'GLU': 'E', 'ASN': 'N', 'TYR': 'Y', 'VAL': 'V'}


def main():
	usage = format_usage('''
		%prog IQTREE_IRM PHYLIP_MULTIALIGN 
	''')
	parser = OptionParser(usage=usage)
	parser.add_option('-d', '--datatype',  default="CODON", help='alignment data type [default: %default]')
	parser.add_option('-P', '--PAML',  default=False, action='store_true', help='do not translate AA into codons [default: %default]')
	parser.add_option('-o', '--output',  default=False, action='store_true', help='print the iqtree output [default: %default]')
	parser.add_option('-n', '--nofreq', default=False, action='store_true', help='use matrix-defined frequencies [default: %default]')
	options, args = parser.parse_args()
	
	if len(args) != 2:
		exit('Unexpected argument number.')
	
	customIRM, multialign_file = args

	#create an input alignment file (PHYLIP) for iqtree
	file_in =  open(multialign_file, "r")	
	if not os.path.exists('iqdir'):
		os.makedirs('iqdir')

	# run iqtree for each multi-sequence alignment in the input file, use empirical state frequencies
	bashCommand = "iqtree -s iqdir/input_align -st " + options.datatype  + " -m " + customIRM + "+F"
	if options.PAML or options.nofreq:
		# don't use empirical frequencies
		bashCommand = "iqtree -s iqdir/input_align -st " + options.datatype  + " -m " + customIRM
	lines = file_in.readlines()

	# numerical-only regexp


	for i,line in enumerate(lines):
		line = line.rstrip('\n').rstrip('\r') + '\n'
		iqtree_file_in = open('iqdir/input_align', 'a+')		
		if options.datatype == "AA":
			if not options.PAML:
				out = []
				# translate back "codons" into 3-letter AA, then into 1-letter AA
				# this is done because the AA sequences are translated into codons in order for codonPhyML to work properly
				if line != '\n':
					ID, field2 = line.rstrip("\n").split("\t") 
					if field2.isdigit():
						line = ID + '\t'  + str(int(field2) / 3)
					else:
						# codons must be space separated or "," separated
						s_seq = line.rstrip("\n").split("\t")[1].split(' ')
						c_seq = line.rstrip("\n").split("\t")[1].split(',')
						if len(s_seq) > len(c_seq):
							sequence = s_seq
						else:
							sequence = c_seq
						for symbol in sequence:
							out.append(three_to_one[codon_AA_dict[symbol]])
						line = ID + '\t'  + ' '.join(map(str, out))
		
		# remove \r, skip multiple empty lines
		line = line.rstrip('\n').rstrip('\r') + '\n'
		if line == '\n' and lines[i-1].rstrip('\n').rstrip('\r') + '\n' == '\n' and lines[i-2].rstrip('\n').rstrip('\r') + '\n' == '\n':
			continue
		else:
			try:
				print >>iqtree_file_in, line
			finally:
				# close file  so that iqtree can read it
				iqtree_file_in.close()
			if line == '\n' and lines[i-1].rstrip('\n').rstrip('\r') + '\n' == '\n':
				process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)	
				output = process.communicate()
				if options.output:
						print output[0]
				# print the inferred newick tree
				with file('iqdir/input_align.treefile', 'r') as fd:
					for out_line in fd:
						print out_line
				# empty the directory
				for f in os.listdir("iqdir"):
					os.remove(os.path.join("iqdir/" + f))
				# empty the iqtree input file (redundant)	
				open('iqdir/input_align', 'w').close()
			# last line
			elif i+1 == len(lines):
				process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)	
				output = process.communicate()
				if options.output:
					print output[0]
				# print the inferred newick tree
				with file('iqdir/input_align.treefile', 'r') as fd:
					for out_line in fd:
						print out_line
				shutil.rmtree('iqdir')

if __name__ == '__main__':
	main()

