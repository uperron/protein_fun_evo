#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr, stdout
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
#from random import shuffle
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader

import numpy as np
import pandas as pd

import matplotlib
# force matplotlib  not to use Xwi
matplotlib.use('Agg')

import matplotlib.pyplot as plt
import matplotlib.figure as fg
import matplotlib.ticker as ticker
import matplotlib.cm as cm


def main():
	usage = format_usage('''
		%prog RF_TAB WRF_TAB 
	''')
	parser = OptionParser(usage=usage)
	
	parser.add_option('-H', '--Heatmap',  action='store_true', default=False, help='plot as heatmap [default: %default]')

	options, args = parser.parse_args()
	
	if len(args) != 2:
		exit('Unexpected argument number.')
	

	RF_df = pd.read_csv(args[0], sep="\t", header=None, index_col=[0,1], names=['RF'])
	RF_df = RF_df.sort_values('RF')
	wRF_df = pd.read_csv(args[1], sep="\t", header=None, index_col=[0,1], names=['weightedRF'])
	wRF_df = wRF_df.sort_values('weightedRF')

	models =  RF_df.index.get_level_values(0).unique().tolist()

	# the square matrix
	sqr_df = pd.DataFrame(index=models, columns=models)
	sqr_df = sqr_df.where(np.triu(np.ones(sqr_df.shape)).astype(np.bool), other=True)

	# put the weightedRF distances in the lower triangular, RF in upper
	for i,j in zip(wRF_df.index.get_level_values(0).tolist(), wRF_df.index.get_level_values(1).tolist()):
		if sqr_df.loc[i,j] == True:
			sqr_df.set_value(i,j, wRF_df.loc[i,j].values[0])
		else:
			try:
				sqr_df.set_value(i,j, RF_df.loc[i,j].values[0])
			except:
				sqr_df.set_value(i,j, 0)
	sqr_df = sqr_df.fillna(0).astype(int)
	if not options.Heatmap:
		 # print as latex table
		print sqr_df.to_latex()

	else:
		fig,ax = plt.subplots()

		a = ax.matshow(sqr_df.mask(np.triu(np.ones(sqr_df.shape)).astype(np.bool)), cmap=cm.Reds, aspect="auto")
		b = ax.matshow(sqr_df.mask(np.tril(np.ones(sqr_df.shape)).astype(np.bool)), cmap=cm.Blues, aspect="auto")


		cba = plt.colorbar(a)
		cbb = plt.colorbar(b)

		cba.set_label('Weighted RF')
		cbb.set_label('RF')
		
		ticks = range(len(models))
		plt.xticks(ticks, sqr_df.columns)
		plt.yticks(ticks, sqr_df.index)
		plt.setp(ax.get_xticklabels(), rotation=45, ha="left", rotation_mode="anchor", size="small")

		for i in ticks:
			for j in ticks:
				text = ax.text(j, i, sqr_df.iloc[i, j], ha="center", va="center", color="k")

		plt.savefig(stdout, format='pdf', bbox_inches='tight', dpi=fig.dpi)	
		

if __name__ == '__main__':
	main()

