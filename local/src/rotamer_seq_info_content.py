#!/usr/bin/env python
from __future__ import with_statement
import math, re
from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
#from random import shuffle
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader



def main():
	usage = format_usage('''
		%prog COLUMNS FREQ_TAB < STDIN
		computes information content for each site (i.e. multialignment column)
		using symbol observed frequency at site and expected frequency defined as 
		product of AA frequency from FREQ_TAB and rotamer configuration probability.

		REQUIRED COLUMNS (comma-separated):
			1	site_id (numerical, incremental)
			2	symbol
			3	config_p
	''')
	parser = OptionParser(usage=usage)
	
	options, args = parser.parse_args()
	
	if len(args) != 2:
		exit('Unexpected argument number.')
	
		
	site_id_col, symbol_col, config_p_col = [int(x)-1 for x in args[0].split(',')]	

	# Parse FREQ_TAB into dictionary	
	freq_dict = {}
	with file(args[1], 'r') as fd:
		for line in fd:
			AA, expected_freq = safe_rstrip(line).split('\t')
			ffreq = float(expected_freq)
			freq_dict[AA] = ffreq
	
	current_site_id = None
	for line in stdin:
		tokens = safe_rstrip(line).split('\t')
		site_id = int(tokens[site_id_col])
		symbol = tokens[symbol_col]
		config_p = float(tokens[config_p_col])
		
		# create dictionary to store symbols present at site & their config_p
		if current_site_id == None:
			current_site_id = site_id
			site_dict = {}
			site_dict.setdefault(symbol, []).append(config_p)

		# add config_p	to existing list of values or create new dict key and list if not existing
		else:
			if current_site_id == site_id:
				site_dict.setdefault(symbol, []).append(config_p)

		# calculate information content from stored symbol values, configuration p used for prior is the sum of all config ps for that symbol.
		# multiple config ps are present when considering only a subset of chi angles. 	
			elif current_site_id < site_id:
				sum_list = []
				for key in site_dict.keys():
					observed_freq = float(len(site_dict[key]))
					tot_config_p = float(sum(site_dict[key]))
					AA = re.sub('([0-9]+)$', '', key)
					prior = tot_config_p * freq_dict[AA]
					if prior == 0:
						exit('Symbol %s at site %d has prior probability 0.' % (key, current_site_id))
					
					# information content formula from https://en.wikipedia.org/wiki/Self-information
					sum_list.append(observed_freq*math.log(observed_freq/prior))	
				site_info_content = sum(sum_list)
				print '%s\t%f' % (current_site_id, site_info_content)
				current_site_id = site_id
				site_dict = {}
				site_dict.setdefault(symbol, []).append(config_p)
			else:
				exit('Disordered input on column %d at %d, %d' % (site_id_col+1, site_id, current_site_id))
	sum_list = []
	for key in site_dict.keys():
		observed_freq = float(len(site_dict[key]))
		tot_config_p = float(sum(site_dict[key]))
		AA = re.sub('([0-9]+)$', '', key)
		prior = tot_config_p * freq_dict[AA]
		if prior == 0:
			exit('Symbol %s at site %d has prior probability 0.' % (key, current_site_id))
		sum_list.append(observed_freq*math.log(observed_freq/prior))	
	site_info_content = sum(sum_list)
	print '%s\t%f' % (current_site_id, site_info_content)

if __name__ == '__main__':
	main()

