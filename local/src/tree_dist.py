#!/usr/bin/env python	
from __future__ import with_statement

from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
#from random import shuffle
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader

from ete2 import Tree
import scipy.cluster.hierarchy as sch
import scipy.spatial.distance
import numpy as np
from itertools import permutations
from itertools import combinations

import random

def main():
	usage = format_usage('''
		%prog NEWICK_FILE
	''')
	parser = OptionParser(usage=usage)
	
	parser.add_option('-l', '--leaf_names_only', dest='leaf_names_only', action="store_true", default=False, help='only print leaf names')
	parser.add_option('-p', '--pruning_lst', dest='pruning_lst', type='str', default='', help='All leaves not present in the pruning list will be pruned [default: %default]')
	parser.add_option('-a', '--adjoining_leaves', dest='adjoining_leaves', action="store_true", default=False, help='print the correct sequence of leaves to be compared in the adjoining-leaves algorithm')
	options, args = parser.parse_args()
	
	if len(args) != 1:
		exit('Unexpected argument number.')
	
	tree = Tree(args[0])

	if options.pruning_lst != '':
		leaves = options.pruning_lst.split(',')
		tree.prune(leaves, preserve_branch_length=True)
	else:	
		leaves = tree.get_leaf_names()
	
	
	if options.adjoining_leaves:
		distances = {}
        	used_leaves = []
		for l1,l2 in permutations(leaves,r=2):
			d = tree.get_distance(l1,l2)
			distances.setdefault(l1, {})[l2] = d
		
		# pick the first leaf
		orig = l1 = leaves[0]
		# find its closest leaf neighbor
		l2 = sorted(((v,k) for k,v in distances[l1].iteritems()))[0][1]
		# print them
		print '%s\t%s' % (l1, l2)
		used_leaves.extend([l1, l2])		

		for i in range(0, len(leaves)-2):
			# pick the previous leaf
			l1 = l2
			# find its closest leaf neighbor among those leaves used 0 or 1 times excluded the original leaf
			l2 = [x[1] for x in sorted(((v,k) for k,v in distances[l1].iteritems())) if used_leaves.count(x[1]) < 2 and x[1] != orig ][0]
			print '%s\t%s' % (l1, l2)
			used_leaves.extend([l1, l2])

		# last comparison
		l1 = [x for x in used_leaves if used_leaves.count(x) < 2 and x != orig][0]
		l2 = orig
		print '%s\t%s' % (l1, l2) 	
		
	else:
		if options.leaf_names_only:
			for l1,l2 in combinations(leaves,2):
				print '%s\t%s' % (l1, l2)
	
		else:
			for l1,l2 in combinations(leaves,2):
				d = tree.get_distance(l1,l2)
				print '%s\t%s\t%f' % (l1, l2, d)


if __name__ == '__main__':
	main()

