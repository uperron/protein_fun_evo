#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr, stdout
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
#from random import shuffle
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader

import matplotlib
# force matplotlib  not to use Xwi
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker 
import matplotlib.patches as mpatches
import pandas as pd
import numpy as np
import itertools



def main():
	usage = format_usage('''
		%prog <STDIN
	STDIN = states x simulation type frequencies tab
	''')
	parser = OptionParser(usage=usage)
	
	parser.add_option('-l', '--label_yaxis', default=False, help='custom label for y axis [default: %default]')
	options, args = parser.parse_args()
	
	if len(args) != 0:
		exit('Unexpected argument number.')
	
	data = pd.read_csv(stdin, sep="\t", header = 0, index_col = 0).transpose()
	fig = plt.figure(figsize=(5,15))
	ax = fig.add_subplot(111) 
	data.plot.barh(ax = ax)
	ax.set_ylabel('States')
	ax.set_xlabel('Frequencies')
	plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
	ax.set_xticklabels(data.index.values.tolist(), rotation=45, size='xx-small')
	plt.savefig(stdout, format='pdf', bbox_inches='tight', dpi=fig.dpi)

if __name__ == '__main__':
	main()

