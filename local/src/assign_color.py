#!/usr/bin/env python
from __future__ import with_statement

import randomcolor
import itertools
import math

from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
#from random import shuffle
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader



def main():
	usage = format_usage('''
		%prog SYMBOL_NUM COLS< STDIN
		Generates random colors for each symbol in imput.
		If 2 columns are specified symbols are grouped on column 1 and 
		all symbols in group share the same color hue. 
	''')
	parser = OptionParser(usage=usage)
	
	parser.add_option('-r', '--residues', type=str, dest='residues', default='', help='only color these residues [default: %default]', metavar='CUTOFF')

	options, args = parser.parse_args()

	if len(args) != 2:
		exit('Unexpected argument number.')
	
	if options.residues != '':
		residues = options.residues.split(',')
	else:
		residues = None
	cols = args[1].split(',')
	rand_color = randomcolor.RandomColor()
	
	if len(cols)==1:
		#generate random colors for all symbols in stdin
		N = int(args[0])
		colors = rand_color.generate(count=N)
	
	elif len(cols)==2:
		N = args[0].split(',')
		if len(N) != 2:
			exit('Number of symbols in both columns needed')
		hues = ['red', 'orange', 'yellow', 'green', 'blue', 'purple', 'pink', 'monochrome']
		s1_repeats = int(math.ceil(float(N[0])/float(len(hues))))	
		s2_repeats = int(N[1])
		hues_lst = hues*s1_repeats			
		colors = []
		for hue in hues_lst:
			colors.extend(rand_color.generate(hue=hue, count=s2_repeats))
	


	i = 0
	for line in stdin:
		tokens = safe_rstrip(line).split('\t')
		if residues is not None:
			if tokens[int(cols[0])-1] in residues:
				c = colors[i]
				i = i + 1
			else:
				c = '#bec1ce'
			tokens.append(c)
			print '\t'.join(map(str,tokens))
		
		else:				 
			c = colors[i]
			tokens.append(c)
			print '\t'.join(map(str,tokens))	
			i = i + 1
						

if __name__ == '__main__':
	main()

