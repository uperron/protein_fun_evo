#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
import itertools
import operator

def most_common(L):
	# get an iterable of (item, iterable) pairs
	SL = sorted((x, i) for i, x in enumerate(L))
	groups = itertools.groupby(SL, key=operator.itemgetter(0))
	
	# auxiliary function to get "quality" for an item
	def _auxfun(g):
		item, iterable = g
		count = 0
		min_index = len(L)
		for _, where in iterable:
		count += 1
		min_index = min(min_index, where)
		return count, -min_index
	
	# pick the highest-count/earliest item
	return max(groups, key=_auxfun)[0]

def main():
	usage = format_usage('''
		%prog SITES_NUM COLOR_DICT < STDIN
		
		META: STDIN
		1	site_id (numerical)
		2	sequence_id
		3	resname
		4	value
		
		META: RES_DICT
		1	PDB_id
		2	_reflns.d_resolution_high
	''')
	parser = OptionParser(usage=usage)
	parser.add_option('-l', '--legend', dest='legend', action="store_true", default=False, help='plot legend  [default: %default]')
	options, args = parser.parse_args()
	
	if len(args) != 2:
		exit('Unexpected argument number.')
	
	# create a color dictionary from file
	resolution_d_file = args[1]
	resolution_d = {}
	with file(resolution_d_file, 'r') as fd:
		for line in fd:
			PDB_id, resolution = safe_rstrip(line).split('\t')
			resolution_d[PDB_id] = float(resolution)

	sites_num = int(args[0])

	# create a list of symbols present at each site
	symbol_lst = [[] for x in xrange(sites_num)]
	labels = []
	site_lst = []
	Seq_d = {}
	for line in stdin:
                tokens = safe_rstrip(line).split('\t')
                site_id = int(tokens[0])
		resname = tokens[2]
		config = tokens[3]
		symbol = resname + config
	
		if site_id not in site_lst:
			site_lst.append(site_id)
		site_idx = site_lst.index(site_id)		

		if symbol not in labels:
			labels.append(symbol)

	
		# with this option create separate symbol_lst(s) for each CrossRefSeq from all the corresponding sequences.
		CrossRefSeq_id = tokens[1].split('-')[-1]
		PDB_id = tokens[1].split('-')[1]
		resolution = resolution_d.get(PDB_id, default=None)
		trio = tuple(symbol, PDB_id, resolution)
		if CrossRefSeq_id in Seq_d.keys():
			Seq_d[CrossRefSeq_id][site_idx].append(trio)
		else:
			Seq_d[CrossRefSeq_id] = [[] for x in xrange(sites_num)]
			Seq_d[CrossRefSeq_id][site_idx].append(trio)
	
	for CrossRefSeq_id in Seq_d.keys():
		for i,site in enumerate(site_lst):
			best_res = ('', '', 100)
			symbols = []
			for trio in Seq_d[CrossRefSeq_id][i]:
				symbols.append(trio[0]) 
				if trio[2] < best_res[2]:
					best_res = trio
				else:
					continue
			if best_res is not None:
				symbol = best_res[0]
				struct_id = trio[1] + "-" + CrossRefSeq_id
			else:
				symbol = most_common(symbols)
				struct_id = "None" + "-" + CrossRefSeq_id
			print "%d\t%s\t%s" % (site, struct_id, symbol)
			
	
if __name__ == '__main__':
	main()

