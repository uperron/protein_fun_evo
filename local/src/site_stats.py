#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
from uBio import Polypeptide

import numpy as np



def main():
	usage = format_usage('''
		%prog < STDIN
		
		META: STDIN
		1	site_id (numerical)
		2	sequence_id
		3	resname
		4	value
		
	''')
	parser = OptionParser(usage=usage)
	options, args = parser.parse_args()
	
	if len(args) != 0:
		exit('Unexpected argument number.')

	# count residues, configuartion and symbols present at each site
	prev_site = None
	for line in stdin:
                tokens = safe_rstrip(line).split('\t')
                align_site = int(tokens[0])
		resname = tokens[2]
		key = str(align_site) + ":" + str(tokens[1].split('-')[0]) + ":" + str(tokens[1].split('-')[2]) + ":" + resname
		config = tokens[3]
		symbol = resname + config
		if prev_site is None:
			prev_site = align_site
			symbol_lst = []
			res_lst = []
			config_lst = []
			key_lst = []
			symbol_lst.append(symbol)
			res_lst.append(resname)
			config_lst.append(config)
			key_lst.append(key)
		elif prev_site == align_site: 
			symbol_lst.append(symbol)
			res_lst.append(resname)
			config_lst.append(config)
			key_lst.append(key)
		elif prev_site < align_site:
			symbol_count = len(set(symbol_lst))
			res_count = len(set(res_lst))
			config_count = len(set(config_lst))
			key_count = len(set(key_lst))
			output = [prev_site, key_count, symbol_count, res_count, config_count, ";".join(map(str, set(symbol_lst))), ";".join(map(str, set(key_lst)))]	
			print("\t".join(map(str, output)))	
			
			prev_site = align_site
			symbol_lst = []
			res_lst = []
			config_lst = []
			key_lst = []
			symbol_lst.append(symbol)
			res_lst.append(resname)
			config_lst.append(config)
			key_lst.append(key)
		else:
			exit('stdin not sorted on column 1')	
			
	
if __name__ == '__main__':
	main()

