#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr, stdout
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage

import ete2
import urllib,urllib2
import pandas as pd
import re

url = 'http://www.uniprot.org/uploadlists/'

def main():
	usage = format_usage('''
		%prog NEWICK_TREE < PDB_2_UNIPROT_ID_TABLE
	''')
	parser = OptionParser(usage=usage)
	
	parser.add_option('-d', '--dict_file', default=False, help='dump Uniprot dataframe')

	options, args = parser.parse_args()
	
	if len(args) != 1:
		exit('Unexpected argument number.')


	tree = ete2.Tree(args[0])
	
	df = pd.read_csv(stdin, sep="\t", header=None, index_col=0, names=['PDB_ID', 'Uniprot_ID'])

	for node in tree.traverse():
		# internal nodes have no names in PFAM trees
		if node.is_leaf():
			orig_PDB_name = node.name
			new_Uniprot_name = df.loc[orig_PDB_name].values[0]
			node.add_feature('name', new_Uniprot_name)
	print tree.write(format=1)	


if __name__ == '__main__':
	main()

