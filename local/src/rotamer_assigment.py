#!/usr/bin/env python
from __future__ import with_statement
from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
import numpy as np
#from random import shuffle
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader

# calculates angle distance of two numpy arrays.
def angle_dist(array1, array2):
	diff = sum(np.abs(array1-array2))
	if diff > 180:
		return np.abs(diff - 360)
	else:
		return diff

# returns a dictionary, keys are: resname;phi_bin_index;psi_bin_index, values are lists of tuples of lists: ([chi1_val, chi2_val, ...], [chi1_config, chi2_config, ... ] config_p).
def parse_library(filename):
	configs_dict = {} 	
	index_lst = range(-180, 190, 10)
	index_dict = {}
	for index, item in enumerate(index_lst):
		index_dict[item] = index
	with file(filename, 'r') as fd:
		for line in fd:
			tokens = safe_rstrip(line).split('\t')
			resname, lower_phi, lower_psi, chi1_config, chi2_config, ch31_config, chi4_config = tokens[:7]
			chi_config = [chi1_config, chi2_config, ch31_config, chi4_config]
			config_p = float(tokens[7])
			chi_val = np.array([float(x) for x in tokens[8:]])
			chi = (chi_val, chi_config, config_p)	
			index_phi = index_dict[int(lower_phi)]
			index_psi = index_dict[int(lower_psi)]
			ids = ";".join(map(str, [resname, index_phi, index_psi]))
			configs_dict.setdefault(ids, []).append(chi)
	return configs_dict	
# returns the tuple that: 1) corresponds to key resname;phi_bin_index;psi_bin_index 2) is closest (angle distance) to provided chi_lst values.
# the tuple is composed of a comma-separated list of configurations for available requested rotamers and a probability.
def get_config(resname, indexes, chi_lst, configs_dict):
	ids = ";".join(map(str, [resname, indexes]))
	d_min = 181
	for tpl in configs_dict[ids]:
		chi_vals = tpl[0]
		dist = angle_dist(chi_lst, chi_vals[:len(chi_lst)])
		if dist < d_min:
			d_min = dist
			closer = tpl
	configs = ",".join(map(str, closer[1][:len(chi_lst)]))
	p = closer[2]
	return [configs, p]

def main():
	usage = format_usage('''
		%prog COLUMNS LIBRARY < MATRIX
		given a rotamer library and a rotamer matrix assigns rotamer configuration and probability
		by binning phi,psi and choosing the closest chi_val.
 
		REQUIRED COLUMNS (comma-separated):
		resname1,resname2,chi_list1,chi_list2,phi_psi1,phi_psi2
		
		.META: LIBRARY
			1	resname
			2	lower_phi
			3	lower_psi
			4	chi1_config
			5	chi2_config
			6	chi3_config
			7	chi4_config
			8	config_p
			9	chi1_val
			10	chi2_val
			11	chi3_val
			12	chi4_val
	''')
	parser = OptionParser(usage=usage)
	
	parser.add_option('-r', '--rotamers', type=int, dest='rotamers', default='1', help='max rotamer to be assigned i.e. -r 3 will assign chi1, chi2 and chi3 [default: %default]', metavar='ROTAMERS')
	parser.add_option('-n', '--non_pairwise', dest='non_pairwise', action="store_true", default=False, help='if True will accept non-pairwise input [default: %default]')

	options, args = parser.parse_args()
	
	if len(args) != 2:
		exit('Unexpected argument number.')
	
	if options.non_pairwise:
		col_resname1,col_chi_list1,col_phi_psi1 = [int(x)-1 for x in args[0].split(',')]
	else:
		col_resname1,col_resname2,col_chi_list1,col_chi_list2,col_phi_psi1,col_phi_psi2 = [int(x)-1 for x in args[0].split(',')]
	
	library = args[1]
	rotamers = int(options.rotamers)
	bins = np.array(range(-180, 180, 10))	
	configs_dict = parse_library(library)
	for line in stdin:
		chi_lst1 = []
		chi_lst2 = []
		config1 = ['clean']
		config2 = ['clean']
		tokens = safe_rstrip(line).split('\t')
		phi_psi = []
		if options.non_pairwise:
			phi_psi.extend(tokens[col_phi_psi1].split(','))
			chi_lst1.extend(tokens[col_chi_list1].split(','))
			resname1 = tokens[col_resname1]
		else:
			for element in tokens[col_phi_psi1:col_phi_psi2+1]:
				phi_psi.extend(element.split(','))
			chi_lst1.extend(tokens[col_chi_list1].split(','))
			chi_lst2.extend(tokens[col_chi_list2].split(','))
			resname1 = tokens[col_resname1]
			resname2 = tokens[col_resname2]
		fchi_lst1 = []
		fchi_lst2 = []
		for element in chi_lst1[:rotamers]:
			if element != 'NA' and element != 'Disordered':
				fchi_lst1.append(float(element))		
			else:
				break
		if len(fchi_lst1) == 0:
			config1 = ['NA', 'NA']

		if not options.non_pairwise:
			for element in chi_lst2[:rotamers]:
				if element != 'NA' and element != 'Disordered':
					fchi_lst2.append(float(element))		
				else:
					break
			if len(fchi_lst2) == 0:
				config2 = ['NA', 'NA']

			if 'None' in phi_psi[:2] and 'None' not in phi_psi[2:]:
				inds = np.digitize(np.array([float(x) for x in phi_psi[2:]]), bins).tolist()
				index2 = ";".join(map(str, inds))
				if config2 == ['clean']:
					config2 = list(get_config(resname2, index2, fchi_lst2, configs_dict))
				config1 = ['NA', 'NA']
			elif 'None' not in phi_psi[:2] and 'None' in phi_psi[2:]:
				inds = np.digitize(np.array([float(x) for x in phi_psi[:2]]), bins).tolist()
				index1 = ";".join(map(str, inds))
				if config1 == ['clean']:
					config1 = list(get_config(resname1, index1, fchi_lst1, configs_dict))
				config2 = ['NA', 'NA']
			elif 'None' in phi_psi[:2] and 'None' in phi_psi[2:]:
				config1 = ['NA', 'NA']
				config2 = ['NA', 'NA']
			else:
				inds = np.digitize(np.array([float(x) for x in phi_psi]), bins).tolist()
				index1 = ";".join(map(str, inds[:2]))
				index2 = ";".join(map(str, inds[2:]))
				if config1 == ['clean']:
					config1 = list(get_config(resname1, index1, fchi_lst1, configs_dict))
				if config2 == ['clean']:
					config2 = list(get_config(resname2, index2, fchi_lst2, configs_dict))
			output = tokens + config1 + config2

		elif options.non_pairwise:
			if 'None' in phi_psi:
				config1 = ['NA', 'NA']
			elif 'None' not in phi_psi:
				inds = np.digitize(np.array([float(x) for x in phi_psi]), bins).tolist()
				index1 = ";".join(map(str, inds))
				if config1 == ['clean']:
					config1 = list(get_config(resname1, index1, fchi_lst1, configs_dict))
			output = tokens + config1
		
		print("\t".join(map(str, output)))	

if __name__ == '__main__':
	main()

