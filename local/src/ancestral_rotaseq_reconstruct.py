#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr, stdout
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage

import ete2
import urllib,urllib2
import pandas as pd
import re

url = "https://www.uniprot.org/uniprot/"

def main():
	usage = format_usage('''
		%prog NEWICK_TREE PHYLIP_ROTA_ALIGN 55x55_IRM FREQ_FILE
	''')
	parser = OptionParser(usage=usage)
	
	parser.add_option('-d', '--dump_dataframe', action="store_true", help='dump Uniprot dataframe')

	options, args = parser.parse_args()
	
	if len(args) != 1:
		exit('Unexpected argument number.')
	
	tree = ete2.Tree(args[0])

	
	# read alignment
	# read matrix
	# read freqs
	
	# a taxon obj
	class NODE(i, Cx(i), Lx(i)):
		#Lx(i)is the likelihood of the best reconstruction of this subtree on the condition that the father
		#of node x is assigned character state i.
		# Cx(i) is the character state assigned to node x in this optimal conditional reconstruction.
		__slots__ = ('Lx', 'Cx')
		def __init__(self, i):
			self.i = i
			self.l = []
	# iter sites
	#for site in range(align_len):
		#iter leaves
		#for leaf in leaf_node_iter():
			# perform sep 1
		#each node visited after its children, exclude root
		#for node in postorder_internal_node_iter()
			#if node._parent_node is None:
				#root = node
			#else:
				# step 2
		#visit root, assign state

		# assign states down the tree
		#for node in [node for node in preorder_node_iter() if node is not root]:
	
	print tree.write(format=1)	


if __name__ == '__main__':
	main()

