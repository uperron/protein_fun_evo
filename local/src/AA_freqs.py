#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
from Bio import AlignIO, SeqIO
from Bio.Align import MultipleSeqAlignment
from uBio import Polypeptide
import re
#from random import shuffle
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader

standard_aa_names = ["ALA", "CYS", "ASP", "GLU", "PHE", "GLY", "HIS", "ILE", "LYS",
                     "LEU", "MET", "ASN", "PRO", "GLN", "ARG", "SER", "THR", "VAL",
                     "TRP", "TYR"]

def main():
	usage = format_usage('''
		%prog ALIGNMENT
		computes aminoacid frequencies in a .fasta multialignment after removing all gaps.
	''')
	parser = OptionParser(usage=usage)

	options, args = parser.parse_args()
	
	if len(args) != 1:
		exit('Unexpected argument number.')
	
	occurrence_dict = {}
	align_filename = args[0]
	tot_sites = 0.
	multialig = AlignIO.parse(align_filename, "fasta")
	for alignment in multialig:
		for record in alignment:
			no_gaps_seq = re.sub('-', '', str(record.seq))
			seq_length = len(no_gaps_seq)
			tot_sites = tot_sites + seq_length
			for aaa in standard_aa_names:
				a = Polypeptide.three_to_one(aaa)
				a_occurrence = float(no_gaps_seq.count(a))
				occurrence_dict.setdefault(aaa, []).append(a_occurrence)
	for aaa in occurrence_dict.keys():
		aaa_freq = sum(occurrence_dict[aaa])/tot_sites
		print '%s\t%f' % (aaa, aaa_freq)
	

if __name__ == '__main__':
	main()

