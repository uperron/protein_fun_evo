#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr, stdout
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
#from random import shuffle
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader

import matplotlib
# force matplotlib  not to use Xwi
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.ticker as ticker 
import matplotlib.colors as colors

import pandas as pd
import numpy as np
import itertools

def heatmap(data, row_labels, col_labels, ax=None,
	cbar_kw={}, cbarlabel="", **kwargs):
	"""
	Create a heatmap from a numpy array and two lists of labels.
	
	Arguments:
	data       : A 2D numpy array of shape (N,M)
	row_labels : A list or array of length N with the labels
	for the rows
	col_labels : A list or array of length M with the labels
	for the columns
	Optional arguments:
	ax         : A matplotlib.axes.Axes instance to which the heatmap
	is plotted. If not provided, use current axes or
	create a new one.
	cbar_kw    : A dictionary with arguments to
	:meth:`matplotlib.Figure.colorbar`.
	cbarlabel  : The label for the colorbar
	All other arguments are directly passed on to the imshow call.
	"""
	
	fig, ax = plt.subplots(figsize=(20,15))

	# Plot the heatmap
	im = ax.imshow(data, interpolation=None, norm=colors.LogNorm(vmin=data.min(), vmax=data.max()),
		aspect='auto',  **kwargs)
	
	# Create colorbar
	axcolor = fig.add_axes([0.91,0.1,0.01,0.8])
	cbar = fig.colorbar(im, cax=axcolor)
	cbar.ax.tick_params(labelsize='xx-large')
	

	labels = col_labels
	collapsed_labels = [''.join([i for i in s if not i.isdigit()]) for s in labels]
	uniq_collapsed_labels = []
	[uniq_collapsed_labels.append(i) for i in collapsed_labels if not uniq_collapsed_labels.count(i)]
	config_labels = [''.join(i for i in l if i.isdigit()) if l not in ["ALA", "GLY"] else "" for l in labels]

	grid_idxs = set([collapsed_labels.index(s) for s in collapsed_labels
		if s != collapsed_labels[collapsed_labels.index(s) -1]])
	grid_idxs = sorted([x for x in grid_idxs])
	
	# position major ticks over the corresponding block of minor ticks 
	major_ticks = [x - (x - grid_idxs[grid_idxs.index(x) -1]) / 2. for x in grid_idxs[1:]] + [len(labels)  - (float(len(labels)) - grid_idxs[len(grid_idxs)-1]) / 2]
	 
	# setup major ticks for residues
	ax.set_xticks(major_ticks)
	ax.set_yticks(major_ticks)

	ax.set_xticks(np.arange(data.shape[1])+.12, minor=True)
	ax.set_yticks(np.arange(data.shape[0])+.12, minor=True)

	# ... and label them with the respective list entries.
	ax.set_xticklabels(uniq_collapsed_labels)
	ax.set_xticklabels(config_labels, minor=True)
	ax.set_yticklabels(uniq_collapsed_labels)
	ax.set_yticklabels(config_labels, minor=True)

	# Let the horizontal axes labeling appear on top.
	ax.tick_params(top=False, bottom=False,
	labeltop=True, labelbottom=False, which="both")

	# stagger major and minor labels
	ax.tick_params(direction='out', pad=15, which="major")
	ax.tick_params(direction='out', pad=5, which="minor")

	# Rotate the tick labels and set their alignment.
	plt.setp(ax.get_xticklabels(which='major'), rotation=-30, ha="right",
	rotation_mode="anchor")
	plt.setp(ax.get_yticklabels(which='major'), rotation=30, ha="right",
			        rotation_mode="anchor")

	# vertically position major labels above
	#for l in ax.get_xticklabels(which='major'):
	#	l.set_y(.1)
	# or shift them to the left
	#for l in ax.get_yticklabels(which='major'):
	#	l.set_x(-.05)

	# Turn spines off and create white grid.
	for edge, spine in ax.spines.items():
		spine.set_visible(False)
	
	ax.tick_params(which="both", bottom=False, left=False)
		
	
	for xmaj in grid_idxs:
		ax.axvline(x=xmaj-.5, ls='-', linewidth=3, color='k')
	
	for ymaj in grid_idxs:
		ax.axhline(y=ymaj-.5, ls='-', linewidth=3, color='k')
	
	
	return fig, im, cbar
	
def main():
	usage = format_usage('''
	%prog <(IRM)	
			''')
	parser = OptionParser(usage=usage)
	parser.add_option('-s', '--subgrid', action="store_true", default=False, help='plot additional subgrid for 20+ states matrix default: %default]')
	
	options, args = parser.parse_args()
	
	if len(args) != 0:
		exit('Unexpected argument number.')


	matrix_df = pd.read_csv(stdin, sep="\t", header=0, index_col=0)
	matrix = np.where(matrix_df.values==0., 1, matrix_df.values)
	labels = list(matrix_df.index.values)
	
	matrix_pseudo_df = pd.DataFrame(matrix, columns=labels, index=labels)
	matrix = matrix_pseudo_df.values
	
	fig, im, cbar = heatmap(matrix, labels, labels, cmap="YlGnBu", cbarlabel="")
	plt.savefig(stdout, format='pdf', bbox_inches='tight', dpi=fig.dpi)
	plt.close(fig)

if __name__ == '__main__':
	main()

