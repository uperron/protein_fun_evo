#!/usr/bin/env python
from __future__ import with_statement
import re
import os
import warnings
from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
from Bio import SeqIO
from StringIO import StringIO
from uBio import MMCIFParser, Polypeptide, StructureAlignment
from Bio import AlignIO
from Bio.Align import MultipleSeqAlignment
from collections import OrderedDict
from Bio.Data import SCOPData

import warnings

#from random import shuffle
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader

# takes a multialignment filename and searches for a string, returns a matching Seq object found in multialignment
def search_4_poly(align_filename, search_str):
	multialig = AlignIO.parse(align_filename, "fasta")
	for alignment in multialig:
		for record in alignment:
			if re.search(search_str, record.id):
				return record


def get_poly(struct_filename, align_filename, phi_psi, tau, theta, PDBmap, PDBmap_dict):
	## takes a .cif filename and a multialignment filename, returns a dictionary of tuples containing a Polypeptide object and a Sequence object along with angle lists if requested
	ppb = Polypeptide.PPBuilder()
	bparser = MMCIFParser.MMCIFParser()	
	structure_id = os.path.split(struct_filename)[1].replace('.cif', '') 
	try:
		structure = bparser.get_structure(structure_id, struct_filename)
	except:
		warnings.warn("Error in while parsing %s, skipping..." % (struct_filename))
		return None
	aligned_poly = {}
	for model in structure:
		model_id = str(model.get_id())
		for chain in model:
			chain_id = str(chain.get_id())
			# if dealing with Pfam allignments, limit polypeptides to the range provided by the Pfam mapping file.
			if PDBmap is not None:
				res_lst = []
				k = structure_id + chain_id
				if k in PDBmap_dict:
					for duo in PDBmap_dict[k]:
						res_dict = {}
						for residue in chain:
							resnum = int(residue.get_pdb_seq_num())
							res_dict[resnum] = residue
						for resnum in OrderedDict(sorted(res_dict.items(), key=lambda t: t[0])).keys():
							if resnum >= duo[0] and resnum <= duo[1]:
								residue = res_dict[resnum]
								res_lst.append(residue)
							else:
								continue
						if res_lst != []:
							entity = res_lst
						else:
							continue
						entity = res_lst
						for pp in ppb.build_peptides(entity, aa_only=False):
							if phi_psi:
								phi_psi_lst = pp.get_phi_psi_list()
							else:
								phi_psi_lst = None
							if tau:
								tau_lst = pp.get_tau_list()
							else:
								tau_lst = None
							if theta:
								theta_lst = pp.get_theta_list()
							else:
								theta_lst = None
							pp_id = str(pp).replace(" ", "_")
							search_str = '-'.join([structure_id, model_id, chain_id, pp_id])
							record = search_4_poly(align_filename, search_str)
							if record is None:
								warnings.warn('%s not found in %s' %(search_str, align_filename))
							else:
								aligned_poly[record.id]=(pp, record, phi_psi_lst, tau_lst, theta_lst)
		
				else:
					continue

			else:
				entity = chain

				for pp in ppb.build_peptides(entity, aa_only=False):
					if phi_psi:
						phi_psi_lst = pp.get_phi_psi_list()
					else:
						phi_psi_lst = None
					if tau:
						tau_lst = pp.get_tau_list()
					else:
						tau_lst = None
					if theta:
						theta_lst = pp.get_theta_list()
					else:
						theta_lst = None
					pp_id = str(pp).replace(" ", "_")
					search_str = '-'.join([structure_id, model_id, chain_id, pp_id])
					record = search_4_poly(align_filename, search_str)
					if record is None:
						warnings.warn('%s not found in %s' %(search_str, align_filename))
					else:
						aligned_poly[record.id]=(pp, record, phi_psi_lst, tau_lst, theta_lst)
			return aligned_poly

def main():
	usage = format_usage('''
		%prog ALIGMENT < STRUCTURE1 STRUCTURE2 [...]
		Given a multi .fasta alignment and a space-separated .cif files in stdin
		maps residues in polypetides to sites in corresponding aligned sequence 
		and outputs requested angle values.
	''')
	parser = OptionParser(usage=usage)
	
	parser.add_option('-p', '--phi_psi', dest='phi_psi', action='store_true', default=False, help='calculate phi and psi angles [default: %default]', metavar='PHI_PSI')
	parser.add_option('-t', '--tau', dest='tau', action='store_true', default=False, help='calculate tau angles [default: %default]', metavar='TAU')
	parser.add_option('-T', '--Theta', dest='Theta', action='store_true', default=False, help='calculate theta angles [default: %default]', metavar='THETA')
	parser.add_option('-P', '--PDBmap', type=str, dest='PDBmap', default=None, help='pdb residue number map [default: %default]')
	parser.add_option('-b', '--bfactor', dest='bfactor', default=False, action="store_true", help='print mean bfactor for residue [default: %default]')
	## option to filter on disordered res, unbonded res
	## option to calculate residue depth
	parser.add_option('-v', '--verbose', dest='verbose', action='store_true', default=False, help='some help [default: %default]')

	options, args = parser.parse_args()
	
	if len(args) < 1:
		exit('Unexpected argument number.')

	filenames = []
	for line in stdin:
		filenames.extend(line.split())
	if len(filenames) < 1:
		exit('At lest one .cof file needed')
	multialignment_fasta = args[0]

	# create a dictionary accessed via PDB_id and chain, values are tuples of residue coordinates for the Pfam domain
	PDBmap_dict = {}
	if options.PDBmap is not None:
		with file(options.PDBmap, 'r') as fd:
			for line in fd:
				tokens = safe_rstrip(line).split('\t')
				key = tokens[0] + tokens[1]
				start, stop = int(tokens[2]), int(tokens[3])
				PDBmap_dict.setdefault(key, []).append((start, stop))


	## map residues from each polypeptide sequence in each structure to the corresponding sequence in an existing multiple sequence alignment (.fasta)
	for i in range(len(filenames)):
		filename1 = filenames[i]
		sequences1=get_poly(filename1, multialignment_fasta, options.phi_psi, options.tau, options.Theta, options.PDBmap, PDBmap_dict)
		if sequences1 == None:
			continue
		for seq1 in sequences1.keys():
			sliced_align = sequences1[seq1][1]
			try:
				structural_alignment = StructureAlignment.SingleAlignment(sliced_align, sequences1[seq1][0])	
			except:
				warnings.warn("Error in StructureAlignment for sequence %s, skipping..." % (sequences1[seq1][0]))	
				continue	
			else:
				for duo in structural_alignment.get_iterator():
					if duo[0] is not None:
						structure_id1 = os.path.split(filename1)[1].strip('.cif')
						res1 = duo[0]
						# this site id is relative to site position in the multialignment
						align_site_id = duo[2]
						PDB_site_id = res1.get_pdb_seq_num()
						resname1 = res1.get_resname()
						# obtain list of chi angles from current Residue objects
						chi1 = ",".join(map(str, res1.get_chi()))

						# obtain list of mean bfactor for atoms in each rotamer
						if options.bfactor: 
							bfactor = res1.get_mean_bfactor()
						opt_output = []
						## obtain other lists of angles from current Polypeptide object
						if options.phi_psi:
							current_p1 = duo[1]
							phi_psi1 = sequences1[seq1][2][current_p1]
							opt_output.append("\t".join(map(str, [",".join(map(str, phi_psi1))])))
						if options.tau:
							opt_output.append(";".join(map(str, [",".join(map(str, sequences1[seq1][3]))])))
						if options.Theta:
							opt_output.append(";".join(map(str, [",".join(map(str, sequences1[seq1][4]))])))
						if options.bfactor:
							opt_output.append(";".join(map(str, [",".join(map(str, bfactor))])))
						if opt_output != []:
							output = [seq1, structure_id1, align_site_id, resname1, chi1] + opt_output + [PDB_site_id]
							print("\t".join(map(str, output)))
						else:
							output = [seq1, structure_id1, align_site_id,  resname1, chi1, PDB_site_id]
							print("\t".join(map(str, output)))


if __name__ == '__main__':
	main()

