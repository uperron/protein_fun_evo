#!/usr/bin/env python	
from __future__ import with_statement

from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
#from random import shuffle
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader

import re

def main():
	usage = format_usage('''
		%prog SCALING_FACTOR_1 [...]  < NEWICK_FILE 
		
		scale branch lengths according to a a given factor leaving tree topology unchanges
	''')
	parser = OptionParser(usage=usage)
	options, args = parser.parse_args()
	

	if len(args) != 1:
		exit('Unexpected argument number.')

	scale = float(args[0])

	for line in stdin:
		tree_string = safe_rstrip(line)
		# assumes branch lengths to be floats
		distances = re.findall(r'\d+\.\d+', tree_string)
		for distance in distances:
			scaled_dist = str(float(distance)*scale)
			tree_string = tree_string.replace(distance, scaled_dist)
		print tree_string

if __name__ == '__main__':
	main()

