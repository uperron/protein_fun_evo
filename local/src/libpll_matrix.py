#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
#from random import shuffle
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader

def main():
	usage = format_usage('''
		%prog < INPUT
		print the exchange rates in the appropriate format for libpll
		INPUT must be a comma-separated list of exchangeabilities, those are taken from the lower triangular matrix and sorted column-wise.
	''')
	parser = OptionParser(usage=usage)
	options, args = parser.parse_args()
	
	if len(args) != 0:
		exit('Unexpected argument number.')
	
	# read the array
	for line in stdin:
		rates_array = line.rstrip("\n").split(",") 
	
	# print the rates in the appopriate format
	print "const double 55x55_rates[%d] =" % (len(rates_array))
	print " {"
	for j in range(0,len(rates_array),25):
		print "   " + str(rates_array[j:j+25]).strip("[").strip("]") + ","
	print " };"

if __name__ == '__main__':
	main()

