#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr, stdout
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
#from random import shuffle
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader

import matplotlib
# force matplotlib  not to use Xwi
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.ticker as ticker 
import matplotlib.patches as mpatches
import pandas as pd
import numpy as np
import itertools


def main():
	usage = format_usage('''
		%prog MODEL1_LABEL,[...]  MODEL1,MODEL2,[...] TREE_SIZE1,TREE_SIZE2,[...] <STDIN
	STDIN = LABELS\tMODELS\tTREE_SIZES
	One plot per tree size comparing all models
	''')
	parser = OptionParser(usage=usage)
	parser.add_option('-y', '--ylabel', type="str", default=False, help='Custom y-axis label [default: %default]')
	options, args = parser.parse_args()
	
	if len(args) != 3:
		exit('Unexpected argument number.')

	labels = args[0].split(",") #i.e. column names
	models = args[1].split(",")
	tree_sizes = args[2].split(",")
	model_num = float(len(models))
	colors =  [cm.YlGnBu(i/model_num,1) for i in np.arange(model_num)]
	
	fig, axs = plt.subplots(len(tree_sizes),1, figsize=(10,10), sharex=True)
	fig.subplots_adjust(hspace = .5, wspace=.001)

	names = ['scale'] + labels
	data = pd.read_csv(stdin, sep="\t", header=None, names=names).dropna()
	data.astype(np.float)

	x = data['scale'].drop_duplicates().values

	# one subplot per tree size
	for ax, tree_size in zip(axs.ravel(), tree_sizes):
		
		patches = []
		displacement = -.03 
		#get the corresponding columns (one per model) & group by scale
		fltr_gp_df = data.filter(regex=tree_size + '|scale').groupby('scale', sort=False)
		for label, color in zip([l for l in labels if 'TX' + tree_size in l], colors):
			# one boxplot per model per scaling factor, 
			# models are arranged around actual x coord in order not to superimpose 
			adjusted_x = [v + displacement for v in x]
			displacement = displacement + .06
			y = []
			for scale in x:
				y.append(fltr_gp_df.get_group(scale)[label].values)
			np.array(y)
			bp = ax.boxplot(y, patch_artist=True, widths=.04, positions=adjusted_x)
			patches.append(mpatches.Patch(color=color, label=label))
			for box in bp['boxes']:
				# change outline color
				box.set( color='#000000', linewidth=0.01)
				# change fill color
				box.set( facecolor = color)
				## change color and linewidth of the whiskers
				for whisker in bp['whiskers']:
					whisker.set(color='#000000', linewidth=0.9)
				## change color and linewidth of the caps
				for cap in bp['caps']:
					cap.set(color='#000000', linewidth=0.9)
				## change color and linewidth of the medians
				for median in bp['medians']:
					median.set(color='#000000', linewidth=0.05)
				## change the style of fliers and their fill
				for flier in bp['fliers']:
					flier.set(marker='.', color=color)
		ax.set_xticklabels(x, rotation=45, size='small')	
		#plt.xlabel('Scaling factor')
		#plt.ylabel('Unweighted RF distance')
		majorLocator = ticker.FixedLocator(x)
		ax.xaxis.set_major_locator(majorLocator)
		ax.set_title(str(tree_size) + ' taxa', loc = 'center')
		ax.autoscale()
	fig.text(0.5, 0.04, 'Scaling factor', ha='center', va='center', size='x-large')
	if options.ylabel:
		ylab = options.ylabel
	else: 
		ylab = 'Unweighted RF distance'
	fig.text(0.06, 0.5, ylab, ha='center', va='center', rotation='vertical', size='x-large')

	plt.figlegend(patches, models, loc = 'upper center', ncol=2)
	plt.savefig(stdout, format='pdf', bbox_inches='tight', dpi=fig.dpi)

if __name__ == '__main__':
	main()
