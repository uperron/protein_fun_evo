#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
#from random import shuffle
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader

import re
import pandas as pd

def main():
	usage = format_usage('''
		%prog < PHYLIP_MULTIALIGN 
	''')
	parser = OptionParser(usage=usage)
	
	parser.add_option('-I', '--ID_uniq', action='store_true', default=False, help='print only one sequence per Uniprot id (the longest)')
	options, args = parser.parse_args()
	
	if len(args) != 0:
		exit('Unexpected argument number.')
			
	# skip the PHYLIP header
	df = pd.read_csv(stdin, sep="\t", header=None, index_col=None, names=["ID", "Sequence"], skiprows=1)
	# remove duplicated sequences
	df.drop_duplicates(subset=['Sequence'], inplace=True)
	if options.ID_uniq:
		# remove sequences from the same uniprot id (i.e. multiple domains in the same protein)
		# keep the longest (the one with less gaps)
		df["len"] = df['Sequence'].str.replace("- ", "").str.replace(" ", "").str.len()
		if len(df["ID"].values[1].split('/', 1)) == 2:
			df['Uniprot'], df['res_range'] = df["ID"].str.split('/', 1).str
		else:
			df['Uniprot'] = df["ID"]
		df.sort_values(['len', 'Uniprot'], ascending=[False, False], inplace=True)
		df.drop_duplicates(subset=['Uniprot'], inplace=True)
		print "%d\t%d" % (len(df), df['Sequence'].str.replace(" ", "").str.len().values[0])
	else:
		print "%d\t%d" % (len(df), df['Sequence'].str.replace(" ", "").str.len().values[0])
	print
	print df.to_csv(header=False, index=False, sep="\t", columns=["ID", "Sequence"])


if __name__ == '__main__':
	main()


