#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr

import sys
import PDB
import subprocess
import re
import os
import shutil


tempdir = 'pycharmm.tmp/'
if not os.path.exists(tempdir):
	os.makedirs(tempdir)



head = """* EEF
*
bomlev -1

OPEN READ FORM UNIT 1 NAME "/nfs/research/goldman/umberto/bioinfotree/prj/protein_fun_evo/local/src/top_all36_prot.ZN.rtf"
! OPEN READ FORM UNIT 1 NAME  "/nfs/research/goldman/umberto/bioinfotree/local/bin/charmm/test/data/top_all36_prot.rtf"
READ RTF CARD UNIT 1
CLOSE UNIT 1

OPEN READ FORM UNIT 2  NAME "/nfs/research/goldman/umberto/bioinfotree/prj/protein_fun_evo/local/src/par_all36_prot.ZN.prm"
! OPEN READ FORM UNIT 2  NAME "/nfs/research/goldman/umberto/bioinfotree/local/bin/charmm/test/data/par_all36_prot.prm"
READ PARAMETER CARD  UNIT 2
CLOSE UNIT 2

"""

stream = "/nfs/research/goldman/umberto/bioinfotree/local/bin/charmm/test/data/toppar_water_ions.str"


ssb = """
patch DISU @seg1 @nr1 @seg2 @nr2
"""

#read a protein, requires @NAME, @SEGNAME and @OFFSET
read_prot = """
open read unit 20 card name "@NAME"
read sequ pdb unit 20
gener @SEGNAME setup
rewind unit 20
read coor pdb unit 20 offset @OFFSET
close unit 20
"""

ic = """
ic purge   ! CLEANUP IC TABLE
IC PARA
IC FILL PRESERVE
IC BUILD
hbuild sele all end

update ctonnb 7. ctofnb 9. cutnb 10. group rdie switch

"""

fix_HET = """
DEFINE CONSHET sele segid @RESSEG .and. atom * * @ANAME END
! CONS HARM FORCE 2.0 MASS SELE CONSHET END

"""

# apply harmonic constrain to residue atoms (side chain and backbone)
fix = """
DEFINE CONSRESIDUE sele segid @RESSEG .and. resid @RESNO end
! CONS HARM FORCE 2.0 MASS SELE CONSRESIDUE END
CONS FIX SELE CONSRESIDUE END
"""

# minimize protein structure
mini = """
mini sd nstep 50
mini conj  nstep 100
mini abnr nstep 200

"""

# calculate interaction energy for residue side chain vs all other protein atoms
calc_ene = """
DEFINE MYRESIDUE sele segid @RESSEG .and. resid @RESNO .and. .not. ( type N .or. type CA .or. type C .or. type O .or. type H) end
inte sele MYRESIDUE end sele .not. MYRESIDUE end

"""

tail = """
open writ unit 20 card name "@OUT"
writ coor pdb unit 20

"""


def write_segment_PDB(chain,segm,filename):
        outfile=open(filename,'w')
        rescounter = 0
        for resi in chain.residues[segm[0]:segm[1]]:
                rescounter += 1
		r_name = resi.r_name.replace('HIS', 'HSE')
                for atm in resi.atoms:
                        outfile.write(('ATOM   %s  %s%s%s %s%s    %8.3f%8.3f%8.3f\n'%(str(atm.a_number).rjust(4),str(atm.a_name).ljust(3),str(atm.a_marker),r_name,chain.chain_id,str(rescounter).rjust(4)[:4],atm.x,atm.y,atm.z)).replace('CD1 ILE','CD  ILE'))
#                outfile.write('TER\n')
        outfile.close()

def write_hetatms_PDB(hetatms, filename):
	outfile=open(filename,'w')
	for atm in hetatms:
		r_name = atm.r_name.replace('ZN', 'ZN2')
		outfile.write(('ATOM   %s  %s%s%s %s%s    %8.3f%8.3f%8.3f\n'%(str(atm.a_number).rjust(4),str(atm.a_name).ljust(3),str(atm.a_marker),str(r_name),str(atm.chain_ref),str(atm.r_number).rjust(4)[:4],atm.x,atm.y,atm.z)))

def write_charmm_head_input(sname):
	charmm_inputfile = open(tempdir + sname + 'charmm.in','w')
	charmm_inputfile.write(head)
	charmm_inputfile.close()

def write_charmm_min_input(sname, chain,segments,ss_bonds):
	offset = 0
	charmm_inputfile = open(tempdir + sname + 'charmm.in','a')
	for i,x in enumerate(segments):
		fname = tempdir + sname + chain.chain_id + '.seg%i.pdb'%(i)
		segname = chain.chain_id + 'se%i'%i
		offset += x[1]-x[0]
		if fname in os.listdir('./'):
			continue
		else:
			write_segment_PDB(chain,x,fname)
			charmm_inputfile.write(read_prot.replace('@OFFSET',str(offset)).replace('@NAME',fname).replace('@SEGNAME',segname))
	charmm_inputfile.close()
	

def write_charmm_het_input(sname):
	fname = tempdir + sname + 'HET.pdb'
	segname = 'HET'
	charmm_inputfile = open(tempdir + sname + 'charmm.in','a')
	charmm_inputfile.write(read_prot.replace('@OFFSET',str(0)).replace('@NAME',fname).replace('@SEGNAME',segname))

#	nrint segments,ss_bonds


def get_int_enr(sname, sites):
	
	tempdir = 'pycharmm.tmp/'
	if not os.path.exists(tempdir):
		os.makedirs(tempdir)

	struct = PDB.PDB(sname)
	h_names = set([h.a_name for h in struct.hetatms])
	print sname,'has',len(struct.chains),'chains:'
	for x in struct.chains:
		print '%s: has %i residues'%(x.chain_id,len(x.residues))
	sname = sname.split('/')[1].strip('pdb')
	write_hetatms_PDB(struct.hetatms, tempdir + sname + 'HET.pdb')
	struct.CLEAN()
	write_charmm_head_input(sname)
	#write_charmm_het_input(sname)
	inte_input = []
	for ch in struct.chains:
		if len(ch.residues) != 0:
			for site in sites:
				if ch.chain_id == site[1]:
					incontigs = ch.check_contiguous()
					ss = ch.find_ss_bonds()
					if len(incontigs) == 0:
						print 'chains %s selected,'%(ch.chain_id),'no incontinguities detected'
						segmens = [[0,len(ch.residues)]]
					else:
						print 'chains %s selected,'%(ch.chain_id),'with incontiguities:',incontigs
						segmens = []
						previous = 0
						for x in incontigs:
							segmens.append([previous,x])
							previous = x
						segmens.append([incontigs[-1],len(ch.residues)])
					print len(ss),'SS bonds detected:',ss
					count = 0
					write_charmm_min_input(sname, ch,segmens,ss)
					for i,x in enumerate(ch.residues):
						if x.r_number == int(site[0]):
							count += 1
							resno = i
							if count != 1:
								print 'SELECTED RESIDUE FOUND',count,'TIMES, QUITTING'
							inte_input.append((ch,segmens,ss,resno))


	ssb_strs = []
	calc_ene_strs = []
	fix_strs = []
	for element in inte_input:
		chain,segments,ss_bonds,residue_number = element
		for i,y in enumerate(segments):
			if (residue_number < y[1]) and (residue_number >= y[0]):
				res_seg = chain.chain_id + 'se%i'%i
				res_segno = str(residue_number-y[0]+1)
		for x in ss_bonds:
			for i,y in enumerate(segments):
				if (x[0] < y[1]) and (x[0] >= y[0]):
					seg1 = (chain.chain_id + 'se%i'%i)
					rnum1 = str(x[0]-y[0]+1)
				if (x[1] < y[1]) and (x[1] >= y[0]):
					seg2 = (chain.chain_id + 'se%i'%i)
					rnum2 = str(x[1]-y[0]+1)
				ssb_strs.append(ssb.replace('@seg1',seg1).replace('@nr1',rnum1).replace('@seg2',seg2).replace('@nr2',rnum2))
		calc_ene_strs.append(calc_ene.replace('@RESSEG',res_seg).replace('@RESNO',res_segno))
		fix_strs.append(fix.replace('@RESSEG',res_seg).replace('@RESNO',res_segno))
	
	#for name in h_names:
		#fix_strs.append(fix_HET.replace('@RESSEG', 'HET').replace('@ANAME', name))

	charmm_inputfile = open(tempdir + sname + 'charmm.in','a')
	#for element in ssb_strs:
		#charmm_inputfile.write(element)

	charmm_inputfile.write(ic)
	for element in fix_strs:
		charmm_inputfile.write(element)
	charmm_inputfile.write(mini)
	
	for element in calc_ene_strs:
		charmm_inputfile.write(element)
	charmm_inputfile.write(tail.replace('@OUT', tempdir + sname +'minimised.pdb'))
	charmm_inputfile.close()


	p = subprocess.Popen(['CHARMM', '-i', tempdir + sname + 'charmm.in'], stdout=subprocess.PIPE)
	output = p.communicate()[0].split('\n')	
	interaction_energy = []
	for line in output:
		print line
		if re.search('INTE>', line):
			interaction_energy.append(" ".join(line.split()).split()[2])

	shutil.rmtree(tempdir)
	return [float(x) for x in interaction_energy]


if __name__ == '__main__':
	main()
