#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
#from random import shuffle
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader

import matplotlib.pyplot as plt
import numpy as np

def main():
	usage = format_usage('''
		%prog < STDIN
	''')
	parser = OptionParser(usage=usage)
	
	parser.add_option('-o', '--outfile', type=str, dest='outfile', default='outfile', help='output file name  [default: %default]',)
	parser.add_option('-p', '--dump-params', dest='params_file', help='some help FILE [default: %default]', metavar='FILE')
	parser.add_option('-v', '--verbose', dest='verbose', action='store_true', default=False, help='some help [default: %default]')

	options, args = parser.parse_args()
	
	if len(args) != 0:
		exit('Unexpected argument number.')
	
	d1 = []
	d2 = []
	d3 = []
	for line in stdin:
		b1, b2, b3  = safe_rstrip(line).split('\t')
		d1.append(float(b1))
		d2.append(float(b2))
		d3.append(float(b3))
	
	d1 = np.sort(d1)
	d2 = np.sort(d2)
	d3 = np.sort(d3)

	yvals1=np.arange(len(d1))/float(len(d1)-1)
	yvals2=np.arange(len(d2))/float(len(d2)-1)
	yvals3=np.arange(len(d3))/float(len(d3)-1)	
	
	plt.plot(d1,yvals1, label='chi1', color='r')
	plt.plot(d2,yvals2, label='chi2', color='b')
	plt.plot(d3,yvals3, label='chi3', color='g')	

	plt.title("Isotropic B factor distribution")
	plt.xlabel("Value")
	plt.ylabel("Probability")
	plt.legend()
	plt.savefig(options.outfile, format='pdf', bbox_inches='tight')	

if __name__ == '__main__':
	main()

