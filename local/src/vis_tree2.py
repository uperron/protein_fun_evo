#!/usr/bin/env python	
from __future__ import with_statement

from sys import stdin, stderr
import os
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
#from random import shuffle
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader

from ete3 import Tree, faces, TreeStyle
import numpy as np

def main():
	usage = format_usage('''
		%prog NEWICK_FILE IMG_DIR
	''')
	parser = OptionParser(usage=usage)
	
	parser.add_option('-o', '--out_file', dest='out_file', type='str', default='tree.out.pdf', help='Output file name [default: %default]')
	options, args = parser.parse_args()
	
	if len(args) != 2:
		exit('Unexpected argument number.')
	tree = Tree(args[0])
	dirname = args[1]
	# for programmable tree drawing refer to http://etetoolkit.org/docs/latest/tutorial/tutorial_drawing.html
	# Create faces based on external images, store in a dictionary keyed by corresponding node name
	faces_dict = {}
	for filename in os.listdir(dirname):
		node_str = filename.replace('.png', '').replace('.pdf', '').split("-")[-1]
		if ":" in node_str:
			node_name = node_str.replace("_", "/").replace(":", "-")
		else:
			node_name = node_str
		face_file = dirname+"/"+filename
		faces_dict.setdefault(node_name, []).append(face_file)
	leaves = faces_dict.keys()
	tree.prune(leaves, preserve_branch_length=True)	
	
	# Creates my own layout function. I will use all previously created
	# faces and will set different node styles depending on the type of
	# node.
	def mylayout(node):
		nameFace = faces.AttrFace("name", fsize=20, fgcolor="#009000")
		# If node is a leaf, add the nodes name and a its scientific name
		if node.is_leaf():	
			# Add an static face that handles the node name
			faces.add_face_to_node(nameFace, node, column=0)	
			# Add the corresponding face(s) to the node
			for face_file in faces_dict[node.name]:
				faces.add_face_to_node(faces.ImgFace(face_file), node, column=0, aligned=True)
			# Sets the style of leaf nodes
			node.img_style["size"] = 12
			node.img_style["shape"] = "circle"
		#If node is an internal node
		else:
			# Sets the style of internal nodes
			node.img_style["size"] = 6
			node.img_style["shape"] = "circle"

			
	
	ts = TreeStyle()
	ts.show_leaf_name = False	
	ts.layout_fn = mylayout
	ts.branch_vertical_margin = 100
	tree.render(options.out_file, tree_style = ts, w=1000, units="mm")

if __name__ == '__main__':
	main()

