#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
from uBio import Polypeptide

import matplotlib
# force matplotlib  not to use Xwi
matplotlib.use('Agg')

import matplotlib.pyplot as plt
import matplotlib.figure as fg
import numpy as np
import matplotlib.ticker as ticker

def discrete_cmap(N, base_cmap=None):
	"""Create an N-bin discrete colormap from the specified input map"""

	 # Note that if base_cmap is a string or None, you can simply do
	 #    return plt.cm.get_cmap(base_cmap, N)
	 # The following works for string, None, or a colormap instance:

	base = plt.cm.get_cmap(base_cmap)
	color_list = base(np.linspace(0, 1, N))
	cmap_name = base.name + str(N)
	return base.from_list(cmap_name, color_list, N)

def plot_heatmap(z_arr, r_arr, label, color_map, outfile):
	w, h = fg.figaspect(1/float(len(z_arr)))
 	fig = plt.figure(figsize=(w*25, h*0.5))
	ax = plt.subplot(2, 1, 1)
	ax.set_ylim([0,0.5])
	
	# set ticks format on y axis
	majorLocator = ticker.LinearLocator(numticks=3)
	majorFormatter = ticker.FormatStrFormatter('%s')
	ax.yaxis.set_major_locator(majorLocator)
	ax.yaxis.set_major_formatter(majorFormatter)

	# plot the value array  with no interpolation
	z_grid = np.expand_dims(z_arr, axis=0)
	r_grid = np.expand_dims(r_arr, axis=0)
    	ax.imshow(z_grid, interpolation=None, cmap='Accent', alpha=0.8, aspect='auto')
    	ax.table(cellText=r_grid,loc='center',cellLoc='center',bbox=[0,0,1,1], zorder=0)

	# set axes format
        ax.spines['top'].set_visible(False)
        ax.spines['right'].set_visible(False)
        ax.spines['bottom'].set_visible(False)
        ax.spines['left'].set_visible(False)
	ax.axes.get_xaxis().set_visible(False)
	
	#label ticks on axes using site_id (x) and sequence_id (y)	
	ax.set_yticklabels(['',label,''])
	plt.savefig(outfile, format='pdf', bbox_inches='tight', dpi=fig.dpi)
	plt.close(fig)

def main():
	usage = format_usage('''
		%prog SITES_NUM SEQ_NUM < STDIN
		draws a heatmap (one for each sequence) considering values and residue categories ()
		META: STDIN
		1	site_id (numerical)
		2	sequence_id
		3	resname
		4	value
	''')
	parser = OptionParser(usage=usage)
	parser.add_option('-o', '--outdir', type=str, dest='outdir', default='./', help='output directory [default: %default]')
	parser.add_option('-y', '--hydrophobic', dest='hydrophobic', action='store_true', default=False, help='only color hydrophobic residues')
	options, args = parser.parse_args()
	
	if len(args) != 2:
		exit('Unexpected argument number.')
	
	sites_num,sequs_num = int(args[0]),int(args[1])	

	B_branched = ['VAL', 'ILE',  'THR']
	non_B_branched = ["CYS", "PHE", "LEU", "MET", "ASN", "PRO", "GLN", "SER", "TRP", "TYR"]
	negative = ["ASP", "GLU"]
	positive = ["ARG", "HIS", "LYS"]
	hydrophobic = {"ALA" : 100, "ILE" : 200, "LEU" : 300, "PHE" : 400, "VAL" : 500, "PRO" : 600, "GLY" : 700}

	seq_lst = []
	site_lst = []
	z_grid = np.empty(shape=[sequs_num, sites_num])
	z_grid.fill(np.nan)
	r_grid = np.empty(shape=[sequs_num, sites_num], dtype=np.str)
	r_grid.fill('')
	for line in stdin:
                tokens = safe_rstrip(line).split('\t')
                site_id = int(tokens[0])
                seq_id = tokens[1]
                resname = tokens[2]
		config = int(tokens[3])
		if seq_id not in seq_lst:
			seq_lst.append(seq_id)

		if site_id not in site_lst:
			site_lst.append(site_id)
		
		if options.hydrophobic:
			if resname in hydrophobic.keys():
				z = config + hydrophobic[resname]
				if config == 1:
					z = z+25
				elif config == 2:
					z = z+50
				elif config == 3:
					z = z+75
			else:
				z = 0
			
		else:	
			# offset config values in order to have widely spaced color ranges for different residue categories
			if resname in B_branched:
				z = config
			elif resname in non_B_branched:
				z = config + 100
			elif resname in positive:
				z = config + 200	
			elif resname in negative:
				z = config + 300

		
		if config == 1:
			z = z+25
		elif config == 2:
			z = z+50
		elif config == 3:
			z = z+75

		# create 2 matrixes for config values and residues
		i = site_lst.index(site_id)
		j = seq_lst.index(seq_id)
		z_grid[j][i] = z
		r_grid[j][i] = Polypeptide.three_to_one(resname)
	
	if options.hydrophobic:
		color_map = discrete_cmap(24, base_cmap='gnuplot2')
	else:
		color_map = 'viridis'
	for i,seq in enumerate(seq_lst):
		z_arr = z_grid[i,]
		r_arr = r_grid[i,]
		outfile = options.outdir+seq.replace("<", '').replace(">", '')+'.pdf'
		plot_heatmap(z_arr, r_arr, seq, color_map, outfile)		
			
	
if __name__ == '__main__':
	main()

