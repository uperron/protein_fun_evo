#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
#from random import shuffle
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader

import math

ASCII_Sym_2_DEC = {'o': 111, 'NAK': 21, 'EOT': 4, 'ESC': 27, ' ': 32, '$': 36, 'RS': 30, '(': 40, ',': 44, '\\': 92, '0': 48, '4': 52, '8': 56, '<': 60, '@': 64, 'GS': 29, 'D': 68, 'H': 72, 'L': 76, 'P': 80, 'T': 84, 'X': 88, 'DC4': 20, 'DC2': 18, 'DC3': 19, 'DC1': 17, '`': 96, 'd': 100, 'h': 104, 'SYN': 22, 'p': 112, 't': 116, 'x': 120, '|': 124, 'BEL': 7, 'BS': 8, 'HT': 9, 'l': 108, '#': 35, 'G': 71, "'": 39, '+': 43, '/': 47, '3': 51, '7': 55, ';': 59, '?': 63, 'EM': 25, 'C': 67, 'DLE': 16, 'K': 75, 'O': 79, 'ENQ': 5, 'S': 83, 'W': 87, '[': 91, '_': 95, 'c': 99, 'g': 103, 'k': 107, 'US': 31, 's': 115, 'w': 119, 'VT': 11, '{': 123, 'FS': 28, 'SUB': 26, 'ACK': 6, 'ETX': 3, 'ETB': 23, 'FF': 12, '"': 34, '&': 38, ']': 93, '*': 42, '.': 46, 'NUL': 0, '2': 50, '6': 54, ':': 58, '>': 62, 'B': 66, 'F': 70, 'J': 74, 'N': 78, 'R': 82, 'V': 86, 'CR': 13, 'Z': 90, '^': 94, 'b': 98, 'f': 102, 'j': 106, 'n': 110, 'SI': 15, 'r': 114, 'SO': 14, 'v': 118, 'z': 122, '~': 126, 'CAN': 24, 'LF': 10, '%': 37, ')': 41, 'SOH': 1, '-': 45, '1': 49, '5': 53, '9': 57, '=': 61, 'A': 65, '!': 33, 'E': 69, 'I': 73, 'M': 77, 'Q': 81, 'U': 85, 'Y': 89, 'STX': 2, 'a': 97, 'e': 101, 'i': 105, 'm': 109, 'q': 113, 'u': 117, 'y': 121, '}': 125}

def main():
	usage = format_usage('''
		%prog < INPUT
		Generate lookup ASCII tables and parsing tables for libpll given a custom set of states 
		represented by single ASCII symbols.

		INPUT must be a \n-separated list of ASCII symbols
	''')
	parser = OptionParser(usage=usage)
	parser.add_option('-a', '--ambiguity',  default=False, help='a file containing a \n-separated state set for ambiguities and the corresponding non-ambiguous states (4	E;G;F) [default: %default]')
	parser.add_option('-p', '--parsing_map',  default=False, action="store_true", help='create a parsing map that sets legal, fatal and stripped characters given a /n-separated list of symbols as INPUT [default: %default]')
	options, args = parser.parse_args()
	
	if len(args) != 0:
		exit('Unexpected argument number.')
	
	# create an array
	array = [0] * 256
	
	if options.parsing_map:
		# lipll parsing values 0=stripped, 1=legal, 2=fatal, 3=silently stripped
		# set fatal symbols: period (46), ascii 0-8, ascii 14-31
		for i in [46] + range(0,9) + range(14, 32):
			array[i] = 2
		# set silently stripped symbols: tab (9), newline (10 and 13), vt (11), formfeed (12)
		for i in range(9,14):
			array[i] = 3
	
	states = []
	i = 0
	for line in stdin:
		symbol = line.rstrip("\n").split("\t")[0] 
		if len(list(symbol)) != 1:
			exit('Symbols must be 1 character long')
		elif symbol not in ASCII_Sym_2_DEC.keys():
			exit('Only standard ASCII symbols are accepted')
		states.append(symbol)
		dec_idx = ASCII_Sym_2_DEC[symbol]
		if not options.parsing_map:
			if not options.ambiguity:
				# states are encoded as powers of two such that the bitwise AND operation on the codes of two states always yields 0.
				array[dec_idx] = hex(int(math.pow(2,i)))
			else:
				array[dec_idx] = int(math.pow(2,i))
		elif options.parsing_map:
			# make the symbol legal
			array[dec_idx] = 1
		i = i + 1


	if options.ambiguity:
		with open(options.ambiguity, "r") as f:
			for line in f:
				ambiguous_symbol, non_ambiguous_symbols = line.rstrip("\n").split("\t")
				# ambiguities are encoded as the results of bitwise OR operations between the respective state codes. 
				# in the case of "first level" ambiguities the resulting value is the sum of the non-ambiguous states' decimal value
				ambiguous_value = sum([array[ASCII_Sym_2_DEC[x]] for x in non_ambiguous_symbols.split(";")])
				ambiguous_ec_idx = ASCII_Sym_2_DEC[ambiguous_symbol]
				array[ambiguous_ec_idx] = int(ambiguous_value)
			harray = [hex(x) if x != 0 else x for x in array]
			array = harray
	
	# print the map
	if options.parsing_map:
		print "const unsigned int %d-statesmap_phylip[256] =" % (len(set(states)))
	else:
		print "unsigned int %d-states_map[256] =" % (len(set(states)))
	print " {"
	for j in range(0,256,16):
		print "   " + str(array[j:j+16]).strip("[").strip("]") + ","
	print " };"

if __name__ == '__main__':
	main()

