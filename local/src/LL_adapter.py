#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
#from random import shuffle
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader

import pandas as pd
import numpy as np
import warnings
import re
import math

D_set = list("ABCDEFGHIJKLMNOPQRSTUVWYXZabcdefghijklmnopqrstuvwxyz012")
C_set = list("ARNDCQEGHILKMFPSTWYV")

def parse_PHYLIP(filename):
	site_list = []
	with file(filename, "r") as f:
		for line in f:
			if line != '\n' and line != "\t":
				ID, field2 = line.rstrip("\n").split("\t")
			if field2.isdigit():
				continue
			else:
				# parse and remove gaps	
				site_list.extend([x for x in field2.split(' ') if "-" not in  x])			
		return site_list

def get_freqs(states, site_list):
	f_dict = {}
	l = len(site_list)
	for s in states:
		f_dict[s] = float(site_list.count(s))/l
	return f_dict

def main():
	usage = format_usage('''
		%prog D_ALIGN C_ALIGN RAXML-NG.LOG
	''')
	parser = OptionParser(usage=usage)
	
	parser.add_option('-c', '--c_set', type="str", default=False, help='User-defined Compound state-set (comma-separated) [default: %default]')
	parser.add_option('-d', '--d_set', type="str", default=False, help='User-defined Distinct state-set (comma-separated) [default: %default]')

	options, args = parser.parse_args()
	
	if len(args) != 3:
		exit('Unexpected argument number.')
	
	if options.c_set:
		C_set = options.c_set.split(",")
	else: 
		C_set = list("ARNDCQEGHILKMFPSTWYV")

	if options.d_set:
		D_set = options.d_set.split(",")
	else:
		D_set = list("ABCDEFGHIJKLMNOPQRSTUVWYXZabcdefghijklmnopqrstuvwxyz012")

	D_sites = parse_PHYLIP(args[0])
	C_sites = parse_PHYLIP(args[1])

	if len(D_sites) != len(C_sites):
		warnings.warn('Number of taxa or sites in %s (%d) does ot correspond to number of taxa or sites in  %s (%d)' %(args[0], len(D_sites), args[1], len(C_sites)))	
	# compute freqs from observed states in aligns
	D_freq_dict = get_freqs(D_set, D_sites)
	C_freq_dict = get_freqs(C_set, C_sites)
	

	# get the final Loglikelihood value from raxml-ng's .log file
	Ln_C = "NA"
	try:
		with file(args[2], "r") as fd:
			for line in fd:
				if re.search("Final LogLikelihood:", line):
					Ln_C = float(line.split()[-1])
	except IOError as e:
		warnings.warn(e.strerror + ': %s'  % (args[2]))
	if Ln_C == "NA":
		warnings.warn('Final LogLikelihood value not found in %s' % (args[2]))
		print "%s\t%s\t%s" % (args[2], Ln_C, Ln_C)	
	else:
		# calculate the adapter correction
		# this allows to compare the likelihood of a compound (AA, 20-state) vs distinct model (AA+ other feature, 20+ states)
		# based on equation (2) in Whelan et al. 2015, PMID 25209223
		# here we use LogLikelihood instead of Likelihood to obtain tractable values
		Ln_adapted = Ln_C
		for D_state, C_state in zip(D_sites, C_sites):
			Ln_adapted = Ln_adapted + (math.log(D_freq_dict[D_state]) - math.log(C_freq_dict[C_state]))
		print "%s\t%f\t%f" % (args[2], Ln_C, Ln_adapted)

if __name__ == '__main__':
	main()

