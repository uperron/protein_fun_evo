#!/usr/bin/env python
from __future__ import with_statement
from sys import stdin, stderr
from optparse import OptionParser
#from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
from prepare import *
import numpy as np
#from random import shuffle
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader




def main():
	usage = format_usage('''
		%prog FILENAME1.pdb FILENAME2.pdb
		Provides root mean square deviation of atom coordinates and dihedral chi angles 
		for each rotamer in each residue of a chain from a .pdb file.
		.META: STDOUT
			1	chain from FILENAME1.pdb
			2	chain from FILENAME2.pdb
			3	residue from FILENAME1.pdb
			4	residue from FILENAME2.pdb
			5	rotamer (chi angle) name
			6	list of atoms that constitute the rotamer
			7	root mean square deviation for the two rotamers
			8	chi angle for residue from FILENAME1.pdb
			9	chi angle for residue from FILENAME2.pdb
	''')
	parser = OptionParser(usage=usage)
	parser.add_option('-r', '--rotamers', type=str, dest='rotamers', default=1, help='comma separated list of rotamers [1 .. 5] [default: %default]', metavar='ROTAMERS')

	options, args = parser.parse_args()
	
	#Check and assign args
	if len(args) != 2:
		exit('Unexpected argument number.')
	else:
		pdb1, pdb2 = args[:2] 

	# Check provided list of rotamers
	rotamers =  str(options.rotamers).split(",")
	verb_rotamers = []
	for rotamer in rotamers:
		verb_rotamer = "chi%s" % rotamer
		if verb_rotamer in chi_atoms.keys():
			verb_rotamers.append(verb_rotamer)
			alt_verb_rotamer = "altchi%s" % rotamer
		elif alt_verb_rotamer in chi_atoms.keys():
			verb_rotamers.append(alt_verb_rotamer)
		else:
			exit("Invalid rotamer %s", rotamer) 
	verb_rotamers.sort()

	chains1 = PDB(pdb1).chains
	chains2 = PDB(pdb2).chains
	for chn1, chn2 in zip(chains1, chains2):
		for resi1, resi2 in zip(chn1.residues, chn2.residues):
			resi1.calculate_chi()
			resi2.calculate_chi()
			angles1 = [x for x in resi1.chi_angles if not x=='NA']
			angles2 = [x for x in resi2.chi_angles if not x=='NA']
			resi1_rotamers = len(angles1)
			resi2_rotamers = len(angles2)
			for i in range(0, min(resi1_rotamers, resi2_rotamers, len(verb_rotamers))):
					verb_rotamer = verb_rotamers[i]
					chi_angle1 =  angles1[i]
					chi_angle2 =  angles2[i]
					rotamer_coords1 = []
					rotamer_coords2 = []
					rotamer_atoms1 = chi_atoms.get(verb_rotamer, {}).get(resi1.r_name)
					rotamer_atoms2 = chi_atoms.get(verb_rotamer, {}).get(resi2.r_name)
					for atom1 in resi1.atoms:
						if  atom1.a_name in rotamer_atoms1:
							rotamer_coords1.append([atom1.x, atom1.y, atom1.z])
						continue
					for atom2 in resi2.atoms:
						if atom2.a_name in rotamer_atoms2:
							rotamer_coords2.append([atom2.x, atom2.y, atom2.z])
						continue
					if len(rotamer_coords1) == 4 and len(rotamer_coords2) == 4:
						rotamer_array1 = np.array(rotamer_coords1)
						rotamer_array2 = np.array(rotamer_coords2)
						#calculates rmsd using formula from Wikipedia "Root-mean-square deviation" article https://en.wikipedia.org/wiki/Root-mean-square_deviation
						rmsd = np.sqrt(((rotamer_array1 - rotamer_array2) ** 2).mean())
					else:
						rmsd = "NA"

					list_atoms = str(rotamer_atoms1 + list(set(rotamer_atoms2) - set(rotamer_atoms1))).strip('[]')
					list_output = [chn1.chain_id, chn2.chain_id, resi1.r_name, resi1.r_number, resi2.r_name, resi2.r_number, verb_rotamer, list_atoms, rmsd, chi_angle1, chi_angle2]
					print("\t".join(map(str, list_output)))



if __name__ == '__main__':
	main()
