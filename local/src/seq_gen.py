#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
#from random import shuffle
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader

import numpy as np

rota_states = ['ALA', 'ARG1', 'ARG2', 'ARG3', 'ASN1', 'ASN2', 'ASN3', 'ASP1', 'ASP2', 'ASP3', 'CYS1', 'CYS2', 'CYS3', 'GLN1', 'GLN2', 'GLN3', 'GLU1', 'GLU2', 'GLU3', 'GLY', 'HIS1', 'HIS2', 'HIS3', 'ILE1', 'ILE2', 'ILE3', 'LEU1', 'LEU2', 'LEU3', 'LYS1', 'LYS2', 'LYS3', 'MET1', 'MET2', 'MET3', 'PHE1', 'PHE2', 'PHE3', 'PRO1', 'PRO2', 'SER1', 'SER2', 'SER3', 'THR1', 'THR2', 'THR3', 'TRP1', 'TRP2', 'TRP3', 'TYR1', 'TYR2', 'TYR3', 'VAL1', 'VAL2', 'VAL3']
AA_states = ['ALA', 'ARG', 'ASN', 'ASP', 'CYS', 'GLN', 'GLU', 'GLY', 'HIS', 'ILE', 'LEU', 'LYS', 'MET', 'PHE', 'PRO', 'SER', 'THR', 'TRP', 'TYR', 'VAL'] 

def main():
	usage = format_usage('''
		%prog SEQ_TYPE SEQ_LEN <FREQS
	''')
	parser = OptionParser(usage=usage)
	parser.add_option('-o', '--one_to_three', dest='one_to_three', action='store_true', default=False, help='1-letter AA to 3-letter AA [default: %default]')
	options, args = parser.parse_args()
	
	if len(args) != 2:
		exit('Unexpected argument number.')
	
	if args[0] == "AA":
		states = AA_states
	elif args[0] == "ROTA":
		states = rota_states
	else:
		exit('Invalid sequence type')
	
	seq_len = int(args[1])

	for line in stdin:
		freqs = [float(x) for x in safe_rstrip(line).split('\t')]
	
	seq = np.random.choice(states, seq_len, p=freqs)

	print "%s" % (' '.join(map(str, seq)))	

if __name__ == '__main__':
	main()

