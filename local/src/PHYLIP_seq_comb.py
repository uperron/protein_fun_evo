#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
#from random import shuffle
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader

from itertools import combinations


def main():
	usage = format_usage('''
		%prog PHYLIP_MULTIALIGN 
		generate all pairwise combinations of sequences in a multialignment
	''')
	
	parser = OptionParser(usage=usage)
	parser.add_option('-e', '--enum', action='store_true', default=False, help='enumerate combinations (pairwise) of sequences [default: %default]')	
	options, args = parser.parse_args()
	
	if len(args) != 1:
		exit('Unexpected argument number.')
	
	multialign_file = args[0]
	file_in =  open(multialign_file, "r")	
	lines = file_in.readlines()
	clean_lines =[]
	for i,line in enumerate(lines):
		l = line.rstrip()
		if l != '\n' and l != '':
			ID, field2 = line.rstrip("\n").rstrip("\r").split("\t") 
			if field2.isdigit():
				continue
			else:
				clean_lines.append(ID + "\t" + field2)
	# generate all pairwise combinations of sequences in alignment	
	i = 1
	for duo in combinations(clean_lines, 2):
		for uno in duo:
			if options.enum:
				print str(i) + "\t" + uno
			else:
				print uno
		i = i + 1 

if __name__ == '__main__':
	main()
