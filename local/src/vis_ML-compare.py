#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
#from random import shuffle
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader

import matplotlib
# force matplotlib  not to use Xwi
matplotlib.use('Agg')
import matplotlib.colors as colors

import matplotlib.pyplot as plt
import matplotlib.figure as fg
import matplotlib.ticker as ticker

import numpy as np
import itertools

import randomcolor

def main():
	usage = format_usage('''
		%prog MODEL1,MODEL2 LABEL1,LABEL2
	''')
	parser = OptionParser(usage=usage)
	
	parser.add_option('-o', '--outfile', dest='outfile', default='outfile', help='output file [default: %default]')
	options, args = parser.parse_args()
	
	if len(args) != 2:
		exit('Unexpected argument number.')
	

	infiles = args[0].split(",")
	labels = args[1].split(",")
	rand_color = randomcolor.RandomColor()
	colors = ['#9d3a6b', '#9dce3a', '#684a94', '#6b9d3a', '#ffce3a']

	fig = plt.figure(figsize=(8,8))

	# parse the input files and plot the scatterplots and the means
	def parse_plot(infile, label, color):
		time = []
		d_ML = []
		with file(infile, 'r') as fd:
			for line in fd:
				t, d = safe_rstrip(line).split('\t')
				time.append(float(t))
				d_ML.append(float(d))
		plt.plot(time, d_ML, label=label,linestyle='--', marker=(3,3,3), color=color)

	for infile, label, color in zip(infiles, labels, colors):
		parse_plot(infile, label, color) 

	# plot the bisector
	axes = plt.gca()
	axes.set_ylim([0,15])
	plt.plot(np.linspace(0,axes.get_xlim()[1]), np.linspace(0,axes.get_xlim()[1]), '--', color='k')
	
	plt.xlabel('t')
	plt.ylabel('d')
	plt.legend()
	plt.savefig(options.outfile, format='pdf', bbox_inches='tight', dpi=fig.dpi)

if __name__ == '__main__':
	main()

