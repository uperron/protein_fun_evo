#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr, stdout
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
#from random import shuffle
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader

import matplotlib
# force matplotlib  not to use Xwi
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.ticker as ticker 
import matplotlib.patches as mpatches
import pandas as pd
import numpy as np
import seaborn as sb
import math

def main():
	usage = format_usage('''
		%prog MODEL1_LABEL,[...]  MODEL1,MODEL2,[...] TREE_SIZE1,TREE_SIZE2,[...] <STDIN
	STDIN = LABELS\tMODELS\tTREE_SIZES
	One plot per tree size comparing all models
	''')
	parser = OptionParser(usage=usage)
	parser.add_option('-y', '--ylabel', type="str", default=False, help='Custom y-axis label [default: %default]')
	options, args = parser.parse_args()
	
	if len(args) != 3:
		exit('Unexpected argument number.')

	labels = args[0].split(",") #i.e. column names
	models = args[1].split(",")
	tree_sizes = args[2].split(",")
	model_num = len(models)
	colors = sb.color_palette("colorblind", 5, desat=.9) 
	if model_num == 6:
		colors[-1] = "r"
	if 'REF' in models:
		colors.insert(0, 'k')

	# to make sure 55x55 is on top of the other plots
	z_orders = np.arange(model_num)[::-1]

	fig, axs = plt.subplots(len(tree_sizes),1, figsize=(12,12), sharex=True)
	fig.subplots_adjust(hspace = .5, wspace=.001)

	names = ['scale'] + labels
	data = pd.read_csv(stdin, sep="\t", header=None, names=names).astype(np.float)
	data.dropna(inplace=True)

	x = data['scale'].drop_duplicates().values

	# one subplot per tree size
	for ax, tree_size in zip(axs.ravel(), tree_sizes):
		
		patches = []
		#get the corresponding columns (one per model) & group by scale
		fltr_gp_df = data.filter(regex=tree_size + '|scale').groupby('scale', sort=False)
		for label, color, z_order in zip([l for l in labels if 'TX' + tree_size in l], colors, z_orders):
			y = []
			for scale in x:
				y.append(fltr_gp_df.get_group(scale)[label].mean())
			if 'REF' in label:
				linestyle="--"
			else:
				linestyle="-"
			ax.plot(x, y, color=color, label=label, linestyle=linestyle, 
					marker='o', linewidth=.5, markersize=4, zorder=5*z_order)
			patches.append(mpatches.Patch(color=color, label=label, linestyle=linestyle, linewidth=1))
		ax.set_xticklabels(x, rotation=45, size='small')	
		majorLocator = ticker.FixedLocator(x)
		ax.xaxis.set_major_locator(majorLocator)
		ax.set_title(str(tree_size) + ' taxa', loc = 'center')
		ax.yaxis.set_major_formatter(ticker.FormatStrFormatter('%d'))	
		ax.set_yticklabels(ax.get_yticks(), rotation=45, size='small')
		ax.autoscale()
	fig.text(0.5, 0.04, 'Scaling factor', ha='center', va='center', size='x-large')
	if options.ylabel:
		ylab = options.ylabel
	else: 
		ylab = 'Log Likelihood'
	fig.text(0.06, 0.5, ylab, ha='center', va='center', rotation='vertical', size='x-large')
	# set figure style and font
	plt.style.use(['seaborn-white', 'seaborn-paper'])
	matplotlib.rc('font', serif='Helvetica Neue')

	plt.figlegend(patches, models, loc = 'upper center', ncol=model_num)
	plt.savefig(stdout, format='pdf', bbox_inches='tight', dpi=fig.dpi)

if __name__ == '__main__':
	main()
