#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
#from random import shuffle
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader

import matplotlib
# force matplotlib  not to use Xwi
matplotlib.use('Agg')
import matplotlib.colors as colors

import matplotlib.pyplot as plt
import matplotlib.figure as fg
import matplotlib.ticker as ticker

import numpy as np
from itertools import groupby

def main():
	usage = format_usage('''
		%prog <COORDS
	''')
	parser = OptionParser(usage=usage)
	
	parser.add_option('-o', '--outfile', dest='outfile', default='outfile', help='output file [default: %default]')
	options, args = parser.parse_args()
	
	if len(args) != 0:
		exit('Unexpected argument number.')


	fig = plt.figure(figsize=(20,20))
	ax = fig.add_subplot(111)

	# setting y axis ticks format
        #majorLocator = ticker.FixedLocator(np.arange(0, 2.5, 0.025))
        #majorFormatter = ticker.FormatStrFormatter('%d')
        #ax.yaxis.set_major_locator(majorLocator)
        #ax.yaxis.set_major_formatter(majorFormatter)

	things = []
	data = []
	timepoints = []
	# parse the stdin, create a list of vectors
	for line in stdin:
		t, d = [float(x) for x in safe_rstrip(line).split('\t')]
		things.append((t,d))
		timepoints.append(t)
	timepoints = sorted(set(timepoints))
	for key, group in groupby(things, lambda x: x[0]):
		box = []
		for thing in group:
			box.append(thing[1])
		data.append(box)
	

	# plot the boxplots
	bp = ax.boxplot(data, patch_artist=True, widths=0.01, positions=timepoints, labels=timepoints)
	
	for box in bp['boxes']:
		# change outline color
		box.set( color='#7570b3', linewidth=1)
		# change fill color
		box.set( facecolor = '#1b9e77' )
	## change color and linewidth of the whiskers
	for whisker in bp['whiskers']:
		    whisker.set(color='#7570b3', linewidth=1)
	## change color and linewidth of the caps
	for cap in bp['caps']:
		    cap.set(color='#7570b3', linewidth=1)
	## change color and linewidth of the medians
	for median in bp['medians']:
		    median.set(color='#b2df8a', linewidth=2)
	## change the style of fliers and their fill
	for flier in bp['fliers']:
		    flier.set(marker='.', color='#e7298a', alpha=0.5)

	#label ticks on axes
	axes = plt.gca()
        ax.set_xticklabels(timepoints, size='xx-small', rotation=45)
	#ax.set_yticklabels(np.arange(0, 10, 0.5), size='xx-small', rotation=45)	

	# plot the bisector
	#axes.set_xlim([0, 10])
	#axes.set_ylim([0, 3.5])
	plt.plot(np.linspace(0,axes.get_xlim()[1]), np.linspace(0,axes.get_xlim()[1]), '--', color='k')
	plt.xlabel('t', fontsize='xx-large')
	plt.ylabel('Estimated branch length', fontsize='xx-large')
	plt.savefig(options.outfile, format='pdf', bbox_inches='tight', dpi=fig.dpi)

if __name__ == '__main__':
	main()

