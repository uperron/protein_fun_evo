#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
#from random import shuffle
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader
import itertools
import numpy as np
import re

def main():
	usage = format_usage('''
		%prog PAML_MATRIX PAML_FREQS ORIG_LABELS NORM_LABELS < ORIG_MATRIX
	''')
	parser = OptionParser(usage=usage)
	
	parser.add_option('-f', '--freq_file', default=False, help='also expand frequencies from file  [default: %default]')

	options, args = parser.parse_args()
	
	if len(args) != 4:
		exit('Unexpected argument number.')
	
	ORIG_LABELS = args[2].split(",")
	NORM_LABELS = args[3].split(",")
	orig_size = len(ORIG_LABELS)
	norm_size = len(NORM_LABELS)
	orig_matrix = np.zeros((orig_size,orig_size), dtype=float)	
	norm_matrix = np.zeros((norm_size,norm_size), dtype=float)
	out_matrix = np.zeros((orig_size,orig_size), dtype=float)
	
	# parse the raw counts matrix to be normalized
	for i,line in enumerate(stdin):
		values = [float(x) for x in safe_rstrip(line).split("\t")]
		for j,value in enumerate(values):
			orig_matrix[i,j] = value
	
	# parse the "normalizer matrix", i.e. a "proper" IRM with negative diagonals derived from a PAML .dat file
	with open(args[0], "r") as f:
		for i,line in enumerate(f):
			values = [float(x) for x in safe_rstrip(line).split("\t")]
			for j,value in enumerate(values):
				norm_matrix[i,j] = value
	# parse the PAML frequencies
	with open(args[1], "r") as f:
		for line in f:
			norm_freqs = [float(x) for x in safe_rstrip(line).split(",")]
	
	orig_matrix = np.where(orig_matrix==0., 1, orig_matrix)

		
	for i,current_col_label in enumerate(NORM_LABELS):
		for j,current_line_label in enumerate(NORM_LABELS):
			if i == j:
				# for main diagonal cells
				norm_value = norm_matrix[i,i] * -1
			else:
				norm_value = norm_matrix[i,j]
			# from label to LABEL(s) in the orig_matrix by searching LABELS elements for matches
			l_rgx = re.compile('%s.*'%re.escape(current_line_label))
			c_rgx = re.compile('%s.*'%re.escape(current_col_label))
			# obtain lists of indices that identify a block (submatrix) in the rotamer counts matrix (i.e. a 1x1, 1x2, 1x3, 2x2, 2x3 or 3x3 block)
			line_LABELS = [ORIG_LABELS.index(m.group(0)) for l in ORIG_LABELS for m in [l_rgx.search(l)] if m] 
			col_LABELS = [ORIG_LABELS.index(m.group(0)) for l in ORIG_LABELS for m in [c_rgx.search(l)] if m]

			# value of each cell in the submatrix defined by line_LABELS,col_LABELS is divided by the sum total of the submatrix, then multiplied by the value in the file_norm cell
			# this is meant to account for the fact that some states are not filtere by B_factor and have therefore higher counts in the rotamer matrix (i.e. ALA, GLY)
			# also filtering by resolution and B_factor might select against residues that are rarer in the core region
			orig_sum = 0.0
			for row in line_LABELS:
				for col in col_LABELS:
					orig_sum = orig_matrix[row][col] + orig_sum
			for row in line_LABELS:
				for col in col_LABELS:
					out_matrix[row][col] = orig_matrix[row][col] / orig_sum * norm_value

	# print the expanded matrix including header and row labels
	print  '\t'.join(map(str, ['X'] + ORIG_LABELS))
	for idx, symbol in enumerate(ORIG_LABELS):
		row = [symbol]
		row.extend([x for x in out_matrix[idx,:]])
		print '\t'.join(map(str, row))

if __name__ == '__main__':
	main()

