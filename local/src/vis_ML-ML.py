#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
#from random import shuffle
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader

import matplotlib
# force matplotlib  not to use Xwi
matplotlib.use('Agg')
import matplotlib.colors as colors

import matplotlib.pyplot as plt
import matplotlib.figure as fg
import matplotlib.ticker as ticker

import numpy as np
import itertools

import randomcolor

def main():
	usage = format_usage('''
		%prog MODEL_1_COORDS MODEL_2_COORDS NSTATES1,NSTATES2
	''')
	parser = OptionParser(usage=usage)
	
	parser.add_option('-o', '--outfile', dest='outfile', default='outfile', help='output file [default: %default]')
	parser.add_option('-n', '--nruns', dest='nruns', type=int, default=5, help='number of simulation runs [default: %default]')
	options, args = parser.parse_args()
	
	if len(args) != 3:
		exit('Unexpected argument number.')
	
	nruns = options.nruns

	infiles = args[:-1]
	labels = args[-1].split(",")
	rand_color = randomcolor.RandomColor()
	colors = rand_color.generate(count=len(infiles))

	# calculate the mean value for each timepoint
	def calc_means(time, dists, nruns):
		means = []
		d = {}
		for k,v in zip(time, dists):
			d.setdefault(k, []).append(v)
		# means order corresponds to uniq_time
		for k,v in sorted(d.items()):
			means.append(np.mean(v))
		return means
	
	fig = plt.figure(figsize=(8,8))

	# parse the input files and plot the scatterplots and the means
	def parse_plot(infile, label, color):
		time_ML = []
		d_ML = []
		with file(infile, 'r') as fd:
			for line in fd:
				t, d = safe_rstrip(line).split('\t')
				time_ML.append(float(t))
				d_ML.append(float(d))
		
		# obtain a sorted list of unique timepoints
		uniq_time = sorted(list(set(time_ML)))

		plt.scatter(time_ML, d_ML, label=label, color=color)
		plt.plot(uniq_time, calc_means(time_ML, d_ML, nruns), linestyle='--', marker='x', color=color)

	for infile, label, color in zip(infiles, labels, colors):
		parse_plot(infile, label, color) 

	# plot the bisector
	axes = plt.gca()
	axes.set_ylim([0,3])
	plt.plot(np.linspace(0,axes.get_xlim()[1]), np.linspace(0,axes.get_xlim()[1]), '--', color='k')
	
	plt.xlabel('t')
	plt.ylabel('d')
	plt.legend()
	plt.savefig(options.outfile, format='pdf', bbox_inches='tight', dpi=fig.dpi)

if __name__ == '__main__':
	main()

