#!/usr/bin/env python
from __future__ import with_statement
import os
from sys import stdin, stderr
import warnings
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
from Bio.Alphabet import generic_protein
from Bio.Seq import Seq
from Bio.Data import SCOPData
from Bio.SeqRecord import SeqRecord
from Bio import SeqIO
from StringIO import StringIO
from uBio import MMCIFParser, Polypeptide
from collections import OrderedDict
#from random import shuffle
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader



def main():
	usage = format_usage('''
		%prog < FILENAME1.cif [...]
		given a list of PDBX (.cif) files as stdin, prints all canonical sequences 
		and all polypeptide sequences in two separate .fasta files.
	''')
	parser = OptionParser(usage=usage)
	parser.add_option('-c', '--can_out', type=str, dest='can_out', default=None, help='output filename for canonical sequences [default: %default]', metavar='CAN_OUT')
	parser.add_option('-p', '--poly_out', type=str, dest='poly_out', default=None, help='output filename for polypeptide sequences [default: %default]', metavar='CAN_OUT')	
	parser.add_option('-P', '--PDBmap', type=str, dest='PDBmap', default=None, help='pdb residue number map [default: %default]')
	options, args = parser.parse_args()
	
	filenames = []
	for line in stdin:
		filenames.extend(line.split())

	if len(args) != 0:
		exit('Unexpected argument number')
	if len(filenames) < 1:
		exit('At least one .cif files needed')

	if options.can_out:
		output1 = options.can_out
	else:
		output1 = 'canonical.fasta'

	if options.poly_out:
		output2 = options.poly_out
	else:
		output2 = 'polypeptide.fasta'
	# create a dictionary accessed via PDB_id and chain, values are tuples of residue coordinates for the Pfam domain
	if options.PDBmap is not None:
		PDBmap_dict = {}
		with file(options.PDBmap, 'r') as fd:
			for line in fd:
				tokens = safe_rstrip(line).split('\t')
				key = tokens[0] + tokens[1]
				start, stop = int(tokens[2]), int(tokens[3]) 
				PDBmap_dict.setdefault(key, []).append((start, stop))
			fd.close()
	## obtain full canonical sequence, polypeptide residue sequence and model objects from a list of .cif files
	bparser = MMCIFParser.MMCIFParser()
	ppb = Polypeptide.PPBuilder()
	out_handle1 = open(output1, 'a')
	out_handle2 = open(output2, 'a')
	for filename in filenames:
		poly_sequences = []
		can_sequences = []
		structure_id = os.path.split(filename)[1].replace('.cif', '')
		try:
			structure = bparser.get_structure(structure_id, filename)
			print structure_id
		except:
			warnings.warn("Error while parsing %s, skipping..." % (filename))
			continue
		for model in structure:
			model_id = str(model.get_id())
			for chain in model:
				chain_id = str(chain.get_id())
				
				# if dealing with Pfam allignments, limit polypeptides to the range provided by the Pfam mapping file.
				if options.PDBmap is not None:
					res_lst = []
					k = structure_id + chain_id
					if k in PDBmap_dict:
						for duo in PDBmap_dict[k]:
							res_dict = {}
							for residue in chain:
								resnum = int(residue.get_pdb_seq_num())
								res_dict[resnum] = residue
							for resnum in OrderedDict(sorted(res_dict.items(), key=lambda t: t[0])).keys():
								if resnum >= duo[0] and resnum <= duo[1]:
									residue = res_dict[resnum]
									res_lst.append(residue)
								else:
									continue
							if res_lst != []:
								entity = res_lst
								print k, PDBmap_dict[k], len(res_lst)
							else:
								continue
							## allow non-standard AA
							for pp in ppb.build_peptides(entity, aa_only=False):
								pp_id = str(pp).replace(" ", "_")
								poly_sequences.append(SeqRecord(pp.get_sequence(), id='-'.join([structure_id, model_id, chain_id, pp_id]), description='poly_sequence'))	
					else:
						continue		
										
				else:
					entity = chain
					## allow non-standard AA
					for pp in ppb.build_peptides(entity, aa_only=False):
						pp_id = str(pp).replace(" ", "_")
						poly_sequences.append(SeqRecord(pp.get_sequence(), id='-'.join([structure_id, model_id, chain_id, pp_id]), description='poly_sequence'))
		can_sequences.append(SeqRecord(structure.get_can_sequence(), id='-'.join([structure_id, model_id]), description='can_sequence'))
		SeqIO.write(can_sequences, out_handle1, "fasta")
		SeqIO.write(poly_sequences, out_handle2, "fasta")
	out_handle1.close()
	out_handle2.close()

if __name__ == '__main__':
	main()

