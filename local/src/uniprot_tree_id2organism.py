#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr, stdout
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage

import ete2
import urllib,urllib2
import pandas as pd
import re

url = "https://www.uniprot.org/uniprot/"

def main():
	usage = format_usage('''
		%prog NEWICK_TREE
		requires a newick tree with leaves identified by Uniprot ID ("P04718")
		maps to protein_names / organism
	''')
	parser = OptionParser(usage=usage)
	
	parser.add_option('-d', '--dump_dataframe', action="store_true", help='dump Uniprot dataframe')

	options, args = parser.parse_args()
	
	if len(args) != 1:
		exit('Unexpected argument number.')
	
	tree = ete2.Tree(args[0])


	names = ' '.join(map(str, set([x for x in tree.get_leaf_names()])))
	
	for name in names:
		params = {'query': name, 'columns':'id, protein names, organism', 'format':'tab'}
		request = urllib2.Request(url, urllib.urlencode(params))	
		response = urllib2.urlopen(request)
	
	df = pd.read_table(response, skiprows=1,  header=None, index_col=0, usecols=[0,2])

	for node in tree.traverse():
		# internal nodes have no names in PFAM trees
		if node.is_leaf():
			entry_name, res_range = node.name.split("/")
			try:
				new_name = df.loc[entry_name].values[0]
				node.add_feature('name', new_name + "/" + res_range)
			except:
				continue
	print tree.write(format=1)	


if __name__ == '__main__':
	main()

