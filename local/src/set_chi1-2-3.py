#!/usr/bin/env python
from __future__ import with_statement
from __future__ import print_function

from sys import stdin, stderr
from optparse import OptionParser
#from vfork.io.util import safe_rstrip
#from vfork.util import exit, format_usage
#from random import shuffle
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader


import re
import numpy as np
import os
import itertools
import pycharmm

# for running Chimera commands from script
from chimera import runCommand as rc


def parse_library(library):
	library_dict = {}
	with file(library, 'r') as fd:
		for line in fd:
			tokens = line.rstrip('\n').split('\t')
			key1 = ';'.join(map(str, tokens[:3])) # resname;phi;psi
			key2 = tokens[3] # chi1_config
			chi1_value = float(tokens[8])
			chi2_config, chi3_config = tokens[4], tokens[5]
			if chi2_config != '0' and chi3_config != '0':
				chi2_value, chi3_value = float(tokens[9]), float(tokens[10])
				config = [(key2, chi1_value), (chi2_config, chi2_value), (chi3_config, chi3_value)]
			elif chi2_config != '0' and chi3_config == '0':
				chi2_value = float(tokens[9])
				config = [(key2, chi1_value), (chi2_config, chi2_value)]
			elif chi2_config == '0' and chi3_config == '0':
				config = [(key2, chi1_value)]
			# A first layer of dictionary accessed by resname;phi;psi, a second layer by chi1_config, 
			# values are lists of all combination on chi2, chi3 (if any) with a fixed chi1: {'CYS;180;0': {'2': [[('1', 178.5)]]}}
			library_dict.setdefault(key1, {}).setdefault(key2, []).append(config)	
	return library_dict
					
def change_angles(in_dir, out_dir,  structure, sites, artefact_chis, alternate_chis, no_charmm):
	filename = in_dir + "/" + structure + ".cif"
	chi1_class_d = {}
	chi1_class_d['alternate'] = alternate_chis
	chi1_class_d['artefact'] = artefact_chis	
	output = [structure]

	# write the original structure (protein only) as a PDB file.
	rc("open #0 " + filename)
	
	print(">" + structure)
	# print original chi1,2,3 configuration to Reply Log
	for site in sites:
		resnum, chain = site
		spec = resnum + "." + chain
		rc("list residue spec :" + spec + " attribute chi1")
		rc("list residue spec :" + spec + " attribute chi2")
		rc("list residue spec :" + spec + " attribute chi3")
	print("<")
	rc("sel protein | Zn")
	path = out_dir + "/" + structure + ".original.pdb"
	
	rc("write format pdb selected #0 " + path)
	
	if no_charmm:
		pass		
	else:
		output.append(sum(pycharmm.get_int_enr(path, sites)))
	rc("close session")

	# write all possible chi2, chi3 combinations of the alternate and artefact chi1 structure
	
	for chi1_class in chi1_class_d.keys():
		rc("open #0 "+ filename)
		min_enr = None
		min_config_str = ''
		for product in itertools.product(*chi1_class_d[chi1_class]): # 121@site1, 231@site2, ... 333@siteN
			config_str = ''
			for index, combination in enumerate(product):
				resnum, chain = sites[index]
				spec = resnum + "." + chain
				config_str = "_" + spec + ":" + config_str	
				chi1_config, chi1_value = [str(x) for x in combination[0]]
				# set chi1 to value specified in current combination for current site
				rc("setattr r " + "chi1 " +  chi1_value + " :" + spec)
				config_str = config_str + chi1_config + "."
				
				# set chi2 (if any)
				if len(combination) > 1:
					chi2_config, chi2_value = [str(x) for x in combination[1]]
					rc("setattr r " + "chi2 " +  chi2_value + " :" + spec)
					config_str = config_str + chi2_config + "."			
				
				# set chi3 (if any)
				if len(combination) > 2:
					chi3_config, chi3_value = [str(x) for x in combination[2]]
					rc("setattr r " + "chi3 " +  chi3_value + " :" + spec)
					config_str = config_str + chi3_config + "."

				# keep track of the current configuration for chi1,2,3
				config_str = config_str.strip('.')		
			
			# write current combination to file
			rc("sel protein | Zn")
			temp_path = out_dir + "/" + structure + config_str + ".out.pdb"
			rc("write format pdb selected #0 " + temp_path)
		
			# call CHARMM script to minimixe each structure and find best one based on interaction energy of modified residue(s	
			if no_charmm:
				path = out_dir + "/" +  structure + config_str + "." + chi1_class + "_chi1.pdb"
				os.rename(temp_path, path)
			else:			
				enr = sum(pycharmm.get_int_enr(temp_path, sites))
	
				if min_enr is  None:
					min_enr = enr
					path = out_dir + "/" +  structure + config_str + "." + chi1_class + "_chi1.pdb"
					os.rename(temp_path, path)
					min_enr = enr
					min_config_str = config_str
	 
				elif enr <  min_enr:
					path = out_dir + "/" +  structure + config_str + "." + chi1_class + "_chi1.pdb"
					os.rename(temp_path, path)
					min_enr = enr
					min_config_str = config_str
 
		chi1_class_d[chi1_class] = [min_config_str, min_enr]
		rc("close session")
	output.extend(chi1_class_d['alternate'])
	output.extend(chi1_class_d['artefact'])
	return(output)




	
def main():
	parser = OptionParser()
	# USAGE: stdin > CHIMERA --nogui --nostatus --script "set_chi1-2-3.py OPTIONS ROTAMER_LIBRARY"
	parser.add_option('-i', '--in_dir', dest='in_dir', default='.',  help='input directory containing structures [default: %default]')
	parser.add_option('-o', '--out_dir', dest='out_dir', default='.', help='output directory [default: %default]')
	parser.add_option('-f', '--out_file', dest='out_file', default='outfile', help='output file [default: %default]')
	parser.add_option('-n', '--no_charmm', dest='no_charmm', default=False, action="store_true", help='print all structure, do not compute interaction energy[default: %default]')
	options, args = parser.parse_args()
	
	if len(args) != 1:
		exit('Unexpected argument number.')
	
	rotamer_library = args[0]
	lib_dict = parse_library(rotamer_library)

	current_struct = None
	sites = []
	artefact_chis = []
	alternate_chis = []

	#.META: stdin
	#59      1TF6    CYS1;CYS2       -90     130     2       A       CYS     80
	f_out = open(options.out_file, "w")
	for line in stdin:
		tokens = line.rstrip('\n').split('\t')
		struct = tokens[1]

		if current_struct is None:
			current_struct = struct

		if current_struct != struct:
			# For the original stucture, calculate residue interaction energy for each residue in sites.
			# For the alternate and artifact structure, return structure w/t lowest residue interaction energy among all combinations.
			out = '\t'.join(map(str, change_angles(options.in_dir, options.out_dir, current_struct, sites, artefact_chis, alternate_chis, options.no_charmm)))	
			print("%s\n" % out, file=f_out)

			current_struct = struct
			sites = []
			artefact_chis = []
			alternate_chis = []

		conserved_config = [re.sub('[a-zA-Z]+', '', x) for x in tokens[2].split(';')]
		phi, psi, res_chi1_config, chain, resname, resnum  = tokens[-6:]
		
		# define the alternate chi1 config, the artefact chi1 config and specify residue number (in the PDB file) and chain name
		all_chi1_config = ['1', '2', '3']
		artefact_chi1_configs = []
		alternate_chi1_config = None
		for config in all_chi1_config:
			if config not in conserved_config:
				# for sites with only 1 conserved chi1 configuration
				artefact_chi1_configs.append(config)
			elif config == res_chi1_config:

				continue
			else:
				alternate_chi1_config = config
		sites.append((resnum, chain))
		
		# obtain all possible combinations of chi2, chi3 configurations, angle values for alternate and artefact chi1 configuration
		key1 = resname + ";" + phi + ";" + psi
		tmp_lst = []
		for artefact_chi1_config in artefact_chi1_configs:
			tmp_lst.extend(lib_dict[key1][artefact_chi1_config])
		artefact_chis.append(tmp_lst)
		if alternate_chi1_config is not None: # for sites with only 1 conserved chi1 configuration
			alternate_chis.append(lib_dict[key1][alternate_chi1_config])

	out = '\t'.join(map(str, change_angles(options.in_dir, options.out_dir, current_struct, sites, artefact_chis, alternate_chis, options.no_charmm)))	
	if options.no_charmm:
		pass
	else:	
		print("%s\n" % out, file=f_out)
	f_out.close()

if __name__ == '__main__':
	main()


