#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
#from random import shuffle
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader

import numpy as np
import matplotlib
# force matplotlib  not to use Xwi
matplotlib.use('Agg')
import matplotlib.colors as colors


import matplotlib.pyplot as plt
import matplotlib.figure as fg
import numpy as np
import matplotlib.ticker as ticker
import scipy
import scipy.cluster.hierarchy as sch


import re
import os
import itertools

def plot_heatmap(matrix, label, outfile):
        fig = plt.figure(figsize=(matrix.shape[0]*0.6, matrix.shape[0]*0.5))
        ax = fig.add_axes([0.1,0.1,0.8,0.8])
       	# [left, bottom, width, height]

	# set ticks format on axes
        majorLocator = ticker.IndexLocator(base=1, offset=.5)
        majorFormatter = ticker.FormatStrFormatter('%s')
        ax.xaxis.set_major_locator(majorLocator)
        ax.xaxis.set_major_formatter(majorFormatter)
        ax.yaxis.set_major_locator(majorLocator)
        ax.yaxis.set_major_formatter(majorFormatter)
	ax.xaxis.tick_top()

        # plot the matrix with no interpolation
        im = ax.imshow(matrix, interpolation=None, norm=colors.LogNorm(vmin=matrix.min(), vmax=matrix.max()),
                   cmap='YlGnBu', aspect='auto')


        #label ticks on axes
        ax.set_yticklabels(label, size='xx-large', rotation=45)
	ax.set_xticklabels(label, size='xx-large', rotation=45)	

	# Plot colorbar
	axcolor = fig.add_axes([0.91,0.1,0.01,0.8])
	cbar = fig.colorbar(im, cax=axcolor)	
	# access to cbar tick labels:
	cbar.ax.tick_params(labelsize='xx-large') 
	plt.savefig(outfile, format='pdf', bbox_inches='tight', dpi=fig.dpi)
        plt.close(fig)


def main():
	usage = format_usage('''
		%prog LABELS < ORIG_MATRIX (no header or first col)
	''')
	parser = OptionParser(usage=usage)

	parser.add_option('-r', '--rep', dest='rep', action='store_true', default=False, help='represent the matrix as an  heatmap [default: %default]')	
	parser.add_option('-o', '--outfile', dest='outfile', default='outfile', help='output file [default: %default]')
	parser.add_option('-f', '--file_norm', dest='file_norm', default='', help='normalize using matrix in file [default: %default]')
	parser.add_option('-s', '--submatrix_norm', dest='submatrix_norm', action='store_true', default=False, help='normalize so that value in each submatrix amount to 1 [default: %default]')
	parser.add_option('-c', '--collapse', dest='collapse', action='store_true', default=False, help='collapse states on one axis [default: %default]')
	parser.add_option('-C', '--COLLAPSE', action='store_true', default=False, help='collapse states on both axes [default: %default]')
	parser.add_option('-I', '--IRM', dest='IRM', action='store_true', default=False, help='compute the instantaneous rate matrix (Q) using the counts [default: %default]')
	parser.add_option('-p', '--prob', dest='prob', action='store_true', default=False, help='compute the probability matrix [default: %default]')
	parser.add_option('-e', '--exchangeability', dest='exchangeability', default=False, help='calculate exchangeability matrix, option argument ins the RAW_COUNTS matrix[default: %default]')
	parser.add_option('-a', '--avg_1_sub_rate', dest='avg_1_sub_rate', default=False, help='obtain a Q matrix with avg 1 substitution per unit of time, option argument in the RAW_COUNTS matrix [default: %default]')
	parser.add_option('-A', '--AVG_1_sub_rate', dest='AVG_1_sub_rate', default=False, help='obtain a Q matrix with avg 1 substitution per unit of time, the scaling parameter is calculated from a IRM  [default: %default]')	
	parser.add_option('-n', '--neg_diag', dest='neg_diag', action='store_true', default=False, help='create a negative diagonal, rows sum to 0 [default: %default]')
	parser.add_option('-R', '--Revert', default=False, help='compute Q (0 diag) from a PAML-style lower triangular exchangeability matrix, option argument points to a file containing comma-separated frequencies [default: %default]')
	options, args = parser.parse_args()
	
	if len(args) != 1:
		exit('Unexpected argument number.')
	
	LABELS = args[0].split(",")
	d_rgx = re.compile('[0-9]$')
	collapsed_labels = sorted(set([re.sub(d_rgx, '', x) for x in LABELS]))
	size = len(LABELS)
	orig_matrix =  norm_matrix  = np.zeros((size,size), dtype='float64')	
	header = ['X']
	
	# parse original matrix
	if not options.Revert:
		i = 0
		for line in stdin:
			orig_matrix[i,:] = safe_rstrip(line).split('\t')	
			i = i + 1
	else:
		# parse a lower triangular matrix appropriately, recontruct the symmetric matrix
		# expects the first line to be [1,0]
		i = 1
		for line in stdin:
			values = [float(x) for x in safe_rstrip(line).split()]
			for j,value in enumerate(values):
				orig_matrix[i,j] = value
				orig_matrix[j,i] = value
			i = i+1
	
	# parse second matrix from file
	if options.file_norm != '':
		#introduce pseudocounts
		orig_matrix = np.where(orig_matrix==0.0, 0.5, orig_matrix)

		filename = options.file_norm
		with open(filename, 'r') as f:
			lines = f.readlines()
			# obtain labels from second matrix
			# assumes that these labels are either the same as in LABELS ("GLY" vs "GLY") or a truncation ("ARG" vs "ARG3")
			current_col_labels = safe_rstrip(lines[0]).split("\t")[1:]
			# from cell to label
			for line in lines[1:]:
				current_line_label = line.split("\t")[0]
				# for cell in matrix
				for i,value in enumerate([float(x) for x in line.split("\t")[1:]]):
					current_col_label = current_col_labels[i]
					# from label to LABEL(s) in the orig_matrix by searching LABELS elements for matches
					l_rgx = re.compile('%s.*'%re.escape(current_line_label))
					c_rgx = re.compile('%s.*'%re.escape(current_col_label))
					# obtain lists of indices that identify a block (submatrix) in the rotamer counts matrix (i.e. a 1x1, 1x2, 1x3, 2x2, 2x3 or 3x3 block)
					line_LABELS = [LABELS.index(m.group(0)) for l in LABELS for m in [l_rgx.search(l)] if m] 
					col_LABELS = [LABELS.index(m.group(0)) for l in LABELS for m in [c_rgx.search(l)] if m]

					# value of each cell in the submatrix defined by line_LABELS,col_LABELS is divided by the sum total of the submatrix, then multiplied by the value in the file_norm cell
					# this is meant to account for the fact that some states are not filtere by B_factor and have therefore higher counts in the rotamer matrix (i.e. ALA, GLY)
					# also filtering by resolution and B_factor might select against residues that are rarer in the core region
					orig_sum = 0.0
					for row in line_LABELS:
						for col in col_LABELS:
							orig_sum = orig_matrix[row][col] + orig_sum
					for row in line_LABELS:
						for col in col_LABELS:
							norm_matrix[row][col] = norm_matrix[col][row] = orig_matrix[row][col] / orig_sum * value
	
	elif options.exchangeability:
		# parse raw counts  matrix from file
		counts_matrix = np.zeros((size,size), dtype='float64')
		with open(options.exchangeability, 'r') as f:
			lines = f.readlines()
			for i,line in enumerate(lines[1:]):
				counts_matrix[i,:] = [float(x) for x in line.split("\t")[1:]]
		n = np.sum(counts_matrix)
		for i in range(len(LABELS)):
			for j in range(len(LABELS)):
				if i != j:
					# the equilibrium frequency of state j
					Pj = np.sum(counts_matrix[j, : ]) / n
					# Sij = Qij / Pj
					norm_matrix[i][j] = orig_matrix[i][j] / Pj
				else:
					# set the diagonal cells to .0
					norm_matrix[i,j] = .0
		f.close()

	elif options.Revert:
		# get the frequencies
		with open(options.Revert, 'r') as f:
			for line in f:
				freqs = [float(x) for x in safe_rstrip(line).split(",")]
			if len(freqs) != len(LABELS):
				exit("frequencies and labels should have the same dimensions")
		f.close()
		# obtain the exchange rates (Q), diagonal is 0.
		for i in range(len(LABELS)):
			for j in range(len(LABELS)):
				Pj = freqs[j]
				# Qij = Sij*Pj
				norm_matrix[i][j] = orig_matrix[i][j] * Pj

	elif options.avg_1_sub_rate:
		# the scaling parameter rho is calculated on the raw counts
		counts_matrix = np.zeros((size,size), dtype='float64')
		with open(options.avg_1_sub_rate, 'r') as f:
			lines = f.readlines()
			for i,line in enumerate(lines[1:]):
				counts_matrix[i,:] = [float(x) for x in line.split("\t")[1:]]
		n = np.sum(counts_matrix)
		# all the main diagonal counts
		d = sum([counts_matrix[x][x] for x in range(len(LABELS))])
		# the scaling factor
		rho = 1 - d / n
		# Q* has mean substitution rate = 1 at equilibrium
		norm_matrix = orig_matrix / rho
		f.close()

	elif options.AVG_1_sub_rate:
		# the scaling parameter rho is calculated on the IRM itself (in input) and the frequencies (in option argument as file, comma-separated)
		with open(options.AVG_1_sub_rate, 'r') as f:
			for line in f:
				freqs = [float(x) for x in line.split(",")]
		f.close()
		rho = sum([freqs[i]*orig_matrix[i,i]*-1 for i in range(0,size)])	
		norm_matrix = orig_matrix / rho

	elif options.neg_diag:
		# rows sum to 0
		norm_matrix = orig_matrix
		for i in range(0,size):
			norm_matrix[i,i] = (np.sum(orig_matrix[i,:]) - orig_matrix[i,i])*-1



	elif options.submatrix_norm:
		values = []
		# iterate over set of collapsed labes
		for line_label in collapsed_labels:
			# obtain corresponding full labels (either 3 or 1)
			l_rgx = re.compile('%s.*'%re.escape(line_label))
			line_LABELS = [LABELS.index(m.group(0)) for l in LABELS for m in [l_rgx.search(l)] if m]
			for col_label in collapsed_labels:
				c_rgx = re.compile('%s.*'%re.escape(col_label))
				col_LABELS = [LABELS.index(m.group(0)) for l in LABELS for m in [c_rgx.search(l)] if m]
				# value of each cell in the submatrix defined by line_LABELS,col_LABELS is divided by sum(submatrix)
				for row in line_LABELS:
					for col in col_LABELS:
						values.append(orig_matrix[row][col])					
				total = sum(values)
				for row in line_LABELS:
					for col in col_LABELS:
						norm_matrix[row][col] = norm_matrix[col][row] = orig_matrix[row][col] / total
				values = []
	
	# collapse states (labels) on x axis
	elif options.collapse:
		values = []
		collapsed_matrix = np.zeros((size,len(collapsed_labels)), dtype='float64')
		for j, col_label in enumerate(collapsed_labels):
			c_rgx = re.compile('%s.*'%re.escape(col_label))
			col_LABELS = [LABELS.index(m.group(0)) for l in LABELS for m in [c_rgx.search(l)] if m]	
			for i, line_label in enumerate(LABELS):
				for col in col_LABELS:
					values.append(orig_matrix[i][col])
				collapsed_matrix[i][j] = sum(values) 
				values = []
		norm_matrix = np.where(collapsed_matrix!=0., np.log(collapsed_matrix), 0.)	
		header.extend(collapsed_labels)
	
	# collapse rotamer states states (labels) on BOTH AXES (i.e counts in ARG1-ALA, ARG2-ALA and ARG3-ALA are summed and put into ARG-ALA)
	elif options.COLLAPSE:
		values = []
		collapsed_matrix = np.zeros((len(collapsed_labels), len(collapsed_labels)), dtype='float64')
		for i, col_label in enumerate(collapsed_labels):
			c_rgx = re.compile('%s.*'%re.escape(col_label))
			# get the indeces of the corresponding "un-collapsed" labels (i.e. for ARG: ARG1,ARG2,ARG3)
			col_LABELS = [LABELS.index(m.group(0)) for l in LABELS for m in [c_rgx.search(l)] if m]	
			for j, line_label in enumerate(collapsed_labels):
				l_rgx = re.compile('%s.*'%re.escape(line_label))
				line_LABELS = [LABELS.index(m.group(0)) for l in LABELS for m in [l_rgx.search(l)] if m]
				# sum all values in the submatrix
				for duo in itertools.product(col_LABELS, line_LABELS):
					values.append(orig_matrix[duo[0],duo[1]])
				collapsed_matrix[i][j] = np.sum(values)
				values = []
		header.extend(collapsed_labels)
	

	# compute the IRM (i.e. the  Q matrix) using (2) from Kosiol and Goldman, 2005, PMID 15483331
	elif options.IRM:
		#introduce pseudocounts
		orig_matrix = np.where(orig_matrix==0., 1, orig_matrix)
		for i,row_label in enumerate(LABELS):
			for j,col_label in enumerate(LABELS):
				norm_matrix[i,j] = orig_matrix[i,j] / sum(orig_matrix[i,:])
	
	# exchange events counts from i to j divided by sum of exchange events for i and j 
	else:
		#pseudocounts
		orig_matrix = np.where(orig_matrix==0., 1, orig_matrix)
		for i,row_label in enumerate(LABELS):
			for j,col_label in enumerate(LABELS):
				# assumes that for a label conserved in 2 leaves 1 was added to diagonal and for each difference 0.5 was added to both symmetrical cells.
				row_freq = sum(orig_matrix[i,:])*2
				col_freq = sum(orig_matrix[:,j])*2
				norm_matrix[i,j] = orig_matrix[i,j] / (row_freq + col_freq)			
	
	
	# print matrix as TSV with header and row labels or represent as heatmap
	if options.rep == False:
		if header == ['X']:
			header.extend(LABELS)

		print  '\t'.join(map(str, header))
		if options.COLLAPSE:
			for idx, symbol in enumerate(collapsed_labels):
				row = [symbol]
				row.extend([x for x in collapsed_matrix[idx,:]])
				print '\t'.join(map(str, row))
		else:
			for idx, symbol in enumerate(LABELS):
				row = [symbol]
				row.extend([x for x in norm_matrix[idx,:]])
				print '\t'.join(map(str, row))
	else:
		plot_heatmap(norm_matrix, LABELS, options.outfile)		


if __name__ == '__main__':
	main()
