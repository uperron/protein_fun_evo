#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
#from random import shuffle
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader


import numpy as np
import random

def main():
	usage = format_usage('''
		%prog LEAVES_FILE CODONS2ROTA_FILE < STDIN
		creates a PHYLIP-like "codon" alignment translating rotamer states into codons according to a provided dictionary

		.META: LEAVES_FILE
			1	sequence_name

		.META: CODONS2ROTA_FILE
			1	codon
			2	rotamer_state

		.META: STDIN
			1	Poly_id
			2	Uniprot_id
			3	multialign_site
			4	symbol
			5 	B_factor
	''')
	parser = OptionParser(usage=usage)
	parser.add_option('-r', '--restype', action='store_true', default=False, help='print a restype-only input in PHYLIP format [default: %default]')	
	options, args = parser.parse_args()
	
	if len(args) != 2:
		exit('Unexpected argument number.')
	
	# create an alignment dictionary where {'leaf':['-', 'GTA', 'AAA']}
	alignment_dict = {}
	with file(args[0], 'r') as fd:
		for line in fd:
			alignment_dict[safe_rstrip(line)] = []
	leaves_num = len(alignment_dict.keys())
	
	# create a codon to rotamer state dictionary
	codon_dict = {}
	with file(args[1], 'r') as fd:
		for line in fd:
			codon, rotamer_state = safe_rstrip(line).split('\t')
			if rotamer_state != "NA":
				codon_dict[rotamer_state] = codon

	# parse stdin, fill alignment_dict
	leaves_at_site = {}
	current_site = None
	i = 0
	for line in stdin:
		Poly_id, Uniprot_id, multialign_site, symbol, B_factor = safe_rstrip(line).split('\t')
		multialign_site = int(multialign_site)
		# within each multialign_site block
		if current_site == multialign_site:	
			leaves_at_site[Poly_id] = symbol

		# first line
		elif current_site is None:
			current_site = multialign_site
			leaves_at_site[Poly_id] = symbol

		# at the end of each multialign_site block
		elif current_site < multialign_site:
			for leaf in alignment_dict.keys():
				if leaf in leaves_at_site.keys():
					# translate rotamer state (i.e. symbol) into codon
					if not options.restype:
						codon = codon_dict[leaves_at_site[leaf]] 
					else:
						codon = leaves_at_site[leaf]
					alignment_dict[leaf].append(codon) 
				else:
					#add gap
					if not options.restype:
						alignment_dict[leaf].append('---')
					else:
						alignment_dict[leaf].append('-')
			leaves_at_site = {}
			current_site = multialign_site
			leaves_at_site[Poly_id] = symbol
			i = i + 1
		# if not sorted on multialign_site column
		elif current_site > multialign_site:
			exit('stdin not sorted on column 3')	
	
	if not options.restype:
		align_len = len(alignment_dict[random.choice(alignment_dict.keys())])*3
	else:
		align_len = len(alignment_dict[random.choice(alignment_dict.keys())])

	# PHYLIP header
	print('')	
	print "\t%d\t%d" % (leaves_num, align_len)
	print('')

	# PHYLIP alignment
	for leaf in sorted(alignment_dict.keys()):
		seq = ' '.join(map(str, alignment_dict[leaf]))
		print "%s\t%s" % (leaf, seq)
	
if __name__ == '__main__':
	main()

