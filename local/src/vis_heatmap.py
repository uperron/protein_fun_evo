#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
from uBio import Polypeptide

import matplotlib
# force matplotlib  not to use Xwi
matplotlib.use('Agg')

import matplotlib.pyplot as plt
import matplotlib.figure as fg
import numpy as np
import matplotlib.ticker as ticker


from ete3 import Tree
import scipy.cluster.hierarchy as sch
import scipy.spatial.distance
from itertools import combinations

from scipy.spatial.distance import _distance_wrap



def plot_heatmap(z_grid, r_grid, sites_num, sequs_num, xlbls, ylbls, leaves, tree, outfile):
	w, h = fg.figaspect(float(sequs_num) / float(sites_num))
 	fig = plt.figure(figsize=(w*10, h*10))
	ax1 = plt.subplot2grid((1,3), (0,0), colspan=1)
	ax2 = plt.subplot2grid((1,3), (0,1), colspan=2)

	# build a distance matrix, transform it into a distance vector, plot the dendrogram
	idx_dict = {}
	for i, leaf in enumerate(leaves):
		idx_dict[leaf] = i
	idx_labels = [idx_dict.keys()[idx_dict.values().index(i)] for i in range(0, len(idx_dict))]
	dmat = np.zeros((len(leaves),len(leaves)))
	
	for l1,l2 in combinations(leaves,2):
		d = tree.get_distance(l1,l2)
		dmat[idx_dict[l1],idx_dict[l2]] = dmat[idx_dict[l2],idx_dict[l1]] = d
	s = dmat.shape
	d = s[0]
	vect = np.zeros(((d * (d - 1) / 2),), dtype=np.double)
	dvect =  _distance_wrap.to_vector_from_squareform_wrap(dmat, vect)
	schlink = sch.linkage(vect, method='average', metric='euclidean')
	dendro = sch.dendrogram(schlink,labels=idx_labels, orientation='left', leaf_rotation=90, leaf_font_size=4, distance_sort=True, show_leaf_counts=True, ax=ax1)


	# set ticks format on y axis
	majorLocator = ticker.LinearLocator(numticks=sequs_num)
	majorFormatter = ticker.FormatStrFormatter('%s')
	ax2.yaxis.set_major_locator(majorLocator)
	ax2.yaxis.set_major_formatter(majorFormatter)

	# plot the value matrix  with no interpolation
    	ax2.imshow(z_grid, interpolation=None, cmap='viridis', alpha=0.8)
    	ax2.table(cellText=r_grid,loc='center',cellLoc='center',bbox=[0,0,1,1], zorder=0)

	#set axes format
	ax1.spines['top'].set_visible(False)
	ax1.spines['right'].set_visible(False)
	ax1.spines['bottom'].set_visible(False)
	ax1.spines['left'].set_visible(False)	
	ax1.yaxis.set_ticks_position('right')
	ax1.axes.get_xaxis().set_visible(False)	
	
	#label ticks on axes using site_id (x) and sequence_id (y)
	ax2.set_xticklabels(xlbls)
	ax2.set_yticklabels(ylbls)
	plt.savefig(outfile, format='pdf', dpi=fig.dpi)

def main():
	usage = format_usage('''
		%prog SITES_NUM SEQ_NUM TREE_FILE < STDIN
		draws a heatmap and a phylogenetic tree considering values and residue categories (B_branched, non_B_branched, negative, positive)
		META: STDIN
		1	site_id (numerical)
		2	sequence_id
		3	resname
		4	value
	''')
	parser = OptionParser(usage=usage)
	parser.add_option('-o', '--outfile', type=str, dest='outfile', default='heatmap_outfile', help='output filename [default: %default]', metavar='CUTOFF')
	options, args = parser.parse_args()
	
	if len(args) != 3:
		exit('Unexpected argument number.')
	
	# read the tree from file	
	sites_num,sequs_num,tree = int(args[0]),int(args[1]),Tree(args[2])
	#ts = TreeStyle()
        #ts.show_leaf_name=True
        #ts.show_branch_length=True
        #ts.show_branch_support=True

	B_branched = ['VAL', 'ILE',  'THR']
	non_B_branched = ["CYS", "PHE", "LEU", "MET", "ASN", "PRO", "GLN", "SER", "TRP", "TYR"]
	negative = ["ASP", "GLU"]
	positive = ["ARG", "HIS", "LYS"]
	seq_lst = []
	site_lst = []
	z_grid = np.empty(shape=[sequs_num, sites_num])
	z_grid.fill(np.nan)
	r_grid = np.empty(shape=[sequs_num, sites_num], dtype=np.str)
	r_grid.fill('')
	for line in stdin:
                tokens = safe_rstrip(line).split('\t')
                site_id = int(tokens[0])
                seq_id = tokens[1]
                resname = tokens[2]
		config = int(tokens[3])
		if seq_id not in seq_lst:
			seq_lst.append(seq_id)

		if site_id not in site_lst:
			site_lst.append(site_id)
		
	
		# offset config values in order to have widely spaced color ranges for different residue categories
		if resname in B_branched:
			z = config
		elif resname in non_B_branched:
			z = config + 100
		elif resname in positive:
			z = config + 200	
		elif resname in negative:
			z = config + 300

		
		if config == 1:
			z = z+25
		elif config == 2:
			z = z+50
		elif config == 3:
			z = z+75

		# create 2 matrixes for config values and residues
		i = site_lst.index(site_id)
		j = seq_lst.index(seq_id)
		z_grid[j][i] = z
		r_grid[j][i] = Polypeptide.three_to_one(resname)

	leaves = []
	for i in seq_lst:
		leaf = i.split('-')[4]
		if leaf not in leaves:
			leaves.append(leaf)
		continue
	plot_heatmap(z_grid, r_grid, sites_num, sequs_num, site_lst, seq_lst, leaves, tree, options.outfile)		
			
	
if __name__ == '__main__':
	main()

