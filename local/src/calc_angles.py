#!/usr/bin/env python
from __future__ import with_statement
import re
import os
import warnings
from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
from Bio import SeqIO
from StringIO import StringIO
from uBio import MMCIFParser, Polypeptide, StructureAlignment
from Bio import AlignIO
from Bio.Align import MultipleSeqAlignment
from collections import OrderedDict
from Bio.Data import SCOPData
#from random import shuffle
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader

# takes a multialignment filename and searches for a string, returns a matching Seq object found in multialignment
def search_4_poly(align_filename, search_str):
	multialig = AlignIO.parse(align_filename, "fasta")
	for alignment in multialig:
		for record in alignment:
			if re.search(search_str, record.id):
				return record


def get_poly(struct_filename, align_filename, phi_psi, tau, theta, PDBmap, PDBmap_dict):
	## takes a .cif filename and a multialignment filename, returns a dictionary of tuples containing a Polypeptide object and a Sequence object along with angle lists if requested
	ppb = Polypeptide.PPBuilder()
	bparser = MMCIFParser.MMCIFParser()	
	structure_id = os.path.split(struct_filename)[1].replace('.cif', '') 
	structure = bparser.get_structure(structure_id, struct_filename)
	aligned_poly = {}
	for model in structure:
		model_id = str(model.get_id())
		for chain in model:
			chain_id = str(chain.get_id())
			# if dealing with Pfam allignments, limit polypeptides to the range provided by the Pfam mapping file.
			if PDBmap is not None:
				res_lst = []
				k = structure_id + chain_id
				if k in PDBmap_dict:
					for duo in PDBmap_dict[k]:
						res_dict = {}
						for residue in chain:
							resnum = int(residue.get_pdb_seq_num())
							res_dict[resnum] = residue
						for resnum in OrderedDict(sorted(res_dict.items(), key=lambda t: t[0])).keys():
							if resnum >= duo[0] and resnum <= duo[1]:
								residue = res_dict[resnum]
								res_lst.append(residue)
							else:
								continue
						if res_lst != []:
							entity = res_lst
						else:
							continue
						entity = res_lst
						for pp in ppb.build_peptides(entity, aa_only=False):
							if phi_psi:
								phi_psi_lst = pp.get_phi_psi_list()
							else:
								phi_psi_lst = None
							if tau:
								tau_lst = pp.get_tau_list()
							else:
								tau_lst = None
							if theta:
								theta_lst = pp.get_theta_list()
							else:
								theta_lst = None
							pp_id = str(pp).replace(" ", "_")
							search_str = '-'.join([structure_id, model_id, chain_id, pp_id])
							record = search_4_poly(align_filename, search_str)
							if record is None:
								warnings.warn('%s not found in %s' %(search_str, align_filename))
							else:
								aligned_poly[record.id]=(pp, record, phi_psi_lst, tau_lst, theta_lst)
		
				else:
					continue

			else:
				entity = chain

				for pp in ppb.build_peptides(entity, aa_only=False):
					if phi_psi:
						phi_psi_lst = pp.get_phi_psi_list()
					else:
						phi_psi_lst = None
					if tau:
						tau_lst = pp.get_tau_list()
					else:
						tau_lst = None
					if theta:
						theta_lst = pp.get_theta_list()
					else:
						theta_lst = None
					pp_id = str(pp).replace(" ", "_")
					search_str = '-'.join([structure_id, model_id, chain_id, pp_id])
					record = search_4_poly(align_filename, search_str)
					if record is None:
						warnings.warn('%s not found in %s' %(search_str, align_filename))
					else:
						aligned_poly[record.id]=(pp, record, phi_psi_lst, tau_lst, theta_lst)
			return aligned_poly

def main():
	usage = format_usage('''
		%prog ALIGMENT STRUCTURE1 STRUCTURE2 [...]
		Given a multi .fasta alignment (of AA sequences) and some .cif files
		performs a multiple "structural mapping"
		(i.e. mapping residue objects to sites in AA sequence alignment) of all polypetide sequences 
		in .cif files and outputs
		requested angle values for corresponding residues.
	''')
	parser = OptionParser(usage=usage)
	
	parser.add_option('-p', '--phi_psi', dest='phi_psi', action='store_true', default=False, help='calculate phi and psi angles [default: %default]', metavar='PHI_PSI')
	parser.add_option('-t', '--tau', dest='tau', action='store_true', default=False, help='calculate tau angles [default: %default]', metavar='TAU')
	parser.add_option('-T', '--Theta', dest='Theta', action='store_true', default=False, help='calculate theta angles [default: %default]', metavar='THETA')
	parser.add_option('-P', '--PDBmap', type=str, dest='PDBmap', default=None, help='pdb residue number map [default: %default]')
	parser.add_option('-b', '--bfactor', dest='bfactor', default=False, action="store_true", help='print mean bfactor for residue [default: %default]')
	## option to filter on disordered res, unbonded res
	## option to calculate residue depth
	parser.add_option('-v', '--verbose', dest='verbose', action='store_true', default=False, help='some help [default: %default]')

	options, args = parser.parse_args()
	
	if len(args) < 3:
		exit('Unexpected argument number.')

	filenames = args[1:]
	multialignment_fasta = args[0]
	PDBmap_dict = {}

	# create a dictionary accessed via PDB_id and chain, values are tuples of residue coordinates for the Pfam domain 
	# i.e. {PDB_IDCHAIN_ID : (start residue coord, stop residue coord)}
	if options.PDBmap is not None:
		with file(options.PDBmap, 'r') as fd:
			for line in fd:
				tokens = safe_rstrip(line).split('\t')
				key = tokens[0] + tokens[1]
				start, stop = int(tokens[2]), int(tokens[3])
				PDBmap_dict.setdefault(key, []).append((start, stop))


	## map residues from each polypeptide sequence in each structure to residues in every other polypeptide sequence in every other structure respecting  existing  multiple sequence alignment
	for i in range(len(filenames)):
		filename1 = filenames[i]
		for j in range(i + 1, len(filenames)):
			filename2 = filenames[j]
			sequences1=get_poly(filename1, multialignment_fasta, options.phi_psi, options.tau, options.Theta, options.PDBmap, PDBmap_dict)
			sequences2=get_poly(filename2, multialignment_fasta, options.phi_psi, options.tau, options.Theta, options.PDBmap, PDBmap_dict)
		
			for seq1 in sequences1.keys():
				for seq2 in sequences2.keys():
					sliced_align = MultipleSeqAlignment([sequences1[seq1][1], sequences2[seq2][1]])
					## perform pairwise structural alignment then iterate over corresponding residues and calculate angles
					structural_alignment = StructureAlignment.StructureAlignment(sliced_align, sequences1[seq1][0], sequences2[seq2][0])	
					for duo in structural_alignment.get_iterator():
						if duo[0] is not None and duo[1] is not None:
							structure_id1 = os.path.split(filename1)[1].strip('.cif')
							structure_id2 = os.path.split(filename2)[1].strip('.cif')
							res1 = duo[0]
							res2 = duo[1]
							# this site id is relative to site position in the multialignment
							align_site_id = duo[4]
							PDB_site_id1 = res1.get_pdb_seq_num()
							PDB_site_id2 = res2.get_pdb_seq_num()
							resname1 = res1.get_resname()
							resname2 = res2.get_resname()
							# obtain list of chi angles from current Residue objects
							chi1 = ",".join(map(str, res1.get_chi()))
							chi2 = ",".join(map(str, res2.get_chi()))
							opt_output = []
							
							# obtain list of mean bfactor for atoms in each rotamer
             						if options.bfactor:
								bfactor1 = res1.get_mean_bfactor()							
								bfactor2 = res2.get_mean_bfactor()

							## obtain other lists of angles from current Polypeptide object
							if options.phi_psi:
								current_p1 = duo[2]
								current_p2 = duo[3]
								phi_psi1 = sequences1[seq1][2][current_p1]
								phi_psi2 = sequences2[seq2][2][current_p2]
								opt_output.append("\t".join(map(str, [",".join(map(str, phi_psi1)), ",".join(map(str, phi_psi2))])))
							if options.bfactor:
								opt_output.append("\t".join(map(str, [",".join(map(str, bfactor1)), ",".join(map(str, bfactor2))])))
							if options.tau:
								opt_output.append(";".join(map(str, [",".join(map(str, sequences1[seq1][3])), ",".join(map(str, sequences2[seq1][3]))])))
							if options.Theta:
								opt_output.append(";".join(map(str, [",".join(map(str, sequences1[seq1][4])), ",".join(map(str, sequences2[seq1][4]))])))
							if opt_output != []:
								opt_output.extend([PDB_site_id1, PDB_site_id2])
								output = [";".join(map(str, [seq1, seq2])), structure_id1, structure_id2, align_site_id, resname1, resname2,  chi1, chi2] + opt_output
								print("\t".join(map(str, output)))
							else:
								output = [";".join(map(str, [seq1, seq2])), structure_id1, structure_id2, align_site_id,  resname1, resname2,  chi1, chi2]
								print("\t".join(map(str, output)))


if __name__ == '__main__':
	main()

