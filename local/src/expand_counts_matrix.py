#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
#from random import shuffle
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader
import itertools
import numpy as np
import re

def main():
	usage = format_usage('''
		%prog ORIG_LABELS EXP_LABELS < ORIG_COUNTS_MATRIX
	''')
	parser = OptionParser(usage=usage)
	
	parser.add_option('-f', '--freq_file', default=False, help='also expand frequencies from file  [default: %default]')
	parser.add_option('-o', '--orig_count', action='store_true', default=False, help='put the original counts in each expanded cell  [default: %default]')
	parser.add_option('-F', '--Freq_exp', default=False, help='distribute counts in each expanded cell according to freq_i * freq_j,requires expanded frequencies file [default: %default]')
	options, args = parser.parse_args()
	
	if len(args) != 2:
		exit('Unexpected argument number.')
	

	ORIG_LABELS = args[0].split(",")
	orig_size = len(ORIG_LABELS)
	EXP_LABELS = args[1].split(",")
	exp_size = len(EXP_LABELS)
	orig_matrix = np.zeros((orig_size,orig_size), dtype=float)	
	expanded_matrix = np.zeros((exp_size,exp_size), dtype=float)
	header = ['X'] + EXP_LABELS
	
	# parse the orginal full matrix
	i = 0
	for line in stdin:
		values = [float(x) for x in line.rstrip("\n").rstrip("\r").split()]
		for j,value in enumerate(values):
			orig_matrix[i,j] = value
		i = i+1

	if options.freq_file:
		EXP_FREQS = [0.0]*exp_size
		with open(options.freq_file, "r") as fd:
			for line in fd:
				ORIG_FREQS = [float(x) for x in safe_rstrip(line).split(",")]
			fd.close()
		if len(ORIG_FREQS) != len(ORIG_LABELS):
			exit("frequencies and labels should have the same dimensions")
	
	if options.Freq_exp:
		with open(options.Freq_exp, "r") as fd:
			for line in fd.readlines()[1:]:
				EXP_FREQS = [float(x) for x in safe_rstrip(line).split("\t")]
			fd.close()

	# map ORIG_LABELS into EXP_LABELS
	for orig_col_index,orig_col_label in enumerate(ORIG_LABELS):
		col_rgx = re.compile('%s.*'%re.escape(orig_col_label))
		exp_col_indeces =  [EXP_LABELS.index(m.group(0)) for l in EXP_LABELS  for m in [col_rgx.search(l)] if m] 
		
		# divide the original frequency by the number of corresponding expanded states
		if options.freq_file:
			for idx in exp_col_indeces:
				EXP_FREQS[idx] = ORIG_FREQS[orig_col_index] / len(exp_col_indeces)

		for orig_row_index,orig_row_label in enumerate(ORIG_LABELS):
			row_rgx = re.compile('%s.*'%re.escape(orig_row_label))
			exp_row_indeces =  [EXP_LABELS.index(m.group(0)) for l in EXP_LABELS for m in [row_rgx.search(l)] if m] 
			submatrix_size = len(exp_col_indeces) * len(exp_row_indeces)
			for i,j in list(itertools.product(exp_col_indeces, exp_row_indeces)):
				
				if options.orig_count:
					expanded_matrix[i][j] = orig_matrix[orig_col_index, orig_row_index]
				
				if options.Freq_exp:
					# the relative frequencies of expanded states i and j within the original un-expanded state
					rel_freq_i = EXP_FREQS[i] / sum([EXP_FREQS[x] for x in exp_col_indeces])
					rel_freq_j =  EXP_FREQS[j] / sum([EXP_FREQS[x] for x in exp_row_indeces])
					# distribute counts accordind to the product of these relative frequencies
					expanded_matrix[i][j] = rel_freq_i * rel_freq_j  * orig_matrix[orig_col_index, orig_row_index]
				
				# uniformly divide the original counts across cells in the submatrix
				else:
					expanded_matrix[i][j] = orig_matrix[orig_col_index, orig_row_index] / submatrix_size


	# print the expanded matrix including header and row labels
	print  '\t'.join(map(str, header))
	for idx, symbol in enumerate(EXP_LABELS):
		row = [symbol]
		row.extend([x for x in expanded_matrix[idx,:]])
		print '\t'.join(map(str, row))
	if options.freq_file:
		print
		print '\t'.join(map(str, EXP_FREQS))

if __name__ == '__main__':
	main()

