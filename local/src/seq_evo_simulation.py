#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
import random
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader

import numpy as np
import itertools
import math

import matplotlib
# force matplotlib  not to use Xwi
matplotlib.use('Agg')
import matplotlib.colors as colors

import matplotlib.pyplot as plt
import matplotlib.figure as fg
import matplotlib.ticker as ticker
import scipy.linalg

import re
import os
import distance
from scipy.optimize import curve_fit



def main():
	usage = format_usage('''
		%prog LABELS CODON2ROTAMER_DICT < ORIG_MATRIX
	''')
	parser = OptionParser(usage=usage)
	parser.add_option('-c', '--comp', default=False, help='a second matrix to perform comparison [default: %default]')	
	parser.add_option('-o', '--outfile', dest='outfile', default='outfile', help='output file [default: %default]')
	parser.add_option('-s', '--seq', action='store_true', default=False, help='output simulated sequences  [default: %default]')
	parser.add_option('-n', '--no_plot', default=False, action='store_true', help='do not plot, output coordinates (t, hamming distance / seq_length)  [default: %default]')
	parser.add_option('-u', '--userseq', default=False, help='custom sequence, symbols must be comma-separated [default: %default]')
	parser.add_option('-f', '--file_seq', default=False, help='custom sequence, symbols must be space-separated [default: %default]')
	parser.add_option('-d', '--dictionary', dest='dictionary', default=False, help='translate COMP sequences to codons using a second dictionary file [default: %default]')
	parser.add_option('-t', '--translate', action='store_true', default=False, help='3-to-1 translation for AA sequences in comp model  [default: %default]')
	parser.add_option('-T', '--Timepoints', default=False, help='custom timepoints, symbols must be comma-separated [default: %default]')
	parser.add_option('-r', '--runs', type="int", default=False, help='number of simulation runs to be performed [default: 5]')
	parser.add_option('-F', '--Freq_file', default=False, help='use frequencies from file to generate the original (i.e. ancestor) sequence at every timepoint [default: %default]')
	parser.add_option('-l', '--length', type="int", default=False, help='length of sequence to be generated [default: 100]')
	options, args = parser.parse_args()
	
	if len(args) != 2:
		exit('Unexpected argument number.')

	LABELS = args[0].split(",")
	d_rgx = re.compile('[0-9]$')
	nstates = len(LABELS)
	orig_matrix = np.zeros((nstates,nstates), dtype='float64')
	header = ['X']
	
	
	# parse original matrix
	i = 0
	for line in stdin:
		orig_matrix[i,:] = safe_rstrip(line).split('\t')
		i = i + 1

	# create a codon to rotamer state dictionary
	codon_dict = {}
	with file(args[1], 'r') as fd:
		for line in fd:
			codon, rotamer_state = safe_rstrip(line).split('\t')
			if rotamer_state != "NA":
				codon_dict[rotamer_state] = codon


	# create a AA to rotamer state dictionary
	AA_dict = {}
	if options.dictionary:
		with file(options.dictionary, 'r') as fd:
			for line in fd:
				codon, state = safe_rstrip(line).split('\t')
				if state != "NA":
					AA_dict[state] = codon
		
	def seq_gen(freqs, seq_len, states):
		seq = np.random.choice(states, seq_len, p=freqs)
		return seq
	
	# specify sequence length if one has to be generated
	if options.length:
		seq_len = options.length
	else:
		seq_len = 100

	# from IRM to Q with lines that sum to 0
	for i in range(0,nstates):
		orig_matrix[i,i] = (np.sum(orig_matrix[i,:]) - orig_matrix[i,i])*-1

	if options.userseq:
		orig_seq = options.userseq.split(",")
		seq_len = len(orig_seq)
		for symbol in orig_seq:
			if symbol not in LABELS:
				exit('Unexpected symbol %s found in userseq') % (symbol)
	elif options.file_seq:
		with open(options.file_seq, 'r') as fs:
			for line in fs:
				orig_seq = safe_rstrip(line).split(' ')
		seq_len = len(orig_seq)
		for symbol in orig_seq:
			if symbol not in LABELS:
				exit('Unexpected symbol %s found in file_seq') % (symbol)
	elif options.Freq_file:
		# generate the ancestor sequence according to  specified frequencies
		with open(options.Freq_file, 'r') as fs:
			for line in fs:
				freqs = [float(x) for x in safe_rstrip(line).split('\t')]
		orig_seq = seq_gen(freqs, seq_len, LABELS)
	else:
		# generate a random sequence using LABELS
		orig_seq = np.random.choice(LABELS, seq_len, replace=True)

	if options.comp:
		# parse second matrix for comparison
		with open(options.comp, 'r') as f:
			lines = f.readlines()
			labels = lines[0].rstrip("\n").split("\t")[1:]
			comp_matrix = np.zeros((len(labels), len(labels)), dtype='float64')
			i = 0
			for line in lines[1:]:
				comp_matrix[i,:] = safe_rstrip(line).split('\t')[1:]
				i = i + 1
		for i in range(0,len(labels)):
			comp_matrix[i,i] = (np.sum(comp_matrix[i,:]) - comp_matrix[i,i])*-1
		# translate random sequence from rotamer states to AA (3 letter)
		d_rgx = re.compile('[0-9]$')
		comp_orig_seq = np.array([re.sub(d_rgx, '', x) for x in orig_seq])

	# specify timeponts
	if options.Timepoints: 
		timepoints = [float(x) for x in options.Timepoints.split(",")]
	else:
		timepoints = np.arange(0, 1.5, 0.05)
	
	# specify the number of simulation runs
	if options.runs:
		runs = options.runs
	else:
		runs = 5

	def simulate_and_plot(orig_matrix, orig_seq, states, output_sequences, no_plot, codon_dict):
		xdata = []
		ydata = []
		align_len = len(orig_seq)
	
		for j in range(0, runs):
			for t in timepoints:	
				if options.Freq_file:
					# generate an ancestor sequence at every timepoint to avoid any composition bias
					orig_seq = seq_gen(freqs, seq_len, LABELS)
				new_seq = np.zeros(seq_len, dtype=object)
				# calculate P(t)
				Pt_matrix = scipy.linalg.expm(t* orig_matrix)
				for site,current_state in enumerate(orig_seq):
					P_state = Pt_matrix[states.index(current_state),:]
					# simulate a change of state according to P(t)
					new_state = np.random.choice(states, 1, p=P_state)[0]
					new_seq[site] = new_state
				# Hamming distance from the original / sequence length (i.e. proportion of different sites p)
				h_dist = float(distance.hamming(orig_seq, new_seq)) / len(orig_seq)	
				xdata.append(float(t))
				ydata.append(h_dist)

				if codon_dict != {} and output_sequences:
					#translate states into codons
					orig_seq_out = [codon_dict[state] for state in orig_seq]
					new_seq_out = [codon_dict[state] for state in new_seq]
					# update alignment length
					align_len = len(orig_seq)*3
				else:
					orig_seq_out = orig_seq
					new_seq_out = new_seq

				if output_sequences:
					# output a PHYLIP-style alignment of each simulated sequence against the original sequence
					# PHYLIP header
					# codonPhyML and iqtree require at least 3 sequences
					print len(states)
					print "%d\t%d\t%d" % (len(states), 3, align_len)
					print len(states)
					# PHYLIP alignment, states are mapped to codons
					print "%d\tOrig_seq\t%s" % (len(states),' '.join(map(str, orig_seq_out)))
					print "%d\tNew_seq-%f-%d\t%s" % (len(states),t,j,' '.join(map(str, new_seq_out)))
					# iqtree needs unique sequence names
					print "%d\tOrig_seq_1\t%s" % (len(states),' '.join(map(str, orig_seq_out)))
					print len(states)

				if no_plot: #print scatterplot coordinates to file
					print "%d\t%f\t%f" % (len(states),t, h_dist)
		
		if not output_sequences and not no_plot:
			# Scatter plot
			plt.scatter(xdata, ydata, alpha=0.5, label='$%dx%d$' % (len(states), len(states)))
			
			# define the model log function
			def func(z, a, b, c):
				out = [] 
				for value in z:
					out.append(a * math.log(value, abs(b) + 1) + c)
				return out
				
			# Add fit a log curve to the scatter plot
			axes = plt.gca()
			X_plot = np.linspace(0.01,axes.get_xlim()[1], len(ydata))
			popt, pcov = curve_fit(func, X_plot, np.array(ydata))	
			plt.plot(X_plot, func(X_plot, *popt), '-')

	fig = plt.figure(figsize=(8,8))
	# simulate using the main model
	simulate_and_plot(orig_matrix, orig_seq, LABELS, options.seq, options.no_plot, codon_dict)
	
	if options.comp:
		# translate 3-letter AA inddto 1-letter AA
		if options.translate:
			three_to_one_dict = {'CYS': 'C', 'ASP': 'D', 'SER': 'S', 'GLN': 'Q', 'LYS': 'K', 'ILE': 'I', 'PRO': 'P', 'THR': 'T', 'PHE': 'F', 'ALA': 'A', 'GLY': 'G', 'HIS': 'H', 'GLU': 'E', 'LEU': 'L', 'ARG': 'R', 'TRP': 'W', 'VAL': 'V', 'ASN': 'N', 'TYR': 'Y', 'MET': 'M'}	
			one_labels = [three_to_one_dict[x] for x in labels]
			one_comp_orig_seq = [three_to_one_dict[x] for x in comp_orig_seq]
			simulate_and_plot(comp_matrix, one_comp_orig_seq, one_labels, options.seq, options.no_plot, AA_dict)
		else:
			# simulate using the comparison model
			simulate_and_plot(comp_matrix, comp_orig_seq, labels, options.seq, options.no_plot, AA_dict)

	if not options.seq and not options.no_plot:
		plt.xlabel('t')
		plt.ylabel('Hamming distance')
		plt.legend()
		plt.savefig(options.outfile, format='pdf', bbox_inches='tight', dpi=fig.dpi)		
	plt.close(fig)	

if __name__ == '__main__':
	main()
