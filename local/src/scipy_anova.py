#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage


import scipy.stats as stats
import re

#from random import shuffle
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader



def main():
	usage = format_usage('''
		%prog LABELS < STDIN
		.META: stdin
			1	LABEL_COL or STRING_COL
			2	value
	''')
	parser = OptionParser(usage=usage)
	
	parser.add_option('-p', '--dump-params', dest='params_file', help='some help FILE [default: %default]', metavar='FILE')
	parser.add_option('-v', '--verbose', dest='verbose', action='store_true', default=False, help='some help [default: %default]')

	options, args = parser.parse_args()
	

	if len(args) != 1:
		 exit('Unexpected argument number.')

	labels = args[0].split(',')
	d = {}
	
	for line in stdin:
		string_col, value  = safe_rstrip(line).split('\t')
		for label in labels:
			if re.search(label, string_col):
				d.setdefault(label, []).append(float(value))
	
	F_val, p_val = stats.f_oneway(*[v for v in d.values()])
	print "%f\t%f" % (F_val, p_val)	
								

if __name__ == '__main__':
	main()

