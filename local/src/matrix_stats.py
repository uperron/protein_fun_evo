#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
import random
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader

import numpy as np
import itertools
import math

import matplotlib
# force matplotlib  not to use Xwi
matplotlib.use('Agg')
import matplotlib.colors as colors

import matplotlib.pyplot as plt
import matplotlib.figure as fg
import matplotlib.ticker as ticker
from scipy.stats import chisquare
from scipy.stats import chi2_contingency

import re
import os
import collections

AAdict = {'CYS': 'special', 'GLN': 'uncharged', 'HIS': 'positive', 'SER': 'uncharged', 
'VAL': 'hydrophobic', 'LYS': 'positive', 'ILE': 'hydrophobic', 'ASN': 'uncharged', 'GLY': 'special', 
'THR': 'uncharged', 'PHE': 'aromatic', 'ALA': 'hydrophobic', 'MET': 'hydrophobic', 'ASP': 'negative', 'SEC': 'special', 
'LEU': 'hydrophobic', 'ARG': 'positive', 'TRP': 'aromatic', 'PRO': 'special', 'GLU': 'negative', 'TYR': 'aromatic'
}


def plot_scatterplot(Vcramers, diag_ratios, xy_labels, colorseq, outfile):
	fig = plt.figure(figsize=(10, 11))
        ax = fig.add_axes([0.1,0.1,0.9,0.9])
	ax.set_xlabel('Cramer\'s V', fontsize='xx-large')
	ax.set_ylabel('Diagonal ratio', fontsize='xx-large')	
		
	
	plt.scatter(Vcramers, diag_ratios, s=100, c=colorseq, alpha=0.5)
	for label, x_val, y_val in zip(xy_labels, Vcramers, diag_ratios):
		plt.annotate(label, xy=(x_val, y_val), xytext=(-15, 15), textcoords='offset points', ha='right', va='bottom', arrowprops=dict(arrowstyle = '-', connectionstyle='arc3,rad=0'))

	ax.set_aspect(1./ax.get_data_ratio()) # make axes square
	
	plt.savefig(outfile, format='pdf', bbox_inches='tight', dpi=fig.dpi)
        plt.close(fig)




def plot_heatmap(matrix, label, outfile):
        fig = plt.figure(figsize=(matrix.shape[0]*0.6, matrix.shape[0]*0.5))
        ax = fig.add_axes([0.1,0.1,0.8,0.8])
       	# [left, bottom, width, height]

	# set ticks format on axes
        majorLocator = ticker.IndexLocator(base=1, offset=.5)
        majorFormatter = ticker.FormatStrFormatter('%s')
        ax.xaxis.set_major_locator(majorLocator)
        ax.xaxis.set_major_formatter(majorFormatter)
        ax.yaxis.set_major_locator(majorLocator)
        ax.yaxis.set_major_formatter(majorFormatter)
	ax.xaxis.tick_top()

        # plot the matrix with no interpolation
        im = ax.imshow(matrix, interpolation=None, cmap='RdYlGn_r', aspect='auto')


        #label ticks on axes
        ax.set_yticklabels(label, size='xx-large', rotation=45)
	ax.set_xticklabels(label, size='xx-large', rotation=45)	

	# Plot colorbar
	axcolor = fig.add_axes([0.91,0.1,0.01,0.8])
	cbar = fig.colorbar(im, cax=axcolor)	
	# access to cbar tick labels:
	cbar.ax.tick_params(labelsize='xx-large') 
	plt.savefig(outfile, format='pdf', bbox_inches='tight', dpi=fig.dpi)
        plt.close(fig)



def main():
	usage = format_usage('''
		%prog LABELS < ORIG_MATRIX
	''')
	parser = OptionParser(usage=usage)
	parser.add_option('-r', '--rep', dest='rep', action='store_true', default=False, help='represent the matrix as an  heatmap [default: %default]')	
	parser.add_option('-l', '--lst', dest='lst', action='store_true', default=False, help='prints a list of labelled values (ARG	ASP	0.003)[default: %default]')
	parser.add_option('-o', '--outfile', dest='outfile', default='outfile', help='output file [default: %default]')
	parser.add_option('-V', '--Vcramer', action='store_true', default=False, help='peform Cramers V to assess associations b/w variables (x,y) in each submatrix')	
	parser.add_option('-s', '--scat',  action='store_true', default=False, help='represent the list as a scatterplot [default: %default]')
	options, args = parser.parse_args()
	
	if len(args) != 1:
		exit('Unexpected argument number.')

	LABELS = args[0].split(",")
	d_rgx = re.compile('[0-9]$')
	collapsed_labels = sorted(set([re.sub(d_rgx, '', x) for x in LABELS]))
	size = len(LABELS)
	#introduce pseudocounts
	orig_matrix = np.zeros((size,size), dtype='float64')
	out_matrix = np.zeros((len(collapsed_labels), len(collapsed_labels)), dtype='float64') 
	header = ['X']
	Vcramers,diag_ratios,xy_labels,colorseq = [[], [], [], []]	
	
	# number of chi tests performed (one per 3x3 submatrix)
	numtest = len([c for c in itertools.combinations([l for l in collapsed_labels if l not in ["PRO", "ALA", "GLY"]], 2)])
	
	# parse original matrix
	i = 0
	for line in stdin:
		orig_matrix[i,:] = safe_rstrip(line).split('\t')
		i = i + 1

	#  discard all but 3x3 blocks		
	for l1, l2 in itertools.combinations([l for l in collapsed_labels if l not in ["PRO", "ALA", "GLY"]], 2):
		out_idx1 = collapsed_labels.index(l1)
		out_idx2 = collapsed_labels.index(l2)
		rgx1 = re.compile('%s.*'%re.escape(l1))
		idx1 = [LABELS.index(m.group(0)) for l in LABELS for m in [rgx1.search(l)] if m]
		rgx2 = re.compile('%s.*'%re.escape(l2))
		idx2 = [LABELS.index(m.group(0)) for l in LABELS for m in [rgx2.search(l)] if m]
		idxgrid = np.ix_(idx1, idx2)
		# extract the block from the matrix
		block = orig_matrix[idxgrid]
		nrows, ncols = block.shape
		n = np.sum(block)
		# flatten the block into  a list
		obs = list(orig_matrix[idxgrid].flatten())
		# create a dictionary: k = counts in cell, v = index of cell, this is done to handle cells with duplicate values
		dups = collections.defaultdict(list)
		for i, e in enumerate(obs):
			dups[e].append(i)
		pval = 1
		Tstat = 0	
		
		# discard all but 3x3 blocks
		if len(obs) == 9:
			# Cramer's V (with bias correction as per Bergsma and Wicher 2013) 
			if options.Vcramer:
				phi_coeff = chi2_contingency(block)[0] / n 
				#phi_coeff = chisquare(obs)[0] / n
				phi_coeff_corr = max((0, phi_coeff - (ncols - 1)*(nrows - 1) / (n-1)))
				if nrows == 1 or ncols == 1:
					maxdim = max(nrows, ncols)
					Vcramer_corr = (phi_coeff_corr / (maxdim -1))**0.5
				else:
					r_corr = nrows - (nrows-1)**2 / (n -1)
					k_corr = ncols - (ncols-1)**2 / (n -1)
					Vcramer_corr = (phi_coeff_corr / min(((k_corr-1), (r_corr-1))))**0.5

				# the proportion of counts on the diagonal
				diag = []
				for key, value in sorted(dups.iteritems()):
					for idx in value:
						if idx in [0,4,8]: # get counts in cells with indeces that correspond to diagonal positions in the flattened array
							diag.append(key)
				diag_ratio = sum(diag) / n # an estimate of the _proportion_ of interchanges from residue XXX to residue YYY which conserve the conformations status
			
				#Tschuprows T (with bias correction as per Bergsma and Wicher 2013)
				if nrows == 1 or ncols == 1:
					TT_corr = np.nan
				else:
					TT_corr = (phi_coeff_corr / ((k_corr-1)*(r_corr-1))**0.5)**0.5

				# the ratio between counts on the diagonal and off the diagonal, handles square and rectangular blocks
				diag = []
				for key, value in sorted(dups.iteritems()):
					for idx in value:
						if idx in [0,4,8]: # get counts in cells with indeces that correspond to diagonal positions in the flattened array
							diag.append(key)
				diag_ratio = sum(diag) / n # an estimate of the _proportion_ of interchanges from residue XXX to residue YYY which conserve the conformations status
				
				if options.scat:
					xy_labels.append(",".join([l1,l2]))
					Vcramers.append(Vcramer_corr)
					diag_ratios.append(diag_ratio)
					if AAdict.get(l1) == AAdict.get(l2):
						colorseq.append('red')
					else:
						colorseq.append('blue')


				else:
					print "%s\t%s\t%f\t%f\t%f" % (l1, l2, Vcramer_corr, TT_corr, diag_ratio) 

			else:
				# chisquared test
				#Applying the test (chi2_contingency) to a one-dimensional table will always result in expected equal to observed and a chi-square statistic equal to 0.
				# therefore to include n x1 blocks use scipy.stats.mstats.chisquare
				Chi_stat, pval = chi2_contingency(block)[:2]
				Bonferroni_pval = pval * numtest
				#Chi_stat, pval = chisquare(obs)
				out_matrix[out_idx1, out_idx2] = out_matrix[out_idx2, out_idx1] = pval 	
			
			if options.lst == True:
				# print list of labels, Tstats and Pvals for off-diagonal blocks
				print "%s\t%s\t%f\t%f\t%f" % (l1, l2, pval, Chi_stat, Bonferroni_pval)

		else:
			# 1x1 blocks (ALA>GLY) are nan
			if options.lst == True:
				print "%s\t%s\t%f\t%f" % (l1, l2, np.nan, np.nan)
			elif options.Vcramer:
				print "%s\t%s\t%f\t%f\t%f" % (l1, l2, np.nan, np.nan, np.nan)
			else:
				:qout_matrix[out_idx1, out_idx2] = out_matrix[out_idx2, out_idx1] = np.nan
	
	# print matrix as TSV with header and row labels or represent as heatmap
	if not options.rep and not options.lst and not options.Vcramer:
		if header == ['X']:
			header.extend(collapsed_labels)
		print  '\t'.join(map(str, header))
		for idx, symbol in enumerate(collapsed_labels):
			row = [symbol]
			row.extend([x for x in out_matrix[idx,:]])
			print '\t'.join(map(str, row))
	elif options.rep:
		plot_heatmap(out_matrix, collapsed_labels, options.outfile)		
	elif options.scat:
		plot_scatterplot(Vcramers, diag_ratios, xy_labels, colorseq, options.outfile)		
	

if __name__ == '__main__':
	main()
