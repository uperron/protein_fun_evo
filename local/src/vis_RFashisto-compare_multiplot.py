#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr, stdout
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
#from random import shuffle
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader

import matplotlib
# force matplotlib  not to use Xwi
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.ticker as ticker 
import matplotlib.patches as mpatches
import pandas as pd
import numpy as np

import seaborn as sb


def main():
	usage = format_usage('''
		%prog MODEL1_LABEL,[...]  MODEL1,MODEL2,[...] TREE_SIZE1,TREE_SIZE2,[...] <STDIN
	STDIN = LABELS\tMODELS\tTREE_SIZES
	One plot per tree size comparing all models
	''')
	parser = OptionParser(usage=usage)
	parser.add_option('-c', '--colors', type=int, default=False, help='Custom palette [default: %default]')
	parser.add_option('-y', '--ylabel', type="str", default=False, help='Custom y-axis label [default: %default]')
	parser.add_option('-b', '--bin_num',  type=int, default=False, help='Custom bin number [default: %default]')
	options, args = parser.parse_args()
	
	if len(args) != 3:
		exit('Unexpected argument number.')

	labels = args[0].split(",") #i.e. column names
	models = args[1].split(",")
	tree_sizes = args[2].split(",")
	markers = ['p', 'v']

	colors = sb.color_palette("colorblind", 5, desat=.9)
	
	if options.colors:
		colors = [colors[0], colors[options.colors]]

	fig, axs = plt.subplots(len(tree_sizes),1, figsize=(12,12), sharex=True)
	fig.subplots_adjust(hspace = .5, wspace=.001)

	names = ['scale'] + labels
	data = pd.read_csv(stdin, sep="\t", header=None, names=names).dropna()
	data.astype(np.float)
	x = data['scale'].drop_duplicates().values
	r = 2.1* data.shape[0] / len(x)

	# evenly spaced positions for histo "axes" allowing enough space for bars
	spaced_x = np.arange(r,(len(x)+1)*r, r)


	for ax, tree_size in zip(axs.ravel(), tree_sizes):
			# "axes" for each histo
			for x_loc in spaced_x:
			    ax.axvline(x=x_loc, color="k", linestyle="-", linewidth=.5)
		
			patches = []
			
			#get the corresponding columns (one per model) & group by scale
			fltr_gp_df = data.filter(regex=tree_size + '|scale').groupby('scale', sort=False)
			
			# set the histogram range and number of bins for all models
			max_y = fltr_gp_df.max().max().max()	
			hist_range = (0, max_y)
			
			# as many bins as there are possible Rf values
			# i.e. even numbers b/w 0 and max_y 
			number_of_bins = int(max_y / 2)
			if options.bin_num:
				number_of_bins = options.bin_num

			i = 1
			for label, color, marker in zip([l for l in labels if 'TX' + tree_size in l], colors, markers):
				patches.append(mpatches.Patch(color=color, label=label))
				y = []
				for scale in x:
					y.append(fltr_gp_df.get_group(scale)[label].values)
				np.array(y)
				
			       # Computed quantities to aid plotting
				binned_y = [np.histogram(d, range=hist_range, bins=number_of_bins)[0] for d in y]
				medians = [np.median(d) for d in y]
				x_locations = spaced_x
			       
				# The bin_edges are the same for all of the histograms
				bin_edges = np.linspace(hist_range[0], hist_range[1], number_of_bins + 1)
				bottoms = bin_edges[:-1]
				heights = np.diff(bin_edges)
	    
			        # Cycle through and plot each histogram
				for x_loc, binned_data, median in zip(x_locations, binned_y, medians):
				    lefts = x_loc
				    binned_data = binned_data * i
				    ax.barh(bottoms, binned_data, height=heights, left=lefts, align = "edge", color=color, label=label)
                                    # add marker for mean
				    marker_size = 200* heights[0] / 3
                                    ax.scatter(x_loc + .2 * r *i, median, marker=marker, s=marker_size, color="r", zorder=10) 
				i = i * -1
			ax.set_xticklabels(x, rotation=45, size='large')
			majorLocator = ticker.FixedLocator(x_locations)
			ax.xaxis.set_major_locator(majorLocator)
			ax.set_title(str(tree_size) + ' taxa', loc = 'center')
			ax.autoscale()
			
	# set figure style and font
	plt.style.use(['seaborn-white', 'seaborn-paper'])
	matplotlib.rc('font', serif='Helvetica Neue')

	fig.text(0.5, 0.04, 'Scaling factor', ha='center', va='center', size='x-large')
	ylab = 'Unweighted RF distance'
	if options.ylabel:
		ylab = options.ylabel
	fig.text(0.06, 0.5, ylab, ha='center', va='center', rotation='vertical', 
		 size='x-large')
	plt.figlegend(patches, models, loc = 'upper left', ncol=2)

	plt.savefig(stdout, format='pdf', bbox_inches='tight', dpi=fig.dpi)

if __name__ == '__main__':
	main()
