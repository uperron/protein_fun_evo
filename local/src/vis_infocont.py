#!/usr/bin/env python
from __future__ import with_statement
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from matplotlib.lines import Line2D
from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
#from random import shuffle
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader



def main():
	usage = format_usage('''
		%prog FEATURES_FILE < STDIN
	''')
	parser = OptionParser(usage=usage)
	
	parser.add_option('-o', '--out_file', dest='out_file', type='str', default='infocont.out.pdf', help='output file name [default: %default]')

	options, args = parser.parse_args()
	
	if len(args) != 1:
		exit('Unexpected argument number.')


	# parse std_in into dict, plot info content over alignment columns
	info_cont = {}
	for line in stdin:
		site, info = safe_rstrip(line).split('\t')
		i_site = int(site)
		f_info = float(info)
		info_cont[i_site] = f_info		
	x = np.arange(min(info_cont.keys()),max(info_cont.keys()),1)
	y = []
	for element in x:
		y.append(info_cont.get(element, 0))
	a_y = np.array(y)
	fig, axes = plt.subplots(2, 1, sharex=True)
	plt.subplots_adjust(left=0.125, bottom=0.1, right=0.9, top=0.9, wspace=0.2, hspace=0.2)
	axes[0].set_ylim([0, 10])	
	axes[1].plot(x, a_y)
	axes[1].set_xlim(xmin=min(x))
	# parse domains, plot domains
	with file(args[0], 'r') as fd:
		colors = "bgrcmy"
		current_analysis = None
		for i,line in enumerate(fd):
			color = colors[i] 
			analysis, dom_id, start, end = safe_rstrip(line).split('\t')
			if current_analysis == None:
				current_analysis = analysis
				top, height = 0., 0.3
				height_mid = top+height/2
	
			if current_analysis != analysis:
				axes[0].text(0., height_mid, current_analysis, verticalalignment='bottom', horizontalalignment='left', transform=axes[0].transAxes, color='green', fontsize=10)
				seq_line = Line2D(axes[1].get_xlim(), [height_mid, height_mid], linewidth = 1, color = 'k', zorder = 0, transform = axes[0].transData)
				axes[0].lines.append(seq_line)
				current_analysis = analysis
				top, height = top+4, 0.3
				height_mid = top+height/2	
			left, width = int(start), int(end) - int(start)
			length_mid = left + width/2
			r = patches.Rectangle((left, top), width, height, fill=True, clip_on=False, zorder = 1, edgecolor='k', facecolor=color)
			axes[0].annotate(dom_id, rotation='vertical',  verticalalignment='bottom', horizontalalignment='left', color='k', xy=(left, top), xytext=(length_mid, top+height+0.2),)
			axes[0].add_patch(r)	


	axes[0].get_xaxis().tick_bottom()
	axes[0].text(0., height_mid, current_analysis, verticalalignment='bottom', horizontalalignment='left', transform=axes[0].transAxes, color='green', fontsize=10)
	seq_line = Line2D(axes[1].get_xlim(), [height_mid, height_mid], linewidth = 1, color = 'k', zorder = 0, transform = axes[0].transData)
	axes[0].lines.append(seq_line)
	axes[0].axes.get_yaxis().set_visible(False)
	axes[0].axis('off')
	plt.savefig(options.out_file, dpi=fig.dpi, format='pdf')

if __name__ == '__main__':
	main()

