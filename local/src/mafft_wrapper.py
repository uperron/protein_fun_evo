#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
#from random import shuffle
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader

import os
import shlex, subprocess
import shutil



def main():
	usage = format_usage('''
		%prog "mafft MAFFT_OPTS MAFFT_ARGS"
	''')
	parser = OptionParser(usage=usage)
	options, args = parser.parse_args()
	
	bashCommand = ''.join(map(str, args))
	#process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)	
	output = subprocess.check_output(shlex.split(bashCommand), shell=True)
	#process.communicate()
	
if __name__ == '__main__':
	main()

