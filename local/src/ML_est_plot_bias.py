#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
#from random import shuffle
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader
import matplotlib
# force matplotlib  not to use Xwi
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import numpy as np
import pandas as pd
import matplotlib.cm as cm

colors=['#f24516','#e099ec','#45d1d5','#83dbe8','#fe721d','#9c86f3','#5d21c5','#99fb91','#3f96b9','#4e914e','#ddfed1','#bc5684','#01296f','#946114','#2d02f0','#250a0f','#eee8aa']

def main():
	usage = format_usage('''
		%prog < STDIN
	''')
	parser = OptionParser(usage=usage)
	
	parser.add_option('-o', '--outfile', dest='outfile', default='outfile', help='output file [default: %default]')
	options, args = parser.parse_args()
	
	if len(args) != 0:
		exit('Unexpected argument number.')

	in_data = pd.read_csv(stdin, sep="\t", header=None, names=['seq_len', 'timepoint', 'bias'])
	
	# create a dictionary of Dataframes accessed by timepoints
	groups = dict(list(in_data.groupby('timepoint')))

	colors = [cm.Reds(i/4.,1) for i in np.arange(1,5)] + [cm.Blues(i/4.,1) for i in np.arange(1,5)] + [cm.Purples(i/4.,1) for i in np.arange(1,5)]  + [cm.Greens(i/4.,1) for i in np.arange(1,5)] + [cm.Oranges(i/4.,1) for i in np.arange(1,5)] + [cm.Greys(i/4.,1) for i in np.arange(1,5)]

	# iterate over the timepoints, and plot
	fig = plt.figure(figsize=(20,20))
	ax = fig.add_subplot(111)
	for timepoint, color in zip(sorted(groups.keys()), colors):
		seq_lengths = [float(x) for x in groups[timepoint].seq_len.unique()]
		labels = [timepoint]*len(seq_lengths)
		# generate a list of lists, one for every seq_len also convert to float
		plt_data = [np.array(x).astype(np.float) for x in groups[timepoint].groupby('seq_len')['bias'].apply(list)]
		#bp = ax.boxplot(plt_data, patch_artist=True, widths=0.01, positions=seq_lengths, labels=labels)
		means = [np.mean(l) for l in plt_data]

		# calculate the errorbar values
		upper_error = np.array([max(l) for l in plt_data]) - np.array(means)
		lower_error = np.array(means) - np.array([min(l) for l in plt_data])
		asymmetric_error = [lower_error, upper_error]

		ax.errorbar(seq_lengths, means, yerr=asymmetric_error, ecolor=color, capsize=5, c=color, marker='o', label=timepoint, alpha=0.5)
		

	# plot a reference line
	plt.axhline(linestyle='--', color='k')

	plt.xlabel('Sequence length')
	plt.ylabel('Branch length estimate bias')
	#ax.set_xticklabels(seq_lengths, size='xx-small', rotation=45)
	plt.legend()
	plt.savefig(options.outfile, format='pdf', bbox_inches='tight', dpi=fig.dpi)



if __name__ == '__main__':
	main()

