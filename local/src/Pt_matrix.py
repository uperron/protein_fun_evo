#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
import random
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader

import numpy as np
import itertools
import math

import matplotlib
# force matplotlib  not to use Xwi
matplotlib.use('Agg')
import matplotlib.colors as colors

import matplotlib.pyplot as plt
import matplotlib.figure as fg
import matplotlib.ticker as ticker
import scipy.linalg

import re
import os




def main():
	usage = format_usage('''
		%prog LABELS < ORIG_MATRIX
	''')
	parser = OptionParser(usage=usage)
	parser.add_option('-r', '--rep', dest='rep', action='store_true', default=False, help='represent the matrix as an  heatmap [default: %default]')	
	parser.add_option('-o', '--outfile', dest='outfile', default='outfile', help='output file [default: %default]')
	options, args = parser.parse_args()
	
	if len(args) != 1:
		exit('Unexpected argument number.')

	LABELS = args[0].split(",")
	d_rgx = re.compile('[0-9]$')
	size = len(LABELS)
	orig_matrix = np.zeros((size,size), dtype='float64')
	header = ['X']
	
	
	# parse original matrix
	i = 0
	for line in stdin:
		orig_matrix[i,:] = safe_rstrip(line).split('\t')
		i = i + 1
	
	# from IRM to Q with lines that sum to 0
	for i in range(0,size):
		orig_matrix[i,i] = (1-orig_matrix[i,i])*-1

	Pt_func = [[] for x in LABELS]
	
	for t in range(0, 10):	
		# calculate P(t)
		Pt_matrix = scipy.linalg.expm(t* orig_matrix)
		for i in range(0,size):
			Pt_func[i].append(Pt_matrix[i][i])
	
	# create a custom color map with n distinct colors	
	cmap = plt.cm.get_cmap('hsv',size)
	fig = plt.figure(figsize=(8,8))

	# plot Pii(t) for each state in orig_matrix
	for i in range(0,size):
		plt.plot(Pt_func[i], color=cmap(i),label=LABELS[i])
	plt.xlabel('t')	
	plt.ylabel('P(t)')
	#plt.legend(loc='best')
	plt.savefig(options.outfile, format='pdf', bbox_inches='tight', dpi=fig.dpi)		
	

if __name__ == '__main__':
	main()
