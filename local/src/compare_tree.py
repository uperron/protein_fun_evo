#!/usr/bin/env python	
from __future__ import with_statement

from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
#from random import shuffle
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader

import dendropy
from dendropy.calculate import treecompare
from itertools import combinations
import matplotlib
# force matplotlib  not to use Xwi
matplotlib.use('Agg')
import matplotlib.colors as colors

import matplotlib.pyplot as plt
import matplotlib.figure as fg
import matplotlib.ticker as ticker

import numpy as np
import itertools


def main():
	usage = format_usage('''
		%prog NEWICK_FILE_1 NEWICK_FILE_2 [...]
		
		performe pairwise tree comparisons using Unweighted Robinson-Foulds distance
	''')
	parser = OptionParser(usage=usage)
	parser.add_option('-o', '--one_to_many', default=False, help='compare multiple trees against a reference tree [default: %default]')
	parser.add_option('-w', '--weigthed_RF', default=False, action="store_true", help='use the weighted Robinsold-Foulds distance [default: %default]')
	parser.add_option('-e', '--euclidean_dist', default=False, action="store_true", help='use the euclidean distance')
	parser.add_option('-l', '--len_of_tree', default=False, action="store_true", help='use the overall tree length [default: %default]')
	parser.add_option('-s', '--single_brlens', default=False, action="store_true", help='compare lenth of second-shortest and second-longest branch among internals and leaves [default: %default]')
	options, args = parser.parse_args()
	

	if len(args) < 2 and not options.one_to_many:
		exit('Unexpected argument number.')

	# establish common taxon namespace
	tns = dendropy.TaxonNamespace(is_case_sensitive=True)

	# define the distance function
	def RFdist(tree1, tree2):
		if options.weigthed_RF:
			return treecompare.weighted_robinson_foulds_distance(tree1, tree2) 
		elif options.euclidean_dist:
			return treecompare.euclidean_distance(tree1, tree2)
		else:
			return treecompare.unweighted_robinson_foulds_distance(tree1, tree2)
		

	# one-to-many comparisons	
	if options.one_to_many:
		reference_tree = dendropy.Tree.get(path=options.one_to_many,  schema='newick', taxon_namespace=tns, case_sensitive_taxon_labels=True, suppress_internal_node_taxa=False)
		ref_tree_len = reference_tree.length()
		if options.single_brlens:
			# create a dictionary {none_name : dist_to_parent}
			int_dist_dict = dict(zip(tns.labels(),[reference_tree.find_node_for_taxon(tns.get_taxon(x)).edge_length for x in tns.labels() if reference_tree.find_node_for_taxon(tns.get_taxon(x)).is_internal()]))
			leaves_dist_dict = dict(zip(tns.labels(),[reference_tree.find_node_for_taxon(tns.get_taxon(x)).edge_length for x in tns.labels() if reference_tree.find_node_for_taxon(tns.get_taxon(x)).is_leaf()]))
			i = sorted(int_dist_dict.items(), key=lambda x:x[1])
			l = sorted(leaves_dist_dict.items(), key=lambda x:x[1])
			#second-shortest and second-longest among internal or leaf branches
			ref_tpls = [i[1],i[-2],l[1],l[-2]]
			branch_names = [x[0] for x in ref_tpls]
			#print   "ref_treefile\tinferred_treefile\tref_int_sec-short\tref_int_sec-long\tref_lf_sec-short\tref_lf_sec-long\tnew_int_sec-short\tnew_int_sec-long\tnew_lf_sec-short\tnew_lf_sec-long"
		for treefile in args:
			new_tree = dendropy.Tree.get(path=treefile,  schema='newick', taxon_namespace=tns, case_sensitive_taxon_labels=True, suppress_internal_node_taxa=False)
			if options.len_of_tree:
				new_tree_len = new_tree.length()
				print "%s\t%s\t%f\t%f" % (options.one_to_many, treefile, ref_tree_len, new_tree_len)
			elif options.single_brlens:
				new_tpls = [(x, new_tree.find_node_for_taxon(tns.get_taxon(x)).edge_length) for x in branch_names]
				print "%s\t%s\t%s\t%s" % (options.one_to_many, treefile,  '\t'.join(map(str, [x[1] for x in ref_tpls])), '\t'.join(map(str, [x[1] for x in new_tpls])))
			else:	
				RF = RFdist(reference_tree, new_tree)
				print "%s\t%s\t%f" % (options.one_to_many, treefile, RF)

	else:
		# load all tree combinations and ensure all trees loaded use common namespace
		for duo in itertools.permutations(args, 2):
			tree1 = dendropy.Tree.get(path=duo[0],	schema='newick', taxon_namespace=tns)
			tree2 = dendropy.Tree.get(path=duo[1],	schema='newick', taxon_namespace=tns)
			if options.len_of_tree:
				print "%s\t%s\t%f\t%f" %  (duo[0], duo[1], tree1.length(),  tree2.length())
			else:
				## Unweighted or weighted Robinson-Foulds distance
				RF = RFdist(tree1, tree2)
				print "%s\t%s\t%d" % (duo[0], duo[1], RF)

if __name__ == '__main__':
	main()
