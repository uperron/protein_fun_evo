#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
from uBio import Polypeptide

import matplotlib
# force matplotlib  not to use Xwi
matplotlib.use('Agg')

import matplotlib.pyplot as plt
import matplotlib.figure as fg
import numpy as np
import matplotlib.ticker as ticker
import matplotlib.patches as mpatches


#http://github.com/minillinim/stackedBarGraph 
from stackedBarGraph import StackedBarGrapher
SBG = StackedBarGrapher()


def plot_stackbar(count_arr, lev_lab, x_lab, colors, outfile, legend=False):
	w, h = fg.figaspect(float(len(lev_lab))/float(len(x_lab)))
 	fig = plt.figure(figsize=(w*5, h*4))
	ax = plt.subplot(2, 1, 1)

	# set ticks format on axes
	majorLocator = ticker.IndexLocator(base=1, offset=.5)
	majorFormatter = ticker.FormatStrFormatter('%s')
	ax.xaxis.set_major_locator(majorLocator)
	ax.xaxis.set_major_formatter(majorFormatter)

	#plot a multi-level stacked bar plot
	SBG.stackedBarPlot(ax, count_arr, colors, lev_lab, xLabels=x_lab, scale=False)	
	
	#plot legend for levels in stacked bar plot
	if legend:
		patches = []
		for col, lab in zip(colors, lev_lab):
			patches.append(mpatches.Patch(color=col, label=lab))
		plt.legend(handles=patches, bbox_to_anchor=(0., 1.02, 0.1, .102), loc=3, ncol=25, mode="expand", borderaxespad=0., fontsize=4)
	
	# borders format
	ax.yaxis.set_visible(False)

	plt.savefig(outfile, format='pdf', bbox_inches='tight', dpi=fig.dpi)
	plt.close(fig)

def main():
	usage = format_usage('''
		%prog SITES_NUM COLOR_DICT < STDIN
		Draws a stacked barplot either collapsing everithing in one plot or creating one plot per CrossRefSeq.
		
		META: STDIN
		1	site_id (numerical)
		2	sequence_id
		3	resname
		4	value
		
		META: COLOR_DICT
		1	symbol
		2	hex-color
	''')
	parser = OptionParser(usage=usage)
	parser.add_option('-o', '--outfile', type=str, dest='outfile', default='stacked-bar.pdf', help='output file [default: %default]')
	parser.add_option('-d', '--dirout', type=str, dest='dirout', default='./', help='output directory [default: %default]')
	parser.add_option('-s', '--single_bar', dest='single_bar', action="store_true", default=False, help='create a single plot for each CrossrefSeq (Uniprot entry or RefSeq)  [default: %default]')
	parser.add_option('-l', '--legend', dest='legend', action="store_true", default=False, help='plot legend  [default: %default]')
	options, args = parser.parse_args()
	
	if len(args) != 2:
		exit('Unexpected argument number.')
	
	# create a color dictionary from file
	color_d_file = args[1]
	color_d = {}
	with file(color_d_file, 'r') as fd:
		for line in fd:
			symbol, color = safe_rstrip(line).split('\t')
			color_d[symbol] = color

	sites_num = int(args[0])

	# create a list of symbols present at each site
	symbol_lst = [[] for x in xrange(sites_num)]
	labels = []
	site_lst = []
	Seq_d = {}
	for line in stdin:
                tokens = safe_rstrip(line).split('\t')
                site_id = int(tokens[0])
		resname = tokens[2]
		config = tokens[3]
		symbol = resname + config
	
		if site_id not in site_lst:
			site_lst.append(site_id)
		site_idx = site_lst.index(site_id)		

		if symbol not in labels:
			labels.append(symbol)

	
		# with this option create separate symbol_lst(s) for each CrossRefSeq from all the corresponding sequences.
		if options.single_bar:
			seq_id = tokens[1].split('-')[-1]
			if seq_id in Seq_d.keys():
				Seq_d[seq_id][site_idx].append(symbol)
			else:
				Seq_d[seq_id] = [[] for x in xrange(sites_num)]
				Seq_d[seq_id][site_idx].append(symbol)
	
		else:
			symbol_lst[site_idx].append(symbol)

	# sort labels using config as primary key, aplhabetical order as secondary key	
	labels = sorted(sorted(labels), key=lambda element: element[-1])	
	colors = [color_d[x] for x in labels]
	
	#Create an array of symbol counts at each site. Either one single array or one per CrossRefSeq.
	if options.single_bar:
		for seq_id in Seq_d.keys():
			count_arr = np.empty(shape=[sites_num, len(labels)], dtype=np.float)
			count_arr.fill(0.0)
			for i,site in enumerate(Seq_d[seq_id]):
				for symbol in set(site):
					s = labels.index(symbol)
					count_arr[i][s] = float(site.count(symbol))
			outfile = options.dirout + seq_id + ".png"
			plot_stackbar(count_arr, labels, site_lst, colors, outfile)		
			

	else:	
		count_arr = np.empty(shape=[sites_num, len(labels)], dtype=np.float)
		count_arr.fill(0.0)
		for i,site in enumerate(symbol_lst):
			for symbol in set(site):
				s = labels.index(symbol)
				count_arr[i][s] = float(site.count(symbol))
		outfile = options.outfile
		if options.legend:
			plot_stackbar(count_arr, labels, site_lst, colors, outfile, legend=True)
		else:
			plot_stackbar(count_arr, labels, site_lst, colors, outfile)		
			
	
if __name__ == '__main__':
	main()

