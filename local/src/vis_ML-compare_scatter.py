#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
#from random import shuffle
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader

import matplotlib
# force matplotlib  not to use Xwi
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker 
import matplotlib.patches as mpatches
import pandas as pd
import numpy as np
import itertools

from scipy.stats import gaussian_kde


#http://github.com/minillinim/stackedBarGraph
from stackedBarGraph import StackedBarGrapher
SBG = StackedBarGrapher()

# offset overlapping datapoints to keep them all visible.
def rand_jitter(arr):
	stdev = .01*(max(arr)-min(arr))
	return arr + np.random.randn(len(arr)) * stdev

def main():
	usage = format_usage('''
		%prog MODEL1_LABEL,[...] <STDIN
	STDIN = X_COORD\tRF_MODEL1[\tRF_MODEL2...]
	''')
	parser = OptionParser(usage=usage)
	
	parser.add_option('-o', '--outfile', dest='outfile', default='outfile', help='output file [default: %default]')
	parser.add_option('-s', '--stackplot', action='store_true', default=False, help='use stackbar plots  [default: %default]')
	parser.add_option('-b', '--boxplot', action='store_true', default=False, help='use boxplots  [default: %default]')
	parser.add_option('-m', '--mean', action='store_true', default=False, help='plot mean maximum likelihood  [default: %default]')
	options, args = parser.parse_args()
	
	if len(args) != 1:
		exit('Unexpected argument number.')
	

	labels = args[0].split(",") #i.e. model names
	colors = ['#c6e2ff', '#aecc5a', '#fda02e', '#ab1010', '#7a2762', '#4ad4ff']

	fig = plt.figure(figsize=(12,12))
	ax = fig.add_subplot(111)

	# parse stdin and plot the boxplots
	data = pd.read_csv(stdin, sep="\t", header=None, names=['scale'] + labels)
	data.astype(np.float)
	# group by scaling factor
	grouped = data.groupby('scale', sort=False)
	x = data['scale'].drop_duplicates().values
	displacement = -4
	patches = []
	# evenly spaced positions 
	alt_x = np.arange(12, 130, 12)

	for label, color in zip(labels, colors):
		# one boxplot per model per scaling factor, 
		# models are arranged around actual x coord in order not to superimpose 
		if options.boxplot:
			y = []
			for scale in x:
				y.append(grouped.get_group(scale)[label].values)
			np.array(y)
			adjusted_x = (alt_x + displacement)
			displacement = displacement + 2
			bp = ax.boxplot(y, patch_artist=True, widths=1.8, positions=adjusted_x)
			for box in bp['boxes']:
				# change outline color
				box.set( color='#000000', linewidth=0.5)
				# change fill color
				box.set( facecolor = color)
			## change color and linewidth of the whiskers
			for whisker in bp['whiskers']:
				whisker.set(color='#000000', linewidth=0.9)
			## change color and linewidth of the caps
			for cap in bp['caps']:
				cap.set(color='#000000', linewidth=0.9)
			## change color and linewidth of the medians
			for median in bp['medians']:
				median.set(color='#000000', linewidth=0.5)
			## change the style of fliers and their fill
			for flier in bp['fliers']:
				flier.set(marker='.', color=color)
			# create a custom legend handle for each series of boxplots
			patches.append(mpatches.Patch(color=color, label=label))
		
		elif options.mean:
			y = grouped.mean()[label].values
			plt.plot(x, y, label=label, color=color, marker='o')
			plt.ylabel('Mean maximum likelihood')
			plt.legend()

		elif options.stackplot:
			y =  grouped.sum()[label].values
			plt.stackplot(x, y, color=color, label=label, baseline='zero')
		else:
			if label != '55x55':
				X = data['55x55'].values
				Y = data[label].values
				plt.scatter(rand_jitter(X), rand_jitter(Y), label=label, marker=(3,3,3), s=10, c=color, alpha=0.5)

		
	
	ax.set_xticklabels(x, rotation=45, size='small')	
	plt.xlabel('Scaling factor')
	
	if not options.mean:
		plt.ylabel('Maximum log likelihood')
	
	if options.boxplot:
		plt.legend(handles=patches)
		majorLocator = ticker.FixedLocator(alt_x)
		majorFormatter = ticker.FormatStrFormatter('%s')
		ax.xaxis.set_major_locator(majorLocator)
		ax.xaxis.set_major_formatter(majorFormatter)
		ax.set_xticklabels(x)
		ax.set_xlim(left=0, right=130, auto=True)
	elif not options.boxplot and not options.mean:
		# the scatter plot graph
		# plot the bisector
		axes = plt.gca()
		plt.plot(np.linspace(0,axes.get_xlim()[1]), np.linspace(0,axes.get_xlim()[1]), '--', color='k')
		
		plt.legend()
		majorLocator = ticker.FixedLocator(np.arange(0, 45, 5))
		ax.xaxis.set_major_locator(majorLocator)
		ax.set_xticklabels(np.arange(0, 45, 5))
		ax.set_xlim(right=45)

	plt.savefig(options.outfile, format='pdf', bbox_inches='tight', dpi=fig.dpi)

if __name__ == '__main__':
	main()

