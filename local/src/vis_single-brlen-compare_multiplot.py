#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr, stdout
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
#from random import shuffle
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader

import matplotlib
# force matplotlib  not to use Xwi
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.ticker as ticker 
import matplotlib.patches as mpatches
import pandas as pd
import numpy as np
import itertools
import seaborn as sb

def main():
	usage = format_usage('''
		%prog MODEL1,MODEL2,[...] BRANTYPE1,BRANTYPE2,[...] TAXA_NUM1,TAXA_NUM2[...] <STDIN
	One boxplot series per branchtype and taxa number (internal, leaf etc.) comparing all models
	Requires header.
	''')
	parser = OptionParser(usage=usage)
	parser.add_option('-c', '--colors', type=int, default=False, help='Custom palette [default: %default]')
	options, args = parser.parse_args()
	
	if len(args) != 3:
		exit('Unexpected argument number.')

	models = args[0].split(",")
	bran_types = args[1].split(",")
	taxa = args[2].split(",")
	model_num = len(models)
	colors = sb.color_palette("colorblind", 5, desat=.9)
	
	if options.colors:
		colors = [colors[0], colors[options.colors]]

	fig, axs = plt.subplots((len(bran_types) / 2) * len(taxa), len(bran_types) / 2, figsize=(20,20), sharex=True, sharey=True)
	fig.subplots_adjust(hspace = .3, wspace=.3)
	
	# requires header
	data = pd.read_csv(stdin, sep="\t", header=0).dropna()
	data.astype(np.float)

	x = data['scale'].drop_duplicates().values

	# one subplot per bran_type and taxa product
	# i.e. 16_taxa_*_lf_sec-short
	for ax, product in zip(axs.ravel(), list(itertools.product(bran_types, taxa))):		
		bran_type, taxa = product
		patches = []
		displacement = -.1 * ((model_num -1 )/ 2) 
		
		#get the corresponding columns (one per model + one reference)
		fltr_df = data.filter(regex = '^' + taxa + '_taxa_.*_'  + bran_type + '|scale')
		
		# plot the reference
		y = fltr_df.filter(regex='.*REF.*').drop_duplicates().values
		ref_label = fltr_df.filter(regex='.*REF.*').columns[0]
		ax.plot(x, y, '--', marker = "o", color = "k", label = ref_label, zorder=10, markersize=1)
		ax.legend()	


		# one boxplot per model per scaling factor,
		# models are arranged around actual x coord in order not to superimpose
		
		fltr_df.drop(ref_label, 1, inplace=True)
		fltr_gp_df = fltr_df.groupby('scale', sort=False)
		
		for label, color in zip(fltr_df.drop('scale', 1).columns, colors):
			adjusted_x = [v + displacement for v in x]
			displacement = displacement + (.03 * model_num)
			y = []
			for scale in x:
				y.append(fltr_gp_df.get_group(scale)[label].values)
			np.array(y)
			bp = ax.boxplot(y, patch_artist=True, widths=.04, positions=adjusted_x)
			patches.append(mpatches.Patch(color=color, label=label))
			for box in bp['boxes']:
				# change outline color
				box.set( color='#000000', linewidth=0.01)
				# change fill color
				box.set( facecolor = color)
				## change color and linewidth of the whiskers
				for whisker in bp['whiskers']:
					whisker.set(color='#000000', linewidth=0.9)
				## change color and linewidth of the caps
				for cap in bp['caps']:
					cap.set(color='#000000', linewidth=0.9)
				## change color and linewidth of the medians
				for median in bp['medians']:
					median.set(color='#000000', linewidth=0.05)
				## change the style of fliers and their fill
				for flier in bp['fliers']:
					flier.set(marker='.', color=color, markersize=1)
			# subplot title
			ax.set_title(taxa + ' taxa ' + bran_type)
			ax.set_xticklabels(x, rotation=45)	
			majorLocator = ticker.FixedLocator(x)
			ax.xaxis.set_major_locator(majorLocator)

	# set figure style and font
	plt.style.use(['seaborn-white', 'seaborn-paper'])
	matplotlib.rc('font', serif='Helvetica Neue')
	
	fig.text(0.5, 0.04, 'Scaling factor', ha='center', va='center', size='x-large')
	fig.text(0.06, 0.5, 'Branch length', ha='center', va='center', rotation='vertical', size='x-large')
	
	plt.figlegend(patches, models, loc = 'upper center', ncol=2)
	plt.savefig(stdout, format='pdf', bbox_inches='tight', dpi=fig.dpi)

if __name__ == '__main__':
	main()
