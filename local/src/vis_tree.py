#!/usr/bin/env python	
from __future__ import with_statement

from sys import stdin, stderr, stdout
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
#from random import shuffle
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader


from Bio import Phylo

import matplotlib
# force matplotlib  not to use Xwi
matplotlib.use('Agg')

import matplotlib.pyplot as plt

def main():
	usage = format_usage('''
		%prog NEWICK_FILE
	''')
	parser = OptionParser(usage=usage)
	
	parser.add_option('-o', '--out_file', dest='out_file', type='str', default='tree.out.pdf', help='Output file name [default: %default]')
	parser.add_option('-p', '--pruning_lst', dest='pruning_lst', type='str', default='', help='All leaves not present in the pruning list will be pruned [default: %default]')
	parser.add_option('-w', '--write', action='store_true', default=False, help='Write newick tree [default: %default]')
	options, args = parser.parse_args()
	
	if len(args) != 1:
		exit('Unexpected argument number.')
	
	tree = Phylo.read(args[0], 'newick')
	all_leaves = [x.name for x in tree.get_terminals()]

	if options.pruning_lst != '':
		leaves_to_prune = set(all_leaves) - set(options.pruning_lst.split(','))
		for leaf in leaves_to_prune:
			tree.prune(target=leaf)

	if options.write:	
		# write newick tree to stdout
		Phylo.write(tree, stdout, 'newick')

	else:
		# Flip branches so deeper clades are displayed at top
		tree.ladderize() 
		
		fig = plt.figure(figsize=(60,60))	
		ax = plt.subplot(2, 1, 1)
		Phylo.draw(tree, axes=ax, do_show=False)
		plt.savefig(options.out_file, format='pdf', bbox_inches='tight', dpi=fig.dpi)


if __name__ == '__main__':
	main()

