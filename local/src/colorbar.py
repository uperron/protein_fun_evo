#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
#from random import shuffle
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader

import matplotlib
# force matplotlib  not to use Xwi
matplotlib.use('Agg')

import matplotlib.figure as fg
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.cm as cm
import matplotlib.colors as mcolors

def colorbar_index(ncolors, cmap, labels):
	cmap = cmap_discretize(cmap, ncolors)
	mappable = cm.ScalarMappable(cmap=cmap)
	mappable.set_array([])
	mappable.set_clim(-0.5, ncolors+0.5)
	ax = plt.subplot(2, 1, 1)	
	colorbar = plt.colorbar(mappable, cax=ax)
	colorbar.set_ticks(np.linspace(0, ncolors, ncolors))
	colorbar.set_ticklabels(labels)

def cmap_discretize(cmap, N):
    """Return a discrete colormap from the continuous colormap cmap.

        cmap: colormap instance, eg. cm.jet. 
        N: number of colors.

    Example
        x = resize(arange(100), (5,100))
        djet = cmap_discretize(cm.jet, 5)
        imshow(x, cmap=djet)
    """

    if type(cmap) == str:
        cmap = plt.get_cmap(cmap)
    colors_i = np.concatenate((np.linspace(0, 1., N), (0.,0.,0.,0.)))
    colors_rgba = cmap(colors_i)
    indices = np.linspace(0, 1., N+1)
    cdict = {}
    for ki,key in enumerate(('red','green','blue')):
        cdict[key] = [ (indices[i], colors_rgba[i-1,ki], colors_rgba[i,ki])
                       for i in xrange(N+1) ]
    # Return colormap object.
    return mcolors.LinearSegmentedColormap(cmap.name + "_%d"%N, cdict, 1024)


def main():
	usage = format_usage('''
		%prog NSTATES CMAP LABELS(comma-separated)
		prints a labelled color bar
	''')
	parser = OptionParser(usage=usage)
	parser.add_option('-o', '--outfile', type=str, dest='outfile', default='colorbar.png', help='output file[default: %default]')	

	options, args = parser.parse_args()
	
	if len(args) != 3:
		exit('Unexpected argument number.')
	
	ncolors = int(args[0])
	cmap = args[1]
	labels = args[2].split(',')

	w, h = fg.figaspect(100./1.)
        fig = plt.figure(figsize=(w, h))
	colorbar_index(ncolors, cmap, labels)    
	plt.savefig(options.outfile, format='png', bbox_inches='tight', dpi=fig.dpi)

if __name__ == '__main__':
	main()

