#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
#from random import shuffle
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader

import matplotlib
# force matplotlib  not to use Xwi
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker 
import matplotlib.patches as mpatches
import pandas as pd
import numpy as np
import itertools

from scipy.stats import gaussian_kde


#http://github.com/minillinim/stackedBarGraph
from stackedBarGraph import StackedBarGrapher
SBG = StackedBarGrapher()

# offset overlapping datapoints to keep them all visible.
def rand_jitter(arr):
	stdev = .01*(max(arr)-min(arr))
	return arr + np.random.randn(len(arr)) * stdev

def main():
	usage = format_usage('''
		%prog MODEL1_LABEL,[...] <STDIN
	STDIN = X_COORD\tRF_MODEL1[\tRF_MODEL2...]
	''')
	parser = OptionParser(usage=usage)
	
	parser.add_option('-o', '--outfile', dest='outfile', default='outfile', help='output file [default: %default]')
	parser.add_option('-s', '--stackplot', action='store_true', default=False, help='use stackbar plots  [default: %default]')
	parser.add_option('-b', '--boxplot', action='store_true', default=False, help='use boxplots  [default: %default]')
	parser.add_option('-v', '--variance', action='store_true', default=False, help='plot RF distance variance  [default: %default]')
	parser.add_option('-S', '--Single_model', action='store_true', default=False, help='plot a single model vs the original tree, requires tree length data and -b  [default: %default]')
	parser.add_option('-l', '--label_yaxis', default=False, help='custom label for y axis [default: %default]')
	parser.add_option('-a', '--alt_clrs', type=str, default=False, help='label color group [default: %default]')
	parser.add_option('-r', '--r_v_w', type=str, default=False, help='consider right and wrong topologies separately  [default: %default]')
	options, args = parser.parse_args()
	
	if len(args) != 1:
		exit('Unexpected argument number.')
	if options.Single_model and not options.boxplot:
		exit('-S requires -b')

	labels = args[0].split(",") #i.e. model names
	colors = ['#c6e2ff', '#aecc5a', '#fda02e', '#ab1010', '#7a2762', '#4ad4ff', '#604343', '#cea852', '#e34799', '#efdd00', '#2fd2f9', "#1b4673", "#e3125c", "#9c0c3f", "#73481b", "#72731b"]
	fig = plt.figure(figsize=(10,10))
	ax = fig.add_subplot(111)

	names = ['scale'] + labels
	if options.r_v_w:
		RF_vals  = options.r_v_w.split(",")
		names = ['scale'] + labels + RF_vals
	data = pd.read_csv(stdin, sep="\t", header=None, names=names)
	data.astype(np.float)

	# group by scaling factor
	grouped = data.groupby('scale', sort=False)
	x = data['scale'].drop_duplicates().values
	displacement = -4
	patches = []
	# evenly spaced positions 
	alt_x = np.arange(12, 130, 12)
	
	single_disp_dict = dict(zip(options.alt_clrs.split(","), [-.04, .04]))

	for label, color in zip(labels, colors):
		if options.alt_clrs:
			for group_label, alt_color in zip(options.alt_clrs.split(","), colors):
				if group_label in label:
					color = alt_color
		# one boxplot per model per scaling factor, 
		# models are arranged around actual x coord in order not to superimpose 
		if options.boxplot:
			adjusted_x = (alt_x + displacement)
			displacement = displacement + 2
			# handles multiple 'REF_*' cols for plotting different topologies
			# plot line instead of  boxplot
			if 'REF' in label:
				y = data[label].drop_duplicates().values
				if options.Single_model:
					if options.alt_clrs or options.r_v_w:
						plt.plot(x, y, '--', marker = "o", color = "k", label = label, zorder=10, markersize=4)		
					else:
						plt.plot(x, y, '--', marker = "o", color = color, label = label, zorder=10, alpha=.5, markersize=4)
				else:
					plt.plot(adjusted_x, y, '--', marker = "o", color = color, label = label)
				continue
			else:
				y = []
				for scale in x:
					y.append(grouped.get_group(scale)[label].values)
				np.array(y)
				if options.Single_model and not options.r_v_w and not options.alt_clrs:
					bp = ax.boxplot(y, patch_artist=True, widths=.05, positions=x)
				elif options.Single_model and options.alt_clrs:
					for model in single_disp_dict.keys():
						if model in label:
							singl_adj_x = [v + single_disp_dict[model] for v in x]
							bp = ax.boxplot(y, patch_artist=True, widths=.05, positions=singl_adj_x)
				elif options.r_v_w:
					y_right = []
					y_wrong = []
					for RF_val in RF_vals:
						if RF_val in label and 'REF' not in label:
							for scale in x:
								y_right.append(data[(data['scale'] == scale) & (data[RF_val] == .0)][label].values)
								y_wrong.append(data[(data['scale'] == scale) & (data[RF_val] != .0)][label].values)
					y_right = np.array(y_right)
					y_wrong = np.array(y_wrong)
					for y_array, color, i in zip([y_right, y_wrong], ["g", "r"], [-0.03, 0.03]):
						if y_array.size != 0:
							bp = ax.boxplot(y_array, patch_artist=True, widths=.05, positions=[v + i for v in x])
							for box in bp['boxes']:
								box.set( facecolor = color)
				else:
					bp = ax.boxplot(y, patch_artist=True, widths=1.8, positions=adjusted_x)
				
				if not options.r_v_w:
					for box in bp['boxes']:
						# change outline color
						box.set( color='#000000', linewidth=0.01)
						# change fill color
						if options.Single_model:
							box.set( facecolor = color, alpha = .5)
						else:
							box.set( facecolor = color)
					## change color and linewidth of the whiskers
					for whisker in bp['whiskers']:
						whisker.set(color='#000000', linewidth=0.9)
					## change color and linewidth of the caps
					for cap in bp['caps']:
						cap.set(color='#000000', linewidth=0.9)
					## change color and linewidth of the medians
					for median in bp['medians']:
						median.set(color='#000000', linewidth=0.05)
					## change the style of fliers and their fill
					for flier in bp['fliers']:
						flier.set(marker='.', color=color)
					patches.append(mpatches.Patch(color=color, label=label))
				else:
					patches.append(mpatches.Patch(color="g", label=label + "_right"))
					patches.append(mpatches.Patch(color="r", label=label + "_wrong"))

		elif options.variance:
			y = grouped.var()[label].values
			plt.plot(x, y, label=label, color=color, marker='o')
			plt.ylabel('Unweighted RF distance variance')
			plt.legend()

		elif options.stackplot:
			y =  grouped.sum()[label].values
			plt.stackplot(x, y, color=color, label=label, baseline='zero')
		else:
			# scatter plots where x-axis = 55x55 RF distance,  y-axis = RF distance when analysed under another model, one  point for each simluated alignment
			if label != '55x55':
				X = data['55x55'].values
				Y = data[label].values
				plt.scatter(rand_jitter(X), rand_jitter(Y), label=label, marker=(3,3,3), s=10, c=color, alpha=0.5)

		
	
	ax.set_xticklabels(x, rotation=45, size='small')	
	plt.xlabel('Scaling factor',  size='x-large')
	
	if not options.variance:
		if not options.label_yaxis:
			plt.ylabel('Unweighted RF distance')
		else:
			plt.ylabel(options.label_yaxis,  size='x-large')
	if options.boxplot:
		plt.legend(handles=patches + ax.get_legend_handles_labels()[0], ncol=3)
		if options.Single_model:
			majorLocator = ticker.FixedLocator(x)
		else:
			majorLocator = ticker.FixedLocator(alt_x)
			ax.set_xlim(left=0, right=130, auto=True)
		majorFormatter = ticker.FormatStrFormatter('%s')
		ax.xaxis.set_major_locator(majorLocator)
		ax.xaxis.set_major_formatter(majorFormatter)
		ax.set_xticklabels(x)
	
	elif not options.boxplot and not options.variance:
		# the scatter plot graph
		# plot the bisector
		axes = plt.gca()
		plt.plot(np.linspace(0,axes.get_xlim()[1]), np.linspace(0,axes.get_xlim()[1]), '--', color='k')
		
		plt.legend()
		majorLocator = ticker.FixedLocator(np.arange(0, 45, 5))
		ax.xaxis.set_major_locator(majorLocator)
		ax.set_xticklabels(np.arange(0, 45, 5))
		ax.set_xlim(right=45)

	plt.savefig(options.outfile, format='pdf', bbox_inches='tight', dpi=fig.dpi)

if __name__ == '__main__':
	main()

