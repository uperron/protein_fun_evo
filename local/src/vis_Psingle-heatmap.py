#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
from uBio import Polypeptide

import matplotlib
# force matplotlib  not to use Xwi
matplotlib.use('Agg')

import matplotlib.pyplot as plt
import matplotlib.figure as fg
import numpy as np
import matplotlib.ticker as ticker



def plot_heatmap(z_arr, r_arr, label, outfile):
	w, h = fg.figaspect(1/float(len(z_arr)))
 	fig = plt.figure(figsize=(w*25, h*0.5))
	ax = plt.subplot(2, 1, 1)
	ax.set_ylim([0,0.5])
	
	# set ticks format on y axis
	majorLocator = ticker.LinearLocator(numticks=3)
	majorFormatter = ticker.FormatStrFormatter('%s')
	ax.yaxis.set_major_locator(majorLocator)
	ax.yaxis.set_major_formatter(majorFormatter)

	# plot the value array  with no interpolation
	z_grid = np.expand_dims(z_arr, axis=0)
	r_grid = np.expand_dims(r_arr, axis=0)
    	ax.imshow(z_grid, interpolation=None, cmap='PuBu', alpha=0.8, aspect='auto')
    	ax.table(cellText=r_grid,loc='center',cellLoc='center',bbox=[0,0,1,1], zorder=0)

	# set axes format
        ax.spines['top'].set_visible(False)
        ax.spines['right'].set_visible(False)
        ax.spines['bottom'].set_visible(False)
        ax.spines['left'].set_visible(False)
	ax.axes.get_xaxis().set_visible(False)
	
	#label ticks on axes using site_id (x) and sequence_id (y)	
	ax.set_yticklabels(['',label,''])
	plt.savefig(outfile, format='png', bbox_inches='tight', dpi=fig.dpi)
	plt.close(fig)

def main():
	usage = format_usage('''
		%prog SITES_NUM SEQ_NUM < STDIN
		draws a heatmap (one for each sequence) considering values and residue categories (B_branched, non_B_branched, negative, positive)
		META: STDIN
		1	site_id (numerical)
		2	sequence_id
		3	resname
		4	value
	''')
	parser = OptionParser(usage=usage)
	parser.add_option('-o', '--outdir', type=str, dest='outdir', default='./', help='output directory [default: %default]')
	options, args = parser.parse_args()
	
	if len(args) != 2:
		exit('Unexpected argument number.')
	
	sites_num,sequs_num = int(args[0]),int(args[1])	

	B_branched = ['VAL', 'ILE',  'THR']
	non_B_branched = ["CYS", "PHE", "LEU", "MET", "ASN", "PRO", "GLN", "SER", "TRP", "TYR"]
	negative = ["ASP", "GLU"]
	positive = ["ARG", "HIS", "LYS"]
	seq_lst = []
	site_lst = []
	z_grid = np.empty(shape=[sequs_num, sites_num])
	z_grid.fill(np.nan)
	r_grid = np.empty(shape=[sequs_num, sites_num], dtype=np.str)
	r_grid.fill('')
	for line in stdin:
                tokens = safe_rstrip(line).split('\t')
                site_id = int(tokens[0])
                seq_id = tokens[1]
                resname = tokens[2]
		z = float(tokens[3])
		if seq_id not in seq_lst:
			seq_lst.append(seq_id)

		if site_id not in site_lst:
			site_lst.append(site_id)
		
	

		# create 2 matrixes for config values and residues
		i = site_lst.index(site_id)
		j = seq_lst.index(seq_id)
		z_grid[j][i] = z
		r_grid[j][i] = Polypeptide.three_to_one(resname)

	for i,seq in enumerate(seq_lst):
		z_arr = z_grid[i,]
		r_arr = r_grid[i,]
		outfile = options.outdir+seq.replace("<", '').replace(">", '')+'.png'
		plot_heatmap(z_arr, r_arr, seq, outfile)		
			
	
if __name__ == '__main__':
	main()

