#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr
import sys
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
import random
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader

import numpy as np

def main():
	usage = format_usage('''
		%prog LABELS CODON_LABELS < ORIG_RATE_MATRIX
	''')
	parser = OptionParser(usage=usage)
	parser.add_option('-F', '--Freq_file', type=str, default=False, help='calculate state frequencies from the raw counts matrix [default: %default]')
	parser.add_option('-s', '--symmetric', action='store_true',  default=False, help='output a symmetric count matrix [default: %default]')
	parser.add_option('-o', '--out_freq', type=str,  default=False, help='output the states and corresponding frequencies to a specified file [default: %default]')
	options, args = parser.parse_args()
	
	if len(args) != 2:
		exit('Unexpected argument number.')

	LABELS = args[0].split(",")
	CODON_LABELS = args[1].split(",")
	size = len(LABELS)
	codon_size = len(CODON_LABELS)
	orig_matrix = np.zeros((size,size), dtype='float64')
	codon_matrix = np.zeros((codon_size,codon_size), dtype='float64')
	
	
	# parse original exchange rate matrix
	i = 0
	for line in stdin:
		orig_matrix[i,:] = safe_rstrip(line).split('\t')
		i = i + 1

	# from rate matrix to fake codon rate matrix  using the label mapping specified in LABELS, CODON_LABELS
	# this correspond to the PAML style of custom codon rate matrix where codons are ordered according to the standard genetic code table (ftp://ftp.ncbi.nih.gov/entrez/misc/data/gc.prt)
	# therefore TTT - 0, TTC - 1, TTA - 3, TTG - 4, and so on.
	for i_row in range(codon_size):
		for i_col in range(codon_size): 
			if CODON_LABELS[i_col] != "NA" and CODON_LABELS[i_row] != "NA":
				j_row = LABELS.index(CODON_LABELS[i_row])
				j_col = LABELS.index(CODON_LABELS[i_col])
				codon_matrix[i_row,i_col] = codon_matrix[i_col,i_row] = orig_matrix[j_row,j_col]
					

	if options.Freq_file:
		i = 0
		# parse original count matrix (skipping first column and header) and calculate state frequencies
		counts = []
		with file(options.Freq_file, 'r') as fd:
			lines = fd.readlines()
			for line in lines[1:]:
				# the sum total of each line
				counts.append(sum([float(x) for x in safe_rstrip(line).split('\t')[1:]]))
			tot_counts = sum(counts)
		freqs = [y/tot_counts for y in counts]

		if options.out_freq:
			f1=open(options.out_freq, 'w+')
			print >>f1, '\t'.join(map(str, LABELS))
			print >>f1, '\t'.join(map(str, freqs))
			f1.close()
			sys.exit(0)
		else:	
			out_freqs = []
			for CODON_LABEL in CODON_LABELS:
				if CODON_LABEL == "NA":
					out_freqs.append(0.0)
				else:
					out_freqs.append(freqs[LABELS.index(CODON_LABEL)])


								
	if options.symmetric:
		for i_row in range(codon_size):
			print '\t'.join(map(str, codon_matrix[i_row,:]))	
	else:
		for i_row in range(codon_size):
			# print the lower triangular matrix (i.e. every cell left of the diagonal)
			print '\t'.join(map(str, codon_matrix[i_row,:].flatten()[:i_row]))


	# print the equilibrium frequencies of all states int the right order, separated by and empty row from the rate matrix
	print
	print '\t'.join(map(str, out_freqs))
	

if __name__ == '__main__':
	main()
