#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
#from random import shuffle
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader


import numpy as np
import matplotlib
# force matplotlib  not to use Xwi
matplotlib.use('Agg')

import matplotlib.pyplot as plt
import matplotlib.figure as fg
import numpy as np
import matplotlib.ticker as ticker
import re


def plot_heatmap(matrix, label, outfile):
        fig = plt.figure(figsize=matrix.shape)
        ax = plt.subplot(2, 1, 1)

        # set ticks format on y axis
        majorLocator = ticker.LinearLocator(numticks=len(label))
        majorFormatter = ticker.FormatStrFormatter('%s')
        ax.xaxis.set_major_locator(majorLocator)
        ax.xaxis.set_major_formatter(majorFormatter)
        ax.yaxis.set_major_locator(majorLocator)
        ax.yaxis.set_major_formatter(majorFormatter)

        # plot the value array  with no interpolation
        ax.imshow(matrix, interpolation=None, cmap='binary', aspect='auto')

        # set axes format
        #ax.spines['top'].set_visible(False)
        #ax.spines['right'].set_visible(False)
        #ax.spines['bottom'].set_visible(False)
        #ax.spines['left'].set_visible(False)

        #label ticks on axes
        ax.set_yticklabels(label)
        ax.set_xticklabels(label)
	plt.savefig(outfile, format='pdf', bbox_inches='tight', dpi=fig.dpi)
        plt.close(fig)

def main():
	usage = format_usage('''
		%prog SEQ_FILE < STDIN
		
		.META: STDIN
		1	Poly_id1
		2	Poly_id2

		.META: SEQ_FILE
		1	Poly_id
		2	Uniprot_id
		3	multialign_site
		4	symbol
		5 	B_factor
	''')
	parser = OptionParser(usage=usage)
	
	parser.add_option('-B', '--B_cutoff', type=float, dest='B_cutoff', default=30, help='iso B factor cutoff [default: %default]')
	parser.add_option('-R', '--R_cutoff', type=float, dest='R_cutoff', default=0.85, help='rotasequence identity cutoff [default: %default]')
	parser.add_option('-o', '--outfile', dest='outfile', default='outfile', help='output file [default: %default]')
	parser.add_option('-r', '--rep', dest='rep', action='store_true', default=False, help='represent the matrix as an  heatmap [default: %default]')
	parser.add_option('-w', '--write', action='store_true', default=False, help='output a PHYLIP alignment (gapped) [default: %default]')
	parser.add_option('-s', '--shared_sites', action='store_true', default=False, help='rotasequence identity cutoff is applied over sites shared by each sequence pair [default: %default]')
	options, args = parser.parse_args()
	
	if len(args) != 1:
		exit('Unexpected argument number.')
	
	
	# parse the rotasequence file, create a dict {'Poly_Id':['Uniprot', [(multialign_site, 'symbol, B_factor')]]}:
	rota_dict = {}
	align_len = 0
	sym_lst = []
	id_dict = {}
	with file(args[0], 'r') as fd:
		for line in fd:
			Poly_id, Uniprot_id, multialign_site, symbol, B_factor = safe_rstrip(line).split('\t')
			id_dict[Uniprot_id] = Poly_id
			if Poly_id in rota_dict.keys():
				trio = (int(multialign_site), symbol, float(B_factor))
				rota_dict[Poly_id][1].append(trio)
			else:
				trio = (int(multialign_site), symbol, float(B_factor))
				rota_dict[Poly_id] = [Uniprot_id, [trio]]
			if int(multialign_site) > align_len:
				align_len = int(multialign_site)
			if symbol not in sym_lst:
				sym_lst.append(symbol)
		
	max_diff = int((1 - options.R_cutoff)*align_len)
	sym_num = len(sym_lst)
	sym_lst.sort()	
	nseq = len(id_dict.keys())

	# generate a matrix
	sub_matrix = np.zeros((sym_num,sym_num), dtype=float)

	if options.write:
		print"%d\t%d" % (nseq, align_len)
		print
		for Uni_id in id_dict.keys():
			Poly_id = id_dict[Uni_id]
			# add gaps
			rotaseq = ["-"]*align_len

			for site in rota_dict[Poly_id][1]:
				# add symbols to sequence  considering the Bfactor cutoff
				if options.B_cutoff >= site[2]:
					# 1-based multialign_site vs. 0-based index
					rotaseq[site[0]-1] = site[1]
			line = Uni_id + '\t'  + ' '.join(map(str, rotaseq))
			print line

	else:	
		# parse stdin and tabulate differences among sequences
		for line in stdin:
			diff = []
			same = []
			shared = 0
			Poly_id1, Poly_id2 = safe_rstrip(line).split('\t')
			if Poly_id1 in rota_dict.keys() and Poly_id2 in rota_dict.keys():
				# calculate rotasequence identity and tabulate differences
				for site1 in rota_dict[Poly_id1][1]:
					for site2 in rota_dict[Poly_id2][1]:
						# if the alignment sites match and the symbols do not match
						if site1[0] == site2[0] and site1[1] != site2[1]:
							shared += 1
							# B factor cutoff
							if options.B_cutoff >= site1[2] and options.B_cutoff >= site2[2]:
								diff.append((site1[1], site2[1]))
						# if the symbols do match
						elif site1[0] == site2[0] and site1[1] == site2[1]:
							shared += 1
							if options.B_cutoff >= site1[2] and options.B_cutoff >= site2[2]:
								same.append(site1[1])
				
				if options.shared_sites:
					# the cutoff is applied over the number of SHARED sites b/w the two rotasequences
					max_diff = int((1 - options.R_cutoff)*shared)
				
				#collapsed_diff = []
				#for duo in diff:
				#	collapsed_duo = set([re.sub(re.compile('[0-9]$'), '', x) for x in duo])
				#	if len(collapsed_duo) == 2:
				#		collapsed_diff.append(collapsed_duo)
				#if len(collapsed_diff) <= max_diff:
				
				if len(diff) <= max_diff:
					for duo in diff:
						i = sym_lst.index(duo[0])
						j = sym_lst.index(duo[1])
						# not knowing which symbol mutated into which, so we count 1/2 in ij and 1/2 in ji					
						sub_matrix[i][j] = sub_matrix[i][j] + 0.5
						sub_matrix[j][i] = sub_matrix[j][i] + 0.5
					for symbol in same:
						i = sym_lst.index(symbol)
						sub_matrix[i][i] = sub_matrix[i][i] + 1.
		if options.rep == False:
			header = ['X']
			header.extend(sym_lst)
			print  '\t'.join(map(str, header))
			for idx, symbol in enumerate(sym_lst):
				row = [symbol]
				# remove nan since nan != nan is True
				#row.extend([0.0 if x != x else x for x in norm_matrix[idx]])
				row.extend([x for x in sub_matrix[idx]])
				print '\t'.join(map(str, row))
		else:
			plot_heatmap(sub_matrix, sym_lst, options.outfile)		

if __name__ == '__main__':
	main()

