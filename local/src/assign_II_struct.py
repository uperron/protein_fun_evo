#!/usr/bin/env python
from __future__ import with_statement
import re
import os
import warnings
from sys import stdin, stderr, stdout
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
from Bio.PDB.MMCIF2Dict import MMCIF2Dict
import pandas as pd
import numpy as np


def main():
	usage = format_usage('''
		%prog <STDIN(structure_id\tPDB_site_id\tAlign_site_id) STRUCT_FILE_PATH
		''')
	parser = OptionParser(usage=usage)
	options, args = parser.parse_args()
	
	if len(args) < 1:
		exit('Unexpected argument number.')
	
	path = args[0]
	data = pd.read_csv(stdin, sep="\t", header=None, names=["struct_id", "resseq", "align_site"])
	data = data.astype({'struct_id' : 'str', 'resseq' : 'int'})
	grouped = data.groupby('struct_id', sort=False)

	for structure, group in grouped:
		filename = path + structure + ".cif"
		mmcif_dict = MMCIF2Dict(filename)
		df_out  = pd.DataFrame(group[["resseq", "align_site"]])
		
		sliced =  {"secondary_struct" : mmcif_dict["_struct_conf.conf_type_id"],  
				"beg" : mmcif_dict["_struct_conf.beg_auth_seq_id"],  
				"end" : mmcif_dict["_struct_conf.end_auth_seq_id"]}
		df_struct =  pd.DataFrame.from_dict(sliced)
		df_struct = df_struct.astype({'beg' : 'int', 'end' : 'int'})

		# iterate over residue pdb seqence numbers (_atom_site.auth_seq_id) and assign secondary structure type
		df_out['struct_class'] = np.vectorize(lambda r: df_struct.query('beg <= @r and end >= @r')['secondary_struct'].values, 
				otypes=[list])(df_out['resseq'])
		
		df_out['struct_class'] = df_out['struct_class'].apply(lambda x: x[0] if len(x) > 0  else "NA")
		df_out['struct_id'] = [structure] * len(df_out)

		df_out.to_csv(stdout, sep='\t', header=False, index=False)


if __name__ == '__main__':
	main()

