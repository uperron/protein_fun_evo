# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Develop and test the 55-states evolution model.

Very few models of sequence evolution have incorporated parameters that describe protein structure. We know, however, that structure is consistently more conserved than protein sequence [1] and that it is essential for protein functions. The increasing availability of structural information opens up the possibility of creating structurally aware substitution models. A promising candidate feature for this is the rotameric state of side-chains: each residue can adopt one or more of a discrete set of rotamers, which is a geometric pattern of atomic positions defined by the dihedral angles between covalently linked atoms.
Rotamer states depend on backbone conformation and play a role in protein folding and docking mechanisms by determining side-chain positioning. Furthermore, they are a feature of the constituent residues of a sequence and therefore fit well with existing approaches to modelling sequence evolution. 

We are able to assign a rotamer state and its corresponding probability to each residue of proteins with known structure, starting from X-ray crystallography data on side chain atoms’ position and using states defined in the Dunbrack rotamer library [2]. Here we present a survey of rotamer state conservation across a number of protein families, highlighting evolutionary patterns for these features. Our work aims ultimately to generate a structurally aware Dayhoff-like model employing an expanded state set (alphabet) composed of symbols containing both residue type and rotamer state information.
This has the potential to improve our understanding of the relationships between protein sequence structure and evolution and aid in obtaining better tree topology and sequence divergence estimates, and may also allow reconstruction of ancestral rotameric states and potentially full modelling of ancestral structures given the observed structures at leaf nodes.


### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
